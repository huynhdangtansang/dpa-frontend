/*
 *
 * ClientDetailsPage actions
 *
 */
import * as Constants from "./constants";

export function getClientAllData(id) {
  return {
    type: Constants.GET_CLIENT_ALL_DATA,
    id: id
  };
}

export function getInfoClient(id) {
  return {
    type: Constants.GET_CLIENT_INFO,
    id: id
  };
}

export function getSuccess(response) {
  return {
    type: Constants.GET_CLIENT_INFO_SUCCESS,
    response: response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_CLIENT_INFO_ERROR,
    response: response
  }
}

export function changeClientStatus(id, updateData) {
  return {
    type: Constants.CHANGE_CLIENT_STATUS,
    id,
    updateData
  }
}

export function deactiveClient(id) {
  return {
    type: Constants.DEACTIVE_CLIENT,
    id
  }
}

export function deleteClient(deleteData) {
  return {
    type: Constants.DELETE_CLIENT,
    deleteData
  }
}

export function deleteSuccess(response) {
  return {
    type: Constants.DELETE_SUCCESS,
    response: response
  }
}

export function deleteError(response) {
  return {
    type: Constants.DELETE_ERROR,
    response: response
  }
}

export function resetPassword(resetData) {
  return {
    type: Constants.RESET_PASSWORD,
    resetData
  }
}

export function changeIsSuccess(value) {
  return {
    type: Constants.CHANGE_IS_SUCCESS,
    value
  }
}

export function showModal(modal, value) {
  return {
    type: Constants.SHOW_MODAL,
    modal,
    value
  }
}

//Payment detail
export function getPaymentDetail(id) {
  return {
    type: Constants.GET_PAYMENT_DETAIL,
    id: id
  };
}

export function getPaymentSuccess(response) {
  return {
    type: Constants.GET_PAYMENT_SUCCESS,
    response: response
  }
}

//Job assigned
export function getJobAssigned(id) {
  return {
    type: Constants.GET_JOB_ASSIGNED,
    id: id
  };
}

export function getJobAssignedSuccess(response) {
  return {
    type: Constants.GET_JOB_ASSIGNED_SUCCESS,
    response: response
  }
}

export function getJobAssignedError(response) {
  return {
    type: Constants.GET_JOB_ASSIGNED_ERROR,
    response: response
  }
}