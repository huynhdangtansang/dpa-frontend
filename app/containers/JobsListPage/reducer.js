/*
 *
 * JobsListPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  CHANGE_COMPANY,
  CHANGE_PAGE,
  CHANGE_STATUS_FILTER,
  CHANGE_STORE_VALUE,
  GET_COMPANIES_SUCCESS,
  GET_SUCCESS,
  REMOVE_SUCCESS,
  SELECT_ALL_JOB,
  SELECT_JOB,
  SHOW_MODAL,
  UPDATE_SEARCH_COMPANY_DATA,
  UPDATE_SEARCH_DATA
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  jobsList: [],
  pagination: {},
  companyPagination: {},
  companiesList: [],
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    status: [
      {
        label: 'All',
        value: ''
      }
    ],
    company: [
      {
        label: 'All',
        value: ''
      }
    ],
    sort: 'jobId',
    sortType: 'desc',
    insurance: false
  },
  searchDataForCompany: {
    offset: 0,
    limit: 10,
    search: '',
    period: '',
    serviceIds: [],
    sort: '',
    sortType: ''
  },
  currentPage: 1,
  selectedJobs: [],
  statusList: [
    {
      label: 'All',
      value: ''
    },
    {
      label: 'New Lead',
      value: 'newLead',
    },
    {
      label: 'Accepted',
      value: 'accepted',
    },
    {
      label: 'On Way',
      value: 'onWay',
    },
    {
      label: 'Paid',
      value: 'paid',
    },
    {
      label: 'Deposit Paid',
      value: 'depositPaid',
    },
    {
      label: 'Completed',
      value: 'completed',
    },
    {
      label: 'Cancelled',
      value: 'cancelled',
    }
  ],
  selectedCompany: 'All',
  showRemoveConfirm: false
});

function multiCheckboxChange(state, action) {
  let newArray = state.get(action.key).map((status, index) => {
    return index === action.id ?
        fromJS({
          label: status.toJS().label,
          value: status.toJS().value,
          checked: !status.toJS().checked
        }) :
        status;
  });
  return state
      .set(action.key, fromJS(newArray))
}

function formatCompanyList(list) {
  return list.map((company) => {
    return {
      label: company.name ? company.name : '',
      value: company._id ? company._id : '',
    }
  })
}

function jobsListPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_SUCCESS:
      return state
          .set('jobsList', fromJS(action.response.data.data))
          .set('pagination', fromJS(action.response.data.pagination));
    case GET_COMPANIES_SUCCESS: {
      let formattedNewCompaniesList = [];
      formattedNewCompaniesList.push({
        label: 'All',
        value: ''
      });
      formattedNewCompaniesList = formattedNewCompaniesList.concat(formatCompanyList(action.response.data.data));
      return state
          .set('companyPagination', fromJS(action.response.data.pagination))
          .set('companiesList', fromJS(formattedNewCompaniesList));
    }
    case UPDATE_SEARCH_COMPANY_DATA:
      return state
          .set('searchDataForCompany', fromJS(action.newSearchData));
    case UPDATE_SEARCH_DATA:
      return state
          .set('jobsList', fromJS([]))
          .set('searchData', fromJS(action.newSearchData));
    case CHANGE_PAGE:
      return state
          .set('currentPage', action.newPage);
    case SELECT_JOB:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedJobs'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedJobs', state.get('selectedJobs').filter((job) => job !== action.selectItem))
      }
    case SELECT_ALL_JOB:
      if (action.isSelected === true) {
        let newArray = state.get('jobsList').toJS().map((job) => {
          return job._id
        });
        return state
            .set('selectedJobs', fromJS(newArray))
      } else {
        return state
            .set('selectedJobs', fromJS([]))
      }
    case CHANGE_STATUS_FILTER:
      return multiCheckboxChange(state, action);
    case CHANGE_COMPANY:
      return state
          .set('selectedCompany', action.companyName);
    case SHOW_MODAL:
      if (action.modal === 'remove') {
        return state
            .set('showRemoveConfirm', action.value);
      } else
        return state;
    case CHANGE_STORE_VALUE:
      return state.setIn(action.event.key, fromJS(action.event.value));
    case REMOVE_SUCCESS:
      return state
          .set('selectedJobs', fromJS([]));
    default:
      return state;
  }
}

export default jobsListPageReducer;
