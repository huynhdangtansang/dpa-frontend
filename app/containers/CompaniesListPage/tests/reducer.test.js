import { fromJS } from 'immutable';
import companiesListPageReducer from '../reducer';

describe('companiesListPageReducer', () => {
  it('returns the initial state', () => {
    expect(companiesListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
