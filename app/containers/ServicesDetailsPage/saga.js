import { put, takeLatest } from 'redux-saga/effects';
import { DELETE_SERVICE, GET_PDF, GET_SERVICE_DATA, UPDATE_COMMISSION } from './constants';
import { getService, getSuccess } from './actions';
import config from 'config';
import axios from 'axios';
import { goBack } from 'react-router-redux';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import _ from "lodash";

export function* apiGetService(data) {
  if (data.id) {
    const requestUrl = config.serverUrl + config.api.service + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteService(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateCommission(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/changeCommissionWithMultiServices';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.updateData);
      yield put(reposLoaded());
      yield put(getService(data.updateData.ids[0]));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetPdf(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/generatePdfServiceMany';
    yield put(loadRepos());
    try {
      const response = yield axios.post(requestUrl, { ids: data.serviceId });
      yield put(reposLoaded());
      let array = response.data.data;
      array.forEach(function (file) {
        window.open(file.path);
      });
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_SERVICE_DATA, apiGetService);
  yield takeLatest(DELETE_SERVICE, apiDeleteService);
  yield takeLatest(UPDATE_COMMISSION, apiUpdateCommission);
  yield takeLatest(GET_PDF, apiGetPdf);
}
