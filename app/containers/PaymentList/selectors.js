import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the paymentList state domain
 */

const selectPaymentListDomain = state => state.get("paymentList", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by PaymentList
 */

const makeSelectPaymentList = () =>
    createSelector(selectPaymentListDomain, substate => substate.toJS());
export default makeSelectPaymentList;
export { selectPaymentListDomain };
