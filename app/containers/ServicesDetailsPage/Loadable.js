/**
 *
 * Asynchronously loads the component for ServicesDetailsPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
