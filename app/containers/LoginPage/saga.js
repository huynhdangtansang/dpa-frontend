import { put, takeLatest } from 'redux-saga/effects';
import { GET_LOGIN_DATA } from './constants';
import { loginFail, loginSuccess } from './actions';
import config from 'config';
import axios from 'axios';
import { push } from 'react-router-redux';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import { urlLink } from 'helper/route';


// Individual exports for testing
export function* loginForUser(data) {
  // See example in containers/HomePage/saga.js
  const userEmail = data.email;
  const userPassword = data.password;
  if (userEmail && userPassword) {
    const requestUrl = config.serverUrl + config.api.auth.sign_in;
    yield put(loadRepos());
    try {
      const response = yield axios.put(requestUrl, {
        emailAddress: userEmail,
        password: userPassword
      });
      yield put(loginSuccess(response.data, userEmail));
      if (data.remember == true) {
        sessionStorage.setItem('rememberMe', true);
      }
      yield put(reposLoaded());
      yield put(push(urlLink.clients));
    } catch (error) {
      yield put(reposLoaded());
      if (error.response) {
        console.log(error.response)
        const offlineData = {
          data: error.response.data,
          error: true,
          errors: [
            { errorCode: 4, errorMessage: error.response.data }
          ]
        }
        yield put(loginFail(offlineData));
      } else {
        const offlineData = {
          data: [],
          error: true,
          errors: [
            { errorCode: 4, errorMessage: 'Error: 500 server internal error' }
          ]
        };
        yield put(loginFail(offlineData));
      }
    }
  }
}

export default function* userData() {
  yield takeLatest(GET_LOGIN_DATA, loginForUser);
}