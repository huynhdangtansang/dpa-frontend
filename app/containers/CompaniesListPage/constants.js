/*
 *
 * CompaniesListPage constants
 *
 */
export const DEFAULT_ACTION = "app/CompaniesListPage/DEFAULT_ACTION";
export const RESET_STATE = "app/CompaniesListPage/RESET_STATE";
export const GET_COMPANIES_LIST = "/CompaniesListPage/GET_COMPANIES_LIST";
export const GET_SUCCESS = "/CompaniesListPage/GET_SUCCESS";
export const GET_SERVICE_DATA = "/CompanyDetailsPage/GET_SERVICE_DATA";
export const GET_SERVICE_SUCCESS = "/CompanyDetailsPage/GET_SERVICE_SUCCESS";
export const SELECT_COMPANY = "/CompanyDetailsPage/SELECT_COMPANY";
export const SELECT_ALL_COMPANY = "/CompanyDetailsPage/SELECT_ALL_COMPANY";
export const GET_PERIOD = "/CompanyDetailsPage/GET_PERIOD";
export const GET_PERIOD_SUCCESS = "/CompanyDetailsPage/GET_PERIOD_SUCCESS";
export const SET_ACTIVE_COMPANY = "/CompaniesListPage/SET_ACTIVE_COMPANY";
export const SET_REMOVE_COMPANY = "/CompaniesListPage/SET_REMOVE_COMPANY";
export const CHANGE_COMPANY_STATUS = "/CompaniesListPage/CHANGE_COMPANY_STATUS";
export const DELETE_COMPANY = "/CompaniesListPage/DELETE_COMPANY";
export const UPDATE_SEARCH_STRING = "/CompaniesListPage/UPDATE_TEXT_SEARCH";
export const UPDATE_FILTER_LIST = "/CompaniesListPage/UPDATE_FILTER_LIST";
export const UPDATE_DROPDOWN_SERVICE = "/CompaniesListPage/UPDATE_DROPDOWN_SERVICE";
export const UPDATE_DROPDOWN_PERIOD = "/CompaniesListPage/UPDATE_DROPDOWN_PERIOD";
export const CLEAR_ALL_SELECTED_COMPANY = "/CompaniesListPage/CLEAR_ALL_SELECTED_COMPANY";
export const SET_TIMER_COUNT = "/CompaniesListPage/SET_TIMER_COUNT";
export const UPDATE_SEARCH_DATA = "/CompaniesListPage/UPDATE_SEARCH_DATA";