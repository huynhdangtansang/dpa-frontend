/**
 *
 * Asynchronously loads the component for ClientEditPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
