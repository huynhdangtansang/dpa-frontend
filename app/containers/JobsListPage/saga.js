import { put, takeLatest } from 'redux-saga/effects';
import { GET_COMPANIES, GET_LIST, REMOVE_JOB, UPDATE_SEARCH_COMPANY_DATA, UPDATE_SEARCH_DATA } from './constants';
import { getCompanies, getCompaniesSuccess, getList, getSuccess, removeSuccess } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from 'lodash';

export function* apiGetJobsList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
        (key) => {
          if (key === 'status' || key === 'company') {
            let temp = key + '=';
            if (params[key].length > 0) {
              params[key].map((item, index) => {
                if (item !== '') {//other option, not all
                  temp += item.value;
                  if (index !== params[key].length - 1) {
                    temp += ',';
                  }
                }
              });
            }
            return temp;
          } else {
            return (key + '=' + esc(params[key]));
          }
        }
    ).join('&');
    if (_.isBoolean(data.loading) && data.loading === true)
      yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.job + '/master/getJobs' + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* getListAgain(data) {
  if (data) {
    if (data.key !== 'searchDataForCompany')
      yield put(getList(data.newSearchData));
  }
}

export function* apiGetCompanies(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.company.companies_list + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getCompaniesSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiRemoveJob(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.job + '/deletes';
    yield put(loadRepos());
    try {
      const response = yield axios.delete(requestUrl, { data: { ids: data.jobIds } });
      yield put(reposLoaded());
      yield put(removeSuccess(response.data));
      yield put(getList(data.searchData));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: error
      }));
      yield put(reposLoaded());
    }
  }
}

export function* getCompaniesAgain(data) {
  if (data) {
    yield put(getCompanies(data.newSearchData));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_LIST, apiGetJobsList);
  yield takeLatest(UPDATE_SEARCH_DATA, getListAgain);
  yield takeLatest(GET_COMPANIES, apiGetCompanies);
  yield takeLatest(REMOVE_JOB, apiRemoveJob);
  yield takeLatest(UPDATE_SEARCH_COMPANY_DATA, getCompaniesAgain);
}
