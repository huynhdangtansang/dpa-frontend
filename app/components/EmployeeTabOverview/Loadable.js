/**
 *
 * Asynchronously loads the component for EmployeeTabOverview
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
