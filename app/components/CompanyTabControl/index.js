/**
 *
 * CompanyTabControl
 *
 */
import React from "react";
import { Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

class CompanyTabControl extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <Nav tabs className="tab">
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '1' })}
                onClick={() => {
                  this.props.toggle('1');
                }}>
              <span>Overview</span>
            </NavLink>
          </NavItem>
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '2' })}
                onClick={() => {
                  this.props.toggle('2');
                }}>
              <span>Employees</span>
            </NavLink>
          </NavItem>
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '3' })}
                onClick={() => {
                  this.props.toggle('3');
                }}>
              <span>Jobs</span>
            </NavLink>
          </NavItem>
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '4' })}
                onClick={() => {
                  this.props.toggle('4');
                }}>
              <span>Earning & Payout Request</span>
            </NavLink>
          </NavItem>
        </Nav>
    );
  }
}

CompanyTabControl.propTypes = {};
export default CompanyTabControl;
