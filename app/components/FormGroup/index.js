/**
 *
 * FormGroup
 *
 */
import React from "react";
// import styled from 'styled-components';
import './style.scss';
import ClassNames from 'classnames';

function FormGroup(props) {
  return (
      <div className={ClassNames('form-group-content', !props.footer && 'more-padding')}>
        <a href='#'>
          <img className={'form-group-title'} src="logo-color.svg" alt="dpa-logo"/>
        </a>
        <div className={'form-group-children'}>{props.children}</div>
        {props.footer && (
            <div className={'form-group-footer'}>{props.footer} <a onClick={props.onClick}>{props.link}</a></div>
        )}
      </div>
  );
}

FormGroup.propTypes = {};
export default FormGroup;
