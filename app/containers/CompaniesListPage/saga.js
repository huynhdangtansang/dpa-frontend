import { put, takeLatest } from 'redux-saga/effects';
import {
  CHANGE_COMPANY_STATUS,
  DELETE_COMPANY,
  GET_COMPANIES_LIST,
  GET_PERIOD,
  GET_SERVICE_DATA,
  UPDATE_SEARCH_DATA
} from './constants';
import {
  clearAllSelectedCompany,
  getCompaniesListData,
  getPeriodSuccess,
  getServiceSuccess,
  getSuccess
} from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetCompaniesList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
        // k =>
        // k + '=' + esc(params[k])
        //this function to process sort by service ID
        function (key) {
          if (key === 'serviceIds') {
            if (params[key].length > 0) {
              return (key + '=' + encodeURIComponent('["' + params[key] + '"]'));
            } else {
              return (key + '=' + esc(params[key]));
            }
          } else {
            return (key + '=' + esc(params[key]));
          }
        }
    ).join('&');
    const requestUrl = config.serverUrl + config.api.company.companies_list + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetServiceList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.servicePublic + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      yield put(getServiceSuccess(response.data.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetPeriod() {
  const requestUrl = config.serverUrl + config.api.company.companies_period;
  try {
    const response = yield axios.get(requestUrl);
    yield put(getPeriodSuccess(response.data));
  } catch (error) {
    yield put(reposLoaded());
    yield put(updateError({
      error: true,
      title: 'Error!!!',
      message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
    }));
  }
}

export function* apiChangeCompanyStatus(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.deactivesCompanies;
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.statusData);
      yield put(getCompaniesListData(data.searchData));
      yield put(clearAllSelectedCompany());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiDeleteCompany(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.companies_details + 'deletes';
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(getCompaniesListData(data.searchData));
      yield put(clearAllSelectedCompany());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* getListAgain(data) {
  if (data) {
    yield put(getCompaniesListData(data.newSearchData));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_COMPANIES_LIST, apiGetCompaniesList);
  yield takeLatest(GET_SERVICE_DATA, apiGetServiceList);
  yield takeLatest(GET_PERIOD, apiGetPeriod);
  yield takeLatest(DELETE_COMPANY, apiDeleteCompany);
  yield takeLatest(CHANGE_COMPANY_STATUS, apiChangeCompanyStatus);
  yield takeLatest(UPDATE_SEARCH_DATA, getListAgain);
}