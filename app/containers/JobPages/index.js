/**
 *
 * JobPages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import JobsListPage from 'containers/JobsListPage/Loadable';
import JobDetailsPage from 'containers/JobDetailsPage/Loadable';
import JobNewPage from 'containers/JobNewPage/Loadable';
import JobEditDescription from 'containers/JobEditDescription/Loadable';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class JobPages extends React.PureComponent {
  render() {
    return (
        <div className="job-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.jobs} component={JobsListPage}/>
              <Route path={urlLink.viewJob} component={JobDetailsPage}/>
              <Route path={urlLink.addJob} component={JobNewPage}/>
              <Route path={urlLink.editJob} component={JobEditDescription}/>
            </Switch>
          </Router>
        </div>
    )
  }
}

JobPages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(JobPages);
