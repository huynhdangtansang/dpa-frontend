/*
 *
 * ResetPasswordPage actions
 *
 */
import {
  DEFAULT_ACTION,
  FORGOT_PASSWORD_EMAIL_ERROR,
  FORGOT_PASSWORD_EMAIL_SUSCCESS,
  FORGOT_PASSWORD_SEND_EMAIL,
  SET_ISSENT
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function sendEmail(email) {
  return {
    type: FORGOT_PASSWORD_SEND_EMAIL,
    email: email
  }
}

export function sendSuccess(response) {
  return {
    type: FORGOT_PASSWORD_EMAIL_SUSCCESS,
    response: response
  }
}

export function sendError(response) {
  return {
    type: FORGOT_PASSWORD_EMAIL_ERROR,
    response: response
  }
}

export function setIsSent() {
  return {
    type: SET_ISSENT,
  }
}