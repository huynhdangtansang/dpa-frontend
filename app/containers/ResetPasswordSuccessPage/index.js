/**
 *
 * ResetPasswordSuccessPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import PurpleRoundButton from 'components/PurpleRoundButton';
import { urlLink } from 'helper/route';
import './style.scss'

/* eslint-disable react/prefer-stateless-function */
export class ResetPasswordSuccessPage extends React.PureComponent {
  render() {
    return (
        <div className="reset-password-success">
          <div className="logo">
            <img src="./logo.png" alt="logo"/>
          </div>
          <div className="image-content">
            <img src="./like.png" alt="like"/>
          </div>
          <div className="title"><span>Password updated</span></div>
          <div className="description"><span>Your password has been updated successfully</span></div>
          <PurpleRoundButton className="btn-purple-round back-login" title="Back To Login" onClick={() => {
            this.props.history.push(urlLink.login);
          }}/>
        </div>
    );
  }
}

ResetPasswordSuccessPage.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(ResetPasswordSuccessPage);
