import { fromJS } from 'immutable';
import jobEditDescriptionReducer from '../reducer';

describe('jobEditDescriptionReducer', () => {
  it('returns the initial state', () => {
    expect(jobEditDescriptionReducer(undefined, {})).toEqual(fromJS({}));
  });
});
