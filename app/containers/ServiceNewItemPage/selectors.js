import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the serviceNewItemPage state domain
 */

const selectServiceNewItemPageDomain = state =>
    state.get("serviceNewItemPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ServiceNewItemPage
 */

const makeSelectServiceNewItemPage = () =>
    createSelector(selectServiceNewItemPageDomain, substate => substate.toJS());
const makeSelectErrors = () =>
    createSelector(selectServiceNewItemPageDomain, state => state.get('errors'));
const makeSelectIsSuccess = () =>
    createSelector(selectServiceNewItemPageDomain, state => state.get('isSuccess'));
const makeSelectCouponList = () =>
    createSelector(selectServiceNewItemPageDomain, state => state.get('couponsList'));
export { makeSelectServiceNewItemPage, makeSelectIsSuccess, makeSelectErrors, makeSelectCouponList };
export { selectServiceNewItemPageDomain };
