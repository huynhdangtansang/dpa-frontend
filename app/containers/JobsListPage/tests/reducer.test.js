import { fromJS } from 'immutable';
import jobsListPageReducer from '../reducer';

describe('jobsListPageReducer', () => {
  it('returns the initial state', () => {
    expect(jobsListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
