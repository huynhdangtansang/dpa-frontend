import { fromJS } from 'immutable';
import servicesListPageReducer from '../reducer';

describe('servicesListPageReducer', () => {
  it('returns the initial state', () => {
    expect(servicesListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
