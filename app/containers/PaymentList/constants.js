/*
 *
 * PaymentList constants
 *
 */
export const DEFAULT_ACTION = "app/PaymentList/DEFAULT_ACTION";
export const RESET_STATE = "PaymentList/RESET_STATE";
//API
export const GET_LIST = "PaymentList/GET_LIST";
export const GET_SUCCESS = "PaymentList/GET_SUCCESS";
export const GET_ERROR = "PaymentList/GET_ERROR";
//Remove
export const REMOVE_PAYMENT = "PaymentList/REMOVE_PAYMENT";
export const REMOVE_SUCCESS = "PaymentList/REMOVE_SUCCESS";
//Serverside
export const UPDATE_SEARCH_DATA = "PaymentList/UPDATE_SEARCH_DATA";
export const CHANGE_PAGE = "PaymentList/CHANGE_PAGE";
export const SELECT_PAYMENT = "PaymentList/SELECT_PAYMENT";
export const SELECT_ALL_PAYMENT = "PaymentList/SELECT_ALL_PAYMENT";
export const SHOW_MODAL = "PaymentList/SHOW_MODAL";
export const CHANGE_STORE_VALUE = "PaymentList/CHANGE_STORE_VALUE";
//Print
export const GET_PDF = "PaymentList/GET_PDF";