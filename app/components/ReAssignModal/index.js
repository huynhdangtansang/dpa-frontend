import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import Checkbox from 'components/Checkbox';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import "./style.scss";

export default class ReAssignModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChangeObject = (type) => {
    this.props.onChange(type);
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="re-assign-modal">
          <ModalBody>
            <div className="re-assign">
              <div className="upper">
                <div className="title">
                  <span>Re-assign Job</span>
                </div>
                <div className="description">
                  <span>Please select who you want to re-assign. </span>
                  <ul>
                    <li>Selecting company will cancel the company currently assigned and resend as a new request to top
                      companies
                    </li>
                    <li>Selecting provider will request the assigned company to change the employee</li>
                    <li>Selecting a specific company will send a new request to a company and an employee</li>
                  </ul>
                </div>
              </div>
              <div className="middle">
                <div className={this.props.assignedObject === 'company' ? "selection selected" : "selection"}
                     onClick={() => {
                       this.handleChangeObject('company');
                     }}>
                  <Checkbox
                      name={'company'}
                      label={'Re-assign Company'}
                      checked={this.props.assignedObject === 'company' ? true : false}
                      onChange={() => {
                        this.handleChangeObject('company');
                      }}
                  />
                </div>
                <div className={this.props.assignedObject === 'provider' ? "selection selected" : "selection"}
                     onClick={() => {
                       this.handleChangeObject('provider');
                     }}>
                  <Checkbox
                      name={'company'}
                      label={'Re-assign Provider'}
                      checked={this.props.assignedObject === 'provider' ? true : false}
                      onChange={() => {
                        this.handleChangeObject('provider');
                      }}
                  />
                </div>
                <div className={this.props.assignedObject === 'specificCompany' ? "selection selected" : "selection"}
                     onClick={() => {
                       this.handleChangeObject('specificCompany');
                     }}>
                  <Checkbox
                      name={'company'}
                      label={'Re-assign a specific Company'}
                      checked={this.props.assignedObject === 'specificCompany' ? true : false}
                      onChange={() => {
                        this.handleChangeObject('specificCompany');
                      }}
                  />
                </div>
              </div>
              <div className="lower">
                <GhostButton
                    className="btn-ghost cancel"
                    title={'Cancel'}
                    onClick={this.props.closeModal}
                />
                <PurpleRoundButton
                    className="btn-purple-round re-assign"
                    title={'Re-assign'}
                    onClick={this.props.reAssign}
                />
              </div>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}