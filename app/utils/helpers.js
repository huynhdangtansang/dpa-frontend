/*eslint-disable */
export function hideKeyboard() {
  if (window.Keyboard) {
    window.Keyboard.hide();
  }
}
export function showKeyboard() {
  if (window.Keyboard) {
    window.Keyboard.show();
  }
}
export function isKeyboardVisible() {
  if (window.Keyboard) {
    return window.Keyboard.isVisible;
  }
}

export function checkConnection() {
  if (window.cordova) {
    let networkState = navigator.connection.type;

    let states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';

    return networkState !== Connection.NONE;
  } else {
    return navigator.onLine;
  }
}

export async function downloadFile(url, targetUrl) {
  return new Promise((resolve, reject) => {
    if (checkTargetPlatform()) {
      //Should works only with Android and iOS
      let fileTransfer = new FileTransfer();
      let uri = encodeURI(url);
      fileTransfer.download(
        uri,
        targetUrl,
        function(entry) {
          resolve(targetUrl);
        },
        function(error) {
          reject(error);
        },
      );
    } else {
      resolve();
    }
  });
}
export async function downloadImageBase64(url) {
  return new Promise((resolve, reject) => {
    if (checkTargetPlatform()) {
      let xhr = new XMLHttpRequest();
      xhr.onload = function() {
        let reader = new FileReader();
        reader.onloadend = function() {
          resolve(reader.result);
        };
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    }
  });
}
export function checkTargetPlatform() {
  if (window.cordova) {
    return (
      window.cordova.platformId === 'ios' ||
      window.cordova.platformId === 'android'
    );
  }
  return false;
}
export function checkPlatform(platform) {
  return window.cordova && window.cordova.platformId === platform;
}
