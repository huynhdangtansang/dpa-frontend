import { fromJS } from 'immutable';
import employeeDetailsPageReducer from '../reducer';

describe('employeeDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(employeeDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
