/*
 *
 * TeamEditMemberPage constants
 *
 */
export const DEFAULT_ACTION = "app/TeamEditMemberPage/DEFAULT_ACTION";
export const GET_MEMBER_DETAILS = "TeamEditMemberPage/GET_MEMBER_DETAILS";
export const GET_SUCCESS = "TeamEditMemberPage/GET_SUCCESS";
export const GET_ERROR = "TeamEditMemberPage/GET_ERROR";
export const UPDATE_PROFILE = "TeamEditMemberPage/UPDATE_PROFILE";
export const UPDATE_SUCCESS = "TeamEditMemberPage/UPDATE_SUCCESS";
export const UPDATE_ERROR = "TeamEditMemberPage/UPDATE_ERROR";
export const CHANGE_AVATAR = "TeamEditMemberPage/CHANGE_AVATAR";
export const RESET_ERROR = "TeamEditMemberPage/RESET_ERROR";