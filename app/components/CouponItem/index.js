import React from 'react';
import './style.scss';
import { Formik } from 'formik';
import * as Yup from 'yup';
import InputForm from "components/InputForm";
import Dropdown from 'components/Dropdown';
import Datepicker from 'components/Datepicker';
//Library
import _ from "lodash";

const validateForm = Yup.object().shape({
  'name': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid coupon name (no special characters)')
      .min(6, 'Invalid coupon name (At least 6 characters)')
      .max(30, 'Invalid coupon name (Maximum at 30 characters)'),
  'code': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid coupon code (no special characters)')
      .min(6, 'Invalid coupon code (At least 6 characters)')
      .max(30, 'Invalid coupon code (Maximum at 30 characters)'),
});
export default class CouponItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  deleteCoupon = (index) => {
    this.props.delete(index);
  };
  handlePropsChange = (index, type, value, values) => {
    let temp = values;
    if (temp) {
      if (type === 'name') {
        temp.name = value;
      } else if (type === 'code') {
        temp.code = value;
      }
    }
    let error = _.isUndefined(temp) ? false : this.regexData(temp);
    this.props.handleChange(index, type, value, error);
  };
  getDiscountType = (value) => {
    this.handlePropsChange(this.props.index, 'disCountType', value);
  };
  handleDateChange = (index, date, errors) => {
    this.handlePropsChange(index, 'expiredAt', date, errors);
  };
  regexData = (values) => {
    let name = (values && values.name) ? values.name : '';
    let code = (values && values.code) ? values.code : '';
    if (name.length >= 6 && name.length <= 30 && code.length >= 6 && code.length <= 30) {
      let regexSpecialCharacter = /^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/;
      if (regexSpecialCharacter.test(name) && regexSpecialCharacter.test(code)) {
        return false;
      }
    } else return true;
  };

  render() {
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return (
        <Formik
            initialValues={{
              name: this.props.name,
              code: this.props.code
            }}
            enableReinitialize={false}
            validationSchema={validateForm}>
          {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              setFieldValue
            }) => (
              <div className="coupon-item" hidden={this.props.hidden}>
                <div className="details">
                  {/* COUPON NAME */}
                  <InputForm
                      label="coupon name"
                      name={'name'}
                      value={values.name}
                      placeholder={'Coupon name'}
                      onChange={(e) => {
                        handleChange(e);
                        setFieldValue('name', e.target.value);
                        this.handlePropsChange(this.props.index, 'name', e.target.value, values);
                      }}
                      onBlur={handleBlur}
                      touched={touched.name}
                      error={errors.name}/>
                  {/* COUPON CODE */}
                  <InputForm
                      label="coupon code"
                      name={'code'}
                      value={values.code}
                      placeholder={'Coupon code'}
                      onChange={(e) => {
                        handleChange(e);
                        setFieldValue('code', e.target.value);
                        this.handlePropsChange(this.props.index, 'code', e.target.value, values);
                      }}
                      onBlur={handleBlur}
                      touched={touched.code}
                      error={errors.code}/>
                  {/* DISCOUNT */}
                  <div className="info-item coupon-discount">
                    <div className="title">
                      <span>Discount</span>
                    </div>
                    <div className="data">
                      <Dropdown
                          defaultValue={this.props.discountType}
                          defaultLabel={this.props.discountType}
                          options={[
                            { label: '%', value: '%' },
                            { label: '$', value: '$' },
                          ]}
                          onSelect={this.getDiscountType}
                      />
                      <input
                          name={'discount'}
                          value={this.props.discount}
                          type="number"
                          placeholder={'Discount'}
                          className="discount-input"
                          onChange={(e) => {
                            if (this.props.discountType === '%') {
                              if (e.target.value > 100) {
                                this.handlePropsChange(this.props.index, 'disCount', 100, values);
                              } else if (e.target.value < 0) {
                                this.handlePropsChange(this.props.index, 'disCount', 0, values);
                              } else {
                                this.handlePropsChange(this.props.index, 'disCount', e.target.value, values);
                              }
                            }
                          }}/>
                    </div>
                  </div>
                  {/* EXPIRY DATE */}
                  <div className="info-item expiry-date">
                    <div className="title">
                      <span>Expiry Date</span>
                    </div>
                    <div className="data">
                      <Datepicker
                          selected={this.props.expiryDate ? new Date(this.props.expiryDate) : undefined}
                          placeholder={"Expiry Date"}
                          minDate={tomorrow}
                          onChange={date => {
                            this.handleDateChange(this.props.index, date, values);
                          }}
                      />
                    </div>
                  </div>
                </div>
                <div className="delete">
                  <div className="icon-contain">
                    <span className="icon-bin" onClick={() => {
                      this.deleteCoupon(this.props.index);
                    }}></span>
                  </div>
                </div>
              </div>
          )}
        </Formik>
    )
  }
}