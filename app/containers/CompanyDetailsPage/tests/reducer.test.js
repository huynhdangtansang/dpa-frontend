import { fromJS } from 'immutable';
import companyDetailsPageReducer from '../reducer';

describe('companyDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(companyDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
