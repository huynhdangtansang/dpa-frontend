import { fromJS } from 'immutable';
import servicesDetailsPageReducer from '../reducer';

describe('servicesDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(servicesDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
