/*
 *
 * DeviceEditPage constants
 *
 */
export const DEFAULT_ACTION = "app/DeviceEditPage/DEFAULT_ACTION";
export const GET_DEVICE_DETAILS = "DeviceEditPage/GET_DEVICE_DETAILS";
export const GET_SUCCESS = "DeviceEditPage/GET_SUCCESS";
export const GET_ERROR = "DeviceEditPage/GET_ERROR";
export const UPDATE_DEVICE_PROFILE = "DeviceEditPage/UPDATE_DEVICE_PROFILE";
export const UPDATE_SUCCESS = "DeviceEditPage/UPDATE_SUCCESS";
export const UPDATE_ERROR = "DeviceEditPage/UPDATE_ERROR";
export const ADD_ADDRESS = "DeviceEditPage/ADD_ADDRESS";
export const EDIT_ADDRESS = "DeviceEdit/EDIT_ADDRESS";
export const CHANGE_AVATAR = "DeviceEdit/CHANGE_AVATAR";
export const SHOW_MODAL = "DeviceEdit/SHOW_MODAL";