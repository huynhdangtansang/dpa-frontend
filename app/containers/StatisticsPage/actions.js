/*
 *
 * StatisticsPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function resetStore() {
  return {
    type: Constants.RESET_STORE
  };
}

export function getAllDataStatistic() {
  return {
    type: Constants.GET_ALL_DATA
  };
}

export function getSuccess(key, response) {
  return {
    type: Constants.GET_ALL_SUCCESS,
    key,
    response
  };
}

export function getCharByType(key, typeChart, searchData) {
  return {
    type: Constants.GET_CHART_BY_TYPE,
    key, typeChart, searchData
  };
}

export function getDropdownSuccess(key, response) {
  return {
    type: Constants.GET_DROPDOWN_SUCCESS,
    key,
    response
  };
}

export function updateFilterChart(position, value) {
  return {
    type: Constants.UPDATE_FILTER_CHART,
    position, value
  };
}