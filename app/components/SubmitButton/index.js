/**
 *
 * SubmitButton
 *
 */
import React from "react";
import ClassNames from "classnames";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './style.scss';

function SubmitButton(props) {
  return (
      <button type={props.type}
              disabled={props.disabled}
              className={ClassNames('btn btn-login', props.className ? props.className : '', !props.disabled && 'isSubmit')}>
        {props.content}
      </button>
  );
}

SubmitButton.propTypes = {};
export default SubmitButton;
