import { put, takeLatest } from 'redux-saga/effects';
import { GET_MEMBER_DETAILS, UPDATE_PROFILE } from './constants';
import { getError, getSuccess, updateError, updateSuccess } from './actions';
import { loadRepos, reposLoaded, updateError as updateErrorApi } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import { goBack } from 'react-router-redux';
import _ from "lodash";

export function* apiGetMemberDetails(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(getError(error.response));
      yield put(updateErrorApi({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateMemberProfile(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.put(requestUrl, data.profile);
      yield put(updateSuccess(response.data));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(updateError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_MEMBER_DETAILS, apiGetMemberDetails);
  yield takeLatest(UPDATE_PROFILE, apiUpdateMemberProfile);
}