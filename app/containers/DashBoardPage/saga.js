/* eslint-disable no-console */
import { put, takeLatest } from 'redux-saga/effects';
import config from 'config';
import _ from "lodash";
import axios from 'axios';
import { GET_NOTIFICATION, GET_USER_PROFILE, HIDE_NOTIFICATION, SELECT_NOTIFICATION, SIGN_OUT } from './constants';
import { getNotification, getNotificationComplete, getSuccess, signOutError, } from './actions';
import { reposLoaded } from 'containers/App/actions';
import { push } from 'react-router-redux';
//set default for all request because dashboard is a bounder of all page after login
//and after reload need set default
axios.defaults.headers.common['x-auth-token'] = 'Bearer ' + localStorage.getItem('token');

export function* apiGetProfile(data) {
  const { resolve, reject } = data;
  const requestUrl = config.serverUrl + config.api.user_profile.get;
  try {
    const response = yield axios.get(requestUrl);
    if (_.isFunction(resolve))
      resolve(response);
    yield put(getSuccess(response.data));
  } catch (error) {
    if (_.isFunction(reject))
      reject(error);
    yield put(reposLoaded());
    localStorage.removeItem('token');
    localStorage.removeItem('tokenExpired');
    if (localStorage.getItem('isRemember')) {
      localStorage.removeItem('isRemember');
    }
  }
}

export function* apiLogout() {
  const requestUrl = config.serverUrl + config.api.auth.log_out;
  try {
    yield axios.post(requestUrl);
    localStorage.removeItem('token');
    localStorage.removeItem('tokenExpired');
    if (localStorage.getItem('isRemember')) {
      localStorage.removeItem('isRemember');
    }
    
    window.location.reload();
  } catch (error) {
    if (error) {
      yield put(signOutError());
    }
  }
}

export function* apiGetNotification() {
  const requestUrl = config.serverUrl + config.api.user_profile.notification;
  try {
    const response = yield axios.get(requestUrl);
    yield put(getNotificationComplete(response.data))
  } catch (error) {
    yield put(reposLoaded());
    yield put(getNotificationComplete(error.response.data))
  }
}

export function* apiMarkRead(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.notification + '/markRead' + '?notificationId=' + data.id;
    try {
      yield axios.put(requestUrl);
      yield put(getNotification());
    } catch (error) {
      yield put(reposLoaded());
      console.log(error.response);
    }
  }
}

export function* apiHideNoti(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.notification + '/markHidden' + '?notificationId=' + data.id;
    try {
      yield axios.put(requestUrl);
      yield put(getNotification());
    } catch (error) {
      yield put(reposLoaded());
      console.log(error.response);
    }
  }
}

export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_USER_PROFILE, apiGetProfile);
  yield takeLatest(SIGN_OUT, apiLogout);
  yield takeLatest(GET_NOTIFICATION, apiGetNotification);
  yield takeLatest(SELECT_NOTIFICATION, apiMarkRead);
  yield takeLatest(HIDE_NOTIFICATION, apiHideNoti);
}
