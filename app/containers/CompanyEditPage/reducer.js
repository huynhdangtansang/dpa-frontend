/*
 *
 * EditCompanyPage reducer
 *
 */
import { fromJS } from "immutable";
import _ from "lodash";
import * as Constants from "./constants";

export const initialState = fromJS({
  companyLogo: '/default-user.png',
  companyData: {},
  areas: {
    timeZone: [],
    country: [],
    city: [],
    suburbs: [],
    formatDay: [],
    formatTime: []
  },
  listLicense: null,
  serviceData: null,
  listAcceptRateCards: null,
  errors: [],
  myServiceData: [],
  indexLicenseData: -1,
  tempListLicenseImage: [],
  tempDateExpired: undefined,
  dataUpdateCompany: {},
  proofPopup: false,
  searchDataForService: {
    offset: 0,
    limit: 20,
    search: '',
    sort: '',
    sortType: ''
  },
  dataBackup: {
    listLicense: {},
    listAcceptRateCards: {},
  }
});

function companyDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return initialState;
    case Constants.RESET_ERROR:
      return state.set('error', fromJS([]));
    case Constants.GET_ALL_DATA:
      return state
        .set('companyData', fromJS({}))
        .set('listLicense', null)
        .set('serviceData', null);
    case Constants.GET_SUCCESS:
      return state
        .set('companyData', fromJS(action.response.data))
        .set('areas', fromJS(!_.isEmpty(action.response.data.areas[0]) ? action.response.data.areas[0] : state.toJS().areas))
        .set('listLicense', fromJS(action.response.data.licenses))
        .set('listAcceptRateCards', fromJS(action.response.data.acceptRateCards))
        .set('companyLogo', action.response.data.logo ? action.response.data.logo.fileName : state.toJS().companyLogo);
    case Constants.GET_SERVICE_SUCCESS:
      return state
        .set('serviceData', action.response);
    case Constants.GET_ALL_DATA_SUCCESS:
      return state
        .set('myServiceData', action.myServiceData);
    case Constants.UPDATE_COMPANY_LIST_SERVICE:
      return state
        .update('myServiceData', list => fromJS(list).setIn([action.index, action.key], action.value));
    case Constants.SELECT_LICENSE:
      if (action.index < 0) {
        return state
          .set('indexLicenseData', -1)
          .set('proofPopup', true)
          .set('tempListLicenseImage', [])
          .set('tempDateExpired', null)
      }
      return state
        .set('indexLicenseData', action.index)
        .set('proofPopup', true)
        .set('tempListLicenseImage', state.toJS().listLicense[action.index].proofs ? state.toJS().listLicense[action.index].proofs : [])
        .set('tempDateExpired', _.isNull(state.get('listLicense').toJS()[action.index].expiredAt) ? undefined : state.get('listLicense').toJS()[action.index].expiredAt);
    case Constants.UPDATE_LIST_LICENSE:
      return state
        .set('listLicense', fromJS(action.dataUpdate));
    case Constants.UPDATE_LIST_ACCEPT_RATE_CARD:
      return state
        .set('listAcceptRateCards', action.dataUpdate);
    case Constants.CHANGE_LOGO:
      return state
        .set('companyLogo', action.src);
    case Constants.UPDATE_ITEMPROOF:
      if (!state.toJS().listLicense[action.index].certificationId) {
        return state.set('proofPopup', false)
      }
      return state
        .update('listLicense', list => fromJS(list).setIn([action.index, action.key], action.value))
        .set('proofPopup', false)
        .setIn(['dataBackup', 'listLicense', state.toJS().listLicense[action.index].certificationId._id, action.key], action.value);
    case Constants.TOOGLE_ACCEPTANCE:
      return state
        .update('listAcceptRateCards', list => fromJS(list).setIn([action.index, 'isAccept'], action.value))
        .setIn(['dataBackup', 'listAcceptRateCards', state.toJS().listAcceptRateCards[action.index].serviceId._id, action.key], action.value);
    case Constants.ADD_LICENSE_IMAGE:
      return state
        .update('tempListLicenseImage', list => fromJS(list).push(action.image));
    case Constants.REMOVE_LICENSE_IMAGE:
      return state
        .update('tempListLicenseImage',
          list => fromJS(list).filter((el, index) => {
            return index !== action.index;
          }));
    case Constants.CLEAR_LICENSE_IMAGE:
      return state
        .set('tempListLicenseImage', [])
        .set('tempDateExpired', undefined)
        .set('proofPopup', false);
    case Constants.UPDATE_SERVICE_IN_STORE:
      return state.setIn(['companyData', 'services'], fromJS(action.newMyServiceData));
    case Constants.SAVE_LICENSE_IMAGE:
      return state
        .update('listLicense', list => fromJS(list).updateIn([action.index, 'proofs'], action.data));
    case Constants.UPDATE_DATE_EXPIRED_TEMP:
      return state
        .set('tempDateExpired', action.date);
    case Constants.UPDATE_SEARCH_DATA_SERVICE:
      return state
        .setIn(['searchDataForService', action.key], action.value);
    case Constants.SAVE_DATA_UPDATE_COMPANY:
      return state
        .set('dataUpdateCompany', action.dataUpdate);
    case Constants.CHANGE_AREA_AND_HOURS:
      return state.setIn(['areas', action.event.name], fromJS(action.event.value));
    default:
      return state;
  }
}

export default companyDetailsPageReducer;
