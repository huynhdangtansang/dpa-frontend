import React from 'react';
import './styles.scss';
import PropTypes from "prop-types";

export default class PurpleRoundButton extends React.Component {
  render() {
    return (
        <button className={this.props.className} onClick={this.props.onClick} type={this.props.type}
                form={this.props.form} disabled={this.props.disabled}>
          {this.props.title}
        </button>
    );
  }
}
PurpleRoundButton.propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  form: PropTypes.string
};