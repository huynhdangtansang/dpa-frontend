/**
 *
 * Asynchronously loads the component for PaymentDetails
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
