/*
 *
 * LoginPage reducer
 *
 */
import { fromJS } from "immutable";
import { DEFAULT_ACTION, LOGIN_FAIL, LOGIN_SUCCESS, SET_ERRORS } from "./constants";

export const initialState = fromJS({
  apiError: '',
  email: '',
});

function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return initialState;
    case SET_ERRORS:
      return state
          .set('apiError', action.data);
    case LOGIN_SUCCESS:
      return state
          .set('apiError', action.response.errors)
          .set('email', action.email);
    case LOGIN_FAIL:
      return state
          .set('apiError', action.response.errors);
    default:
      return state;
  }
}

export default loginPageReducer;
