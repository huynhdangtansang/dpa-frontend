/*
 *
 * CompanyEditPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function resetError() {
  return {
    type: Constants.RESET_ERROR
  };
}

export function getAllDataSetup(id, searchDataForService) {
  return {
    type: Constants.GET_ALL_DATA,
    id,
    searchDataForService
  };
}

export function getAllDataSuccess(myServiceData) {
  return {
    type: Constants.GET_ALL_DATA_SUCCESS,
    myServiceData
  };
}

export function getCompany(companyID) {
  return {
    type: Constants.GET_COMPANY_DATA,
    id: companyID
  };
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response: response
  }
}

export function getService(companyData, searchDataForService) {
  return {
    type: Constants.GET_SERVICE_DATA,
    companyData,
    searchDataForService
  };
}

export function getServiceSuccess(response) {
  return {
    type: Constants.GET_SERVICE_SUCCESS,
    response: response
  }
}

export function updateAllCompany(companyID, data, resolve, reject) {
  return {
    type: Constants.UPDATE_COMPANY,
    id: companyID,
    data,
    resolve,
    reject
  };
}

export function updateSuccess(response) {
  return {
    type: Constants.UPDATE_SUCCESS,
    response: response
  }
}

export function processMyServiceData(companyServiceData, listServiceData) {
  return {
    type: Constants.PROCESS_MY_SERVICE_DATA,
    companyServiceData,
    listServiceData
  }
}

export function editCompanyListServiceData(key, value, index) {
  return {
    type: Constants.UPDATE_COMPANY_LIST_SERVICE,
    key: key,
    value: value,
    index: index
  }
}

export function selectService(newMyServiceData, companyID, serviceIds, dataBackup) {
  return {
    type: Constants.SELECT_SERVICE,
    newMyServiceData,
    companyID,
    serviceIds,
    dataBackup
  }
}

export function updateServiceInStore(newMyServiceData) {
  return {
    type: Constants.UPDATE_SERVICE_IN_STORE,
    newMyServiceData,
  }
}

export function selectLicense(index) {
  return {
    type: Constants.SELECT_LICENSE,
    index
  }
}

export function updateDateExpiredTemp(date) {
  return {
    type: Constants.UPDATE_DATE_EXPIRED_TEMP,
    date
  }
}

export function updateItemProof(key, value, index) {
  return {
    type: Constants.UPDATE_ITEMPROOF,
    key,
    value,
    index
  }
}

export function changeLogo(src) {
  return {
    type: Constants.CHANGE_LOGO,
    src
  }
}

export function addLicenseImage(image) {
  return {
    type: Constants.ADD_LICENSE_IMAGE,
    image
  }
}

export function removeLicenseImage(index) {
  return {
    type: Constants.REMOVE_LICENSE_IMAGE,
    index
  }
}

export function clearLicenseImage() {
  return {
    type: Constants.CLEAR_LICENSE_IMAGE,
  }
}

export function updateSearchDataService(key, value) {
  return {
    type: Constants.UPDATE_SEARCH_DATA_SERVICE,
    key,
    value
  }
}

export function toogleAcceptance(key, value, index) {
  return {
    type: Constants.TOOGLE_ACCEPTANCE,
    key,
    value,
    index
  }
}

export function changeAreaAndHours(event) {
  return {
    type: Constants.CHANGE_AREA_AND_HOURS,
    event
  }
}

export function saveDataUpdateCompany(dataUpdate) {
  return {
    type: Constants.SAVE_DATA_UPDATE_COMPANY,
    dataUpdate
  }
}

export function updateListLicense(dataUpdate) {
  return {
    type: Constants.UPDATE_LIST_LICENSE,
    dataUpdate
  }
}

export function updateListAcceptRateCard(dataUpdate) {
  return {
    type: Constants.UPDATE_LIST_ACCEPT_RATE_CARD,
    dataUpdate
  }
}
