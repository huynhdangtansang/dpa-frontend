/**
 *
 * BotMessagesPages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectBotMessagesPages from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from './actions';
//Lib
import { DropdownItem, DropdownMenu, DropdownToggle, Form, Input, InputGroup, UncontrolledDropdown } from 'reactstrap';
//Table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//CSS
import './style.scss';
//Components
import classnames from 'classnames';

/* eslint-disable react/prefer-stateless-function */
export class BotMessagesPages extends React.PureComponent {
  sortData = (field, order,) => {
    if (field === 'address') {
      this.setState(prevState => ({
        searchData: {
          ...prevState.searchData,
          offset: 0,
          sort: 'suburb',
          sortType: order
        }
      }), () => {
        this.handleFilter();
      })
    } else {
      this.setState(prevState => ({
        searchData: {
          ...prevState.searchData,
          offset: 0,
          sort: field,
          sortType: order
        }
      }), () => {
        this.handleFilter();
      })
    }
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getMessageList();
  }

  render() {
    const { dataList } = this.props.botmessagespages;
    const options = {
      defaultSortName: 'messageName',
      defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      withoutNoDataText: true,
      //onSortChange: this.sortData
    };
    const actionFormat = () => {
      return <span className="icon-edit"></span>
    };
    return (
        <div className="bot-message-list">
          <div className="header-list-page">
            <Form inline onSubmit={() => {
              this.handleFilter();
            }} className=" align-items-center">

              <InputGroup className="search-bar">
                <Input placeholder="Search message"
                       onChange={(e) => {
                         this.handleSearchChange(e.target.value);
                       }}
                />
                <span className="icon-search"></span>
              </InputGroup>

              <UncontrolledDropdown className="period-dropdown">
                <DropdownToggle>
                  <span className="title">User:&nbsp;</span>
                  <span className="content">{'All'}</span>
                  <span className="fixle-caret icon-triangle-down"></span>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => {
                    this.handleSuburbsChange('');
                  }}>All</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Form>

          </div>
          <div className={classnames("content content-list-page")}>
            <BootstrapTable
                ref="table_messages"
                data={dataList instanceof Array ? dataList : []}
                options={options}
                bordered={false}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'>
              <TableHeaderColumn dataField='_id' isKey hidden>Message ID</TableHeaderColumn>
              <TableHeaderColumn dataSort width={'20%'} dataField='messageName' className="message-name"
                                 columnClassName="message-name">Message name</TableHeaderColumn>
              <TableHeaderColumn dataSort width={'10%'} dataField='speaker'
                                 dataAlign={'center'}>Speaker</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='content' columnClassName="speaker">Content</TableHeaderColumn>
              <TableHeaderColumn dataField='data' dataFormat={actionFormat} columnClassName={'edit-dropdown'}
                                 dataAlign='right' width={'7%'}>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
    );
  }
}

BotMessagesPages.propTypes = {
  dispatch: PropTypes.func,
  getMessageList: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  botmessagespages: makeSelectBotMessagesPages()
});

function mapDispatchToProps(dispatch) {
  return {
    getMessageList: () => {
      dispatch(Actions.getMessageList());
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "botMessagesPages", reducer });
const withSaga = injectSaga({ key: "botMessagesPages", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(BotMessagesPages);
