import { put, takeLatest } from 'redux-saga/effects';
import {
  CANCEL_JOB,
  CHANGE_SPECIFIC_SELECTION,
  COMPLETE_PAY,
  GET_ACTS,
  GET_COMPANIES,
  GET_JOB,
  REASSIGN_JOB,
  REASSIGN_SPECIFIC,
  REFUND,
  RESCHEDULE_JOB,
  SUBMIT_ESTIMATE,
  UPDATE_ESTIMATE
} from './constants';
import {
  getActs,
  getActsSuccess,
  getCompaniesError,
  getCompaniesSuccess,
  getEmployeesError,
  getEmployeesSuccess,
  getSuccess
} from './actions';
import config from 'config';
import axios from 'axios';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import _ from "lodash";

export function* apiGetJob(data) {
  if (data) {
    const { resolve, reject } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      if (_.isFunction(resolve))
        resolve(response.data);
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      if (_.isFunction(reject))
        reject(error.response.data);
      yield put(reposLoaded());
    }
  }
}

export function* apiGetActivities(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.activity.acts_list + data.jobId + '/activities';
    try {
      const response = yield axios.get(requestUrl);
      yield put(getActsSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetCompanies(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.job + '/getListCompanyByServiceId?serviceId=' + data.serviceId;
    try {
      const response = yield axios.get(requestUrl);
      yield put(getCompaniesSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(getCompaniesError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetEmployees(data) {
  if (data) {
    const { selector, searchData } = data;
    if (selector === 'company') {
      yield put(loadRepos());
      let params = searchData;
      const esc = encodeURIComponent;
      const query = Object.keys(params)
          .map(k => k + '=' + esc(params[k]))
          .join('&');
      const requestUrl = config.serverUrl + config.api.job + '/getListEmployeeByCompanyId?' + query;
      try {
        const response = yield axios.get(requestUrl);
        yield put(getEmployeesSuccess(response.data));
        yield put(reposLoaded());
      } catch (error) {
        yield put(getEmployeesError(error.response.data));
        yield put(updateError({
          error: true,
          title: 'Error!!!',
          message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
        }));
        yield put(reposLoaded());
      }
    }
  }
}

export function* apiReAssignJob(data) {
  if (data) {
    const { reAssignType, reAssignData } = data;
    yield put(loadRepos());
    if (reAssignType === 'company') {
      let { jobId } = reAssignData;
      const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/reassignCompanies';
      try {
        yield axios.put(requestUrl);
        yield put(reposLoaded());
        yield put(getActs(jobId));
      } catch (error) {
        yield put(updateError({
          error: true,
          title: 'Error!!!',
          message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
        }));
        yield put(reposLoaded());
      }
    } else if (data.reAssignType === 'provider') {
      let { jobId } = reAssignData;
      const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/reassignProvider';
      try {
        yield axios.put(requestUrl);
        yield put(reposLoaded());
        yield put(getActs(jobId));
      } catch (error) {
        yield put(updateError({
          error: true,
          title: 'Error!!!',
          message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
        }));
        yield put(reposLoaded());
      }
    }
  }
}

export function* apiReAssignSpecific(data) {
  if (data) {
    const { jobId } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/reassignSpecificCompanies';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.assignData);
      yield put(reposLoaded());
      yield put(getActs(jobId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiReScheduleJob(data) {
  if (data) {
    const { jobId, appointmentTime, reason } = data;
    const requestUrl = config.serverUrl + config.api.job + '/rescheduleJob/' + jobId;
    yield put(loadRepos());
    try {
      yield axios.post(requestUrl, {
        appointmentTime: appointmentTime,
        reason: reason
      });
      yield put(reposLoaded());
      yield put(getActs(jobId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiCancelJob(data) {
  if (data) {
    const { jobId, reason } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/cancelJob';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, { reason: reason });
      yield put(reposLoaded());
      yield put(getActs(jobId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiSubmitEstimate(data) {
  if (data) {
    const { updateData, jobId } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/submitEstimate';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, updateData);
      yield put(reposLoaded());
      yield put(getActs(jobId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateEstimate(data) {
  if (data) {
    const { updateData, jobId } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/updateEstimateForMaster';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, updateData);
      yield put(reposLoaded());
      yield put(getActs(jobId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiRefund(data) {
  if (data) {
    const { refundData, jobId } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/refundPayment';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, refundData);
      yield put(getActs(jobId));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiCompleteAndPay(data) {
  if (data) {
    const { jobId } = data;
    const requestUrl = config.serverUrl + config.api.job + '/' + jobId + '/completedAndPay';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl);
      yield put(getActs(jobId));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_JOB, apiGetJob);
  yield takeLatest(GET_ACTS, apiGetActivities);
  yield takeLatest(REASSIGN_JOB, apiReAssignJob);
  yield takeLatest(CANCEL_JOB, apiCancelJob);
  yield takeLatest(GET_COMPANIES, apiGetCompanies);
  yield takeLatest(CHANGE_SPECIFIC_SELECTION, apiGetEmployees);
  yield takeLatest(REASSIGN_SPECIFIC, apiReAssignSpecific);
  yield takeLatest(RESCHEDULE_JOB, apiReScheduleJob);
  yield takeLatest(SUBMIT_ESTIMATE, apiSubmitEstimate);
  yield takeLatest(UPDATE_ESTIMATE, apiUpdateEstimate);
  yield takeLatest(REFUND, apiRefund);
  yield takeLatest(COMPLETE_PAY, apiCompleteAndPay);
}
