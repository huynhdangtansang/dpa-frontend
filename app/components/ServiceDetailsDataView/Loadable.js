/**
 *
 * Asynchronously loads the component for ServiceDetailsDataView
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
