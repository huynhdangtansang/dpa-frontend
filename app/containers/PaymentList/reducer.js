/*
 *
 * PaymentList reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  paymentList: [],
  pagination: {},
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    status: [
      {
        label: 'All',
        value: ''
      }
    ],
    sort: 'payoutId',
    sortType: 'desc',
  },
  selectedPayments: [],
  currentPage: 1,
});

function paymentListReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case Constants.DEFAULT_ACTION:
      return state;
    case Constants.GET_LIST:
      return state
        .set('paymentList', fromJS([]));
    case Constants.GET_SUCCESS:
      return state
        .set('paymentList', fromJS(action.response.data))
        .set('pagination', fromJS(action.response.pagination));
    case Constants.CHANGE_PAGE:
      return state
        .set('currentPage', action.newPage);
    case Constants.UPDATE_SEARCH_DATA:
      return state
        .set('searchData', fromJS(action.newSearchData));

    //Select
    case Constants.SELECT_PAYMENT:
      if (action.isSelected === true) {
        return state
          .updateIn(['selectedPayments'], arr => arr.push(action.selectItem))
      } else {//unselect
        return state
          .set('selectedPayments', fromJS(state.get('selectedPayments').toJS().filter((payment) => payment.id !== action.selectItem.id)));
      }
    case Constants.SELECT_ALL_PAYMENT:
      if (action.isSelected === true) {
        let newArray = state.get('paymentList').toJS().map((payment) => {
          return { id: payment._id }
        });
        return state
          .set('selectedPayments', fromJS(newArray))
      } else {
        return state
          .set('selectedPayments', fromJS([]))
      }

    //PRINT
    default:
      return state;
  }
}

export default paymentListReducer;
