/*
 *
 * ServicesListPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  GET_SERVICES_LIST,
  GET_SUCCESS,
  SELECT_ALL_SERVICE,
  SELECT_SERVICE,
  SET_CURRENT_SERVICE,
  SHOW_COMMISSION_CONFIRM,
  SHOW_REMOVE_CONFIRM,
  UPDATE_MODIFY_TYPE,
  UPDATE_PAGE,
  UPDATE_SEARCH_DATA
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  currentPage: 1,
  servicesList: [],
  pagination: {},
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    sort: '',
    sortType: ''
  },
  selectedServices: [],
  chosenService: {},
  modifyType: 'single',
  showRemoveConfirm: false,
  showChangeCommissionConfirm: false
});

function servicesListPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_SERVICES_LIST:
      return state.set('servicesList', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('servicesList', action.response.data)
          .set('pagination', fromJS(action.response.pagination))
          .set('showRemoveConfirm', false)
          .set('showChangeCommissionConfirm', false);
    case UPDATE_SEARCH_DATA:
      return state
          .set('searchData', action.newSearchData);
    case UPDATE_PAGE:
      return state
          .set('currentPage', action.newPage);
    case SELECT_SERVICE:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedServices'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedServices', state.get('selectedServices').filter((service) => service.id !== action.selectItem.id))
      }
    case SELECT_ALL_SERVICE:
      if (action.isSelected === true) {
        let newArray = state.get('servicesList').map((service) => {
          return {
            id: service._id,
          }
        });
        return state
            .set('selectedServices', newArray)
      } else {
        return state
            .set('selectedServices', fromJS([]))
      }
    case SHOW_REMOVE_CONFIRM:
      return state
          .set('showRemoveConfirm', action.value);
    case SHOW_COMMISSION_CONFIRM:
      return state
          .set('showChangeCommissionConfirm', action.value);
    case UPDATE_MODIFY_TYPE:
      return state
          .set('modifyType', action.value);
    case SET_CURRENT_SERVICE:
      return state
          .set('chosenService', action.service);
    default:
      return state;
  }
}

export default servicesListPageReducer;
