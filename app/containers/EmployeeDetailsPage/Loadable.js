/**
 *
 * Asynchronously loads the component for EmployeeDetailsPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
