import React from "react";
import NumberFormat from 'react-number-format';
import './style.scss';
import EstimateConfirmModal from 'components/EstimateConfirmModal';
import EditEstimateModal from 'components/EditEstimateModal';

export default class EstimateResponse extends React.Component {
  estimateCharge = (data) => {
    const estimate = data;
    let estimateView = [];
    if (estimate) {
      for (let key in estimate) {
        let name = '';
        if (key === 'minCharge') {
          name = 'Minimum charge'
        } else if (key === 'gst') {
          name = (estimate[key].percent ? estimate[key].percent : '') + '% ' + 'GST'
        } else if (key === 'afterHourFee') {
          name = 'After hour fee'
        } else if (key === 'coupon') {
          name = 'Coupon'
        } else if (key === 'insuranceCover') {
          name = 'Insurance cover'
        } else if (key === 'additionalHours') {
          name = 'Additional hours';
        } else if (key === 'additionalParts') {
          name = 'Additional parts';
        }
        let view = (
            <div key={key} className="response-item">
              <span className="name">{name}</span>
              <NumberFormat
                  className="price"
                  value={estimate[key].value}
                  className="price"
                  displayType={'text'}
                  prefix={estimate[key].unit}
                  decimalScale={2}
                  fixedDecimalScale={estimate[key].value > 0 ? true : false}
              />
            </div>
        );
        if (name && estimate[key].value > 0 && estimate[key].active === true) {
          estimateView.push(view);
        }
      }
    }
    return estimateView;
  };
  submitEdit = (estimateData, total) => {
    this.setState({
      estimateData,
      total
    }, () => {
      this.props.closeModal();
      this.setState({ showEstimateConfirm: true });
    });
  };
  getDeposit = () => {
    if (this.props.data && this.props.data.deposit && this.props.data.deposit.active === true) {
      return this.props.data.deposit.value;
    } else return 0;
  };

  constructor(props) {
    super(props);
    this.state = {
      showEstimateConfirm: false
    };
  }

  render() {
    const { data, showEditEstimate, closeModal } = this.props;
    return (
        <div className="estimate-content response">
          <div className="section">
            <div className="title">Estimate</div>
            <div className="response-list">
              {this.estimateCharge(data)}
            </div>
          </div>
          {data.deposit && data.deposit.active === true &&
          <div className="deposit">
            <span className='name'>Deposit</span>
            <NumberFormat
                value={data.deposit ? data.deposit.value : 0}
                className="price"
                displayType={'text'}
                prefix={'$'}
                decimalScale={2}
                fixedDecimalScale={true}
            />
          </div>
          }
          <div className="total">
            <span className='name'>{(data.deposit && data.deposit.active === true) ? 'Balance' : 'Total'}</span>
            <NumberFormat
                value={data.total ? data.total - this.getDeposit() : 0}
                className="price"
                displayType={'text'}
                prefix={'$'}
                decimalScale={2}
                fixedDecimalScale={true}
            />
          </div>
          <EditEstimateModal
              show={showEditEstimate}
              data={data}
              closeModal={closeModal}
              onSubmit={(estimateData, total) => {
                this.submitEdit(estimateData, total);
              }}/>
          {(this.state.estimateData && this.state.total) &&
          <EstimateConfirmModal
              show={this.state.showEstimateConfirm}
              charges={this.state.estimateData}
              total={this.state.total}
              isEdit={true}
              closeModal={() => {
                this.props.showModal();
                this.setState({ showEstimateConfirm: false });
              }}
              submit={() => {
                this.setState({ showEstimateConfirm: false });
                let data = this.state.estimateData;
                data['total'] = this.state.total;
                this.props.onSubmit(this.state.estimateData);
              }}/>
          }
        </div>
    )
  }
}