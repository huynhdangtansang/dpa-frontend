import React from "react";
import "./style.scss";
//Libs
import _, { findIndex } from 'lodash';
import ClassNames from 'classnames';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Input } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import uuidv1 from 'uuid';
//Components
import Checkbox from 'components/Checkbox';

export default class SingleSelectDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    let status = this.state.isOpen;
    this.setState({ isOpen: !status });
  };
  selectItem = (index) => {
    this.props.onSelect(index);
  };
  statusContent = (statusList) => {
    let checkedList = statusList.filter(status => status.checked === true);
    if (checkedList.length === 0 || checkedList.length === statusList.length) {
      return 'All';
    } else {
      return checkedList.length + ' item(s) selected';
    }
  };
  handleScrollBottom = () => {
    const { scrollbars } = this.refs;
    const isBottomScroll = scrollbars.getValues().top;
    //Scroll is in bottom
    if (isBottomScroll >= 1) {
      //start load next page in dropdown
      if (_.isBoolean(this.props.searchOption) && this.props.searchOption) {
        this.props.handleScrollBottom();
      }
    } else {
      //start load prev page in dropdown
    }
  };

  render() {
    return (
        <div className={'input-group form-input'}>
          {this.props.label && (<div className="form-label" htmlFor={this.props.name}>{this.props.label}</div>)}
          <Dropdown isOpen={this.state.isOpen} toggle={this.toggle} className="single-selection">
            <DropdownToggle className='input-form'>
              <span className={ClassNames('title', !_.isEmpty(this.props.chosenItemLabel) ? 'chosen' : '')}><span
                  className="prefix">{this.props.title}</span>{!_.isUndefined(this.props.chosenItemLabel) ? `${this.props.chosenItemLabel}` : ''}</span>
              <span className="fixle-caret icon-triangle-down"></span>
            </DropdownToggle>
            <DropdownMenu key={uuidv1()}>
              <Scrollbars
                  ref="scrollbars"
                  // This will activate auto hide
                  autoHide
                  // Hide delay in ms
                  autoHideTimeout={1000}
                  autoHeight
                  autoHeightMin={0}
                  autoHeightMax={456}
                  onScroll={() => {
                    this.handleScrollBottom();
                  }}
              >
                {_.isBoolean(this.props.searchOption) && this.props.searchOption && (
                    <DropdownItem key={uuidv1()} toggle={true}>
                      <div className="search-bar-dropdown">
                        <Input placeholder="Search"
                               onChange={(e) => {
                                 this.props.handleSearchChange(e.target.value);
                               }}
                        />
                        <span className="icon-search"></span>
                      </div>
                    </DropdownItem>
                )}


                {this.props.options.length > 0 ? this.props.options.map((option, index) => {
                  //list company return logo field
                  //list service return icon field
                  //so need process this
                  // let logo = option.logo || option.icon || undefined;
                  let image = option[this.props.imageField];
                  let name = option[this.props.nameField];
                  return (
                      <DropdownItem key={uuidv1()} toggle={true}
                                    onClick={(e) => {
                                      e.preventDefault();
                                      this.props.onSelect(option);
                                    }}>
                        <Checkbox
                            type={'radio'}
                            onChange={(e) => {
                              e.preventDefault();
                            }}
                            checked={index === findIndex(this.props.options, val => val._id === this.props.chosenItemID)}
                        />
                        {!_.isUndefined(image) &&
                        <img src={image.fileName}
                             onError={(e) => {
                               e.target.onerror = null;
                               e.target.src = './default-user.png';
                             }}
                        />}
                        <div className="label">{name}</div>
                      </DropdownItem>
                  );
                }) : <div className="text-center">Loading...</div>}
              </Scrollbars>
            </DropdownMenu>
          </Dropdown>
        </div>
    )
  }
}