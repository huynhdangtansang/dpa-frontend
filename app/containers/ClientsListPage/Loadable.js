/**
 *
 * Asynchronously loads the component for ClientsListPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
