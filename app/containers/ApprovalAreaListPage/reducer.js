/*
 *
 * ApprovalAreaListPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { GET_SERVICE_SUCCESS, UPDATE_DROPDOWN_SERVICE } from "containers/CompaniesListPage/constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  approvalList: [],
  dataList: [],
  serviceList: [],
  selectedCompanies: [],
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    serviceIds: [],
    sort: '',
    sortType: ''
  },
  currentPage: 1,
  filterService: {
    value: '',
    label: 'All'
  },
  searchDataForService: {},
});

function approvalAreaListPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case Constants.RESET_STATE:
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_APPROVAL_LIST:
      return state
          .set('approvalList', fromJS([]))
          .set('dataList', fromJS([]));
    case Constants.GET_SUCCESS:
      return state
          .set('approvalList', action.response.data)
          .set('dataList', action.response.data.data);
    case GET_SERVICE_SUCCESS:
      return state
          .set('serviceList', action.response.data);
    case Constants.SELECT_COMPANY:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedCompanies'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedCompanies', state.get('selectedCompanies').filter((company) => company.id !== action.selectItem.id))
      }
    case Constants.SELECT_ALL_COMPANY:
      if (action.isSelected === true) {
        let newArray = state.get('dataList').map((company) => {
          return {
            id: company._id,
            status: company.active
          }
        });
        return state
            .set('selectedCompanies', newArray)
      } else {
        return state
            .set('selectedCompanies', fromJS([]))
      }
    case Constants.UPDATE_SEARCH_STRING:
      return state
          .setIn(['searchData', 'search'], action.textSearch);
    case Constants.UPDATE_FILTER_LIST:
      return state
          .set('approvalList', fromJS([]))
          .set('dataList', fromJS([]))
          .set('searchData', fromJS(action.newSearchData))
          .set('currentPage', fromJS(action.newPage));
    case UPDATE_DROPDOWN_SERVICE:
      return state
          .set('filterService', action.newFilterService)
          .setIn(['searchData', 'serviceIds'], action.newFilterService.value ? [action.newFilterService.value] : []);
    case Constants.REMOVE_COMPANY:
    case Constants.APPROVE_COMPANY:
      return state
          .set('selectedCompanies', fromJS([]));
    default:
      return state;
  }
}

export default approvalAreaListPageReducer;
