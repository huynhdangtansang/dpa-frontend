import { fromJS } from 'immutable';
import teamEditMemberPageReducer from '../reducer';

describe('teamEditMemberPageReducer', () => {
  it('returns the initial state', () => {
    expect(teamEditMemberPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
