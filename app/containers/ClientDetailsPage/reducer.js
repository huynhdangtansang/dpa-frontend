/*
 *
 * ClientDetailsPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  errors: [],
  clientData: {},
  paymentDetail: [],
  jobAssigned: [],
  isSent: false,
  addressList: [],
  showResetPasswordConfirm: false
});

function clientDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.GET_CLIENT_ALL_DATA:
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_CLIENT_INFO_SUCCESS:
      return state
          .set('clientData', action.response.data)
          .set('addressList', action.response.data.address ? action.response.data.address : [])
          .set('errors', action.response.errors);
    case Constants.GET_CLIENT_INFO_ERROR:
      return state
          .set('clientData', action.response.data)
          .set('errors', action.response.errors);

      //PAYMENT
    case Constants.GET_PAYMENT_DETAIL:
      return state
          .set('paymentDetail', fromJS([]));
    case Constants.GET_PAYMENT_SUCCESS:
      return state
          .set('paymentDetail', fromJS(action.response.data));

      //JOB ASSIGNED
    case Constants.GET_JOB_ASSIGNED:
      return state
          .set('jobAssigned', fromJS([]));
    case Constants.GET_JOB_ASSIGNED_SUCCESS:
      return state
          .set('jobAssigned', fromJS(action.response.data.data));
    case Constants.CHANGE_IS_SUCCESS:
      return state
          .set('isSent', action.value);
    case Constants.SHOW_MODAL:
      if (action.modal === 'resetPassword') {
        return state
            .set('showResetPasswordConfirm', action.value)
      }
      return state;
    default:
      return state;
  }
}

export default clientDetailsPageReducer;
