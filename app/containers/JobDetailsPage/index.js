/**
 *
 * JobDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import _ from "lodash";
import moment from "moment";
import queryString from 'query-string';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectJobDetailsPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  addNewAct,
  cancelJob,
  changeAppointment,
  changeReassignObject,
  changeSpecificSelection,
  changeTab,
  completePay,
  getActs,
  getCompanies,
  getJob,
  reassignJob,
  reAssignSpecific,
  refund,
  rescheduleJob,
  resetState,
  showModal,
  submitEstimate,
  updateEstimate
} from "./actions";
import { TabContent, TabPane, UncontrolledTooltip } from 'reactstrap';
import "./style.scss";
//Tab view
import JobDetailsTabOverview from "components/JobDetailsTabOverview";
import JobDetailsTabAvtivities from "components/JobDetailsTabAvtivities";
import JobDetailsTabControl from "components/JobDetailsTabControl";
import { urlLink } from 'helper/route';
import { getEvent } from 'helper/socketConnection';

/* eslint-disable react/prefer-stateless-function */
export class JobDetailsPage extends React.PureComponent {
  reAssignJob = (id, reAssignType) => {
    if (reAssignType !== 'specificCompany') {
      let reAssignData = {
        jobId: id
      };
      this.props.reassign(reAssignType, reAssignData);
    }
  };
  addNewActivity = (date, actData) => {
    const { activitiesList } = this.props.jobdetailspage;
    let dateList = activitiesList;
    let [latestDate] = dateList;//get dateList[0]
    if (moment(latestDate && latestDate.day || null).isSame(moment(date))) {
      this.props.addNewAct('existed', date, actData);
    } else {
      this.props.addNewAct('new', date, actData);
    }
  };
  viewClient = () => {
    const { job } = this.props.jobdetailspage;
    this.props.history.push(urlLink.viewClient + `?id=${job.createdBy._id}`);
  };
  viewCompany = () => {
    const { job } = this.props.jobdetailspage;
    this.props.history.push(urlLink.viewCompany + `?id=${job.companyId._id}`);
  };
  viewProvider = () => {
    const { job } = this.props.jobdetailspage;
    this.props.history.push(urlLink.employee + `?id=${job.assignBy._id}`);
  };

  constructor(props) {
    super(props);
    this.state = {};
    getEvent('notification:updateJob', (response) => {
      let { pathname } = this.props.history.location;
      let { id } = queryString.parse(this.props.history.location.search);
      let jobId = response.dataUpdate._id; //socket JobId
      if (pathname === urlLink.viewJob && id === jobId) {
        this.props.getJob(jobId);
      }
    })
  }

  loadData() {
    let query = queryString.parse(this.props.history.location.search);
    let jobId = !_.isUndefined(query.id) ? query.id : '';
    //have tab param in url query
    if (!_.isUndefined(query.tab)) {
      this.props.changeTab(query.tab)
    }
    this.props.getJob(jobId).then(() => {
      this.props.getActs(jobId);
    });
  }

  componentWillMount() {
    this.loadData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.search !== this.props.location.search) {
      let query = queryString.parse(this.props.history.location.search);
      //have tab param in url query
      if (!_.isUndefined(query.tab)) {
        this.props.changeTab(query.tab)
      }
    }
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const {
      currentTab, job, objectType,
      showReassignModal, showCancelJobConfirm, showRescheduleModal, showReassignSpecificModal, showRefundModal,
      activitiesList, companiesList, employeesList,
      selectedCompany, selectedEmployee,
      selectedAppointment,
      paid
    } = this.props.jobdetailspage;
    return (
        <div className="job-details">
          <div className="header-edit-add-page job-details-header">
            <div className="action">
              <div className="return" id="return" onClick={(e) => {
                e.preventDefault();
                this.props.history.replace({ pathname: urlLink.jobs });
              }}>
                <span className="icon-arrow-left"></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
            </div>
            <div className="title job-type-id">
              <span>{(job && job.serviceId) ? job.serviceId.name : ''} #{job ? job.jobId : ''}</span>
            </div>
            <JobDetailsTabControl activeTab={currentTab.toString()}
                                  toggle={(tab) => {
                                    this.props.changeTab(tab);
                                    this.props.history.replace({ search: '?id=' + job._id + '&tab=' + tab });
                                  }}/>
          </div>
          <div className="content">
            <TabContent activeTab={currentTab}>
              <TabPane tabId="1">
                <JobDetailsTabAvtivities
                    id={job._id}
                    history={this.props.history}
                    list={activitiesList}
                    assignedObject={objectType}
                    companiesList={companiesList}
                    employeesList={employeesList}
                    appointment={selectedAppointment}
                    paid={paid}

                    //Modal
                    showReassignModal={showReassignModal}
                    showRescheduleModal={showRescheduleModal}
                    showCancelJobConfirm={showCancelJobConfirm}
                    showReassignSpecificModal={showReassignSpecificModal}
                    showRefundModal={showRefundModal}
                    showModal={(modal, value) => {
                      if (modal === 'reassignSpecific' && value === true) {
                        this.props.getCompanies(job.serviceInfo._id);
                      }
                      this.props.showModal(modal, value);
                    }}

                    //Serverside
                    handleChangeObjectType={object => {
                      this.props.changeReassignObject(object);
                    }}
                    specificSelect={(selector, value) => {
                      this.props.changeSpecificSelection(selector, value, job.appointmentTime, job.serviceInfo._id);
                    }}
                    addNew={(date, actData) => this.addNewActivity(date, actData)}
                    changeAppointment={value => {
                      this.props.changeAppointment(value)
                    }}

                    //Call API
                    reloadJob={() => {
                      this.props.getJob(job._id);
                    }}
                    reAssign={() => {
                      this.reAssignJob(job._id, objectType);
                    }}
                    reAssignSpecific={() => {
                      let data = {
                        companyId: selectedCompany,
                        providerId: selectedEmployee
                      };
                      this.props.reAssignSpecific(job._id, data);
                    }}
                    reschedule={reason => {
                      this.props.rescheduleJob(job._id, selectedAppointment, reason);
                    }}
                    cancelJob={reason => {
                      this.props.cancelJob(job._id, reason);
                    }}
                    submitEstimate={(data) => {
                      this.props.submitEstimate(job._id, data);
                    }}
                    updateEstimate={(data) => {
                      this.props.updateEstimate(job._id, data);
                    }}
                    refund={(amount, unit, reason) => {
                      this.props.refund(job._id, {
                        feedback: reason,
                        value: amount,
                        type: unit
                      });
                    }}
                    completeAndPay={() => {
                      this.props.completePay(job._id);
                    }}
                />
              </TabPane>
              <TabPane tabId="2">
                <JobDetailsTabOverview
                    job={job}
                    goToEditDescription={() => {
                      this.props.history.push(urlLink.editJob + '?id=' + job._id);
                    }}
                    history={this.props.history}
                    viewClient={this.viewClient}
                    viewCompany={this.viewCompany}
                    viewProvider={this.viewProvider}
                />
              </TabPane>
            </TabContent>
          </div>
        </div>
    )
  }
}

JobDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  resetState: PropTypes.func,
  changeTab: PropTypes.func,
  getJob: PropTypes.func,
  getActs: PropTypes.func,
  changeReassignObject: PropTypes.func,
  showModal: PropTypes.func,
  reassign: PropTypes.func,
  cancelJob: PropTypes.func,
  getCompanies: PropTypes.func,
  changeSpecificSelection: PropTypes.func,
  addNewAct: PropTypes.func,
  reAssignSpecific: PropTypes.func,
  changeAppointment: PropTypes.func,
  rescheduleJob: PropTypes.func,
  submitEstimate: PropTypes.func,
  updateEstimate: PropTypes.func,
  completePay: PropTypes.func,
  refund: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  jobdetailspage: makeSelectJobDetailsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(resetState());
    },
    changeTab: (tab) => {
      dispatch(changeTab(tab));
    },
    getJob: (id) => {
      return new Promise((resolve, reject) => {
        dispatch(getJob(id, resolve, reject));
      })
    },
    getActs: (jobId) => {
      dispatch(getActs(jobId));
    },
    changeReassignObject: (object) => {
      dispatch(changeReassignObject(object));
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    reassign: (reAssignType, reAssignData) => {
      dispatch(reassignJob(reAssignType, reAssignData));
    },
    reAssignSpecific: (jobId, assignData) => {
      dispatch(reAssignSpecific(jobId, assignData));
    },
    cancelJob: (jobId, reason) => {
      dispatch(cancelJob(jobId, reason));
    },
    getCompanies: (serviceId) => {
      dispatch(getCompanies(serviceId));
    },
    changeSpecificSelection: (selector, value, appointmentTime, serviceId) => {
      dispatch(changeSpecificSelection(selector, value, appointmentTime, serviceId));
    },
    addNewAct: (dateType, date, actData) => {
      dispatch(addNewAct(dateType, date, actData))
    },
    changeAppointment: (time) => {
      dispatch(changeAppointment(time));
    },
    rescheduleJob: (jobId, appointmentTime, reason) => {
      dispatch(rescheduleJob(jobId, appointmentTime, reason));
    },
    submitEstimate: (jobId, data) => {
      dispatch(submitEstimate(jobId, data));
    },
    updateEstimate: (jobId, data) => {
      dispatch(updateEstimate(jobId, data));
    },
    completePay: (jobId) => {
      dispatch(completePay(jobId));
    },
    refund: (jobId, refundData) => {
      dispatch(refund(jobId, refundData));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "jobDetailsPage", reducer });
const withSaga = injectSaga({ key: "jobDetailsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(JobDetailsPage);
