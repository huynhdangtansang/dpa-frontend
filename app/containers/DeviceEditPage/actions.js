/*
 *
 * DeviceEditPage actions
 *
 */
import {
  ADD_ADDRESS,
  CHANGE_AVATAR,
  DEFAULT_ACTION,
  EDIT_ADDRESS,
  GET_DEVICE_DETAILS,
  GET_ERROR,
  GET_SUCCESS,
  SHOW_MODAL,
  UPDATE_DEVICE_PROFILE,
  UPDATE_ERROR,
  UPDATE_SUCCESS
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getDeviceDetails(id) {
  return {
    type: GET_DEVICE_DETAILS,
    id: id
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function getError(response) {
  return {
    type: GET_ERROR,
    response: response
  }
}

export function updateDevice(id, profile) {
  return {
    type: UPDATE_DEVICE_PROFILE,
    id: id,
    profile: profile
  }
}

export function updateSuccess(response) {
  return {
    type: UPDATE_SUCCESS,
    response: response
  }
}

export function updateErr(response) {
  return {
    type: UPDATE_ERROR,
    response: response
  }
}

export function addAddress(addressList) {
  let newAddress = {
    id: '',
    name: '',
    isPrimary: false,
    action: 'add',
    error: true
  };
  if (addressList.length === 0) {
    newAddress.isPrimary = true;
  }
  return {
    type: ADD_ADDRESS,
    newAddress
  }
}

export function editAddress(id, newAddress) {
  return {
    type: EDIT_ADDRESS,
    id,
    newAddress
  }
}

export function changeAvatar(src) {
  return {
    type: CHANGE_AVATAR,
    src
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}