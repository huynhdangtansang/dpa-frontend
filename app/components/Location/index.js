import React from "react";
import _ from 'lodash';
import ClassNames from 'classnames';
import './style.scss';
import { listError } from "helper/data";
import 'components/InputForm/style.scss';
//Library
import { findIndex, indexOf } from "lodash";
export default class Location extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.setInit();
  }

  getLocality = (address) => {
    return address.types.filter((type) => type === 'locality');
  };
  getAdministrativeAreaLevel = (address) => {
    return address.types.filter((type) => type.includes('administrative_area_level'));
  };
  getCountryType = (address) => {
    return address.types.filter((type) => type === 'country');
  };
  getSuburbFromAdministrative = (list) => {
    let max = '';
    let suburb = {};
    list.map((address) => {
      if (address.types[0] > max) {
        max = address.types[0];
        suburb = address;
      }
    });
    return suburb;
  };
  getSuburb = (addressList) => {
    if (_.isArray(addressList)) {
      let locality = addressList.filter((address) => this.getLocality(address).length > 0);
      let administrativeAreas = addressList.filter((address) => this.getAdministrativeAreaLevel(address).length > 0);
      let suburb = this.getSuburbFromAdministrative(administrativeAreas);
      if (locality.length > 0) {
        return locality[0].long_name;
      } else {
        return suburb.long_name;
      }
    }
  };
  getCountry = (addressList) => {
    if (_.isArray(addressList)) {
      let country = addressList.filter((address) => this.getCountryType(address).length > 0);
      return country[0].long_name;
    }
  };
  setInit = () => {
    let id = this.props.id ? this.props.id : 'pac-input';
    let inputNode = document.getElementById(id);
    let { country: countryProps = '' } = this.props
    let options = {};
    if (!_.isEmpty(countryProps)) {
      options = {
        componentRestrictions: { country: countryProps }//config country is au
      };
    }
    let autoComplete = new window.google.maps.places.Autocomplete(inputNode, options);
    autoComplete.addListener('place_changed', () => {
      let place = autoComplete.getPlace();
      let location = place.geometry.location;
      let data = {
        name: place.formatted_address,
        geoInfo: {
          lat: location.lat(),
          lng: location.lng()
        },
        suburb: this.getSuburb(place.address_components),
        city: place.vicinity,
        country: this.getCountry(place.address_components)
      };
      this.props.onSelect(data);
    })
  };

  checkApiError(name, error) {
    if (!_.isEmpty(error)) {
      let index = findIndex(listError, val => val.name === name);
      if (index > -1) {
        if (indexOf(listError[index].error, error[0].errorCode) > -1) {
          return true;
        }
      }
      return false;
    }
  }

  render() {
    return (
      <div className="location form-input">
        <div className="form-label">
          <span>{this.props.title}</span>
        </div>
        <input
          className={ClassNames('input-form',
            ((this.props.touched && this.props.error) || this.checkApiError(this.props.name, this.props.apiError)) && 'error-form')}
          id={this.props.id ? this.props.id : 'pac-input'}
          defaultValue={this.props.value}
          placeholder={_.isEmpty(this.props.placeholder) ? 'Location' : this.props.placeholder}
          type={'text'}
          onChange={(e) => {
            this.props.onChange(e.target.value);
          }}
        />
      </div>
    )
  }
}