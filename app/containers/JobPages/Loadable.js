/**
 *
 * Asynchronously loads the component for JobPages
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
