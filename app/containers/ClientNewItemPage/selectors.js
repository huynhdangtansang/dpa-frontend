import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the clientNewItemPage state domain
 */

const selectClientNewItemPageDomain = state =>
    state.get("clientNewItemPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ClientNewItemPage
 */

const makeSelectClientNewItemPage = () =>
    createSelector(selectClientNewItemPageDomain, substate => substate.toJS());
const makeSelectErrors = () =>
    createSelector(selectClientNewItemPageDomain, state => state.get('errors'));
export { makeSelectClientNewItemPage, makeSelectErrors };
export { selectClientNewItemPageDomain };
