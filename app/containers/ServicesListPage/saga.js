import { put, takeLatest } from 'redux-saga/effects';
import { DELETE_SERVICE, GET_PDF, GET_SERVICES_LIST, UPDATE_COMMISSION, UPDATE_SEARCH_DATA } from './constants';
import { deleteError, deleteSuccess, getServicesList, getSuccess, selectAllService } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetServicesList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => k + '=' + esc(params[k]))
      .join('&');
    const requestUrl = config.serverUrl + config.api.service + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteService(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/deletes';
    yield put(loadRepos());
    try {
      const response = yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(deleteSuccess(response.data));
      let searchData = {
        offset: 0,
        limit: 10
      };
      yield put(selectAllService(false));
      yield put(getServicesList(searchData));
      yield put(reposLoaded());
    } catch (error) {
      yield put(deleteError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateCommission(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/changeCommissionWithMultiServices';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.updateData);
      let searchData = {
        offset: 0,
        limit: 10
      };
      yield put(selectAllService(false));
      yield put(getServicesList(searchData));
      yield put(reposLoaded());
    } catch (error) {
      yield put(selectAllService(false));
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* getListAgain(data) {
  if (data) {
    yield put(getServicesList(data.newSearchData));
  }
}

export function* apiGetPdf(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/generatePdfServiceMany';
    yield put(loadRepos());
    try {
      const response = yield axios.post(requestUrl, { ids: data.serviceId });
      yield put(reposLoaded());
      let array = response.data.data;
      array.forEach(function (file) {
        window.open(file.path);
      });
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_SERVICES_LIST, apiGetServicesList);
  yield takeLatest(DELETE_SERVICE, apiDeleteService);
  yield takeLatest(UPDATE_SEARCH_DATA, getListAgain);
  yield takeLatest(UPDATE_COMMISSION, apiUpdateCommission);
  yield takeLatest(GET_PDF, apiGetPdf);
}
