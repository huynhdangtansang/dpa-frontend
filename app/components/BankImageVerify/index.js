/**
 *
 * BankImageVerify
 *
 */

import React from "react";
import './style.scss';
import ClassNames from 'classnames';

const imageValidateMessage = {
  required: 'Missing verify image',
  wrongFormat: 'Invalid file (jpeg, png or gif format required)',
  exceedSize: 'Invalid file (maximum 2MB size)'
};

export default class BankImageVerify extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: ''
    };
  }

  // Check validate in case user already input other fields
  checkValidate = (values) => {
    let checkNullValue = Object.keys(values).filter((fieldName) => {
      return values[fieldName] === '' || values[fieldName] === null
    });

    if (checkNullValue.length === 1) {
      this.setState({
        error: checkNullValue[0] === 'imageVerify', // Only have avatar field left
        message: imageValidateMessage.required
      })
    }
  };

  handleImageChange = (e) => {
    e.preventDefault();

    return new Promise((resolve) => {

      let reader = new FileReader();
      let file = e.target.files[0];
      let type = file.name.split('.');
      let typeLen = type.length;
      // Check type
      if (type[typeLen - 1] === 'jpeg' || type[typeLen - 1] === 'jpg' || type[typeLen - 1] === 'png') {

        // Check size
        if (file.size > 2 * 1024 * 1024) {//2MB
          // This allow clear file input value, allow adding the same file in the next time
          e.target.value = null;

          resolve({ error: true, errorType: 'exceedSize' })
        } else {
          reader.onloadend = () => {

            // This allow clear file input value, allow adding the same file in the next time
            resolve({ error: false, data: { fileUrl: reader.result, file: file, rawData: e } });

            // This allow clear file input value, allow adding the same file in the next time
            e.target.value = null;
          };
        }


        reader.readAsDataURL(file);
      } else {
        // This allow clear file input value, allow adding the same file in the next time
        e.target.value = null;

        resolve({ error: true, errorType: 'wrongFormat' })
      }
    });
  };

  changeImageValue = (data) => {
    if (data.error) {
      data.message = imageValidateMessage[data.errorType];
      if (this.props.inlineErrorMessge !== false) {
        this.setState({ error: true, message: data.message })
      }
    } else {
      //this.setState({error: false, message: ''});
    }

    this.props.onChange(data)
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    // If current error is shown
    if (this.state.error) {
      // User choose the correct image
      if ((nextProps.src !== '' && nextProps.src !== null)) {
        // Hide message
        this.setState({ error: false, message: '' });
      }
    } else { // No error related to photo, check missing image
      // If this mode is enable
      if (this.props.validateAfterOtherInputFields === true) {
        // Validate image if user have on all fields
        this.checkValidate(nextProps.relatedValues)
      }
    }
  }


  render() {
    const { error, message } = this.state;
    const {
      className: classNameProp = '',
      subscription: subscription = '',
      src: srcProp = '',
      name: name = 'image_verfiy',
      onClick,
      btnText: btnText = '',
      alt: alt = '',
      label: label = 'Verify image',
      isVerify: isVerify = false,
      isEdit: isEdit = false,
      status: status = 'unverified'
    } = this.props;

    return (
      <div className={ClassNames('file-wrapper-verify', classNameProp)}>
        <div className="label">{label}</div>
        {!isVerify ?
          (isEdit ?
            <div className="edit">
              <div className="upload-wrapper">
                <div className={ClassNames('image-frame', !srcProp && 'visible-overflow')}>
                  {srcProp ?
                    <img className={'image'} src={srcProp} alt={alt}
                      onError={(e) => {
                        e.target.src = 'default-user.png'
                      }} />
                    :
                    <i className={'icon-violet-camera icon-style'} />
                  }
                </div>
                <input className={ClassNames('input-file', srcProp && 'left-style')}
                  type="file"
                  name={name}
                  onClick={onClick}
                  onChange={(e) => {
                    e.persist(); // Make this event can be reused
                    this.handleImageChange(e).then((data) => {
                      this.changeImageValue(data);
                    })
                  }}
                />
                <div className={'btn-content'}>{btnText}</div>
              </div>
              <div className="subscription">{subscription}</div>
            </div>
            ://display real status like pending, unverified, .....
            <div className="view-status">
              <div className="unverified">
                <span className="icon-error"></span>
                <span className="status">{status}</span>
              </div>
            </div>
          )
          :
          //verified
          (<div className="view-status">
            <div className="verified">
              <span className="icon-checked"></span>
              <span className="status">Verified</span>
            </div>
          </div>)
        }

        {error && <div className={'error-text'}>
          <i className={'icon-red-warning'} />
          <span className={'error-content'}>{message}</span>
        </div>}
      </div>
    );
  }
}
