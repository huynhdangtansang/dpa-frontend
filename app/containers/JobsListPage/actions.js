/*
 *
 * JobsListPage actions
 *
 */
import {
  CHANGE_COMPANY,
  CHANGE_PAGE,
  CHANGE_STATUS_FILTER,
  CHANGE_STORE_VALUE,
  GET_COMPANIES,
  GET_COMPANIES_SUCCESS,
  GET_ERROR,
  GET_LIST,
  GET_SUCCESS,
  REMOVE_JOB,
  REMOVE_SUCCESS,
  RESET_STATE,
  SELECT_ALL_JOB,
  SELECT_JOB,
  SHOW_MODAL,
  UPDATE_SEARCH_COMPANY_DATA,
  UPDATE_SEARCH_DATA
} from "./constants";

export function resetState() {
  return {
    type: RESET_STATE
  };
}

//API
export function getList(searchData, loading = true) {
  return {
    type: GET_LIST,
    searchData,
    loading
  };
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: GET_ERROR,
    response
  }
}

export function getCompanies(searchData) {
  return {
    type: GET_COMPANIES,
    searchData
  }
}

export function getCompaniesSuccess(response) {
  return {
    type: GET_COMPANIES_SUCCESS,
    response
  }
}

//Serverside
export function updateSearchData(newSearchData) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData
  }
}

export function updateSearchCompanyData(newSearchData) {
  return {
    type: UPDATE_SEARCH_COMPANY_DATA,
    newSearchData
  }
}

export function changePage(newPage) {
  return {
    type: CHANGE_PAGE,
    newPage
  }
}

export function selectJob(isSelected, selectItem) {
  return {
    type: SELECT_JOB,
    isSelected,
    selectItem
  }
}

export function selectAllJob(isSelected) {
  return {
    type: SELECT_ALL_JOB,
    isSelected
  }
}

export function changeStatusFilter(key, id) {
  return {
    type: CHANGE_STATUS_FILTER,
    key,
    id
  }
}

export function changeCompany(companyName) {
  return {
    type: CHANGE_COMPANY,
    companyName
  }
}

export function removeJob(jobIds, searchData) {
  return {
    type: REMOVE_JOB,
    jobIds,
    searchData
  }
}

export function removeSuccess(resposne) {
  return {
    type: REMOVE_SUCCESS
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}

export function changeStoreValue(event) {
  return {
    type: CHANGE_STORE_VALUE,
    event
  };
}