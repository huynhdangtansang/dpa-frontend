/*
 *
 * StatisticsPage constants
 *
 */
export const DEFAULT_ACTION = "app/StatisticsPage/DEFAULT_ACTION";
export const RESET_STORE = "app/StatisticsPage/RESET_STORE";
export const RESET_STATE = "app/StatisticsPage/RESET_STATE";
export const GET_ALL_DATA = "app/StatisticsPage/GET_ALL_DATA";
export const GET_ALL_SUCCESS = "app/StatisticsPage/GET_ALL_SUCCESS";
export const GET_CHART_BY_TYPE = "app/StatisticsPage/GET_CHART_BY_TYPE";
//Dropdown
export const GET_DROPDOWN = "app/StatisticsPage/GET_DROPDOWN";
export const GET_DROPDOWN_SUCCESS = "app/StatisticsPage/GET_DROPDOWN_SUCCESS";
export const UPDATE_FILTER_CHART = "app/StatisticsPage/UPDATE_FILTER_CHART";
//Service
export const GET_SERVICE_DATA = "app/StatisticsPage/GET_SERVICE_DATA";
export const GET_SERVICE_SUCCESS = "app/StatisticsPage/GET_SERVICE_SUCCESS";
//Company
export const GET_COMPANY_DATA = "app/StatisticsPage/GET_COMPANY_DATA";
export const GET_COMPANY_SUCCESS = "app/StatisticsPage/GET_COMPANY_SUCCESS";
//Suburb
export const GET_SUBURB_DATA = "app/StatisticsPage/GET_SUBURB_DATA";
export const GET_SUBURB_SUCCESS = "app/StatisticsPage/GET_SUBURB_SUCCESS";
