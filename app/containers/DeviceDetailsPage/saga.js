/* eslint-disable no-console */
import axios from 'axios';
import config from 'config';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import { goBack } from 'react-router-redux';
import { all, put, takeLatest } from 'redux-saga/effects';
import _ from "lodash";
import * as Actions from './actions';
import * as Constants from './constants';

export function* apiGetDeviceAllData(data) {
  if (data && data.id) {
    yield put(loadRepos());
    yield all([
      apiGetInfoDevice(data),
      apiGetJobAssigned(data),
      apiGetPaymentDetails(data),
    ]);
    yield put(reposLoaded());
  }
}

export function* apiGetInfoDevice(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/' + data.id;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(response.data));
    } catch (error) {
      yield put(Actions.getError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetJobAssigned(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/' + data.id + '/getJobsAssigned';
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getJobAssignedSuccess(response.data));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetPaymentDetails(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.payment + '/' + data.id + '/getPaymentsByUserId';
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getPaymentSuccess(response.data));
    } catch (error) {
      console.log(error);
    }
  }
}

export function* apiDeactiveDevice(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/deactives';
    try {
      yield axios.put(requestUrl, data.updateData);
      yield put(Actions.getDeviceAllData(data.id));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiDeleteDevice(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/deletes';
    try {
      const response = yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(Actions.deleteSuccess(response.data));
      yield put(goBack());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiResetPassword(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/requestResetPasswords';
    try {
      yield axios.put(requestUrl, data.resetData);
      yield put(Actions.changeIsSuccess(true));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_DEVICE_ALL_DATA, apiGetDeviceAllData);
  yield takeLatest(Constants.GET_DEVICE_INFO, apiGetInfoDevice);
  //Payment
  yield takeLatest(Constants.GET_PAYMENT_DETAIL, apiGetPaymentDetails);
  //Job assigned
  yield takeLatest(Constants.GET_JOB_ASSIGNED, apiGetJobAssigned);
  yield takeLatest(Constants.CHANGE_DEVICE_STATUS, apiDeactiveDevice);
  yield takeLatest(Constants.DELETE_DEVICE, apiDeleteDevice);
  yield takeLatest(Constants.RESET_PASSWORD, apiResetPassword);
}
