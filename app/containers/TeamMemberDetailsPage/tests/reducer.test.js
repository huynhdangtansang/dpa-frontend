import { fromJS } from 'immutable';
import teamMemberDetailsPageReducer from '../reducer';

describe('teamMemberDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(teamMemberDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
