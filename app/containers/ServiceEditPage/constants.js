/*
 *
 * ServiceEditPage constants
 *
 */
export const RESET_STATE = "ServiceEditPage/RESET_STATE";
export const GET_SERVICE_DETAILS = "ServiceEditPage/GET_SERVICE_DETAILS";
export const GET_SUCCESS = "ServiceEditPage/GET_SUCCESS";
export const UPDATE_SERVICE = "ServiceEditPage/UPDATE_SERVICE";
export const ADD_COUPON = "ServiceEditPage/ADD_COUPON";
export const UPDATE_COUPON = "ServiceEditPage/UPDATE_COUPON";
export const ADD_CERT = "ServiceEditPage/ADD_CERT";
export const UPDATE_CERT = "ServiceEditPage/UPDATE_CERT";
export const ADD_AREA = "ServiceEditPage/ADD_AREA";
export const UPDATE_AREA = "ServiceEditPage/UDPATE_AREA";
export const CHANGE_IMAGE = "ServiceEditPage/CHANGE_IMAGE";
export const ADD_TAG = "ServiceEditPage/ADD_TAG";
export const DELETE_TAG = "ServiceEditPage/DELETE_TAG";
export const SHOW_MODAL = "ServiceEditPage/SHOW_MODAL";