/**
 *
 * SetupPasswordPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectErrors, makeSelectResetPasswordPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import FormGroup from "components/FormGroup";
import { Formik } from "formik";
import InputForm from "components/InputForm";
import * as Yup from "yup";
import PageInfo from "components/PageInfo";
import SubmitButton from "components/SubmitButton";
import './style.scss';
import { resetPassword } from './actions';
//lib
import _ from "lodash";
/* eslint-disable react/prefer-stateless-function */
const validateForm = Yup.object().shape({
  'password': Yup.string()
      .min(8, 'Invalid password (At least 8 and no special characters)')
      .required('')
      .matches(/(^[a-zA-Z0-9]{4,10}$)/, 'Invalid password (No special characters)'),
  'passwordConfirm': Yup.string()
      .min(8, 'Invalid password confirm (At least 8 and no special characters)')
      .oneOf([Yup.ref('password'), null], "New Password and Confirm Password is not the same")
      .required('')
      .matches(/(^[a-zA-Z0-9]{4,10}$)/, 'Invalid password (No special characters)'),
});

export class SetupPasswordPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false,
      showConfirmPassword: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'passwordConfirm',
      'password',
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    return (
        <FormGroup>
          <PageInfo title={'Set Up Password'}/>
          <Formik
              ref={ref => (this.formik = ref)}
              initialValues={{ password: '', passwordConfirm: '' }}
              enableReinitialize={true}
              validationSchema={validateForm}
              onSubmit={evt => {
                this.props.onSubmit(evt);
              }}
          >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                /* and other goodies */
              }) => (
                <div>
                  <form onSubmit={handleSubmit}>
                    <InputForm label={'password'}
                               name={'password'}
                               value={values.password}
                               error={errors.password}
                               touched={touched.password}
                               onChange={evt => {
                                 handleChange(evt);
                               }}
                               onBlur={handleBlur}
                               type={!this.state.showPassword ? 'password' : 'text'}
                               placeholder={'At least 8 and no special characters'}
                               iconClassShow={'icon-invisible'}
                               iconClassHide={'icon-visible'}
                               togglePassword={() => {
                                 this.setState({
                                   showPassword: !this.state.showPassword
                                 });
                               }}
                               showPassword={this.state.showPassword}/>

                    <InputForm label={'confirm password'}
                               name={'passwordConfirm'}
                               value={values.passwordConfirm}
                               error={errors.passwordConfirm}
                               touched={touched.passwordConfirm}
                               onChange={evt => {
                                 handleChange(evt);
                               }}
                               onBlur={handleBlur}
                               type={!this.state.showConfirmPassword ? 'password' : 'text'}
                               placeholder={'At least 8 and no special characters'}
                               iconClassShow={'icon-invisible'}
                               iconClassHide={'icon-visible'}
                               togglePassword={() => {
                                 this.setState({
                                   showConfirmPassword: !this.state.showConfirmPassword
                                 });
                               }}
                               showPassword={this.state.showConfirmPassword}/>

                    <SubmitButton type={"submit"}
                                  disabled={this._disableButton(values, errors)}
                                  content={'submit'}/>
                    {(this.props.errors && this.props.errors.length > 0) ? this.props.errors.map((error) => {
                      return (
                          <div key={error.errorCode} className="errors">
                            <span className="icon-error"></span>
                            <div className="error-item">
                              <span>{error.errorMessage}</span>
                            </div>
                          </div>
                      );
                    }) : []}
                  </form>
                </div>
            )}
          </Formik>
        </FormGroup>
    );
  }
}

SetupPasswordPage.propTypes = {
  dispatch: PropTypes.func,
  onSubmit: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  SetupPasswordPage: makeSelectResetPasswordPage(),
  errors: makeSelectErrors()
});

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: evt => {
      if (!_.isUndefined(evt) && !_.isUndefined(evt.password)) {
        const urlString = window.location.href;
        let url = urlString.split('?token=');
        let token = url[1];
        dispatch(resetPassword(token, evt.password));
      }
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "SetupPasswordPage", reducer });
const withSaga = injectSaga({ key: "SetupPasswordPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(SetupPasswordPage);
