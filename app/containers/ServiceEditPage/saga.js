import { put, takeLatest } from 'redux-saga/effects';
import { GET_SERVICE_DETAILS, UPDATE_SERVICE } from './constants';
import { getServiceDetails, getSuccess } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import { goBack } from 'react-router-redux';
import _ from "lodash";

export function* apiGetService(data) {
  const { id, resolve, reject } = data.param;
  if (id) {
    const requestUrl = config.serverUrl + config.api.service + '/' + id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      resolve(response);
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      reject(error);
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateService(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service + '/' + data.id;
    try {
      yield axios.put(requestUrl, data.updateData);
      yield put(getServiceDetails(data.id));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_SERVICE_DETAILS, apiGetService);
  yield takeLatest(UPDATE_SERVICE, apiUpdateService);
}
