/*
 *
 * ClientDetailsPage constants
 *
 */
export const GET_CLIENT_ALL_DATA = "/ClientDetailsPage/GET_CLIENT_ALL_DATA";//get all data
export const GET_CLIENT_INFO = "/ClientDetailsPage/GET_CLIENT_INFO";
export const GET_CLIENT_INFO_SUCCESS = "/ClientDetailsPage/GET_CLIENT_INFO_SUCCESS";
export const GET_CLIENT_INFO_ERROR = "/ClientDetailsPage/GET_CLIENT_INFO_ERROR";
export const CHANGE_CLIENT_STATUS = "/ClientDetailsPage/CHANGE_CLIENT_STATUS";
export const DEACTIVE_CLIENT = "/ClientDetailsPage/DEACTIVE_CLIENT";
export const DELETE_CLIENT = "/ClientDetailsPage/DELETE_CLIENT";
export const DELETE_SUCCESS = "/ClientDetailsPage/DELETE_SUCCESS";
export const DELETE_ERROR = "/ClientDetailsPage/DELETE_CLIENT";
export const RESET_PASSWORD = "/ClientDetailsPage/RESET_PASSWORD";
export const CHANGE_IS_SUCCESS = "/ClientDetailsPage/CHANGE_IS_SUCCESS";
export const SHOW_MODAL = "ClientDetailsPage/SHOW_MODAL";
//Payment
export const GET_PAYMENT_DETAIL = "ClientDetailsPage/GET_PAYMENT_DETAIL";
export const GET_PAYMENT_SUCCESS = "ClientDetailsPage/GET_PAYMENT_SUCCESS";
export const GET_PAYMENT_ERROR = "ClientDetailsPage/GET_PAYMENT_ERROR";
//Job assigned
export const GET_JOB_ASSIGNED = "/ClientDetailsPage/GET_JOB_ASSIGNED";
export const GET_JOB_ASSIGNED_SUCCESS = "/ClientDetailsPage/GET_JOB_ASSIGNED_SUCCESS";
export const GET_JOB_ASSIGNED_ERROR = "/ClientDetailsPage/GET_JOB_ASSIGNED_ERROR";