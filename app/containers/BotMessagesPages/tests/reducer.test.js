import { fromJS } from 'immutable';
import botMessagesPagesReducer from '../reducer';

describe('botMessagesPagesReducer', () => {
  it('returns the initial state', () => {
    expect(botMessagesPagesReducer(undefined, {})).toEqual(fromJS({}));
  });
});
