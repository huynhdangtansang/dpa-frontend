/*
 *
 * EditCompanyPage constants
 *
 */
export const DEFAULT_ACTION = "app/EditCompanyPage/DEFAULT_ACTION";
export const RESET_ERROR = "/EditCompanyPage/RESET_ERROR";
export const GET_ALL_DATA = "/EditCompanyPage/GET_ALL_DATA";
export const GET_ALL_DATA_SUCCESS = "/EditCompanyPage/GET_ALL_DATA_SUCCESS";
export const GET_COMPANY_DATA = "/EditCompanyPage/GET_COMPANY_DATA";
export const GET_SUCCESS = "/EditCompanyPage/GET_SUCCESS";
export const UPDATE_COMPANY = "/EditCompanyPage/UPDATE_COMPANY";
export const UPDATE_SUCCESS = "/EditCompanyPage/UPDATE_SUCCESS";
export const SAVE_DATA_UPDATE_COMPANY = "/EditCompanyPage/SAVE_DATA_UPDATE_COMPANY";
export const CHANGE_LOGO = "/EditCompanyPage/CHANGE_LOGO";
//Service
export const GET_SERVICE_DATA = "/EditCompanyPage/GET_SERVICE_DATA";
export const GET_SERVICE_SUCCESS = "/EditCompanyPage/GET_SERVICE_SUCCESS";
export const PROCESS_MY_SERVICE_DATA = "/EditCompanyPage/PROCESS_MY_SERVICE_DATA";
export const UPDATE_COMPANY_LIST_SERVICE = "/EditCompanyPage/UPDATE_COMPANY_DATA";
export const SELECT_SERVICE = "/EditCompanyPage/SELECT_SERVICE";
export const UPDATE_SERVICE_IN_STORE = "/EditCompanyPage/UPDATE_SERVICE_IN_STORE";
export const UPDATE_SEARCH_DATA_SERVICE = "/EditCompanyPage/UPDATE_SEARCH_DATA_SERVICE";
//Lisence
export const SELECT_LICENSE = "/EditCompanyPage/SELECT_LICENSE";
export const UPDATE_LIST_LICENSE = "/EditCompanyPage/UPDATE_LIST_LICENSE";
export const UPDATE_ITEMPROOF = "/EditCompanyPage/UPDATE_ITEMPROOF";
export const ADD_LICENSE_IMAGE = "/EditCompanyPage/ADD_LICENSE_IMAGE";
export const DELETE_LICENSE_IMAGE = "/EditCompanyPage/DELETE_LICENSE_IMAGE";
export const SAVE_LICENSE_IMAGE = "/EditCompanyPage/SAVE_LICENSE_IMAGE";
export const REMOVE_LICENSE_IMAGE = "/EditCompanyPage/REMOVE_LICENSE_IMAGE";
export const CLEAR_LICENSE_IMAGE = "/EditCompanyPage/CLEAR_LICENSE_IMAGE";
export const UPDATE_DATE_EXPIRED_TEMP = "/EditCompanyPage/UPDATE_DATE_EXPIRED_TEMP";
export const TOOGLE_TOP_PRIORITY = "/EditCompanyPage/TOOGLE_TOP_PRIORITY";
//Accept rate card
export const TOOGLE_ACCEPTANCE = "/EditCompanyPage/TOOGLE_ACCEPTANCE";
export const UPDATE_LIST_ACCEPT_RATE_CARD = "/EditCompanyPage/UPDATE_LIST_ACCEPT_RATE_CARD";
//AREA AND HOURS
export const CHANGE_AREA_AND_HOURS = "/EditCompanyPage/CHANGE_AREA_AND_HOURS";
