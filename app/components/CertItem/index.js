import React from "react";
import { Formik } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import InputForm from "components/InputForm";
import "./style.scss";

const validateForm = Yup.object().shape({
  'content': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid certification name (no special characters)')
      .min(8, 'Invalid certification name (At least 8 characters)')
      .max(30, 'Invalid certification name (Maximum at 30 characters)'),
});
export default class CertItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  regexData = (value) => {
    if (8 <= value.length && value.length <= 30) {
      let regexSpecialCharacter = /^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/;
      return !regexSpecialCharacter.test(value);
    } else {
      return true;
    }
  };

  render() {
    return (
        <Formik
            initialValues={{
              content: (_.isObject(this.props.cert) && this.props.cert.content) ? this.props.cert.content : '',
            }}
            enableReinitialize={false}
            validationSchema={validateForm}>
          {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              setFieldValue
            }) => (
              <div className="certification-item" hidden={this.props.hidden ? this.props.hidden : false}>
                <div className="details">
                  <InputForm
                      label="certification name"
                      name={'content'}
                      type={'text'}
                      value={values.content}
                      error={errors.content}
                      touched={touched.content}
                      onChange={evt => {
                        handleChange(evt);
                        setFieldValue('content', evt.target.value);
                        this.props.onChange(evt.target.value, this.regexData(evt.target.value));
                      }}
                      onBlur={handleBlur}
                      placeholder={'Certification name'}/>
                </div>
                <div className="delete">
                  <div className="icon-contain">
                  <span className="icon-bin" onClick={() => {
                    this.props.delete();
                  }}></span>
                  </div>
                </div>
              </div>
          )}
        </Formik>
    )
  }
}