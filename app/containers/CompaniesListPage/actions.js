/*
 *
 * CompaniesListPage actions
 *
 */
import {
  CHANGE_COMPANY_STATUS,
  CLEAR_ALL_SELECTED_COMPANY,
  DEFAULT_ACTION,
  DELETE_COMPANY,
  GET_COMPANIES_LIST,
  GET_PERIOD,
  GET_PERIOD_SUCCESS,
  GET_SERVICE_DATA,
  GET_SERVICE_SUCCESS,
  GET_SUCCESS,
  RESET_STATE,
  SELECT_ALL_COMPANY,
  SELECT_COMPANY,
  SET_ACTIVE_COMPANY,
  SET_REMOVE_COMPANY,
  SET_TIMER_COUNT,
  UPDATE_DROPDOWN_PERIOD,
  UPDATE_DROPDOWN_SERVICE,
  UPDATE_FILTER_LIST,
  UPDATE_SEARCH_DATA,
  UPDATE_SEARCH_STRING
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setDefault() {
  return {
    type: DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: RESET_STATE
  };
}

export function getCompaniesListData(searchData) {
  return {
    type: GET_COMPANIES_LIST,
    searchData: searchData
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function getServiceListData(searchData) {
  return {
    type: GET_SERVICE_DATA,
    searchData: searchData
  }
}

export function getServiceSuccess(response) {
  return {
    type: GET_SERVICE_SUCCESS,
    response: response
  }
}

export function getPeriod() {
  return {
    type: GET_PERIOD
  }
}

export function getPeriodSuccess(response) {
  return {
    type: GET_PERIOD_SUCCESS,
    response
  }
}

export function selectCompany(isSelected, selectItem) {
  return {
    type: SELECT_COMPANY,
    isSelected,
    selectItem
  }
}

export function selectAllCompany(isSelected) {
  return {
    type: SELECT_ALL_COMPANY,
    isSelected
  }
}

export function setActiveCompany(companyID, reason, isActive) {
  return {
    type: SET_ACTIVE_COMPANY,
    companyID: companyID,
    reason: reason,
    isActive: isActive
  }
}

export function setRemoveCompany(companyID, reason) {
  return {
    type: SET_REMOVE_COMPANY,
    companyID: companyID,
    reason: reason
  }
}

export function deleteCompany(deleteData, searchData) {
  return {
    type: DELETE_COMPANY,
    deleteData,
    searchData
  }
}

export function changeCompanyStatus(statusData, searchData) {
  return {
    type: CHANGE_COMPANY_STATUS,
    statusData,
    searchData
  }
}

export function clearAllSelectedCompany() {
  return {
    type: CLEAR_ALL_SELECTED_COMPANY
  }
}

export function updateSearchString(textSearch) {
  return {
    type: UPDATE_SEARCH_STRING,
    textSearch
  }
}

export function updateFilter(newSearchData, newPage) {
  return {
    type: UPDATE_FILTER_LIST,
    newSearchData,
    newPage
  }
}

export function updateDropdownService(newFilterService) {
  return {
    type: UPDATE_DROPDOWN_SERVICE,
    newFilterService
  }
}

export function updateDropdownPeriod(newFilterPeriod) {
  return {
    type: UPDATE_DROPDOWN_PERIOD,
    newFilterPeriod
  }
}

export function setTimerCount(value) {
  return {
    type: SET_TIMER_COUNT,
    value
  }
}

export function updateSearchData(newSearchData) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData
  }
}