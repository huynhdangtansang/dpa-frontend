import Datepicker from 'components/Datepicker';
import GhostButton from 'components/GhostButton';
import TimePicker from 'components/NewTimePicker';
import PurpleRoundButton from 'components/PurpleRoundButton';
import TextareaCounter from 'components/TextareaCounter';
import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import "./style.scss";

export default class RescheduleModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reason: ''
    };
  }

  render() {
    const { appointment } = this.props;
    return (
        <Modal isOpen={this.props.show} className="reschedule-modal">
          <ModalBody>
            <div className="upper">
              <div className="title">
                <span>Reschedule This Job</span>
              </div>
              <div className="description">
                <span>Reschedule Job will cancel the current booking and create a new job id to be sent for acceptance by top companies</span>
              </div>
            </div>
            <div className="middle">
              <div className="appointment">
                <div className="label">
                  <span>Appointment Time</span>
                </div>
                <TimePicker
                    name={'appoinmentTime'}
                    hoursFromNow={3}
                    time={appointment}
                    onChange={time => {
                      this.props.changeAppointment(time);
                    }}
                />

                <Datepicker
                    name={'appoinmentTime'}
                    hoursFromNow={3}
                    minDate={new Date()}
                    selected={appointment}
                    onChange={date => {
                      this.props.changeAppointment(date);
                    }}
                />
              </div>
              <div className="note">
                <div className="label">
                  <span>Note</span>
                </div>
                <div className="modify-item">
                  <TextareaCounter
                      placeholder={'Describe the reason why reschedule this job'}
                      rows={8}
                      maxLength={50}
                      value={this.state.reason}
                      onChange={(e) => {
                        e.preventDefault();
                        this.setState({ reason: e.target.value });
                      }}/>
                </div>
              </div>
            </div>
            <div className="lower">
              <GhostButton className="btn-ghost" title={'Cancel'} onClick={this.props.closeModal}/>
              <PurpleRoundButton className="btn-purple-round" title={'Reschedule'} onClick={() => {
                this.props.reschedule(this.state.reason);
              }}
                                 disabled={this.state.reason === ''}/>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}