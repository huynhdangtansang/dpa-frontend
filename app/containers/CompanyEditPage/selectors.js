import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the editCompanyPage state domain
 */

const selectEditCompanyPageDomain = state =>
    state.get("editCompanyPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by EditCompanyPage
 */

const makeSelectEditCompanyPage = () =>
    createSelector(selectEditCompanyPageDomain, substate => substate.toJS());
const makeSelectCompanyData = () =>
    createSelector(selectEditCompanyPageDomain, state => state.get('companyData'));
const makeSelectLogo = () =>
    createSelector(selectEditCompanyPageDomain, state => state.get('companyLogo'));
const makeSelectServiceData = () =>
    createSelector(selectEditCompanyPageDomain, state => state.get('serviceData'));
export { makeSelectEditCompanyPage, makeSelectCompanyData, makeSelectServiceData, makeSelectLogo };
export { selectEditCompanyPageDomain };
