import { fromJS } from 'immutable';
import teamListPageReducer from '../reducer';

describe('teamListPageReducer', () => {
  it('returns the initial state', () => {
    expect(teamListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
