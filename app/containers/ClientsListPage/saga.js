import { put, takeLatest } from 'redux-saga/effects';
import { CHANGE_CLIENT_STATUS, DELETE_CLIENT, GET_CLIENTS_LIST, GET_SUBURBS_LIST, RESET_PASSWORD } from './constants';
import { changeIsSuccess, clearAllSelectedClient, getClientsList, getSuburbsSuccess, getSuccess } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetCLientsList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.client;
    // const requestUrl = config.serverUrl + config.api.client + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response));
      yield put(reposLoaded());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiDeleteClient(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(getClientsList(data.searchData));
      yield put(clearAllSelectedClient());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiChangeClientStatus(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/deactives';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.statusData);
      yield put(getClientsList(data.searchData));
      yield put(clearAllSelectedClient());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiResetPassword(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/requestResetPasswords';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.resetData);
      yield put(reposLoaded());
      yield put(changeIsSuccess(true));
      yield put(clearAllSelectedClient());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetSuburbsList(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/suburb';
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuburbsSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_CLIENTS_LIST, apiGetCLientsList);
  yield takeLatest(DELETE_CLIENT, apiDeleteClient);
  yield takeLatest(CHANGE_CLIENT_STATUS, apiChangeClientStatus);
  yield takeLatest(RESET_PASSWORD, apiResetPassword);
  yield takeLatest(GET_SUBURBS_LIST, apiGetSuburbsList);
}
