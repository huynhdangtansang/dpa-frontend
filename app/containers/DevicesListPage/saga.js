import { put, takeLatest } from 'redux-saga/effects';
import { CHANGE_DEVICE_STATUS, DELETE_DEVICE, GET_DEVICES_LIST, GET_SUBURBS_LIST, RESET_PASSWORD } from './constants';
import { changeIsSuccess, clearAllSelectedDevice, getDevicesList, getSuburbsSuccess, getSuccess } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetDevicesList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.device;
    // const requestUrl = config.serverUrl + config.api.device + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response));
      yield put(reposLoaded());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiDeleteDevice(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(getDevicesList(data.searchData));
      yield put(clearAllSelectedDevice());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiChangeDeviceStatus(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/deactives';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.statusData);
      yield put(getDevicesList(data.searchData));
      yield put(clearAllSelectedDevice());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiResetPassword(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/requestResetPasswords';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.resetData);
      yield put(reposLoaded());
      yield put(changeIsSuccess(true));
      yield put(clearAllSelectedDevice());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetSuburbsList(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device + '/suburb';
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuburbsSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_DEVICES_LIST, apiGetDevicesList);
  yield takeLatest(DELETE_DEVICE, apiDeleteDevice);
  yield takeLatest(CHANGE_DEVICE_STATUS, apiChangeDeviceStatus);
  yield takeLatest(RESET_PASSWORD, apiResetPassword);
  yield takeLatest(GET_SUBURBS_LIST, apiGetSuburbsList);
}
