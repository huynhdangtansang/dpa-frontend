// import { take, call, put, select } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import { FORGOT_PASSWORD_SEND_EMAIL } from './constants';
import { sendError, sendSuccess } from './actions';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';

// Individual exports for testing
export function* apiSendEmail(data) {
  // See example in containers/HomePage/saga.js
  const email = data.email;
  if (email) {
    const requestUrl = config.serverUrl + config.api.auth.forgot_password;
    yield put(loadRepos());
    try {
      const response = yield axios.post(requestUrl, {
        email: email
      });
      yield put(reposLoaded());
      yield put(sendSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      if (error) {
        yield put(sendError(error.response.data));
      }
    }
  }
}

export default function* getEmail() {
  yield takeLatest(FORGOT_PASSWORD_SEND_EMAIL, apiSendEmail)
}
