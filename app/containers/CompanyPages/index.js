/**
 *
 * CompanyPages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import CompaniesListPage from '../CompaniesListPage/Loadable';
import CompanyDetailsPage from '../CompanyDetailsPage/Loadable';
import CompanyEditPage from '../CompanyEditPage/Loadable';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class CompanyPages extends React.PureComponent {
  render() {
    return (
        <div className="company-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.companies} component={CompaniesListPage}/>
              <Route path={urlLink.viewCompany} component={CompanyDetailsPage}/>
              <Route path={urlLink.editCompany} component={CompanyEditPage}/>
            </Switch>
          </Router>
        </div>
    )
  }
}

CompanyPages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(CompanyPages);
