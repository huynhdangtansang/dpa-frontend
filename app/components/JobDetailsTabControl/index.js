/**
 *
 * JobDetailsTabControl
 *
 */
import React from "react";
import { Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

/* eslint-disable react/prefer-stateless-function */
class JobDetailsTabControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <Nav tabs className="tab">
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '1' })}
                onClick={() => {
                  this.props.toggle('1');
                }}>
              <span>Activities</span>
            </NavLink>
          </NavItem>
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '2' })}
                onClick={() => {
                  this.props.toggle('2');
                }}>
              <span>Overview</span>
            </NavLink>
          </NavItem>
        </Nav>
    );
  }
}

JobDetailsTabControl.propTypes = {};
export default JobDetailsTabControl;
