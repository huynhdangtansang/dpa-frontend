/**
 *
 * EmployeeTabReviews
 *
 */
import React from 'react';
//lib
import moment from 'moment';
import ClassNames from 'classnames';

/* eslint-disable react/prefer-stateless-function */
class EmployeeTabReviews extends React.PureComponent {
  starCount = (number) => {
    let temp = [];
    for (var i = 0; i < number; i++) {
      temp.push(<span key={i} className="icon-star-full"/>);
    }
    return temp;
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { review } = this.props.employeedetailspage;
    return (
        <div className="reviews">
          <div className="content-add-edit reviews-content">
            <div className="reviews-module rate-module">
              {review.rate && (
                  <div className="rate-number">
                    <div className="number">
                      {review.rate.averageValue}
                      <i className="icon-star-full"/>
                      <i className={ClassNames(review.rate.increment === true ? "icon-arrow-up" : 'icon-arrow-down')}/>
                    </div>

                    <div className="view">
                      {review.rate.total} {review.rate.total > 1 ? 'reviews' : 'review'} in total
                    </div>
                  </div>
              )}
              <div className="group-rate-progress">
                {review.dataRate && review.dataRate.reverse().map((item, index) => (
                    <div className="rate-progress" key={index}>
                      <div className="group-icon">
                        {this.starCount(review.dataRate.length - index)}
                      </div>
                      <div className="progress">
                        {/* Need || 0 because review.rate.total in case 0, number/0 is to NaN, NaN || 0 correct parse */}
                        <div className="progress-bar" role="progressbar" aria-valuenow={`${(item.value * 100) || 0}`}
                             aria-valuemin="0" aria-valuemax="100" style={{ width: `${(item.value * 100) || 0}%` }}>
                          <span className="sr-only">{(item.value * 100) || 0}% Complete</span>
                        </div>
                      </div>
                    </div>
                ))}
              </div>

            </div>
            {/*Review detail*/}
            {review.dataReviews && review.dataReviews.length > 0 && (
                <div className="list-module-reviews">
                  {review.dataReviews.map((item, index) => {
                    return (
                        <div className="reviews-module" key={index}>
                          <div className="img-module">
                            <div className="image-employ">
                              <img src={item.createdBy.avatar ? item.createdBy.avatar.fileName : './default-user.png'}
                                   alt="user" onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = './default-user.png'
                              }}/>
                            </div>
                          </div>
                          <div className="detail-module">
                            <div className="title-module">
                              {item.jobId.name} #{item.jobId.jobId}
                            </div>
                            <div className="rating-module">
                              {this.starCount(item.rate)}
                            </div>
                            <div className="content-module">
                              {item.content}
                            </div>
                          </div>
                          <div className="time">
                            {moment(item.createdAt).format('DD MMM YYYY')}
                          </div>
                        </div>
                    );
                  })}
                </div>
            )}
          </div>
        </div>
    );
  }
}

EmployeeTabReviews.propTypes = {};
export default EmployeeTabReviews;
