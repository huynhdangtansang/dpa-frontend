/*
 *
 * ServiceNewItemPage actions
 *
 */
import * as Constants from "./constants";

export function addService(data) {
  return {
    type: Constants.ADD_SERVICE,
    data: data
  }
}

export function addSuccess(response) {
  return {
    type: Constants.ADD_SUCCESS,
    response: response
  }
}

export function addError(response) {
  return {
    type: Constants.ADD_ERROR,
    response: response
  }
}

export function setSuccess(value) {
  return {
    type: Constants.SET_SUCCESS,
    value: value
  }
}

export function addCoupon(newItem) {
  return {
    type: Constants.ADD_COUPON,
    newItem
  }
}

export function deleteCoupon(index) {
  return {
    type: Constants.DELETE_COUPON,
    index
  }
}

export function updateCoupon(id, updatedItem) {
  return {
    type: Constants.UPDATE_COUPON,
    id,
    updatedItem
  }
}

export function addCert(newItem) {
  return {
    type: Constants.ADD_CERT,
    newItem
  }
}

export function deleteCert(index) {
  return {
    type: Constants.DELETE_CERT,
    index
  }
}

export function updateCert(id, updatedItem) {
  return {
    type: Constants.UPDATE_CERT,
    id,
    updatedItem
  }
}

export function addArea(newItem) {
  return {
    type: Constants.ADD_AREA,
    newItem
  }
}

export function deleteArea(index) {
  return {
    type: Constants.DELETE_AREA,
    index
  }
}

export function updateArea(id, updatedItem) {
  return {
    type: Constants.UPDATE_AREA,
    id,
    updatedItem
  }
}

export function showModal(modal, value) {
  return {
    type: Constants.SHOW_MODAL,
    modal,
    value
  }
}

export function setInit() {
  return {
    type: Constants.SET_INIT
  }
}

export function changeImage(key, value) {
  return {
    type: Constants.CHANGE_IMAGE,
    key,
    value
  }
}