/**
 *
 * JobDetailsTabOverview
 *
 */
import React from "react";
import moment from "moment";
import NumberFormat from 'react-number-format';
import _ from 'lodash';

/* eslint-disable react/prefer-stateless-function */
class JobDetailsTabOverview extends React.Component {
  jobStep = (status) => {
    if (status === 'newLead') {
      return 1
    } else if (status === 'accepted') {
      return 2
    } else if (status === 'onWay') {
      return 3
    } else if (status === 'paying') {
      return 3
    } else if (status === 'paid') {
      return 4
    } else {
      return 5
    }
  };
  iconFormat = (status) => {
    switch (status) {
      case 'newLead':
        return 'icon-new';
      case 'accepted':
        return 'icon-client';
      case 'onWay':
        return 'icon-on-way';
      case 'paid':
        return 'icon-cheap_2';
      case 'cancelled':
        return 'icon-cancel'
    }
  };
  statusName = (type) => {
    switch (type) {
      case 'newLead':
        return 'New lead';
      case 'accepted':
        return 'Accepted';
      case 'onWay':
        return 'On way';
      case 'paid':
        return 'Paid';
      case 'cancelled':
        return 'Cancelled';
    }
  };
  jobProgress = (currentStatus, statusList) => {
    if (currentStatus !== 'cancelled' || !_.isArray(statusList)) {
      let step = this.jobStep(currentStatus);
      return (
          <div className="job-status">
            <div className={step >= 1 ? "status-item active" : "status-item"}>
              <span className="icon-new icon"></span>
              <div className="status-title">
                <span>New lead</span>
              </div>
            </div>
            <div className={step >= 2 ? "status-item active" : "status-item"}>
              <span className="icon-client icon"></span>
              <div className="status-title">
                <span>Accepted</span>
              </div>
            </div>
            <div className={step >= 3 ? "status-item active" : "status-item"}>
              <span className="icon-on-way icon"></span>
              <div className="status-title">
                <span>On way</span>
              </div>
            </div>
            <div className={step >= 4 ? "status-item active" : "status-item"}>
              <span className="icon-cheap_2 icon"></span>
              <div className="status-title">
                <span>Paid</span>
              </div>
            </div>
            <div className={step >= 5 ? "status-item active" : "status-item"}>
              <span className="icon-approval icon"></span>
              <div className="status-title">
                <span>Completed'</span>
              </div>
            </div>
          </div>
      )
    } else {
      return (
          <div className="job-status">
            {statusList.map((item) => {
              if (item !== 'paying') {
                return (
                    <div className={"status-item active"} key={item}>
                      <span className={this.iconFormat(item) + ' icon'}></span>
                      <div className="status-title">
                        <span>{this.statusName(item)}</span>
                      </div>
                    </div>
                )
              }
            })}
          </div>
      )
    }
  };
  estimateCharge = () => {
    const { job } = this.props;
    //sort object by key to alphabet
    let estimate = [];
    if (!_.isUndefined(job.estimation)) {
      Object.keys(job.estimation).sort().forEach((k) => {
        if (k === 'minCharge' || k === 'insuranceCover') {
          estimate.unshift({ key: k, value: job.estimation[k] });
        } else
          estimate.push({ key: k, value: job.estimation[k] });
      });
    }
    let estimateView = [];
    if (!_.isEmpty(estimate)) {
      estimate.map((item) => {
        let name;
        if (item.key === 'minCharge') {
          name = 'Minimum charge'
        } else if (item.key === 'gst') {
          name = (item.value.percent ? item.value.percent : '') + '% ' + 'GST'
        } else if (item.key === 'afterHourFee') {
          name = 'After hour fee'
        } else if (item.key === 'coupon') {
          name = 'Coupon'
        } else if (item.key === 'insuranceCover') {
          name = 'Insurance'
        } else if (item.key === 'additionalHours') {
          name = 'Additional hours';
        } else if (item.key === 'additionalParts') {
          name = 'Additional parts';
        }
        let view = (
            <div key={item.key} className="charge-fee">
              <span className="name">{name}</span>
              <NumberFormat
                  className={'price'}
                  value={item.value.value}
                  className="price"
                  displayType={'text'}
                  prefix={item.value.unit}
                  decimalScale={2}
                  fixedDecimalScale={item.value.value > 0 ? true : false}
              />
            </div>
        );
        if (name && item.value.value > 0 && item.value.active === true) {
          estimateView.push(view);
        }
      })
    }
    return estimateView;
  };
  getAddress = (client) => {
    if (_.isArray(client.address) && client.address.length > 0) {
      let primary = client.address.filter((address) => address.isPrimary === true);
      if (primary.length > 0) {
        return primary[0].name;
      } else {
        return client.address[0].name;
      }
    } else return '';
  };
  appointmentFormat = (appointmentTime, minHours = 0) => {
    let endTime = moment(appointmentTime).add(!_.isUndefined(minHours) ? minHours : 0, 'h');
    if (endTime.isAfter(appointmentTime, 'day')) {
      return (
          <div className="appointment">
            <div>
              <span className="time">{moment(appointmentTime).format('h:mm A')}</span>
              <span className="date">{moment(appointmentTime).format(' | ddd DD MMM YYYY')}</span>
            </div>
            <div>
              <span className="time">{moment(endTime).format('h:mm A')}</span>
              <span className="date">{moment(endTime).format(' | ddd DD MMM YYYY')}</span>
            </div>
          </div>);
    } else {
      return (
          <div className="appointment">
            <span
                className="time">{moment(appointmentTime).format('hh:mm A')} - {moment(appointmentTime).add(!_.isUndefined(minHours) ? minHours : 0, 'h').format('hh:mm A')}</span>
            <span className="date">{moment(appointmentTime).format(' | ddd DD MMM YYYY')}</span>
          </div>);
    }
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { job } = this.props;
    const client = job.createdBy;
    const company = job.companyId;
    const provider = job.assignBy;
    const service = job.serviceInfo;
    // const step = this.jobStep(job.status);
    return (
        <div className="overview">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-4 left">
                <div className="information">
                  <div className="object-info client-info">
                    <div className="title">
                      <span>Client Information</span>
                    </div>
                    <div className="row data">
                      <div className="col-md-3">
                        <div className="present-image">
                          <img src={(client && client.avatar) ? client.avatar.fileName : './default-user.png'}
                               alt="client-ava" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}/>
                        </div>
                      </div>
                      <div className="col-md-9 info-right">
                        <div className="info-item">
                          <div className="label">
                            <span>Client</span>
                          </div>
                          <div className="details">
                            <span>{client ? client.fullName : ''}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="label">
                            <span>Email</span>
                          </div>
                          <div className="details">
                            <span>{client ? client.email : ''}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="label">
                            <span>Phone Number</span>
                          </div>
                          <div className="details">
                            <span>{(client && client.countryCode) ? client.countryCode + ' ' : ''}{client ? client.phoneNumber : ''}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="label">
                            <span>Primary Address</span>
                          </div>
                          <div className="details">
                            <span>{client ? this.getAddress(client) : ''}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="label">
                            <span>Payment Method</span>
                          </div>
                          <div className="details">
                            <span>Activated</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span className="view-profile fixle-link" onClick={() => {
                      this.props.viewClient();
                    }}>View profile</span>
                  </div>
                  {_.isObject(company) &&
                  <div className="object-info company-info">
                    <div className="title">
                      <span>Company Information</span>
                    </div>
                    <div className="row data">
                      <div className="col-md-3">
                        <div className="present-image">
                          <img src={(company && company.logo) ? company.logo.fileName : './default-user.png'}
                               alt="company-logo" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}/>
                        </div>
                      </div>
                      <div className="col-md-9 info-right">
                        <div className="info-item">
                          <div className="label">
                            <span>Company</span>
                          </div>
                          <div className="details">
                            <span>{company ? company.name : ''}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span className="view-profile fixle-link" onClick={() => {
                      this.props.viewCompany();
                    }}>View profile</span>
                  </div>
                  }
                  {_.isObject(provider) &&
                  <div className="object-info provider-info">
                    <div className="title">
                      <span>Provider Information</span>
                    </div>
                    <div className="row data">
                      <div className="col-md-3">
                        <div className="present-image">
                          <img src={(provider && provider.avatar) ? provider.avatar.fileName : './default-user.png'}
                               alt="company-logo" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}/>
                        </div>
                      </div>
                      <div className="col-md-9 info-right">
                        <div className="info-item">
                          <div className="label">
                            <span>Provider</span>
                          </div>
                          <div className="details">
                            <span>{provider ? provider.fullName : ''}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <span className="view-profile fixle-link" onClick={() => {
                      this.props.viewProvider();
                    }}>View profile</span>
                  </div>
                  }
                </div>
              </div>
              <div className="col-md-8 right">
                <div className="information">
                  {this.jobProgress(job.status, job.realStatus)}
                </div>
                <div className="container-fluid">
                  <div className="row description-and-estimate">
                    <div className="col-md-6">
                      <div className="information description-info">
                        <div className="title">
                          <span>Job Description</span>
                        </div>
                        <div className="data">
                          <div className="info-item">
                            <div className="label">
                              <span>Created by</span>
                            </div>
                            <div className="details time">
                              <span className="time">{moment(job.createdAt).format('hh:mm A |')}</span>
                              <span className="date">{moment(job.createdAt).format(' ddd DD MMM YYYY')}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="label">
                              <span>Appointment Time</span>
                            </div>
                            <div className="details time">
                              {!_.isUndefined(job.appointmentTime) && this.appointmentFormat(job.appointmentTime, !_.isUndefined(service) ? service.minHours.value : 0)}
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="label">
                              <span>Location</span>
                            </div>
                            <div className="details">
                              <span>{(job && job.location) ? job.location.name : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="label">
                              <span>Comments</span>
                            </div>
                            <div className="details">
                              <span>{(job && job.description) ? job.description : ''}</span>
                              <div className="image-list">
                                {(job.images && job.images.length > 0) ? job.images.map((image) => {
                                  return (
                                      <img key={image._id} src={image.fileName} alt="comment-image" onError={(e) => {
                                        e.target.onerror = null;
                                        e.target.src = './default-user.png'
                                      }}/>
                                  )
                                }) : []}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="icon-contain">
                        <span className="icon-edit" onClick={() => {
                          this.props.goToEditDescription();
                        }}></span>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="information estimate-info">
                        <div className="charges">
                          <div className="title">
                            <span>Estimate</span>
                          </div>
                          {this.estimateCharge()}
                        </div>
                        <div className="deposit"
                             hidden={job && job.estimation && _.isUndefined(job.estimation.disposit)}>
                          <span className="name">Deposit</span>
                          <NumberFormat
                              className={'price'}
                              value={(job && job.estimation) ? job.estimation.disposit : 0}
                              className="price"
                              displayType={'text'}
                              prefix={'$'}
                              decimalScale={2}
                              fixedDecimalScale={((job && job.estimation) ? job.estimation.disposit : 0) > 0 ? true : false}
                          />
                        </div>
                        <div className="total">
                          <span className="name">Total</span>
                          <NumberFormat
                              className={'price'}
                              value={(job && job.estimation) ? job.estimation.total : 0}
                              className="price"
                              displayType={'text'}
                              prefix={'$'}
                              decimalScale={2}
                              fixedDecimalScale={((job && job.estimation) ? job.estimation.total : 0) > 0 ? true : false}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

JobDetailsTabOverview.propTypes = {};
export default JobDetailsTabOverview;
