/**
 *
 * JobEditDescription
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectJobEditDescription from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  addImage,
  changeComment,
  changeLocation,
  getJob,
  removeImage,
  resetState,
  updateJobDescription
} from "./actions";
import './style.scss';
import 'components/InputForm/style.scss';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import TextareaCounter from 'components/TextareaCounter';
import ProofUpload from "components/ProofUpload";
import Location from 'components/Location';

const validateForm = Yup.object().shape({
  'location': Yup.string()
      .required(''),
  'comment': Yup.string()
      .required(''),
});
const MAX_NUMBER = 4;
const MAX_FILE_SIZE = 2 * 1024 * 1024;//2MB
/* eslint-disable react/prefer-stateless-function */
export class JobEditDescription extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errorUpload: false,
      showConfirmModal: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let jobId = temp[1];
    this.props.getJob(jobId);
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'location',
      'comment'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  _handleImageChange(e) {
    const { images } = this.props.jobeditdescription;
    e.preventDefault();
    this.setState({ errorUpload: false });
    let currentSize = e.target.files.length + images.length;
    //upload over MAX_PROOFS images will not do
    if (0 <= currentSize && currentSize <= MAX_NUMBER) {
      Array.from(e.target.files).forEach(item => {
        let image = { file: '', fileName: '' };
        let reader = new FileReader();
        let file = item;
        let type = file.name.split('.');
        if (type[1] === 'jpeg' || type[1] === 'jpg' || type[1] === 'png' || type[1] === 'gif') {
          reader.onloadend = () => {
            //file size need lower than 2MB
            if (file.size <= MAX_FILE_SIZE) {
              image.file = file;
              image.fileName = reader.result;
              this.props.addImage(image);
            }
          };
          reader.readAsDataURL(file);
        } else {
          this.setState({ errorUpload: true });
        }
      });
      e.target.value = null;
    } else {
      this.setState({ errorUpload: true });
    }
  }

  render() {
    const { description, images, location, errors } = this.props.jobeditdescription;
    let updateErrors = errors;
    return (
        <div className="edit-job-description">
          <div className="header-edit-add-page edit-job-decription-header">
            <div className="action">
              <div className="return" id="return" onClick={() => {
                this.props.history.goBack();
              }}>
                <span className="icon-arrow-left"></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
            </div>
            <div className="title">
              <span>Edit Job Description</span>
            </div>
          </div>
          {<Formik
              ref={ref => (this.formik = ref)}
              initialValues={{
                location: !_.isUndefined(location.name) ? location.name : '',
                comment: description ? description : ''
              }}
              enableReinitialize={true}
              validationSchema={validateForm}
              onSubmit={(e) => {
                let formData = new FormData();
                formData.append('location', JSON.stringify(location));
                formData.append('description', e.comment);
                let dataImage = [];
                if (!_.isEmpty(images)) {
                  images.map((image) => {
                    if (_.isObject(image.file)) {
                      formData.append('files', image.file);
                    } else {
                      dataImage.push(image.id);
                    }
                  })
                }
                let temp = JSON.stringify(dataImage);
                formData.append('imageIds', temp);
                let url = window.location.href;
                let tempUrl = url.split('?id=');
                let jobId = tempUrl[1];
                this.props.updateJobDescription(jobId, formData);
              }}>{({
                     values,
                     errors,
                     handleChange,
                     handleBlur,
                     handleSubmit,
                     setFieldValue
                   }) => (
              <form id={'form'} onSubmit={handleSubmit}>
                <div className="content-add-edit edit-job-description-content">
                  <div className="information">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Job Information</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">
                          <Location
                              name={'location'}
                              title={'Location'}
                              value={values.location}
                              onSelect={newLocation => {
                                this.props.changeLocation(newLocation);
                                setFieldValue('location', newLocation.name);
                              }}
                              onChange={evt => {
                                handleChange(evt);
                                setFieldValue('location', '');
                              }}
                          />
                          <div className="comment-images">
                            <div className="label">
                              <span>Comment</span>
                            </div>
                            <div>
                              <TextareaCounter
                                  name={'comment'}
                                  placeholder={'Job description'}
                                  rows={5}
                                  maxLength={50}
                                  value={values.comment}
                                  error={errors.comment}
                                  onChange={evt => {
                                    handleChange(evt);
                                    setFieldValue('comment', evt.target.value);
                                    //this.props.changeLocation(newLocation);
                                  }}
                                  onBlur={handleBlur}/>
                              <div className="images">
                                <ProofUpload
                                    images={images ? images : []}
                                    removeItem={index => {
                                      this.props.removeImage(index);
                                    }}
                                    onChange={evt => {
                                      if (images.length < MAX_NUMBER) {
                                        this._handleImageChange(evt);
                                      }
                                    }}/>
                              </div>
                            </div>
                          </div>
                        </div>
                        {(updateErrors && updateErrors.length > 0) ? updateErrors.map((error) => {
                          return (
                              <div key={error.errorCode} className="errors">
                                <span className="icon-error"></span>
                                <div className="error-item">
                                  <span>{error.errorMessage}</span>
                                </div>
                              </div>
                          );
                        }) : []}

                        {this.state.errorUpload ? (
                            <div className="form-input">
                              <div className="error-text">
                                <i className="icon-error"></i>
                                <div className="error-item">
                                  <span>Invalid proof (maximum {MAX_NUMBER} proofs)</span>
                                </div>
                              </div>
                            </div>
                        ) : []}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer edit-job-description-footer">
                  <GhostButton
                      type="button"
                      className="btn-ghost cancel"
                      title={'Cancel'}
                      onClick={() => {
                        this.props.history.goBack();
                      }}/>
                  <PurpleRoundButton type={'button'}
                                     className="btn-purple-round save"
                                     disabled={this._disableButton(values, errors) || _.isEmpty(images) || (updateErrors && updateErrors.length > 0)}
                                     title={'Save'}
                                     onClick={() => {
                                       this.setState({ showConfirmModal: true });
                                     }}
                  />
                </div>
              </form>
          )}
          </Formik>
          }

          <Modal isOpen={this.state.showConfirmModal} className="logout-modal">
            <ModalBody>
              <div className="upper">
                <div className="title">
                  <span>Save This Change</span>
                </div>
                <div className="description">
                  <span>Are you sure to want to save this change? This action could influence on all datas involved.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost" title={'Cancel'} onClick={() => {
                  this.setState({ showConfirmModal: false });
                }}/>
                <PurpleRoundButton form={'form'} type={'submit'} className="btn-purple-round" title={'Save'}/>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

JobEditDescription.propTypes = {
  dispatch: PropTypes.func,
  getJob: PropTypes.func,
  addImage: PropTypes.func,
  removeImage: PropTypes.func,
  updateJobDescription: PropTypes.func,
  changeLocation: PropTypes.func,
  changeComment: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  jobeditdescription: makeSelectJobEditDescription()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(resetState());
    },
    getJob: (jobId) => {
      dispatch(getJob(jobId));
    },
    addImage: (newImage) => {
      dispatch(addImage(newImage));
    },
    removeImage: (index) => {
      dispatch(removeImage(index));
    },
    updateJobDescription: (jobId, updateData) => {
      dispatch(updateJobDescription(jobId, updateData));
    },
    changeLocation: (newLocation) => {
      dispatch(changeLocation(newLocation));
    },
    changeComment: (newComment) => {
      dispatch(changeComment(newComment));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "jobEditDescription", reducer });
const withSaga = injectSaga({ key: "jobEditDescription", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(JobEditDescription);
