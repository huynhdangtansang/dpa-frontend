/*
 *
 * JobEditDescription actions
 *
 */
import {
  ADD_IMAGE,
  CHANGE_COMMENT,
  CHANGE_LOCATION,
  GET_JOB,
  GET_SUCCESS,
  REMOVE_IMAGE,
  RESET_STATE,
  UPDATE_ERROR,
  UPDATE_JOB_DESCRIPTION
} from "./constants";

export function resetState() {
  return {
    type: RESET_STATE
  }
}

export function getJob(jobId) {
  return {
    type: GET_JOB,
    jobId
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function updateJobDescription(jobId, updateData) {
  return {
    type: UPDATE_JOB_DESCRIPTION,
    jobId,
    updateData
  }
}

export function updateDesError(response) {
  return {
    type: UPDATE_ERROR,
    response
  }
}

export function addImage(newImage) {
  return {
    type: ADD_IMAGE,
    newImage
  }
}

export function removeImage(id) {
  return {
    type: REMOVE_IMAGE,
    id
  }
}

export function changeLocation(newLocation) {
  return {
    type: CHANGE_LOCATION,
    newLocation
  }
}

export function changeComment(newComment) {
  return {
    type: CHANGE_COMMENT,
    newComment
  }
}