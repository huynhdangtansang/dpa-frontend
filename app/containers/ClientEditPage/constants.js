/*
 *
 * ClientEditPage constants
 *
 */
export const DEFAULT_ACTION = "app/ClientEditPage/DEFAULT_ACTION";
export const GET_CLIENT_DETAILS = "ClientEditPage/GET_CLIENT_DETAILS";
export const GET_SUCCESS = "ClientEditPage/GET_SUCCESS";
export const GET_ERROR = "ClientEditPage/GET_ERROR";
export const UPDATE_CLIENT_PROFILE = "ClientEditPage/UPDATE_CLIENT_PROFILE";
export const UPDATE_SUCCESS = "ClientEditPage/UPDATE_SUCCESS";
export const UPDATE_ERROR = "ClientEditPage/UPDATE_ERROR";
export const ADD_ADDRESS = "ClientEditPage/ADD_ADDRESS";
export const EDIT_ADDRESS = "ClientEdit/EDIT_ADDRESS";
export const CHANGE_AVATAR = "ClientEdit/CHANGE_AVATAR";
export const SHOW_MODAL = "ClientEdit/SHOW_MODAL";