import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the employeeDetailsPage state domain
 */

const selectEmployeeDetailsPageDomain = state =>
    state.get("employeeDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by EmployeeDetailsPage
 */

const makeSelectEmployeeDetailsPage = () =>
    createSelector(selectEmployeeDetailsPageDomain, substate => substate.toJS());
export default makeSelectEmployeeDetailsPage;
export { selectEmployeeDetailsPageDomain };
