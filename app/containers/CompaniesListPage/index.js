/**
 *
 * CompaniesListPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectCompaniesListPage } from "./selectors";
import {
  changeCompanyStatus,
  deleteCompany,
  getCompaniesListData,
  getPeriod,
  getServiceListData,
  resetState,
  selectAllCompany,
  selectCompany,
  setActiveCompany,
  setRemoveCompany,
  setTimerCount,
  updateDropdownPeriod,
  updateDropdownService,
  updateFilter,
  updateSearchData,
  updateSearchString
} from './actions';
import { loadRepos } from 'containers/App/actions';
import reducer from "./reducer";
import saga from "./saga";
import './style.scss';
//Library
import _ from "lodash";
import moment from 'moment';
import debounce from 'components/Debounce';
import classnames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import uuidv1 from 'uuid';
// Modal
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { DropdownItem, DropdownMenu, DropdownToggle, Form, Input, UncontrolledDropdown, } from 'reactstrap';
// Combo import table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
//Component
import GhostButton from 'components/GhostButton';
import Checkbox from 'components/Checkbox';
import { urlLink } from 'helper/route';
// -------------------
/* eslint-disable react/prefer-stateless-function */
export class CompaniesListPage extends React.PureComponent {
  showRemoveConfirm = (data) => {
    this.setState({
      showRemoveConfirm: true,
      chosenService: data
    });
  };
  handleChangeReason = (value) => {
    this.setState({ reason: value });
  };
  handleSearch = (e) => {
    e.preventDefault();
    const { searchData } = this.props.companieslistpage;
    this.props.getCompaniesList(searchData);
  };
  handleTopCompaniesCheck = (value) => {
    const { searchData } = this.props.companieslistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.isTopCompany = value;
    this.props.updateSearchData(searchData);
  };
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
      status: row.active
    };
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  checkChangeStatusButton = () => {
    const { selectedCompanies } = this.props.companieslistpage;
    let list = selectedCompanies;
    if (list.length > 0) {
      const compareStatus = list[0].status;
      let differentItems = list.filter((item) => item.status !== compareStatus);
      if (differentItems.length === 0) {
        return {
          isHide: false,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      } else {
        return {
          isHide: true,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      }
    }
    return {
      isHide: false,
      title: 'Deactive',
    }
  };
  //-----------------------PAGINATION-----------------------
  changePagination = (type) => {
    const { companiesList, searchData, currentPage } = this.props.companieslistpage;
    const { limit } = companiesList.pagination;
    let newSearchData = searchData;
    let newPage, newOffset;
    if (type === 'prev') {
      newPage = currentPage - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    } else {//next
      newPage = currentPage + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    }
    this.props.updateFilter(newSearchData, newPage);
    //this.setState(state);
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { searchData } = this.props.companieslistpage;
    this.props.getCompaniesList(searchData);
  }, 1000);
  singleAction = (type, companyData) => {
    if (type === 'changeStatus') {
      this.setState({ chosenCompany: companyData }, () => {
        this.openStatusConfirm('single');
      });
    } else if (type === 'remove') {
      this.setState({ chosenCompany: companyData }, () => {
        this.openRemoveConfirm('single');
      })
    } else {
      let listIds = [];
      listIds.push(companyData.id);
      let data = {
        ids: listIds
      };
      this.props.resetPass(data);
    }
  };
  multiAction = (type) => {
    if (type === 'changeStatus') {
      this.openStatusConfirm('multi');
    } else if (type === 'remove') {
      this.openRemoveConfirm('multi');
    } else {
      let listIds = [];
      let list = this.state.selectedCompanies;
      let i = 0;
      list.forEach((company) => {
        listIds.push(company.id);
        if (i === list.length - 1) {
          let data = {
            ids: listIds
          };
          this.props.resetPass(data);
        } else {
          i++;
        }
      });
    }
  };
  openStatusConfirm = (type) => {
    this.setState({
      modifyType: type,
      showChangeStatusConfirm: true
    });
  };
  //-------------------------------------------------------
  openRemoveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showRemoveConfirm: true
    });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  changeStatus = () => {
    const { searchData } = this.props.companieslistpage;
    this.refs.table_company.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType == 'single') {
      let id = this.state.chosenCompany.id;
      let status = this.state.chosenCompany.status;
      let listIds = [];
      listIds.push(id);
      let data = {};
      if (status == true) {
        data['ids'] = listIds;
        data['reason'] = this.state.reason;
        data['active'] = !status;
      } else {
        data['ids'] = listIds;
        data['active'] = !status;
      }
      this.props.changeStatus(data, searchData);
    } else {
      const changeStatusButton = this.checkChangeStatusButton();
      let listIds = [];
      let { selectedCompanies } = this.props.companieslistpage;
      let list = selectedCompanies;
      let i = 0;
      list.forEach((company) => {
        listIds.push(company.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds,
            active: changeStatusButton.title === 'Deactive' ? false : true
          };
          if (this.state.changeStatusType === 'Deactive') {
            data['reason'] = this.state.reason;
          }
          this.props.changeStatus(data, searchData);
        } else {
          i++;
        }
      });
    }
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  handleSearchTextChange = debounce(text => {
    const { searchData } = this.props.companieslistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.search = text;
    this.props.updateSearchData(temp);
  }, 700);
  handleChangeFilterService = (newfilterService) => {
    this.props.updateDropdownService(newfilterService);
    const { searchData } = this.props.companieslistpage;
    let temp = searchData;
    let array = [];
    if (!_.isEmpty(newfilterService.value))
      array.push(newfilterService.value);
    else array = [];
    temp.offset = 0;
    temp.serviceIds = array;
    this.props.updateSearchData(temp);
  };
  handleChangeFilterPeriod = debounce(newfilterPeriod => {
    this.props.updateDropdownPeriod(newfilterPeriod);
    const { searchData } = this.props.companieslistpage;
    this.props.getCompaniesList(searchData);
  }, 400);
  changeStatusModal = () => {
    if (this.state.modifyType === 'single') {
      let currentCompany = this.state.chosenCompany;
      return (
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              action={this.state.modifyType}
              currentStatus={currentCompany.status}
              object={'company'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
      )
    } else {
      const { selectedCompanies } = this.props.companieslistpage;
      const changeStatusButton = this.checkChangeStatusButton();
      if (selectedCompanies.length > 0) {
        return (
            <ChangeStatusConfirmModal
                show={this.state.showChangeStatusConfirm}
                reason={this.state.reason}
                action={this.state.modifyType}
                currentStatus={changeStatusButton.title === 'Deactive' ? true : false}
                object={'company'}
                handleChange={this.handleReasonChange}
                closeModal={this.closeStatusConfirm}
                changeStatus={this.changeStatus}/>
        )
      }
    }
  };
  sortData = (field, order) => {
    const { searchData } = this.props.companieslistpage;
    let temp = searchData;
    temp.sort = field;
    temp.sortType = order;
    if (field === 'staffs') {
      temp.sort = 'employees';
    }
    this.props.updateFilter(temp, 1);
    this.props.getCompaniesList(searchData);
    //this.props.startLoad();
  };
  removeCompany = () => {
    this.refs.table_company.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType === 'single') {
      const { searchData } = this.props.companieslistpage;
      let id = this.state.chosenCompany.id;
      let listIds = [];
      listIds.push(id);
      let data = {
        ids: listIds,
        reason: this.state.reason
      };
      this.props.deleteCompanyFromList(data, searchData);
    } else {
      let listIds = [];
      const { selectedCompanies, searchData } = this.props.companieslistpage;
      let list = selectedCompanies;
      let i = 0;
      list.forEach((company) => {
        listIds.push(company.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds,
            reason: this.state.reason
          };
          this.props.deleteCompanyFromList(data, searchData);
        } else {
          i++;
        }
      });
    }
    //this.props.startLoad();
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      showChangeStatusConfirm: false,
      selectedCompanies: [],
      isActive: true,
      reason: '',
      chosenCompany: {},
      modifyType: 'single',
      hideChangeStatusButton: false,
      changeStatusType: 'Deactive',
      page: 1
    };
  }

  componentWillMount() {
    const { searchData, searchDataForService } = this.props.companieslistpage;
    this.props.getServiceList(searchDataForService);
    this.props.getCompaniesList(searchData);
    this.props.getPeriod();
  }

  //this function will reset store in reducer
  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const {
      companiesList, selectedCompanies, dataList, searchData, currentPage,
      serviceList, periodList, filterPeriod, filterService
    } = this.props.companieslistpage;
    const changeStatusButton = this.checkChangeStatusButton();
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll,
    };
    const options = {
      withoutNoDataText: true,
      defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    const actionFormat = (cell, row) => {
      let editMenu = (
          <DropdownMenu key={uuidv1()}>
            <DropdownItem onClick={() => {
              this.props.history.push(urlLink.viewCompany + '?id=' + row._id);
            }}>View</DropdownItem>
            <DropdownItem onClick={() => {
              this.singleAction('changeStatus', { id: row._id, status: row.active });
            }}>
              {row.active ? 'Deactivate' : 'Activate'}
            </DropdownItem>
            <DropdownItem onClick={() => {
              this.singleAction('remove', { id: row._id, status: row.active });
            }}>Remove</DropdownItem>
          </DropdownMenu>
      );
      return (
          <ActionFormatter menu={editMenu}/>
      );
    };
    const iconFormat = (cell, row) => {
      return (
          <img className='avatar-company'
               src={row.logo ? row.logo.fileName : './default-user.png'}
               onError={(e) => {
                 e.target.onerror = null;
                 e.target.src = './default-user.png'
               }}
               onClick={() => {
                 this.props.history.push(urlLink.viewCompany + '?id=' + row._id);
               }}>
          </img>
      )
    };
    const nameFormat = (cell, row) => {
      return (
          <span onClick={() => {
            this.props.history.push(urlLink.viewCompany + '?id=' + row._id);
          }}>{cell}</span>
      )
    };
    const statusFormat = (cell) => {
      return (
          <div className="status">
            <div className={classnames("content", cell ? 'active' : 'inactive')}>{cell ? 'Active' : 'Inactive'}</div>
          </div>
      )
    };
    const ratingFormat = (cell) => {
      return (
          <div className="rating">
            <span>{parseFloat(cell.averageValue).toFixed(1)}</span>
            <span className="icon-star-full"></span>
            <span className={cell.increment === true ? 'icon-arrow-up' : 'icon-arrow-down'}></span>
          </div>
      )
    };
    return (
        <div className="companies-list">
          <div className="header-list-page">
            <Form inline onSubmit={(e) => this.handleSearch(e)} className="align-items-center">

              <div className="search-bar services-search">
                <Input placeholder="Search company"
                       defaultValue={searchData.search}
                       onChange={(e) => {
                         e.preventDefault();
                         this.handleSearchTextChange(e.target.value);
                       }}/>
                <span className="icon-search"></span>
              </div>
              <UncontrolledDropdown className="period-dropdown">
                <DropdownToggle>
                  <span className="title">Period:&nbsp;</span>
                  <span className="content">{filterPeriod.label}</span>
                  <i className="fixle-caret icon-triangle-down"></i>
                </DropdownToggle>
                <DropdownMenu>
                  <Scrollbars
                      // This will activate auto hide
                      autoHide
                      // Hide delay in ms
                      autoHideTimeout={1000}
                      autoHeight
                      autoHeightMin={0}
                      autoHeightMax={200}>
                    <DropdownItem key={uuidv1()}
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.handleChangeFilterPeriod({
                                      value: '', label: 'All'
                                    });
                                  }}>All</DropdownItem>

                    {periodList && (periodList.map((item) => (
                        <DropdownItem key={uuidv1()}
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.handleChangeFilterPeriod({
                                          value: moment(item).format('MM/YYYY'), label: item
                                        })
                                      }}
                        >{item}
                        </DropdownItem>
                    )))}

                  </Scrollbars>
                </DropdownMenu>
              </UncontrolledDropdown>

              <UncontrolledDropdown className="services-dropdown">
                <DropdownToggle>
                  <span className="title">Services:&nbsp;</span>
                  <span className="content">{filterService.label}</span>
                  <i className="fixle-caret icon-triangle-down"></i>
                </DropdownToggle>

                <DropdownMenu>
                  <Scrollbars
                      // This will activate auto hide
                      autoHide
                      // Hide delay in ms
                      autoHideTimeout={1000}
                      autoHeight
                      autoHeightMin={0}
                      autoHeightMax={500}>
                    <DropdownItem key={uuidv1()}
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.handleChangeFilterService({
                                      value: '', label: 'All'
                                    });
                                  }}>All</DropdownItem>
                    {serviceList && (serviceList.map((item) => (
                        <DropdownItem key={uuidv1()}
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.handleChangeFilterService({
                                          value: item._id, label: item.name
                                        })
                                      }}
                        >{item.name}
                        </DropdownItem>
                    )))}
                  </Scrollbars>
                </DropdownMenu>
              </UncontrolledDropdown>

              <Checkbox name={'is-top-companies'}
                        label={'Only top companies'}
                        onChange={(e) => {
                          this.handleTopCompaniesCheck(e.target.checked);
                        }}
              />

              {companiesList.data && companiesList.data.length > 0 && companiesList.pagination && (
                  <TotalFormatter
                      offset={companiesList.pagination.offset}
                      limit={companiesList.pagination.limit}
                      total={companiesList.pagination.total}
                      page={currentPage}
                      changePagination={this.changePagination}
                  />
              )}

            </Form>
          </div>

          <div className={classnames("content content-list-page", selectedCompanies.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_company"
                data={dataList instanceof Array ? dataList : []}
                options={options}
                bordered={false}
                containerClass='table-fixle services-table'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'
                bodyContainerClass='table-fixle-body'
                selectRow={selectRowProp}>
              <TableHeaderColumn dataField='_id' isKey hidden>Company ID</TableHeaderColumn>
              <TableHeaderColumn dataField='icon' width={46 / 14 + 'rem'} dataAlign='center'
                                 dataFormat={iconFormat}></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='name' width={198 / 14 + 'rem'} columnClassName='name'
                                 dataFormat={nameFormat}>Company Name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='active' width={118 / 14 + 'rem'} headerAlign='left'
                                 dataAlign='center' dataFormat={statusFormat}
                                 columnClassName='status'>Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='accepted' headerAlign='left'
                                 dataAlign='center'>Accepted</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='completed' dataAlign='center'>Completed</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='staffs' dataFormat={(cell) => {
                return (cell.length)
              }} headerAlign='left' dataAlign='center'>Employees</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='earning' dataFormat={(cell) => {
                return cell.unit + parseFloat(cell.value).toFixed(2)
              }}>Earning</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='rate' width={92 / 14 + 'rem'}
                                 dataFormat={ratingFormat}>Rating</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='createdAt' width={114 / 14 + 'rem'} dataFormat={(cell) => {
                return moment(cell).format('ddd D MMM YYYY')
              }} headerAlign='left' dataAlign='center'>Joined</TableHeaderColumn>
              <TableHeaderColumn dataFormat={actionFormat} dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div className="footer" hidden={selectedCompanies.length === 0}>
            <GhostButton hidden={changeStatusButton.isHide}
                         title={changeStatusButton.title === 'Deactive' ? 'Deactivate' : 'Activate'}
                         className="btn-ghost btn-edit-charge"
                         onClick={() => {
                           if (selectedCompanies.length === 1) {
                             this.singleAction('changeStatus', selectedCompanies[0]);
                           } else
                             this.multiAction('changeStatus');
                         }}/>
            <GhostButton title={'Remove'} className="btn-ghost btn-remove"
                         onClick={() => {
                           if (selectedCompanies.length === 1) {
                             this.singleAction('remove', selectedCompanies[0]);
                           } else
                             this.multiAction('remove');
                         }}/>
          </div>

          {this.changeStatusModal()}

          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'company'}
              timer={3}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeCompany}/>

        </div>
    );
  }
}

CompaniesListPage.propTypes = {
  dispatch: PropTypes.func,
  getCompaniesList: PropTypes.func,
  getServiceList: PropTypes.func,
  getPeriod: PropTypes.func,
  activeCompany: PropTypes.func,
  removeCompany: PropTypes.func,
  changeStatus: PropTypes.func,
  startLoad: PropTypes.func,
  select: PropTypes.func,
  updateSearchData: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  companieslistpage: makeSelectCompaniesListPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    getCompaniesList: (searchData) => {
      dispatch(getCompaniesListData(searchData));
    },
    getServiceList: (searchData) => {
      dispatch(getServiceListData(searchData));
    },
    getPeriod: () => {
      dispatch(getPeriod());
    },
    select: (isSelected, selectItem) => {
      dispatch(selectCompany(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(selectAllCompany(isSelected));
    },
    activeCompany: (companyID, reason, isActive) => {
      dispatch(setActiveCompany(companyID, reason, isActive));
    },
    changeStatus: (data, searchData) => {
      dispatch(changeCompanyStatus(data, searchData));
    },
    deleteCompanyFromList: (data, searchData) => {
      dispatch(deleteCompany(data, searchData));
    },
    removeCompany: (companyID, reason) => {
      dispatch(setRemoveCompany(companyID, reason));
    },
    updateFilter: (newSearchData, currentPage) => {
      dispatch(updateFilter(newSearchData, currentPage));
    },
    updateDropdownService: (newfilterService) => {
      dispatch(updateDropdownService(newfilterService));
    },
    updateDropdownPeriod: (newfilterPeriod) => {
      dispatch(updateDropdownPeriod(newfilterPeriod));
    },
    updateSearchString: (text) => {
      dispatch(updateSearchString(text));
    },
    resetState: () => {
      dispatch(resetState());
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    setTimerCount: (value) => {
      dispatch(setTimerCount(value));
    },
    updateSearchData: (newSearchData) => {
      dispatch(updateSearchData(newSearchData));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "companiesListPage", reducer });
const withSaga = injectSaga({ key: "companiesListPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(CompaniesListPage);
