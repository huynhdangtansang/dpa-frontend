import React from 'react';
import './styles.scss';
import PropTypes from "prop-types";

export default class CheckBoxSquare extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className="fixle-checkbox-square">
          <label>
            <input
                type="checkbox"
                className="fixle-input"
                checked={this.props.value}
                onChange={this.props.onChange}
            />
            <span className="fixle-span"/>
          </label>
        </div>
    );
  }
}
CheckBoxSquare.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.bool,
  disabled: PropTypes.bool
};
