/**
 *
 * ProofUpload
 *
 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import './style.scss';

const MAX_PROOFS = 4;

function ProofUpload(props) {
  return (
      <div className={'proof-upload-wrapper'}>
        <div className={'proof-label'}>Proof screenshot</div>

        <div className={'image-frame-wrapper'}>
          <div className={'image-frame left-image proof'} hidden={props.images.length >= MAX_PROOFS}>

            <i className={'icon-camera icon-style'}/>

            <input className={'input-file left-style'}
                   type="file"
                   name={'file-upload'}
                   multiple={true}
                   onChange={props.onChange}
            />
          </div>

          <div className='proof-wrapper d-flex flex-row'>
            {props.images && props.images.map((image, index) => (
                <div className="item-proof" key={index}
                     onClick={(e) => {
                       e.preventDefault();
                       props.removeItem(index)
                     }}>
                  <i className="icon-cancel"></i>
                  <img className='proof-image' src={image.fileName}/>
                </div>
            ))}
          </div>
        </div>
      </div>
  );
}

export default ProofUpload;