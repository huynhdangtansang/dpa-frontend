import React from "react";
//Lib
import DatePicker from 'react-datepicker';
import moment from 'moment';
import _ from "lodash";
import './style.scss';
import InputForm from 'components/InputForm';

export default class Datepicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  // handleDateChange = (date) => {
  //   this.props.selected = moment(date);
  // }
  toggle = (value) => {
    if (this.props.hoursFromNow) {
      let now = moment();
      let chosen = moment(value);
      let diff = moment.duration(chosen.diff(now)).asHours();
      if (diff < this.props.hoursFromNow) {
        this.props.onChange(moment().add(this.props.hoursFromNow, 'h'));
      }
    }
  };

  render() {
    return (
        <div className="date-picker">
          <DatePicker
              className={'input-form'}
              selected={moment(this.props.selected).toDate()}
              minDate={new Date()}
              onChange={date => {
                this.props.onChange(date);
              }}
              onSelect={(e) => {
                this.toggle(e)
              }}
          />
          <InputForm
              name={this.props.name}
              apiError={this.props.apiError}
              touched={true}
              placeholder={'Select date'}
              value={!_.isUndefined(this.props.selected) ? moment(this.props.selected).format('ddd D MMM YYYY') : ''}
              onChange={() => {
              }}/>
        </div>
    )
  }
}
