import React from 'react';
import './styles.scss';
import PropTypes from "prop-types";

export default class GhostButton extends React.Component {
  render() {
    return (
        <button hidden={this.props.hidden}
                className={this.props.className}
                onClick={this.props.onClick}
                type={this.props.type}>
          {this.props.title}
        </button>
    );
  }
}
GhostButton.propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string
};