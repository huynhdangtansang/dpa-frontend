import React from "react";
import { Modal, ModalBody } from "reactstrap";
import NumberFormat from 'react-number-format';
import "./style.scss";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import Dropdown from 'components/Dropdown';
import TextareaCounter from 'components/TextareaCounter';

export default class RefundModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paid: 165,
      refundType: '%',
      refund: 0,
      reason: ''
    };
  }

  caculate = () => {
    if (this.state.refundType === '%') {
      return this.state.refund < 100 ? (this.props.paid * this.state.refund) / 100 : this.props.paid;
    } else {
      return this.state.refund < this.props.paid ? this.state.refund : this.props.paid;
    }
  };
  disableButton = () => {
    if (this.state.reason !== '' && this.state.refund > 0) {
      return false;
    }
    return true;
  };
  refundValue = () => {
    if (this.state.refundType === '%') {
      return this.state.refund > 100 ? 100 : this.state.refund
    } else {
      return (this.state.refund > this.props.paid) ? this.props.paid : this.state.refund;
    }
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="refund-modal">
          <ModalBody>
            <div className="upper">
              <div className="title">
                <span>Refund and Complete This job?</span>
              </div>
              <div className="description">
                <span>Are your sure to want to refund and cancel this job? Clicking refund, you can not reverse the action</span>
              </div>
            </div>
            <div className="payment">
              <div className="modify-item">
                <div className="label">
                  <span>Paid</span>
                </div>
                <div className="modify">
                  <NumberFormat
                      value={this.props.paid}
                      disabled={true}
                      prefix={'$'}
                      decimalScale={2}
                      fixedDecimalScale={this.props.paid > 0 ? true : false}
                  />
                </div>
              </div>
              <div className="modify-item">
                <div className="label">
                  <span>Refund</span>
                </div>
                <div className="modify refund-modify">
                  <Dropdown
                      defaultValue={this.state.refundType}
                      defaultLabel={this.state.refundType}
                      options={[
                        { label: '%', value: '%' },
                        { label: '$', value: '$' },
                      ]}
                      onSelect={(value) => {
                        this.setState({ refundType: value });
                      }}
                  />
                  <input
                      name={'discount'}
                      value={this.refundValue()}
                      type="number"
                      placeholder={'Refund'}
                      className="refund-input"
                      onChange={(e) => {
                        this.setState({ refund: e.target.value });
                      }}/>
                </div>
              </div>
            </div>
            <div className="total">
              <NumberFormat
                  className={'price'}
                  value={this.caculate()}
                  disabled={true}
                  prefix={'$'}
                  decimalScale={2}
                  fixedDecimalScale={this.caculate() > 0 ? true : false}
                  displayType={'text'}
              />
              <span className="description">in total of refund</span>
            </div>
            <div className="note">
              <div className="label">
                <span>Note</span>
              </div>
              <div className="modify-item">
                <TextareaCounter
                    placeholder={'Describe the reason why you refund'}
                    rows={8}
                    maxLength={50}
                    value={this.state.reason}
                    onChange={(e) => {
                      e.preventDefault();
                      this.setState({ reason: e.target.value });
                    }}/>
              </div>
            </div>
            <div className="lower">
              <GhostButton title={'Cancel'} className={'btn-ghost cancel'} onClick={this.props.closeModal}/>
              <PurpleRoundButton disabled={this.disableButton()} title={'Refund'} className={'btn-purple-round submit'}
                                 onClick={() => {
                                   this.props.refund(this.state.refund, this.state.refundType, this.state.reason);
                                 }}/>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}