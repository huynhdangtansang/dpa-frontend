import React from "react";
import './style.scss';

export default class Rating extends React.Component {
  rateStarClass = (number) => {
    if (this.props.rate >= number) {
      return 'star active';
    }
    return 'star';
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <div className="estimate-content rating">
          <div className="rating-point">
            <div className={this.rateStarClass(1)}>
              <span className="icon-star-full"></span>
            </div>
            <div className={this.rateStarClass(2)}>
              <span className="icon-star-full"></span>
            </div>
            <div className={this.rateStarClass(3)}>
              <span className="icon-star-full"></span>
            </div>
            <div className={this.rateStarClass(4)}>
              <span className="icon-star-full"></span>
            </div>
            <div className={this.rateStarClass(5)}>
              <span className="icon-star-full"></span>
            </div>
          </div>
          <div className="commend">
            <div className="row">
              <div className="col-md-9 user">
                <div className="avatar">
                  <img className="avatar" src={this.props.avatar} alt='avatar'/>
                </div>
                <div className="user-info">
                  <div className="user-name">
                    <span>{this.props.clientName}</span>
                  </div>
                  <div className="comment">
                    <span>{this.props.comment}</span>
                  </div>
                </div>
              </div>
              <div className="col-md-3 time">
                <span>{this.props.time}</span>
              </div>
            </div>
          </div>
        </div>
    )
  }
}