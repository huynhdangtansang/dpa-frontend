/**
 *
 * Asynchronously loads the component for ClientPages
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
