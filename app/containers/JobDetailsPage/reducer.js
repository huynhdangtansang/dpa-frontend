/*
 *
 * JobDetailsPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  ADD_NEW_ACT,
  CHANGE_APPOINTMENT,
  CHANGE_SPECIFIC_SELECTION,
  CHANGE_TAB,
  GET_ACTS,
  GET_ACTS_ERROR,
  GET_ACTS_SUCCESS,
  GET_COMPANIES_SUCCESS,
  GET_EMPLOYEES_SUCCESS,
  GET_SUCCESS,
  REASSIGN_OBJECT,
  RESET_STATE,
  SHOW_MODAL
} from "./constants";

export const initialState = fromJS({
  currentTab: '1',
  job: {},
  objectType: 'company',
  activitiesList: [],
  selectedCompany: '',
  companiesList: [],
  selectedEmployee: '',
  employeesList: [],
  showReassignModal: false,
  showCancelJobConfirm: false,
  showRescheduleModal: false,
  showReassignSpecificModal: false,
  showRefundModal: false,
  selectedAppointment: '',
  paid: 0,
  apiErrors: []
});

function jobDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case RESET_STATE:
      return initialState;
    case CHANGE_TAB:
      return state
          .set('currentTab', action.tab);
    case GET_SUCCESS: {
      let { job } = action.response.data;
      return state
          .set('job', job)
          .set('paid', job.paid ? job.paid : 0)
          .set('selectedAppointment', job.appointmentTime);
    }
    case REASSIGN_OBJECT:
      return state
          .set('objectType', action.object);
    case SHOW_MODAL:
      if (action.modal === 'reassign') {
        return state.set('showReassignModal', action.value);
      } else if (action.modal === 'cancelJob') {
        return state.set('showCancelJobConfirm', action.value);
      } else if (action.modal === 'reschedule') {
        return state
            .set('showRescheduleModal', action.value)
            .set('selectedAppointment', state.get('job').appointmentTime);
      } else if (action.modal === 'reassignSpecific') {
        return state.set('showReassignSpecificModal', action.value);
      } else if (action.modal === 'refund') {
        return state.set('showRefundModal', action.value);
      }
      return state;
    case GET_ACTS:
    case GET_ACTS_ERROR:
      return state
          .set('activitiesList', fromJS([]));
    case GET_ACTS_SUCCESS:
      return state
          .set('activitiesList', fromJS(action.response.data))
          .set('showReassignModal', false)
          .set('showCancelJobConfirm', false)
          .set('showRescheduleModal', false)
          .set('showReassignSpecificModal', false);
    case GET_COMPANIES_SUCCESS:
      return state
          .set('companiesList', action.response.data);
    case GET_EMPLOYEES_SUCCESS:
      return state
          .set('employeesList', action.response.data.data);
    case CHANGE_SPECIFIC_SELECTION:
      if (action.selector === 'company') {
        return state
            .set('selectedCompany', action.value)
      } else {
        return state
            .set('selectedEmployee', action.value)
      }
    case ADD_NEW_ACT:
      if (action.dateType === 'new') {
        let actsList = [];
        actsList.push(action.actData);
        let newDate = {
          day: action.date,
          job: actsList
        };
        return state
            .updateIn(['activitiesList'], fromJS(arr => arr.unshift(fromJS(newDate))));
      } else {
        return state
            .updateIn(['activitiesList', 0, 'job'], fromJS(arr => arr.unshift(fromJS(action.actData))));
      }
    case CHANGE_APPOINTMENT:
      return state
          .set('selectedAppointment', action.value);
    default:
      return state;
  }
}

export default jobDetailsPageReducer;
