/*
 *
 * PaymentDetails actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function getPaymentDetails(id, loadSpinner) {
  return {
    type: Constants.GET_PAYMENT_DETAILS,
    id,
    loadSpinner
  };
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response: response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_ERROR,
    response: response
  }
}

export function addImage(newImage) {
  return {
    type: Constants.ADD_IMAGE,
    newImage
  }
}

export function removeImage(id) {
  return {
    type: Constants.REMOVE_IMAGE,
    id
  }
}

export function removePayment(deleteData) {
  return {
    type: Constants.REMOVE_PAYMENT,
    deleteData
  }
}

export function addNote(id, noteData) {
  return {
    type: Constants.ADD_NOTE,
    id,
    noteData
  }
}

export function deleteNote(paymentId, noteId) {
  return {
    type: Constants.DELETE_NOTE,
    paymentId,
    noteId
  }
}

export function getPdf(paymentId) {
  return {
    type: Constants.GET_PDF,
    paymentId
  }
}