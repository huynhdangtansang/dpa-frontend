/**
 *
 * Asynchronously loads the component for ServicesListPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
