import React from "react";
import moment from "moment";
import _ from "lodash";
import { urlLink } from 'helper/route';
import './style.scss';

export default class ActivitiesCompany extends React.Component {
  actContent = (act) => {
    let content = !_.isEmpty(act.content) ? act.content : '';
    let type = !_.isEmpty(act.typeActivity) ? act.typeActivity : '';
    let ipAddress, memberName, adminCompanyName, userName, serviceName, jobName = '';
    let serviceId, memberId, adminCompanyId, jobId;
    let serviceLink, memberLink, adminCompanyLink, jobLink;
    switch (type) {
      case 'memberLogin':
        ipAddress = !_.isEmpty(act.ipAddress) ? act.ipAddress : '';
        content = content.replace('{ip}', `<span class="highlight">${ipAddress}</span>`);
        memberName = !_.isEmpty(act.owner) ? act.owner.fullName : '';
        memberId = _.isObject(act.owner) ? act.owner._id : '';
        memberLink = urlLink.employee + `?id=${memberId}`;
        content = content.replace('{memberName}', `<span class="highlight" onclick=" hyperlink('${memberLink}');">${memberName}</span>`);
        break;
      case 'createMember':
      case 'updateMember':
      case 'deleteMember':
        userName = !_.isEmpty(act.userId) ? act.userId.fullName : '';
        memberId = _.isObject(act.userId) ? act.userId._id : '';
        memberLink = urlLink.viewMember + `?id=${memberId}`;
        content = content.replace('{userName}', `<span class="highlight" onclick=" hyperlink('${memberLink}');">${userName}</span>`);
        if (!_.isUndefined(act.typeView) && act.typeView === 'master') {
          memberName = !_.isEmpty(act.owner) ? act.owner.fullName : '';
          content = content.replace('{memberName}', `<span class="highlight">${memberName}</span>`);
        } else {//company
          adminCompanyName = !_.isEmpty(act.owner) ? act.owner.fullName : '';
          adminCompanyId = _.isObject(act.owner) ? act.owner._id : '';
          adminCompanyLink = urlLink.employee + `?id=${adminCompanyId}`;
          content = content.replace('{memberName}', `<span class="highlight" onclick=" hyperlink('${adminCompanyLink}');">${adminCompanyName}</span>`);
        }
        break;
      case 'updateService':
        serviceName = (_.isObject(act.serviceId) && act.serviceId.name) ? act.serviceId.name : '';
        userName = (_.isObject(act.owner) && act.owner.fullName) ? act.owner.fullName : '';
        serviceId = _.isObject(act.serviceId) ? act.serviceId._id : '';
        serviceLink = urlLink.viewService + `?id=${serviceId}`;
        content = content.replace('{serviceName}', `<span class="highlight" onclick="hyperlink('${serviceLink}');">${serviceName}</span>`);
        content = content.replace('{userName}', `<span class="highlight">${userName}</span>`);
        break;
      case 'updateJob':
      case 'createJob':
        userName = (_.isObject(act.owner) && act.owner.fullName) ? act.owner.fullName : '';
        jobName += (_.isObject(act.jobId) && act.jobId.name) ? act.jobId.name + '#' : '';
        jobName += (_.isObject(act.jobId) && act.jobId.jobId) ? act.jobId.jobId : '';
        jobId = (_.isObject(act.jobId) && act.jobId._id) ? act.jobId._id : '';
        jobLink = urlLink.viewJob + `?id=${jobId}`;
        content = content.replace('{userName}', `<span class="highlight">${userName}</span>`);
        content = content.replace('{jobName}#{jobId}', `<span class="highlight" onclick="hyperlink('${jobLink}');">${jobName}</span>`);
        break;
      default:
        // code block
    }
    return content;
  };
  iconClassName = (icon) => {
    let iconReplace = '';
    switch (icon) {
      case 'member':
        iconReplace = 'icon-client';
        break;
      case 'team':
        iconReplace = 'icon-team';
        break;
      case 'service':
        iconReplace = 'icon-service';
        break;
      case 'job':
        iconReplace = ' icon-job';
        break;
      default:
        iconReplace = icon;
    }
    return iconReplace;
  };

  constructor(props) {
    super(props);
  }

  render() {
    const act = this.props.activity;
    return (
        <div className="item-activity">
          <div className="avatar-activity">
            <span className={this.iconClassName(act.icon)}></span>
          </div>
          <div className="info">
            <div className="category">{act.notifyBy}</div>
            <div className="description" dangerouslySetInnerHTML={{ __html: this.actContent(act) }}>
            </div>
            <div className="time">{moment(act.createdAt).format('HH:mm A')}</div>
          </div>
        </div>
    )
  }
}