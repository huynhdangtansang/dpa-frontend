/**
 *
 * JobsListPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
//Lib
import _, { findIndex } from 'lodash';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectJobsListPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  changeCompany,
  changePage,
  changeStatusFilter,
  changeStoreValue,
  getCompanies,
  getList,
  removeJob,
  resetState,
  selectAllJob,
  selectJob,
  showModal,
  updateSearchCompanyData,
  updateSearchData
} from "./actions";
//Lib
import { DropdownItem, DropdownMenu, Input } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from "moment";
import classnames from 'classnames';
import NumberFormat from 'react-number-format';
import './style.scss';
//Component
import GhostButton from 'components/GhostButton';
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
import debounce from 'components/Debounce';
import Checkbox from 'components/Checkbox';
import MultiSelectDropdown from 'components/MultiSelectDropdown';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { urlLink } from 'helper/route';
import { getEvent } from 'helper/socketConnection';

/* eslint-disable react/prefer-stateless-function */
export class JobsListPage extends React.PureComponent {
  //Table Formatter
  serviceTypeFormat = (cell, row) => {
    return (
        <div className="service-type" onClick={() => {
          this.props.history.push({
            pathname: urlLink.viewJob,
            search: '?id=' + row._id + '&tab=1'
          })
        }}>
          <span>{!_.isUndefined(cell) ? cell : '-'}</span>
        </div>
    )
  };
  actionFormat = (cell, row) => {
    const editMenu = (
        <DropdownMenu>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.viewJob + '?id=' + row._id);
          }}>View Job</DropdownItem>
          <DropdownItem onClick={() => {
            row.createdBy ? this.props.history.push(urlLink.viewClient + '?id=' + row.createdBy._id) : this.props.history.push(urlLink.clients)
          }}>View Client Profile</DropdownItem>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.editJob + '?id=' + row._id);
          }}>Edit Jobs Description</DropdownItem>
        </DropdownMenu>
    );
    return (
        <ActionFormatter menu={editMenu}/>
    );
  };
  appointmentFormat = (cell, row) => {
    let endTime = moment(cell).add(row.serviceInfo ? row.serviceInfo.minHours.value : 0, 'h');
    if (endTime.isAfter(cell, 'day')) {
      return (
          <div className="appointment">
            <div>
              <span className="time">{moment(cell).format('LT | ddd D MMM YYYY')}</span>

            </div>
            <div>
              <span className="time">{moment(endTime).format('LT | ddd D MMM YYYY')} </span>

            </div>
          </div>
      )
    } else {
      return (
          <div className="appointment">
            <div className="time">{moment(cell).format('h:mm A') + ' - ' + endTime.format('h:mm A')}</div>
            <div className="date">{moment(cell).format('ddd D MMM YYYY')}</div>
          </div>
      )
    }
  };
  statusFormat = (cell) => {
    return (
        <div className="content active">{cell ? cell : '-'}</div>
    )
  };
  clientFormat = (cell) => {
    return (
        <div className="assigned">
          {cell ?
              <img key={cell._id}
                   onClick={() => this.props.history.push(urlLink.viewClient + '?id=' + cell._id)}
                   onError={(e) => {
                     e.target.onerror = null;
                     e.target.src = './default-user.png'
                   }}
                   className="image" src={cell.avatar ? cell.avatar.fileName : './default-user.png'}
                   alt="image"/> :
              <div className="undefined">
                <span className="icon-minus"></span>
              </div>
          }
        </div>
    )
  };
  assignFormat = (cell, row) => {
    return (
        <div className="assigned" key={row._id}>
          <div className="company">
            {row.companyId ?
                <img onClick={() => this.props.history.push(urlLink.viewCompany + '?id=' + row.companyId._id)}
                     onError={(e) => {
                       e.target.onerror = null;
                       e.target.src = './default-user.png'
                     }}
                     className="logo-company"
                     src={row.companyId.logo.fileName}
                     alt="company"/> :
                <div className="undefined">
                  <span className="icon-minus"></span>
                </div>
            }
          </div>
          <div className="provider">
            {row.assignBy ?
                <img onClick={() => this.props.history.push(urlLink.employee + '?id=' + row.assignBy._id)}
                     onError={(e) => {
                       e.target.onerror = null;
                       e.target.src = './default-user.png'
                     }}
                     className="logo-provider"
                     src={row.assignBy.avatar.fileName}
                     alt="provider"/> :
                <div className="undefined">
                  <span className="icon-minus"></span>
                </div>
            }
          </div>
        </div>
    )
  };
  priceFormat = (cell) => {
    return (
        <NumberFormat
            className={'price'}
            value={cell && !_.isUndefined(cell.total) ? cell.total : 0}
            className="price"
            displayType={'text'}
            prefix={'$'}
            decimalScale={2}
            fixedDecimalScale={cell && cell.minCharge.value > 0 ? true : false}
        />
    )
  };
  onRowSelect = (row, isSelected) => {
    let selectItem = row._id;
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  //Search and Filter
  handleSearchChange = debounce(value => {
    const { searchData } = this.props.jobslistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.search = value;
    this.props.changePage(1);
    this.props.updateSearchData(temp);
  }, 700);
  handleInsuranceCheck = (value) => {
    const { searchData } = this.props.jobslistpage;
    let temp = searchData;
    temp.insurance = value;
    temp.offset = 0;
    this.props.updateSearchData(temp);
  };
  sortData = (field, order) => {
    const { searchData } = this.props.jobslistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.sort = field;
    temp.sortType = order;
    if (field === 'location') {
      temp.sort = 'suburb';
    }
    if (field === 'createdBy') {
      temp.sort = 'client';
    }
    if (field === 'estimation') {
      temp.sort = 'value';
    }
    this.props.changePage(1);
    this.props.updateSearchData(temp);
  };
  changePagination = (type) => {
    const { currentPage, searchData, pagination } = this.props.jobslistpage;
    let newSearchData = searchData;
    let limit = pagination.limit;
    let newPage, newOffset;
    if (type === 'prev') {
      newPage = currentPage - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
      this.props.changePage(newPage);
    } else {
      newPage = currentPage + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
      this.props.changePage(newPage);
    }
    this.actionAfterChangePage(newSearchData);
  };
  actionAfterChangePage = debounce(searchData => {
    this.props.updateSearchData(searchData);
  }, 700);
  handleCompanyChange = (companyName) => {
    const { searchData } = this.props.jobslistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.company = companyName;
    this.props.updateSearchCompanyData(temp);
    if (companyName !== '') {
      this.props.changeCompany(companyName);
    } else {
      this.props.changeCompany('All');
    }
  };
  //Handle status change
  onStatusChange = (option) => {
    const { statusList, searchData } = this.props.jobslistpage;
    if (option.value === '') {//option all
      this.props.onChangeStoreData({ key: ['searchData', 'status'], value: [statusList[0]] });
    } else {
      if (findIndex(searchData.status, item => item.value === option.value) > -1) {
        this.props.onChangeStoreData({
          key: ['searchData', 'status'],
          value: searchData.status.filter(item => item.value !== option.value)
        });
      } else {
        searchData.status.push(option);
        this.props.onChangeStoreData({
          key: ['searchData', 'status'],
          value: searchData.status.filter(item => item.value !== '')
        });
      }
    }
    this.getListAfterChangeData('job');
  };
  //Handle employee change
  onCompanyChange = (option) => {
    const { companiesList, searchData } = this.props.jobslistpage;
    if (option.value === '') {//option all
      this.props.onChangeStoreData({ key: ['searchData', 'company'], value: [companiesList[0]] });
    } else {
      if (findIndex(searchData.company, val => val.value === option.value) > -1) {
        this.props.onChangeStoreData({
          key: ['searchData', 'company'],
          value: searchData.company.filter(val => val.value !== option.value)
        });
      } else {
        searchData.company.push(option);
        this.props.onChangeStoreData({
          key: ['searchData', 'company'],
          value: searchData.company.filter(val => val.value !== '')
        });
      }
    }
    this.getListAfterChangeData('job');
  };
  handleChangeSearchCompany = debounce(searchText => {
    const { searchDataForCompany } = this.props.jobslistpage;
    let newSearchDataForCompany = searchDataForCompany;
    newSearchDataForCompany.search = searchText;
    newSearchDataForCompany.limit = 10;
    this.props.updateSearchCompanyData(newSearchDataForCompany);
  }, 700);
  getCompaniesNextPage = () => {
    const { searchDataForCompany, companyPagination } = this.props.jobslistpage;
    const limit = companyPagination.limit;
    const total = companyPagination.total;
    let temp = searchDataForCompany;
    if (limit < total) {
      let newLimmit = limit + 5;
      temp.limit = newLimmit;
      this.props.updateSearchCompanyData(temp);
    }
  };
  //Get list after change search data
  getListAfterChangeData = debounce((key) => {
    if (key === 'job') {
      const { searchData } = this.props.jobslistpage;
      let newSearchData = searchData;
      newSearchData.offset = 0;
      this.props.changePage(1);
      this.props.getList(newSearchData);
    }
    if (key === 'company') {
      const { searchDataForCompany } = this.props.jobslistpage;
      this.props.getCompanies(searchDataForCompany);
    }
  }, 700);

  constructor(props) {
    super(props);
    this.state = {
      reason: ''
    };
    getEvent('notification:newJob', () => {
      let url = window.location.hash;
      url = url.replace('#', '');
      if (url === urlLink.jobs) {
        const { searchData } = this.props.jobslistpage;
        this.props.getList(searchData, false);
      }
    });
    getEvent('notification:deleteJob', () => {
      let url = window.location.hash;
      url = url.replace('#', '');
      if (url === urlLink.jobs) {
        const { searchData } = this.props.jobslistpage;
        this.props.getList(searchData, false);
      }
    });
  }

  componentWillMount() {
    const { searchData, searchDataForCompany } = this.props.jobslistpage;
    this.props.selectAll(false);
    this.props.getList(searchData, true);
    this.props.getCompanies(searchDataForCompany);
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const {
      jobsList, pagination, selectedJobs, statusList, currentPage,
      companiesList,
      showRemoveConfirm,
      searchData
    } = this.props.jobslistpage;
    const options = {
      defaultSortName: 'jobId',
      defaultSortOrder: 'desc',
      withoutNoDataText: true,
      sortIndicator: false,
      onSortChange: this.sortData
    };
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    return (
        <div className="jobs-list">
          <div className="header-list-page jobs-list-header">
            <div className="form-inline align-items-center row-item">
              <PurpleAddRoundButton className={'btn-add-purple-round'} onClick={() => {
                this.props.history.push(urlLink.addJob)
              }} title={'New Job'} iconClassName={'icon-plus'}/>
              <div className="search-bar jobs-search">
                <Input placeholder="Search Service Type, ID, Surburb, Client, Company, Provider"
                       onChange={(e) => {
                         this.handleSearchChange(e.target.value);
                       }}
                />
                <span className="icon-search" onClick={(e) => {
                  e.preventDefault();
                  this.props.changePage(1);
                  this.props.updateSearchCompanyData(searchData);
                }}></span>
              </div>

              <TotalFormatter
                  offset={pagination.offset}
                  limit={pagination.limit}
                  total={pagination.total}
                  page={currentPage}
                  changePagination={this.changePagination}
              />

            </div>
            <div className="form-inline align-items-center row-item">
              <MultiSelectDropdown
                  title={'Status'}
                  value={searchData.status}
                  options={_.isEmpty(statusList) ? [] : statusList}
                  onSelect={(option) => {
                    this.onStatusChange(option);
                  }}
              />

              <MultiSelectDropdown
                  title={'Company'}
                  searchOption={true}
                  handleScrollBottom={() => {
                    this.getCompaniesNextPage();
                  }}
                  handleSearchChange={(searchText) => {
                    this.handleChangeSearchCompany(searchText);
                  }}

                  value={searchData.company}
                  options={companiesList}
                  onSelect={(option) => {
                    this.onCompanyChange(option);
                  }}
              />

              <div className="insurance-included">
                <Checkbox name={'insuranceIncluded'}
                          label={'Insurance included'}
                          onChange={(e) => {
                            this.handleInsuranceCheck(e.target.checked);
                          }}
                />
              </div>
            </div>
          </div>
          <div
              className={classnames("content content-list-page jobs-management-content", selectedJobs.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_job"
                data={jobsList}
                options={options}
                selectRow={selectRowProp}
                bordered={false}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'
                trClassName={this.trClassFormat}>
              <TableHeaderColumn dataField="_id" isKey hidden>Job ID</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="name" width={164 / 14 + 'rem'} dataFormat={this.serviceTypeFormat}
                                 columnClassName="name">Service Type</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="jobId" dataAlign={'center'}>Job ID</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="location" dataFormat={(cell) => {
                return !_.isEmpty(cell.suburb) ? cell.suburb : '-'
              }}>Suburb</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="appointmentTime"
                                 dataFormat={this.appointmentFormat}>Appointment</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="createdBy" width={96 / 14 + 'rem'} dataFormat={this.clientFormat}
                                 dataAlign={'center'}>Client</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="status" dataFormat={this.statusFormat}
                                 columnClassName="status status-job">Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="assigned" width={106 / 14 + 'rem'}
                                 dataFormat={this.assignFormat}>Assigned</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="estimation" width={98 / 14 + 'rem'} headerAlign={'left'}
                                 dataFormat={(cell, row) => this.priceFormat(cell, row)}>Value</TableHeaderColumn>
              <TableHeaderColumn dataField='data' width={84 / 14 + 'rem'} dataFormat={this.actionFormat}
                                 columnClassName={'edit-dropdown'} dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>
          {selectedJobs.length > 0 &&
          <div className="footer jobs-list-footer">
            <GhostButton title={'Remove Job'} className="btn-ghost btn-remove" onClick={() => {
              this.setState({ reason: '' });
              if (selectedJobs.length > 1)
                this.setState({ modifyType: 'multi' }, () => {
                  this.props.showModal('remove', true);
                });
              else
                this.setState({ modifyType: 'single' }, () => {
                  this.props.showModal('remove', true);
                })
            }}/>
          </div>
          }

          <RemoveConfirmModal
              show={showRemoveConfirm}
              reason={this.state.reason}
              object={'job'}
              action={this.state.modifyType}
              handleChange={(e) => {
                this.setState({ reason: e });
              }}
              closeModal={() => {
                this.props.showModal('remove', false);
              }}
              removeObject={() => {
                this.props.showModal('remove', false);
                this.props.removeJob(selectedJobs, searchData);
                this.refs.table_job.cleanSelected();  // this.refs.table is a ref for BootstrapTable
              }}/>
        </div>
    );
  }
}

JobsListPage.propTypes = {
  dispatch: PropTypes.func,
  resetState: PropTypes.func,
  getList: PropTypes.func,
  updateSearchData: PropTypes.func,
  changePage: PropTypes.func,
  select: PropTypes.func,
  selectAll: PropTypes.func,
  changeStatusFilter: PropTypes.func,
  getCompanies: PropTypes.func,
  changeCompany: PropTypes.func,
  removeJob: PropTypes.func,
  showModal: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  jobslistpage: makeSelectJobsListPage()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(resetState());
    },
    getList: (dataSearch, loading) => {
      dispatch(getList(dataSearch, loading));
    },
    updateSearchData: (key, newSearchData) => {
      dispatch(updateSearchData(key, newSearchData));
    },
    updateSearchCompanyData: (key, value) => {
      dispatch(updateSearchCompanyData(key, value));
    },
    changePage: (newPage) => {
      dispatch(changePage(newPage));
    },
    select: (isSelected, selectItem) => {
      dispatch(selectJob(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(selectAllJob(isSelected));
    },
    changeStatusFilter: (key, id) => {
      dispatch(changeStatusFilter(key, id));
    },
    getCompanies: (searchData) => {
      dispatch(getCompanies(searchData));
    },
    changeCompany: (companyName) => {
      dispatch(changeCompany(companyName));
    },
    removeJob: (jobIds, searchData) => {
      dispatch(removeJob(jobIds, searchData));
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    onChangeStoreData(event) {
      dispatch(changeStoreValue(event));
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "jobsListPage", reducer });
const withSaga = injectSaga({ key: "jobsListPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(JobsListPage);
