/**
 *
 * Asynchronously loads the component for CompanyTabControl
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
