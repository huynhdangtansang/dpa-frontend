/*
 *
 * UserProfilePage reducer
 *
 */
import { fromJS } from "immutable";
import {
  CHANGE_AVATAR,
  CHANGE_IS_SENT,
  CHANGE_UPDATE_STATE,
  DEFAULT_ACTION,
  GET_SUCCESS,
  RESET_ERROR,
  SEND_CODE_ERROR,
  SEND_CODE_SUCCESS,
  SET_EDIT_MODE,
  SET_VALUE_ERRORS,
  UPDATE_ERROR,
  UPDATE_SUCCESS
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  userData: {},
  edit: false,
  avatar: './default-user.png',
  isUpdated: false,
  verifyErrors: [],
  updateErrors: [],
  isSent: false
});

function userProfilePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case RESET_ERROR:
      return state.set('updateErrors', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('userData', action.response.data)
          .set('avatar', action.response.data.avatar ? action.response.data.avatar.fileName : state.toJS().avatar);
    case SET_EDIT_MODE:
      return state
          .set('edit', action.value);
    case CHANGE_AVATAR:
      return state
          .set('avatar', action.url);
    case UPDATE_SUCCESS:
      return state
          .set('isUpdated', true)
          .set('phoneNotVerify', action.response.data.isSendPhone)
          .set('updateErrors', action.response.errors);
    case UPDATE_ERROR:
      return state
          .set('isUpdated', false)
          .set('updateErrors', action.response.errors);
    case CHANGE_UPDATE_STATE:
      return state
          .set('isUpdated', action.value);
    case CHANGE_IS_SENT:
      return state
          .set('isSent', action.value);
    case SEND_CODE_SUCCESS:
      return state
          .set('isSent', true)
          .set('verifyErrors', action.response.errors);
    case SEND_CODE_ERROR:
      return state
          .set('isSent', false)
          .set('verifyErrors', action.response.errors);
    case SET_VALUE_ERRORS:
      return state
          .set(action.key, action.value);
    default:
      return state;
  }
}

export default userProfilePageReducer;
