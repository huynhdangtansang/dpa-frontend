/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */
import React from 'react';
import { HashRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { makeSelectGlobalData, makeSelectLoading } from "./selectors";
import { updateError } from "./actions";
import { compose } from "redux";
import { Scrollbars } from 'react-custom-scrollbars';
import LoginPage from 'containers/LoginPage/Loadable';
import ForgotPassword from 'containers/ForgotPasswordPage/Loadable';
import ResetPassword from 'containers/ResetPasswordPage/Loadable';
import ResetPasswordSuccessPage from 'containers/ResetPasswordSuccessPage/Loadable';
import SetupPasswordPage from 'containers/SetupPasswordPage/Loadable';
import SecurityPage from 'containers/SecurityPage/Loadable';
import DashBoardPage from 'containers/DashBoardPage/Loadable';
import ClientsListPage from 'containers/ClientsListPage/Loadable';
import DevicesListPage from 'containers/DevicesListPage/Loadable';
import PrivateRoute from 'components/PrivateRoute';
import LoadingIndicator from 'components/LoadingIndicator';
import ErrorPopup from 'components/ErrorPopup';
import { urlLink } from 'helper/route';

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  render() {
    const isRemember = localStorage.getItem('rememberMe');
    const token = localStorage.getItem('token');
    const { loading } = this.props;
    const { globalError } = this.props.globalData;
    return (
        <Scrollbars universal
            // This will activate auto hide
                    autoHide
            // Hide delay in ms
                    autoHideTimeout={1000}
                    ref={this.myRef}
        >
          <div className="master-admin">
            <Router>
              <Switch>
                <Route exact path="/" render={(props) => (
                    (token && (isRemember === true)) || token ? (
                        <Redirect to={urlLink.statistics}/>
                    ) : (
                        <LoginPage {...props} />
                    )
                )}/>
                <Route path={urlLink.forgotPassword} component={ForgotPassword}/>
                <Route path={urlLink.resetPassword} component={ResetPassword}/>
                <Route path={urlLink.resetPasswordSuccess} component={ResetPasswordSuccessPage}/>
                <Route path={urlLink.setupPassword} component={SetupPasswordPage}/>
                <Route path={urlLink.verifyCode} component={SecurityPage}/>
                <Route path={urlLink.dashboard} component={DashBoardPage}/>
                <Route path={urlLink.clients} component={ClientsListPage}/>
                <Route path={urlLink.devices} component={DevicesListPage}/>
                {/* <PrivateRoute path={urlLink.dashboard} component={DashBoardPage} scrollBarRef={this.myRef}/> */}
              </Switch>
            </Router>
            {loading == true && (<LoadingIndicator/>)}

            <ErrorPopup title={globalError.title}
                        visible={globalError.error}
                        content={globalError.message}
                        btnText={'OK'}
                        className={'float-right'}
                        onSubmit={() => {
                          this.props.updateError({
                            error: false,
                            title: globalError.title,
                            message: globalError.message
                          })
                        }}/>
          </div>
        </Scrollbars>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  globalData: makeSelectGlobalData(),
});

function mapDispatchToProps(dispatch) {
  return {
    updateError(data) {
      dispatch(updateError(data));
    }
  }
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
export default compose(
    withConnect
)(App);
