import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the deviceNewItemPage state domain
 */

const selectDeviceNewItemPageDomain = state =>
    state.get("deviceNewItemPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by DeviceNewItemPage
 */

const makeSelectDeviceNewItemPage = () =>
    createSelector(selectDeviceNewItemPageDomain, substate => substate.toJS());
const makeSelectErrors = () =>
    createSelector(selectDeviceNewItemPageDomain, state => state.get('errors'));
export { makeSelectDeviceNewItemPage, makeSelectErrors };
export { selectDeviceNewItemPageDomain };
