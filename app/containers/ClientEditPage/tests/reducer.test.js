import { fromJS } from 'immutable';
import clientEditPageReducer from '../reducer';

describe('clientEditPageReducer', () => {
  it('returns the initial state', () => {
    expect(clientEditPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
