/**
 *
 * Asynchronously loads the component for CompanyTabEmployee
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
