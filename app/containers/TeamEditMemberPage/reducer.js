/*
 *
 * TeamEditMemberPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  CHANGE_AVATAR,
  DEFAULT_ACTION,
  GET_ERROR,
  GET_MEMBER_DETAILS,
  GET_SUCCESS,
  RESET_ERROR,
  UPDATE_ERROR,
  UPDATE_SUCCESS
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  memberDetails: {},
  apiErrors: [],
  avatar: './default-user.png'
});

function teamEditMemberPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
    case GET_MEMBER_DETAILS:
      return initialState;
    case GET_SUCCESS:
      return state
          .set('memberDetails', action.response.data)
          .set('apiErrors', action.response.errors)
          .set('avatar', action.response.data.avatar ? action.response.data.avatar.fileName : state.avatar);
    case GET_ERROR:
      return state
          .set('memberDetails', action.response.data)
          .set('apiErrors', action.response.errors);
    case UPDATE_SUCCESS:
    case UPDATE_ERROR:
      return state
          .set('apiErrors', action.response.errors);
    case RESET_ERROR:
      return state
          .set('apiErrors', []);
    case CHANGE_AVATAR:
      return state
          .set('avatar', action.src);
    default:
      return state;
  }
}

export default teamEditMemberPageReducer;
