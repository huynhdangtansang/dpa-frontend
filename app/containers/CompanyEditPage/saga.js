/* eslint-disable no-console */
// import { take, call, put, select } from 'redux-saga/effects';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import config from 'config';
import axios from 'axios';
import _ from "lodash";
import { Map } from "immutable";
import {
  getAllDataSuccess,
  getServiceSuccess,
  getSuccess,
  updateListAcceptRateCard,
  updateListLicense,
  updateServiceInStore
} from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';

export function* apiGetAllData(data) {
  if (data.id) {
    yield put(loadRepos());
    const responseCompanyData = yield apiGetCompany(data);
    yield apiGetService({ companyData: responseCompanyData.data, searchDataForService: data.searchDataForService });
  }
}

export function* apiGetCompany(data) {
  if (data.id) {
    const requestUrl = config.serverUrl + config.api.company.companies_details + data.id;
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(getSuccess(response.data));
      return response.data;
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      return error;
    }
  }
}

export function* apiGetService(data) {
  if (data.searchDataForService) {
    let params = data.searchDataForService;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => k + '=' + esc(params[k]))
      .join('&');
    const requestUrl = config.serverUrl + config.api.statistic.serviceListDropdown + '?' + query;
    //yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);

      yield put(getServiceSuccess(response.data.data.data));
      //yield put(reposLoaded());
      yield apiProcessMyServiceData(data.companyData, response.data.data.data);
      return response.data.data.data;
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      //yield put(reposLoaded());
      return error;
    }
  }
}

function updateInfo(data) {
  if (data.data.formDataInfo) {
    const requestUrl = config.serverUrl + config.api.company.companies_details + data.id + '/updateInfoCompany';
    return axios.put(requestUrl, data.data.formDataInfo);
  }
}

function updateBank(data) {
  if (data.data.objectBank) {
    const requestUrlBank = config.serverUrl + config.api.company.companies_details + data.id + '/updateBankDetailCompany';
    return axios.put(requestUrlBank, data.data.objectBank);
  }
}

function updateServices(data) {
  if (data.data.objectService) {
    const requestUrlService = config.serverUrl + config.api.company.companies_details + data.id + '/updateServiceCompany';
    return axios.put(requestUrlService, data.data.objectService);
  }
}

function updateAcceptRateCard(data) {
  if (data.data.objectAcceptRateCard) {
    const requestUrlAccepRateCard = config.serverUrl + config.api.company.companies_details + data.id + '/updateAcceptRateCard';
    return axios.put(requestUrlAccepRateCard, data.data.objectAcceptRateCard);
  }
}

function updateArea(data) {
  if (data.data.objectRegionHours) {
    const requestUrlArea = config.serverUrl + config.api.company.companies_details + data.id + '/updateAreaCompany';
    return axios.put(requestUrlArea, data.data.objectRegionHours);
  }
}

function* updateOneItemLicense(companyID, formdata) {
  if (formdata) {
    const requestUrl = config.serverUrl + config.api.company.companies_details + companyID + '/updateCertificationCompany';
    yield axios.put(requestUrl, formdata);
  }
}

export function* apiUpdateAllInfoCompany(data) {
  if (data) {
    const { resolve, reject } = data;
    yield put(loadRepos());

    // SEQUENCING SAGAS VIA YIELD*
    yield updateInfo(data).catch(error => {
      reject(error.response.data);
    });
    yield updateServices(data).catch(error => {
      reject(error.response.data);
    });
    yield updateAcceptRateCard(data).catch(error => {
      reject(error.response.data);
    });
    // yield* data.data.objectListLicenses.map(function* (item) {
    //   yield (updateOneItemLicense(data.id, item));
    // })
    if (!_.isEmpty(data.data.objectListLicenses)) {
      yield all(data.data.objectListLicenses.map((item) => call(updateOneItemLicense, data.id, item)));
    }
    yield updateBank(data).catch(error => {
      reject(error.response.data);
    });
    yield updateArea(data).catch(error => {
      reject(error.response.data);
    });
    yield put(reposLoaded());
    yield resolve(true);
  }
}

export function* apiProcessMyServiceData(companyData, serviceData) {
  let myServiceData = serviceData;
  //Process list service with field isCheck
  companyData.services.map(function (service) {
    myServiceData.map(function (item) {
      if (item._id === service._id) {
        item.isCheck = true;
      }
    })
  });
  myServiceData.map(function (item) {
    if (item && !item.hasOwnProperty('isCheck')) {
      item.isCheck = false;
    }
  });
  yield put(getAllDataSuccess(myServiceData));
}

export function* updateServiceAcceptRateCardLisenceList(data) {
  yield all([
    put(updateServiceInStore(data.newMyServiceData)),
    call(apiUpdateListAcceptRateCard, data),
    call(apiUpdateListliscense, data),
  ]);
}

export function* apiUpdateListAcceptRateCard(data) {
  if (data.serviceIds) {
    let params = data.serviceIds;
    const esc = encodeURIComponent;
    let query = Object.keys(params).map(
      function (keys) {
        return (params[keys]);
      }
    ).join('","');
    if (query)
      query = esc('["' + query + '"]');
    else
      query = esc('[]');
    const requestUrl = config.serverUrl + config.api.company.companies_details + data.companyID + '/getAcceptRateCards' + '?serviceIds=' + query;
    //yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      let newDataAcceptRateCard = response.data.data;
      newDataAcceptRateCard.map((item, index) => {
        if (!_.isEmpty(data.dataBackup.listAcceptRateCards[item.serviceId._id])) {
          //Update only in some field of object so need do it
          const one = Map(item);
          const two = Map(data.dataBackup.listAcceptRateCards[item.serviceId._id]);
          const newMerge = one.merge(two);
          //update this to data
          newDataAcceptRateCard[index] = newMerge.toJS();
        }
      });
      yield put(updateListAcceptRateCard(newDataAcceptRateCard));
      return response.data.data;
    } catch (error) {
      console.log(error);
      yield put(reposLoaded());
      return error;
    }
  }
}

export function* apiUpdateListliscense(data) {
  if (data.serviceIds) {
    let params = data.serviceIds;
    const esc = encodeURIComponent;
    let query = Object.keys(params).map(
      function (keys) {
        return (params[keys]);
      }
    ).join('","');
    if (query)
      query = esc('["' + query + '"]');
    else
      query = esc('[]');
    const requestUrl = config.serverUrl + config.api.company.companies_details + data.companyID + '/getLicenses' + '?serviceIds=' + query;
    //yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      //process with data backup
      let newDataListLicense = response.data.data;
      newDataListLicense.map((item, index) => {
        if (!_.isEmpty(data.dataBackup.listLicense[item.certificationId._id])) {
          //Update only in some field of object so need do it
          const one = Map(item);
          const two = Map(data.dataBackup.listLicense[item.certificationId._id]);
          const newMerge = one.merge(two);
          //update this to data
          newDataListLicense[index] = newMerge.toJS();
        }
      });
      yield put(updateListLicense(newDataListLicense));
      return response.data.data;
    } catch (error) {
      console.log(error);
      yield put(reposLoaded());
      return error;
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_ALL_DATA, apiGetAllData);
  yield takeLatest(Constants.GET_COMPANY_DATA, apiGetCompany);
  yield takeLatest(Constants.GET_SERVICE_DATA, apiGetService);
  yield takeLatest(Constants.UPDATE_COMPANY, apiUpdateAllInfoCompany);
  yield takeLatest(Constants.SELECT_SERVICE, updateServiceAcceptRateCardLisenceList);
}