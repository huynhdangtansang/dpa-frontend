import { put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetMessageList() {
  const requestUrl = config.serverUrl + config.api.botMessages + "/getAllMessages";
  yield put(loadRepos());
  try {
    const response = yield axios.get(requestUrl);
    yield put(Actions.getMessageListSuccess(response.data.data));
    yield put(reposLoaded());
  } catch (error) {
    yield put(reposLoaded());
    yield put(updateError({
      error: true,
      title: 'Error!!!',
      message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
    }));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_MESSAGE_LIST, apiGetMessageList);
}
