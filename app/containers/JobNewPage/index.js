/**
 *
 * JobNewPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectJobNewPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import * as AppActions from "containers/App/actions";
import "./style.scss";
//Lib
import { Formik } from 'formik';
import * as Yup from 'yup';
import moment from "moment";
import { Modal, ModalBody } from 'reactstrap';
import _ from 'lodash';
import { clearApiError } from 'helper/exportFunction';
//Components
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import SingleSelectDropdown from 'components/SingleSelectDropdown';
import TextareaCounter from 'components/TextareaCounter';
import ProofUpload from "components/ProofUpload";
import Location from 'components/Location';
import Datepicker from 'components/Datepicker';
import TimePicker from 'components/NewTimePicker';

const MAX_NUMBER = 4;
const MAX_FILE_SIZE = 2 * 1024 * 1024;//2MB
const validateForm = Yup.object().shape({
  'serviceName': Yup.string()
    .required(''),
  'clientName': Yup.string()
    .required(''),
  'location': Yup.string()
    .required(''),
});

export class JobNewPage extends React.PureComponent {
  errorMessage = (error) => {
    return (
      <div className={'error-text'}>
        <i className={'icon-error'} />
        <span>{error}</span>
      </div>
    )
  };

  constructor(props) {
    super(props);
    this.state = {
      errorUpload: false,
      showConfirmModal: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    //load data is serviceList, clientList, companyList
    this.props.getAllDataDropdown();
    //when click select company name, will continue load employee this company selected
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'serviceName',
      'clientName',
      'location'
    ];
    for (let key of keys) {
      if ((value[key] === null || error[key] || !value[key].toString())) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  _handleImageChange(e) {
    const { images } = this.props.jobnewpage;
    e.preventDefault();
    this.setState({ errorUpload: false });
    let currentSize = e.target.files.length + images.length;
    //upload over MAX_PROOFS images will not do
    if (0 <= currentSize && currentSize <= MAX_NUMBER) {
      Array.from(e.target.files).forEach(item => {
        let image = { file: '', fileName: '' };
        let reader = new FileReader();
        let file = item;
        let type = file.name.split('.');
        if (type[1] === 'jpeg' || type[1] === 'jpg' || type[1] === 'png' || type[1] === 'gif') {
          reader.onloadend = () => {
            //file size need lower than 2MB
            if (file.size <= MAX_FILE_SIZE) {
              image.file = file;
              image.fileName = reader.result;
              this.props.addImage(image);
            } else {
              this.props.showPopupError({
                error: true,
                title: 'Error!!!',
                message: `Invalid size (maximum ${MAX_FILE_SIZE / 1024 / 1024} each image)`
              });
            }
          };
          reader.readAsDataURL(file);
        } else {
          this.setState({ errorUpload: true });
          this.props.showPopupError({
            error: true,
            title: 'Error!!!',
            message: 'Invalid format image (jpeg, jpg, png, gif is valid)'
          });
        }
      });
      e.target.value = null;
    } else {
      this.setState({ errorUpload: true });
      this.props.showPopupError({
        error: true,
        title: 'Error!!!',
        message: `Invalid numbers of image (maximum ${MAX_NUMBER} images)`
      });
    }
  }

  render() {
    const {
      companyListDropdown, serviceListDropdown, clientListDropdown, employeeListDropdown,
      images, apiErrors,
      chosenService, chosenClient, chosenCompany, chosenEmployee,
      appointment, location
    } = this.props.jobnewpage;
    return (
      <div className="new-job">
        <div className="header-edit-add-page new-job-header">
          <div className="action">
            <div className="return" id="return" onClick={() => {
              this.props.history.goBack();
            }}>
              <span className="icon-arrow-left"></span>
            </div>
          </div>
          <div className="title">
            <span>New Job</span>
          </div>
        </div>
        <input type="file" id="file" ref="fileUploader" style={{ display: "none" }}
          onChange={(e) => {
            this.uploadImage(e);
            e.target.value = null;
          }}
        />

        <Formik ref={ref => {
          this.formik = ref
        }}
          initialValues={{
            serviceName: '',
            clientName: '',
            location: '',
            comment: '',
            companyName: '',
            employeeName: ''
          }}
          enableReinitialize={false}
          validationSchema={validateForm}
          onSubmit={(e) => {
            let formData = new FormData();
            formData.append('serviceId', chosenService._id);
            formData.append('createdBy', chosenClient._id);
            formData.append('appointmentTime', moment(appointment).toISOString());
            formData.append('location', JSON.stringify(location));
            formData.append('description', e.comment);
            if (!_.isEmpty(images)) {
              images.map((image) => {
                if (_.isObject(image.file)) {
                  formData.append('files', image.file);
                }
              })
            }
            if (chosenCompany._id !== '') {
              formData.append('companyId', chosenCompany._id);
            }
            if (chosenEmployee._id !== '') {
              formData.append('assignBy', chosenEmployee._id);
            }
            this.props.addNewJob(formData);
            this.setState({ showConfirmModal: false });
          }}>
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue
          }) => (
              <form id={'form_new_job'} onSubmit={handleSubmit}>
                <div className="content-add-edit new-job-content">
                  <div className="information">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Job Information</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">

                          <SingleSelectDropdown
                            name="serviceName"
                            touched={touched.serviceName}
                            label={'service'}
                            title={_.isEmpty(values.serviceName) ? 'Type of service' : ''}
                            chosenItemLabel={values.serviceName}
                            imageField={'icon'}
                            nameField={'name'}
                            chosenItemID={chosenService._id}
                            onBlur={handleBlur}
                            onChange={evt => {
                              handleChange(evt);
                            }}
                            options={_.isEmpty(serviceListDropdown) ? [] : serviceListDropdown}
                            onSelect={(item) => {
                              setFieldValue('serviceName', item.name);
                              this.props.chooseItem('chosenService', item);
                              this.props.getCompanies(item._id);
                            }}
                          />
                          {errors.serviceName ? this.errorMessage(errors.serviceName) : []}

                          <SingleSelectDropdown
                            name="clientName"
                            label={'client'}
                            title={_.isEmpty(values.clientName) ? 'Select a client' : ''}
                            chosenItemLabel={values.clientName}
                            imageField={'avatar'}
                            nameField={'fullName'}
                            chosenItemID={chosenClient._id}
                            onBlur={handleBlur}
                            onChange={evt => {
                              handleChange(evt);
                            }}
                            options={_.isEmpty(clientListDropdown) ? [] : clientListDropdown}
                            onSelect={(item) => {
                              setFieldValue('clientName', item.fullName);
                              this.props.chooseItem('chosenClient', item);
                            }}
                          />
                          {errors.clientName ? this.errorMessage(errors.clientName) : []}

                          {/* Appoinment time */}
                          <div className="input-group form-input appoinment-time">
                            <div className="form-label">Appoinment time</div>
                            <div className="date-time-section">
                              <div className="row">
                                <div className="col-6">
                                  <TimePicker
                                    name={'appoinmentTime'}
                                    time={appointment}
                                    hoursFromNow={3}
                                    apiError={apiErrors}
                                    onChange={time => {
                                      this.props.changeAppointment(time);
                                      clearApiError('appoinmentTime', apiErrors, this.props.clearErrors());
                                      if (chosenCompany._id !== '') {
                                        let searchData = {
                                          companyId: chosenCompany._id,
                                          appointmentTime: time,
                                          serviceId: chosenService._id
                                        };
                                        this.props.getEmployees(searchData);
                                      }
                                    }}
                                  />
                                </div>
                                <div className="col-6">
                                  <Datepicker
                                    name={'appoinmentTime'}
                                    hoursFromNow={3}
                                    apiError={apiErrors}
                                    selected={appointment}
                                    placeholder={"Select Date"}
                                    onChange={date => {
                                      this.props.changeAppointment(date);
                                      clearApiError('appoinmentTime', apiErrors, this.props.clearErrors());
                                      if (chosenCompany._id !== '') {
                                        let searchData = {
                                          companyId: chosenCompany._id,
                                          appointmentTime: date,
                                          serviceId: chosenService._id
                                        };
                                        this.props.getEmployees(searchData);
                                      }
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <Location
                            name={'location'}
                            title={'Location'}
                            placeholder={'Add location'}
                            value={values.location}
                            onSelect={newLocation => {
                              setFieldValue('location', newLocation.name);
                              this.props.changeLocation(newLocation);
                              clearApiError('location', apiErrors, this.props.clearErrors());
                            }}
                            onChange={evt => {
                              handleChange(evt);
                              setFieldValue('location', '');
                            }}
                          />
                          {errors.location ? this.errorMessage(errors.location) : []}

                          <div className="comment-images">
                            <div className="label">
                              <span>Comment</span>
                            </div>
                            <div>
                              <TextareaCounter
                                name={'comment'}
                                placeholder={'Please describe your job'}
                                rows={5}
                                maxLength={50}
                                value={values.comment}
                                error={errors.comment}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              <div className="images">
                                <ProofUpload
                                  images={images ? images : []}
                                  removeItem={index => {
                                    this.props.removeImage(index);
                                  }}
                                  onChange={evt => {
                                    if (images.length < MAX_NUMBER) {
                                      this._handleImageChange(evt);
                                    }
                                  }} />
                              </div>
                            </div>
                          </div>
                        </div>

                        {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                          return (
                            <div key={error.errorCode} className="errors">
                              <span className="icon-error"></span>
                              <div className="error-item">
                                <span>{error.errorMessage}</span>
                              </div>
                            </div>
                          );
                        }) : []}
                      </div>
                    </div>
                  </div>
                  <div className="information">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Provider</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">

                          <SingleSelectDropdown
                            name="companyName"
                            label={'company'}
                            title={_.isEmpty(values.companyName) ? 'Select a company' : values.companyName}
                            imageField={'logo'}
                            nameField={'name'}
                            chosenItemID={chosenCompany._id}
                            onBlur={handleBlur}
                            onChange={evt => {
                              handleChange(evt);
                            }}
                            options={_.isEmpty(companyListDropdown) ? [] : companyListDropdown}
                            onSelect={(item) => {
                              setFieldValue('companyName', item.name);
                              this.props.chooseItem('chosenCompany', item);
                              let searchData = {
                                companyId: item._id,
                                appointmentTime: appointment,
                                serviceId: chosenService._id
                              };
                              this.props.getEmployees(searchData);
                            }}
                          />

                          <SingleSelectDropdown
                            label={'employee'}
                            title={_.isEmpty(values.employeeName) ? 'Select an employee' : values.employeeName}
                            imageField={'avatar'}
                            nameField={'fullName'}
                            chosenItemID={chosenEmployee._id}
                            onBlur={handleBlur}
                            onChange={evt => {
                              handleChange(evt);
                            }}
                            options={_.isEmpty(employeeListDropdown) ? [] : employeeListDropdown}
                            onSelect={(item) => {
                              setFieldValue('employeeName', item.fullName);
                              this.props.chooseItem('chosenEmployee', item);
                            }}
                          />

                          <span className='description'>Company and employee seleted here will be accept to job without system. Please notice that this should be only for urgent jobs</span>
                        </div>

                        {(this.props.errors && this.props.errors.length > 0) ? this.props.errors.map((error) => {
                          return (
                            <div key={error.errorCode} className="errors">
                              <span className="icon-error"></span>
                              <div className="error-item">
                                <span>{error.errorMessage}</span>
                              </div>
                            </div>
                          );
                        }) : []}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer new-job-footer">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.history.goBack();
                  }} type='button' />
                  <PurpleRoundButton
                    type={'button'}
                    className="btn-purple-round"
                    disabled={this._disableButton(values, errors) || (!_.isEmpty(apiErrors) && apiErrors.length > 0) || images.length === 0}
                    title={'Save'}
                    onClick={() => {
                      this.setState({ showConfirmModal: true });
                    }}
                  />
                </div>
              </form>
            )}
        </Formik>
        <Modal isOpen={this.state.showConfirmModal} className="logout-modal">
          <ModalBody>
            <div className="upper">
              <div className="title">
                <span>Save This Job</span>
              </div>
              <div className="description">
                <span>Are you sure want to save this job?</span>
              </div>
            </div>
            <div className="lower">
              <GhostButton className="btn-ghost" title={'Cancel'} onClick={() => {
                this.setState({ showConfirmModal: false });
              }} />
              <PurpleRoundButton form={'form_new_job'} type={'submit'} className="btn-purple-round" title={'Save'} />
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

JobNewPage.propTypes = {
  dispatch: PropTypes.func,
  resetState: PropTypes.func,
  clearErrors: PropTypes.func,
  addImage: PropTypes.func,
  removeImage: PropTypes.func,
  updateJobDescription: PropTypes.func,
  changeLocation: PropTypes.func,
  changeAppointment: PropTypes.func,
  getCompanies: PropTypes.func,
  getEmployees: PropTypes.func,
  addNewJob: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  jobnewpage: makeSelectJobNewPage()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(Actions.resetState());
    },
    clearErrors: () => {
      dispatch(Actions.clearErrors());
    },
    showPopupError: (error) => {
      dispatch(AppActions.updateError(error));
    },
    getAllDataDropdown: () => {
      dispatch(Actions.getAllDataDropdown());
    },
    addImage: (newImage) => {
      dispatch(Actions.addImage(newImage));
    },
    removeImage: (index) => {
      dispatch(Actions.removeImage(index));
    },
    updateJobDescription: (jobId, updateData) => {
      dispatch(Actions.updateJobDescription(jobId, updateData));
    },
    changeLocation: (newLocation) => {
      dispatch(Actions.changeLocation(newLocation));
    },
    chooseItem: (key, item) => {
      dispatch(Actions.chooseItem(key, item));
    },
    changeAppointment: (newAppointment) => {
      dispatch(Actions.changeAppointment(newAppointment));
    },
    getCompanies: (serviceId) => {
      dispatch(Actions.getCompanies(serviceId));
    },
    getEmployees: (searchData) => {
      dispatch(Actions.getEmployees(searchData));
    },
    addNewJob: (data) => {
      dispatch(Actions.addJob(data));
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "jobNewPage", reducer });
const withSaga = injectSaga({ key: "jobNewPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(JobNewPage);
