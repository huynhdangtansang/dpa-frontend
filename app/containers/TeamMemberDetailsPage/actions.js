/*
 *
 * TeamMemberDetailsPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function getMemberDetails(id) {
  return {
    type: Constants.GET_MEMBER_DETAILS,
    id
  }
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_ERROR,
    response
  }
}

export function changeMemberStatus(id, dataUpdate) {
  return {
    type: Constants.CHANGE_MEMBER_STATUS,
    id,
    dataUpdate
  }
}

export function deleteMember(data) {
  return {
    type: Constants.DELETE_MEMBER,
    data
  }
}

//Activities
export function getActivitiesMember(id) {
  return {
    type: Constants.GET_ACTIVITIES,
    id
  }
}

export function getActivitiesMemberSuccess(response) {
  return {
    type: Constants.GET_ACTIVITIES_SUCCESS,
    response
  }
}