import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the teamMemberDetailsPage state domain
 */

const selectTeamMemberDetailsPageDomain = state =>
    state.get("teamMemberDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by TeamMemberDetailsPage
 */

const makeSelectTeamMemberDetailsPage = () =>
    createSelector(selectTeamMemberDetailsPageDomain, substate =>
        substate.toJS()
    );
export default makeSelectTeamMemberDetailsPage;
export { selectTeamMemberDetailsPageDomain };
