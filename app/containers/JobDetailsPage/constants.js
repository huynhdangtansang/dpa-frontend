/*
 *
 * JobDetailsPage constants
 *
 */
//Server-side
export const RESET_STATE = "JobDetailsPage/RESET_STATE";
export const CHANGE_TAB = "JobDetailsPage/CHANGE_TAB";
export const REASSIGN_OBJECT = "JobDetailsPage/REASSIGN_OBJECT";
export const CHANGE_SPECIFIC_SELECTION = "JobDetailsPage/CHANGE_SPECIFIC_SELECTION";
export const ADD_NEW_ACT = "JobDetailsPage/ADD_NEW_ACT";
export const CHANGE_APPOINTMENT = "JobDetailsPage/CHANGE_APPOINTMENT";
//Modal
export const SHOW_MODAL = "JobDetailsPage/SHOW_MODAL";
//Get
export const GET_JOB = "JobDetailsPage/GET_JOB";
export const GET_SUCCESS = "JobDetailsPage/GET_SUCCESS";
export const GET_ERROR = "JobDetailsPage/GET_ERROR";
export const GET_ACTS = "JobDetailsPage/GET_ACTS";
export const GET_ACTS_SUCCESS = "JobDetailsPage/GET_ACTS_SUCCESS";
export const GET_ACTS_ERROR = "JobDetailsPage/GET_ACTS_ERROR";
export const GET_COMPANIES = "JobDetailsPage/GET_COMPANIES";
export const GET_COMPANIES_SUCCESS = "JobDetailsPage/GET_COMPANIES_SUCCESS";
export const GET_COMPANIES_ERROR = "JobDetailsPage/GET_COMPANIES_ERROR";
export const GET_EMPLOYEES = "JobDetailsPage/GET_EMPLOYEES";
export const GET_EMPLOYEES_SUCCESS = "JobDetailsPage/GET_EMPLOYEES_SUCCESS";
export const GET_EMPLOYEES_ERROR = "JobDetailsPage/GET_EMPLOYEES_ERROR";
//Modify
export const REASSIGN_JOB = "JobDetailsPage/REASSIGN_JOB";
export const REASSIGN_SPECIFIC = "JobDetailsPage/REASSIGN_SPECIFIC";
export const CANCEL_JOB = "JobDetailsPage/CANCEL_JOB";
export const RESCHEDULE_JOB = "JobDetailsPage/RESCHEDULE_JOB";
export const SUBMIT_ESTIMATE = "JobDetailsPage/SUBMIT_ESTIMATE";
export const UPDATE_ESTIMATE = "JobDetailsPage/UPDATE_ESTIMATE";
export const REFUND = "JobDetailsPage/REFUND";
export const COMPLETE_PAY = "JobDetailsPage/COMPLETE_PAY";