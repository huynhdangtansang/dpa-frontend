/*
 *
 * DevicesListPage constants
 *
 */
export const DEFAULT_ACTION = "app/DevicesListPage/DEFAULT_ACTION";
export const RESET_STATE = "app/DevicesListPage/RESET_STATE";
export const GET_DEVICES_LIST = "/DevicesListPage/GET_DEVICES_LIST";
export const GET_SUCCESS = "/DevicesListPage/GET_SUCCESS";
export const DELETE_DEVICE = "/DevicesListPage/DELETE_DEVICE";
export const CHANGE_DEVICE_STATUS = "/DevicesListPage/CHANGE_DEVICE_STATUS";
export const RESET_PASSWORD = "/DeviceListPage/RESET_PASSWORD";
export const CHANGE_IS_SUCCESS = "/DeviceListPage/CHANGE_IS_SUCCESS";
export const CLEAR_ALL_SELECTED_DEVICE = "/DeviceListPage/CLEAR_ALL_SELECTED_DEVICE";
export const SELECT_DEVICE = "DeviceListPage/SELECT_DEVICE";
export const SELECT_ALL_DEVICE = "DeviceListPage/SELECT_ALL_DEVICE";
export const GET_SUBURBS_LIST = "DeviceListPage/GET_SUBURBS_LIST";
export const GET_SUBURBS_SUCCESS = "DeviceListPage/GET_SUBURBS_SUCCESS";
export const SHOW_MODAL = "DeviceListPage/SHOW_MODAL";
export const UPDATE_SEARCH_DATA = "DeviceListPage/UPDATE_SEARCH_DATA";
