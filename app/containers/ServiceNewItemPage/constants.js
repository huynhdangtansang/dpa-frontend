/*
 *
 * ServiceNewItemPage constants
 *
 */
export const ADD_SERVICE = "/ServiceNewItemPage/ADD_SERVICE";
export const ADD_SUCCESS = "/ServiceNewItemPage/ADD_SUCCESS";
export const ADD_ERROR = "/ServiceNewItemPage/ADD_ERROR";
export const SET_SUCCESS = "/ServiceNewItemPage/SET_SUCCESS";
export const ADD_COUPON = "/ServiceNewItemPage/ADD_COUPON";
export const DELETE_COUPON = "/ServiceNewItemPage/DELETE_COUPON";
export const UPDATE_COUPON = "/ServiceNewItemPage/UPDATE_COUPON";
export const ADD_CERT = "/ServiceNewItemPage/ADD_CERT";
export const DELETE_CERT = "/ServiceNewItemPage/DELETE_CERT";
export const UPDATE_CERT = "/ServiceNewItemPage/UPDATE_CERT";
export const ADD_AREA = "/ServiceNewItemPage/ADD_AREA";
export const DELETE_AREA = "/ServiceNewItemPage/DELETE_AREA";
export const UPDATE_AREA = "/ServiceNewItemPage/UPDATE_AREA";
export const SHOW_MODAL = "/ServiceNewItemPage/SHOW_MODAL";
export const SET_INIT = "/ServiceNewItemPage/SET_INIT";
export const CHANGE_IMAGE = "/ServiceNewItemPage/CHANGE_IMAGE";