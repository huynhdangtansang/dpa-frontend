/**
 *
 * ApprovalAreaPages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
//style
import './style.scss';
import ApprovalAreaListPage from 'containers/ApprovalAreaListPage/Loadable';
import CompanyDetailsPage from 'containers/CompanyDetailsPage/Loadable';
import CompanyEditPage from 'containers/CompanyEditPage/Loadable';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ApprovalAreaPages extends React.PureComponent {
  render() {
    return (
        <div className="approval-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.approvals} component={ApprovalAreaListPage}/>
              <Route path={urlLink.viewApproval} component={CompanyDetailsPage}/>
              <Route path={urlLink.editApproval} component={CompanyEditPage}/>
            </Switch>
          </Router>
        </div>
    );
  }
}

ApprovalAreaPages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(ApprovalAreaPages);
