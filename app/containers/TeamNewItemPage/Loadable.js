/**
 *
 * Asynchronously loads the component for TeamNewItemPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
