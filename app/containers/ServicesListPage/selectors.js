import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the servicesListPage state domain
 */

const selectServicesListPageDomain = state =>
    state.get("servicesListPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ServicesListPage
 */

const makeSelectServicesListPage = () =>
    createSelector(selectServicesListPageDomain, substate => substate.toJS());
const makeSelectServicesList = () =>
    createSelector(selectServicesListPageDomain, state => state.get('servicesList'));
export { makeSelectServicesListPage, makeSelectServicesList } ;
export { selectServicesListPageDomain };
