/*
 *
 * ResetPasswordPage constants
 *
 */
export const DEFAULT_ACTION = "app/ResetPasswordPage/DEFAULT_ACTION";
export const FORGOT_PASSWORD_SEND_EMAIL = "/ResetPasswordPage/SEND_EMAIL";
export const FORGOT_PASSWORD_EMAIL_SUSCCESS = "/ResetPasswordPage/SEND_SUCCESS";
export const FORGOT_PASSWORD_EMAIL_ERROR = "/ResetPasswordPage/SEND_ERROR";
export const SET_ISSENT = "/ResetPasswordPage/SET_ISSENT";