/**
 *
 * TeamEditMemberPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
//lib
import _, { debounce, findIndex, indexOf } from "lodash";
import { Modal, ModalBody } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectTeamEditMemberPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { changeAvatar, getMemberDetails, resetError, updateProfile } from "./actions";
import { updateError } from 'containers/App/actions';
import './style.scss';
import CropImage from 'components/CropImage';
import SubmitButton from 'components/SubmitButton';
import InputForm from "components/InputForm";
import InputFile from "components/InputFile";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import Selection from 'components/Selection';
//data
import { country, listError } from 'helper/data';
import { loadCountries, removePlus } from 'helper/exportFunction';

const validateForm = Yup.object().shape({
  'email': Yup.string()
      .email(" Invalid email"),
  'name': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (no special characters)')
      .min(6, 'Invalid name (At least 6 characters)')
      .max(30, 'Invalid name (Maximum at 30 characters)'),
  'phoneNumber': Yup.string()
      .matches(/^[0-9]*$/, 'Invalid phone number')
      .min(5, 'Invalid phone number'),
  'location': Yup.string()
});

/* eslint-disable react/prefer-stateless-function */
export class TeamEditMemberPage extends React.PureComponent {
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeAvatar(src);
    this.setState({
      cropSrc: '',
      avatarFile: file
    });
  };
  closeModal = () => {
    this.setState({
      showAvatarModal: false,
      cropSrc: ''
    });
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e);
    }
  };
  //Clear api error
  clearApiError = (type) => {
    const { apiErrors } = this.props.teameditmemberpage;
    if (apiErrors && apiErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, apiErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };
  // Convert value from api to select option value
  processOptionValue = (type, value) => {
    switch (type) {
      case 'position': {
        return {
          label: this.capitalizeTheFirstLetter(value),
          value: value
        }
      }
      case 'status': {
        return {
          label: value === true ? 'Active' : 'Inactive',
          value: value
        }
      }
    }
  };
  capitalizeTheFirstLetter = (str) => {
    if (str === '' || str === null || str === undefined) {
      return ''
    }
    return str.charAt(0).toUpperCase() + str.slice(1)
  };

  constructor(props) {
    super(props);
    this.state = {
      cropSrc: '',
      showAvatarModal: false,
      showConfirmModal: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let memberID = temp[1];
    this.props.getDetails(memberID);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'name',
      'email',
      'phoneNumber',
      'location'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { memberDetails, apiErrors, avatar } = this.props.teameditmemberpage;
    return (
        <div className="edit-member-profile">
          <div className="header-edit-add-page edit-member-header">
            <div className="action">
              <div className="return" id="return" onClick={() => {
                this.props.history.goBack();
              }}>
                <span className="icon-arrow-left"/>
              </div>
            </div>
            <div className="title">
              <span>Edit Profile</span>
            </div>
          </div>

          <Formik
              ref={ref => (this.formik = ref)}
              initialValues={{
                email: (memberDetails && memberDetails.email) || '',
                name: (memberDetails && memberDetails.fullName) || '',
                phoneNumber: (memberDetails && memberDetails.phoneNumber) || '',
                countryName: (memberDetails && memberDetails.countryName) ? memberDetails.countryName : 'Australia',
                contactArea: {
                  value: memberDetails ? memberDetails.countryCode : '',
                  label: memberDetails ? memberDetails.countryName : ''
                },
                location: memberDetails && memberDetails.location ? memberDetails.location : '',
                position: this.processOptionValue('position', memberDetails.subRole),
              }}
              enableReinitialize={true}
              validationSchema={validateForm}
              onSubmit={(e) => {
                let formData = new FormData();
                if (this.state.avatarFile) {
                  formData.append('file', this.state.avatarFile);
                }
                formData.append('fullName', e.name);
                formData.append('email', e.email);
                formData.append('countryName', e.contactArea.label);
                formData.append('countryCode', '+' + removePlus(e.contactArea.value));
                let phoneNumber = '';
                if (e.phoneNumber.toString().substring(0, 1) === '0') {
                  phoneNumber = e.phoneNumber.toString().substring(1);
                } else {
                  phoneNumber = e.phoneNumber;
                }
                formData.append('phoneNumber', phoneNumber);
                formData.append('location', e.location);
                formData.append('subRole', e.position.value);
                this.setState({ updateData: formData }, () => {
                  this.setState({ showConfirmModal: true });
                })
              }}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue
              }) => (
                <form id="profileForm" onSubmit={handleSubmit}>
                  <div className="content-add-edit user-profile-content">
                    <div className="information">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Personal Information</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          <div className="avatar-image user-profile-avatar">
                            <div className="avatar-wrapper">
                              <img src={avatar} alt="avatar" onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = './default-user.png'
                              }}/>
                            </div>
                            <InputFile name={'avatar'}
                                       src={avatar ? avatar : ''}
                                       onBlur={handleBlur}
                                       btnText={!_.isEmpty(avatar) ? 'change avatar' : 'upload avatar'}
                                       relatedValues={values}
                                       validateAfterOtherInputFields={true}
                                       onChange={evt => {
                                         setFieldValue('avatar', evt);
                                         this._handleImageChange(evt);
                                       }}/>

                          </div>
                          <div className="details">
                            {/* NAME */}
                            <InputForm label={'name'}
                                       name={'name'}
                                       placeholder='Full name'
                                       value={values.name}
                                       error={errors.name}
                                       apiError={apiErrors}
                                       touched={touched.name}
                                       onChange={evt => {
                                         handleChange(evt);
                                         this.clearApiError('name');
                                       }}
                                       onBlur={handleBlur}/>

                            {/* EMAIL */}
                            <InputForm label={'email'}
                                       name={'email'}
                                       placeholder='you@example.com'
                                       value={values.email}
                                       error={errors.email}
                                       apiError={apiErrors}
                                       touched={touched.email}
                                       onChange={evt => {
                                         handleChange(evt);
                                         this.clearApiError('email');
                                       }}
                                       onBlur={handleBlur}/>

                            {/* CONTACT NUMBER */}
                            <div className="info-item contact-number">
                              <div className="input-group form-input">
                                <div className="info-item contact-number">

                                  <Selection options={country}
                                             title={'Contact number'}
                                             name={'contactArea'}
                                             value={values.contactArea}
                                             isAsyncList={true}
                                             loadOptions={debounce(loadCountries, 700)}
                                             getOptionLabel={option => option.label}
                                             getOptionValue={option => option.value}
                                             type={'contactArea'}
                                             onBlur={handleBlur}
                                             onChange={option => {
                                               setFieldValue('contactArea', {
                                                 value: option.country_code,
                                                 label: option.label
                                               });
                                             }}/>
                                  <InputForm
                                      name={'phoneNumber'}
                                      placeholder='Phone number'
                                      prependLabel={'+' + removePlus(values.contactArea.value)}
                                      value={values.phoneNumber}
                                      error={errors.phoneNumber}
                                      apiError={apiErrors}
                                      touched={touched.phoneNumber}
                                      onChange={evt => {
                                        handleChange(evt);
                                        this.clearApiError('phoneNumber');
                                      }}
                                      onBlur={handleBlur}/>
                                </div>
                              </div>
                            </div>
                            {/* EMAIL */}
                            <InputForm
                                label={'location'}
                                name={'location'}
                                value={values.location}
                                error={errors.location}
                                apiError={apiErrors}
                                touched={touched.location}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('location');
                                }}
                                onBlur={handleBlur}/>

                            {/* POSITION */}


                            <Selection
                                options={[
                                  { label: 'Admin', value: 'Admin' },
                                  { label: 'Member', value: 'Member' },
                                ]}
                                name={'position'}
                                title={'position'}
                                selectTabIndex={5}
                                value={values.position}
                                getOptionLabel={option => option.label}
                                getOptionValue={option => option.value}
                                type={'position'}
                                onBlur={handleBlur}
                                onChange={option => {
                                  setFieldValue('position', option);
                                }}/>

                          </div>
                          {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                            return (
                                <div key={error.errorCode} className="errors">
                                  <span className="icon-error"></span>
                                  <div className="error-item">
                                    <span>{error.errorMessage}</span>
                                  </div>
                                </div>
                            );
                          }) : []}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="footer edit-member-footer">
                    <GhostButton className="btn-ghost cancel" title={'Cancel'} type={'button'} onClick={() => {
                      this.props.history.goBack();
                    }}/>
                    <SubmitButton type={'submit'}
                                  disabled={this._disableButton(values, errors)}
                                  content={'Save'}/>
                  </div>
                </form>
            )}
          </Formik>
          {(this.state.cropSrc && this.state.cropSrc !== '') ?
              <CropImage
                  show={this.state.showAvatarModal}
                  cropSrc={this.state.cropSrc}
                  closeModal={this.closeModal}
                  changeImage={this.changeImage}
              />
              : []}
          <Modal isOpen={this.state.showConfirmModal} className="logout-modal update-confirm">
            <ModalBody>
              <div className="upper">
                <div className="title">
                  <span>Save This Change</span>
                </div>
                <div className="description">
                  <span>Are you sure want to save this change. This action could influence on all data influence.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.setState({ showConfirmModal: false });
                }}/>
                <PurpleRoundButton className="btn-purple-round sign-out"
                                   title={'Save'}
                                   onClick={() => {
                                     if (this.state.updateData) {
                                       this.props.update(memberDetails._id, this.state.updateData);
                                       this.setState({ showConfirmModal: false });
                                     }
                                   }}/>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

TeamEditMemberPage.propTypes = {
  dispatch: PropTypes.func,
  getDetails: PropTypes.func,
  update: PropTypes.func,
  changeAvatar: PropTypes.func,
  updateError: PropTypes.func,
  resetError: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  teameditmemberpage: makeSelectTeamEditMemberPage()
});

function mapDispatchToProps(dispatch) {
  return {
    getDetails: (id) => {
      dispatch(getMemberDetails(id));
    },
    update: (id, profile) => {
      dispatch(updateProfile(id, profile));
    },
    changeAvatar: (src) => {
      dispatch(changeAvatar(src));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
    resetError: () => {
      dispatch(resetError());
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "teamEditMemberPage", reducer });
const withSaga = injectSaga({ key: "teamEditMemberPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(TeamEditMemberPage);
