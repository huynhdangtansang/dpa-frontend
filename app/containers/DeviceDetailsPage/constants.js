/*
 *
 * DeviceDetailsPage constants
 *
 */
export const GET_DEVICE_ALL_DATA = "/DeviceDetailsPage/GET_DEVICE_ALL_DATA";//get all data
export const GET_DEVICE_INFO = "/DeviceDetailsPage/GET_DEVICE_INFO";
export const GET_DEVICE_INFO_SUCCESS = "/DeviceDetailsPage/GET_DEVICE_INFO_SUCCESS";
export const GET_DEVICE_INFO_ERROR = "/DeviceDetailsPage/GET_DEVICE_INFO_ERROR";
export const CHANGE_DEVICE_STATUS = "/DeviceDetailsPage/CHANGE_DEVICE_STATUS";
export const DEACTIVE_DEVICE = "/DeviceDetailsPage/DEACTIVE_DEVICE";
export const DELETE_DEVICE = "/DeviceDetailsPage/DELETE_DEVICE";
export const DELETE_SUCCESS = "/DeviceDetailsPage/DELETE_SUCCESS";
export const DELETE_ERROR = "/DeviceDetailsPage/DELETE_DEVICE";
export const RESET_PASSWORD = "/DeviceDetailsPage/RESET_PASSWORD";
export const CHANGE_IS_SUCCESS = "/DeviceDetailsPage/CHANGE_IS_SUCCESS";
export const SHOW_MODAL = "DeviceDetailsPage/SHOW_MODAL";
//Payment
export const GET_PAYMENT_DETAIL = "DeviceDetailsPage/GET_PAYMENT_DETAIL";
export const GET_PAYMENT_SUCCESS = "DeviceDetailsPage/GET_PAYMENT_SUCCESS";
export const GET_PAYMENT_ERROR = "DeviceDetailsPage/GET_PAYMENT_ERROR";
//Job assigned
export const GET_JOB_ASSIGNED = "/DeviceDetailsPage/GET_JOB_ASSIGNED";
export const GET_JOB_ASSIGNED_SUCCESS = "/DeviceDetailsPage/GET_JOB_ASSIGNED_SUCCESS";
export const GET_JOB_ASSIGNED_ERROR = "/DeviceDetailsPage/GET_JOB_ASSIGNED_ERROR";