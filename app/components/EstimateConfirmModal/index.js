import React from "react";
import { Modal, ModalBody } from "reactstrap";
import NumberFormat from 'react-number-format';
import "./style.scss";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';

export default class EstimateConfirmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  estimateCharge = (data) => {
    const estimate = data;
    let estimateView = [];
    if (estimate) {
      for (let key in estimate) {
        let name = '';
        if (key === 'minCharge') {
          name = 'Minimum charge'
        } else if (key === 'gst') {
          name = (estimate[key].percent ? estimate[key].percent : '') + '% ' + 'GST'
        } else if (key === 'afterHourFee') {
          name = 'After hour fee'
        } else if (key === 'coupon') {
          name = 'Coupon'
        } else if (key === 'insuranceCover') {
          name = 'Insurance cover'
        } else if (key === 'additionalHours') {
          name = 'Additional Hours';
        } else if (key === 'additionalParts') {
          name = 'Additional Parts';
        }
        let view = (
            <div key={key} className="charge-item">
              <span className="name">{name}</span>
              <NumberFormat
                  className="price"
                  value={estimate[key].value ? estimate[key].value : 0}
                  className="price"
                  displayType={'text'}
                  prefix={estimate[key].unit ? estimate[key].unit : '$'}
                  decimalScale={2}
                  fixedDecimalScale={estimate[key].value > 0 ? true : false}
              />
            </div>
        );
        if (name && estimate[key].value > 0 && estimate[key].active === true) {
          estimateView.push(view);
        }
      }
    }
    return estimateView;
  };
  getDeposit = () => {
    if (this.props.charges && this.props.charges.deposit && this.props.charges.deposit.active === true) {
      return this.props.charges.deposit.value;
    } else return 0;
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="estimate-confirm-modal">
          <ModalBody>
            <div className="upper">
              <div className="title">
                <span>Submit Estimate</span>
              </div>
              <div className="description">
                <span>Please check the estimate carefully before submitting.</span>
              </div>
            </div>
            <div className="upper estimate">
              <div className="title">
                <span>Estimate</span>
              </div>
              <div className="content">
                {this.props.charges ? this.estimateCharge(this.props.charges) : []}
              </div>
            </div>
            {(this.props.charges && this.props.charges.deposit && this.props.charges.deposit.active === true) &&
            <div className="deposit">
              <span className='name'>Deposit</span>
              <NumberFormat
                  value={this.props.charges.deposit.value}
                  className="price"
                  displayType={'text'}
                  prefix={'$'}
                  decimalScale={2}
                  fixedDecimalScale={true}
              />
            </div>
            }
            <div className="total">
            <span
                className='name'>{(this.props.charges && this.props.charges.deposit && this.props.charges.deposit.active === true) ? 'Balance' : 'Total'}</span>
              <NumberFormat
                  value={this.props.total}
                  className="price"
                  displayType={'text'}
                  prefix={'$'}
                  decimalScale={2}
                  fixedDecimalScale={true}
              />
            </div>
            <div className="lower">
              <GhostButton title={this.props.isEdit === true ? 'Back To Edit' : 'Cancel'} className={'btn-ghost cancel'}
                           onClick={this.props.closeModal}/>
              <PurpleRoundButton title={'Submit'} className={'btn-purple-round submit'} onClick={this.props.submit}/>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}