/*
 *
 * employeeDetailsPage reducer
 *
 */
//Lib
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  memberDetails: {},
  FORMAT_CURRENCY: Intl.NumberFormat('en-IN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }),
  FORMAT_RATING: Intl.NumberFormat('en-IN', { minimumFractionDigits: 1, maximumFractionDigits: 1 }),
  activityList: [],
  jobAssignedList: null,
  periodJob: new Date(),
  earningDate: new Date(),
  earning: [],
  review: {},
  errors: [],
});

function employeeDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    //store
    case Constants.CHANGE_EMPLOYEE_DETAIL:
      return state.setIn(action.event.key, action.event.value);
    case Constants.GET_MEMBER_DETAILS:
      return state
        .set('memberDetails', fromJS({}))
        .set('errors', null);
    case Constants.GET_SUCCESS:
    case Constants.GET_ERROR:
      return state
        .set('memberDetails', action.response.data)
        .set('errors', action.response.errors);

    //Get employee review
    case Constants.GET_EMPLOYEE_REVIEW:
      return state.set('review', fromJS({}));
    case Constants.GET_EMPLOYEE_REVIEW_SUCCESS:
      return state.set('review', fromJS(action.list));

    //Get employee earning
    case Constants.GET_EMPLOYEE_EARNING:
      return state.set('earning', fromJS({}));
    case Constants.GET_EMPLOYEE_EARNING_SUCCESS:
      return state.set('earning', fromJS(action.list));

    //Job
    case Constants.GET_JOB_ASSIGNED:
      return state
        .set('jobAssignedList', fromJS([]));
    case Constants.GET_JOB_ASSIGNED_SUCCESS:
      return state
        .set('jobAssignedList', fromJS(action.response.data));

    //Activities
    case Constants.GET_ACTIVITIES_SUCCESS:
      return state
        .set('activityList', fromJS(action.response.data));
    default:
      return state;
  }
}

export default employeeDetailsPageReducer;
