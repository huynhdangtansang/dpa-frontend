import React from "react";
import './style.scss';

export default class RefundNote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <div className="estimate-content refund-note">
          <div className="label">
            <span>Note</span>
          </div>
          <div className="reason">
            <span>{this.props.note}</span>
          </div>
        </div>
    )
  }
}