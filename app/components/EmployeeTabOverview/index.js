/**
 *
 * EmployeeTabOverview
 *
 */
import React from 'react';
//Lib
import moment from 'moment';
import _, { debounce } from 'lodash';
//Components
import ActivitiesCompany from 'components/ActivitiesCompany';
//Socket
import { getEvent } from 'helper/socketConnection';
//helper
import { capitalizeTheFirstLetter } from 'helper/exportFunction';

/* eslint-disable react/prefer-stateless-function */
class EmployeeTabOverview extends React.PureComponent {
  onChangeMonth = (key, valueChange) => {
    const { earningDate, periodJob } = this.props.employeedetailspage;
    if (key === 'periodJob') {
      let newDate = moment(periodJob).add(valueChange, 'month');
      this.props.onChangeStore({ key: [key], value: newDate });
      this.getJobAssignedAfterChangeMonth();
    }
    if (key === 'earningDate') {
      let newDate = moment(earningDate).add(valueChange, 'month');
      this.props.onChangeStore({ key: [key], value: newDate });
      this.getEmployeeEarningAfterChangeMonth();
    }
  };
  getEmployeeEarningAfterChangeMonth = debounce(() => {
    const { earningDate, memberDetails } = this.props.employeedetailspage;
    this.props.getEmployeeEarning(memberDetails._id, moment(earningDate).format('MM/YYYY'));
  }, 700);
  getJobAssignedAfterChangeMonth = debounce(() => {
    const { periodJob, memberDetails } = this.props.employeedetailspage;
    this.props.getJobAssigned(memberDetails._id, { dateTime: moment(periodJob).format('MM/YYYY') });
  }, 700);

  constructor(props) {
    super(props);
    this.state = {};
    getEvent('notification:newActivities', () => {
      const { memberDetails } = this.props.employeedetailspage;
      this.props.getActivitiesEmployee(memberDetails._id);
    });
  }

  render() {
    const {
      memberDetails, jobAssignedList, periodJob, activityList,
      earningDate, earning, review,
      FORMAT_CURRENCY, FORMAT_RATING
    } = this.props.employeedetailspage;
    return (
        <div className="overview content-details-with-activities">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                <div className="earning-rate">
                  <div className="row statistic">
                    <div className="col-md-6">
                      <div className="employee-item">
                        <div className="employee-item-left">
                          <div className="name">
                            Earnings
                          </div>
                          {!_.isEmpty(earning.resultEarning) && (
                              <div className="bottom price">
                                {earning.resultEarning.unit}{FORMAT_CURRENCY.format(earning.resultEarning.value)}
                              </div>
                          )}
                          {_.isEmpty(earning.resultEarning) && (
                              <div className="bottom price">
                                ${FORMAT_CURRENCY.format(0)}
                              </div>
                          )}
                        </div>
                        <div className="employee-item-right">
                          <div className="top time-navigation">
                            <i className="icon-chevron-left"
                               onClick={() => {
                                 this.onChangeMonth('earningDate', -1);
                               }}></i>
                            <span>{moment(earningDate).format('MMM YYYY')}</span>
                            <i className="icon-chevron-right"
                               onClick={() => {
                                 this.onChangeMonth('earningDate', 1);
                               }}></i>
                          </div>
                          {earning.percentEarning && earning.percentEarning.value > 0 && (
                              <div className="bottom percent">
                                {earning.percentEarning.value}% of last month
                              </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="employee-item">
                        <div className="employee-item-left">
                          <div className="name">
                            Rating
                          </div>
                          <div className="bottom rating">
                            {FORMAT_RATING.format(!_.isUndefined(review.rate) && !_.isUndefined(review.rate.averageValue) ? review.rate.averageValue : 0)}
                            <i className="icon-star-full"/>
                            {!_.isUndefined(review.rate) && !_.isUndefined(review.rate.increment) ? (
                                <i className="icon-arrow-up"/>) : (<i className="icon-arrow-down"/>)}
                          </div>

                        </div>
                        <div className="employee-item-right">
                          <button className="btn top view"
                                  onClick={() => {
                                    this.props.toggle('2');
                                  }}
                          >View
                          </button>
                          {review.rate && (
                              <div className="bottom reviews">
                                {review.rate.total} {review.rate.total > 1 ? 'reviews' : 'review'} in total
                              </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="information">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Personal Information</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="company-logo">
                          <img src={memberDetails.avatar ? memberDetails.avatar.fileName : './default-user.png'}
                               alt="logo" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}/>
                        </div>
                        <div className="details">
                          <div className="info-item">
                            <div className="title">
                              <span>Name</span>
                            </div>
                            <div className="data">
                              <span>{memberDetails.fullName}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Email</span>
                            </div>
                            <div className="data">
                              <span>{memberDetails.email}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Phone Nubmer</span>
                            </div>
                            <div className="data">
                              <span>{memberDetails.phoneNumber}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Position</span>
                            </div>
                            <div className="data">
                              <span>{capitalizeTheFirstLetter(memberDetails.subRole)}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Status</span>
                            </div>
                            <div className="data">
                              <span>{memberDetails.active === true ? 'Active' : 'Inactive'}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="information">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Introduction</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">

                        <div className="details">
                          <div className="info-item">
                            <div className="title">
                              <span>Description</span>
                            </div>
                            <div className="data">
                              <span>{memberDetails.description}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="information">
                    <div className="row">
                      <div className="col left">
                        <div className="title">
                          <span>Jobs assigned</span>
                        </div>
                      </div>
                      <div className="col text-right">
                        <div className="top time-navigation">
                          <i className="icon-chevron-left" onClick={() => {
                            this.onChangeMonth('periodJob', -1);
                          }}></i>
                          <span>{moment(periodJob).format('MMM YYYY')}</span>
                          <i className="icon-chevron-right" onClick={() => {
                            this.onChangeMonth('periodJob', 1);
                          }}></i>
                        </div>
                      </div>
                    </div>
                    <div className="list table-assigned">
                      <table>
                        <thead className="title">
                        <tr>
                          <td className="service"><span>Service</span></td>
                          <td className="status"><span>Status</span></td>
                          <td className="top-priority"><span>Value</span></td>
                        </tr>
                        </thead>

                        <tbody className="content">

                        {_.isArray(jobAssignedList) && jobAssignedList.map(item => (
                            <tr key={item.createdAt}>
                              <td className="service">
                                <span>{item.serviceName}</span>
                              </td>
                              <td className="status">
                                <div className="content">{item.status}</div>
                              </td>
                              <td className="top-priority">
                                <span>{item.balance.unit}{parseFloat(item.balance.value).toFixed(2)}</span></td>
                            </tr>
                        ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 right-part activities">

                <div className="information">
                  <div className="title">
                    <span>Activities</span>
                  </div>
                  {!_.isEmpty(activityList) && _.isArray(activityList) && activityList.map((perday) => (
                      <div className="time-count" key={perday.day}>
                        <div className="section">
                          <div
                              className="date">{moment(perday.day).isSame(moment().format('MM/DD/YYYY')) ? 'Today' : perday.day}</div>
                          <div className="list-activities">
                            {perday.activities.map((activity) => (
                                <ActivitiesCompany
                                    key={activity._id}
                                    activity={activity}
                                />
                            ))}
                          </div>
                        </div>
                      </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

EmployeeTabOverview.propTypes = {};
export default EmployeeTabOverview;
