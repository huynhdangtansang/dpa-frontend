/*
 *
 * BotMessagesPages reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  dataList: []
});

function botMessagesPagesReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_MESSAGE_LIST_SUCCESS:
      return state.set('dataList', action.data);
    default:
      return state;
  }
}

export default botMessagesPagesReducer;
