/**
 *
 * ActivityNotification
 *
 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import Button from "../Button";
import './style.scss'

//Noti content
/**
 * @return {string}
 */
function NotiContent(data) {
  switch (data.type) {
    case "assignBy":
      return `${data.data.assignedTo && data.data.assignedTo.fullName} was assigned by ${data.data.companyInfo.name}`;
    case "cancelJob":
      return "Booking has been cancelled";
    case "assignProvider":
      return `Lead has been assigned to ${data.data.assignedTo.fullName}`;
    case "declinedJobCompanies":
      return `${data.data.providerInfo.fullName} decline this job. Please re-assign another employee`;
    case "notifyEstimateCompany":
      return `${data.data.providerInfo.fullName} has confirmed they are on the way`;
    case "providerConfirmOriginalForCompany":
      return `${data.data.providerInfo && data.data.providerInfo.fullName} has confirmed the original estimate`;
    case "confirmJobWhenDepositCompany":
      return `Client has paid ${data.data.deposit}`;
    case "providerCompleteMaster":
      return `${data.data.providerName} has completed.`;
    case "paymentCompany":
      return `Payment was sent to ${data.data.companyInfo.name}`
  }
  switch (data.typeActivity) {
    case "memberLogin":
      return `${data.ipAddress.split('ffff:')[1]} login as ${data.owner.fullName}`;
    case "createMember":
      return `${data.userId.fullName} was created by ${data.owner.fullName}`;
    case "updateMember":
      return `${data.userId.fullName} was updated by ${data.owner.fullName}`;
    case "deleteMember":
      return `${data.userId.fullName} was removed by ${data.owner.fullName}`
  }
}

//Noti format
function NotiFormat(props) {
  const { data } = props;
  return (
      <div className={'activity-notification'}>
        {data.data && data.data.serviceInfo && (
            <div className={'noti-title'}>
              {data.data.serviceInfo.name} #{props.data.data.jobId}
            </div>
        )}
        <div className="noti-content">
          {props.content}
          <button className="btn btn-close" onClick={() => {
            props.onClick(data._id)
          }}>
            <i className="icon-cancel"/>
          </button>
        </div>
        {props.btnContent && (
            <Button className="btn-purple btn-complete" onClick={props.onSubmit} text={props.btnContent}/>
        )}
      </div>
  )
}

function ActivityNotification(props) {
  return (
      <div className={'notification-wrapper'}>
        {props.data.map((item, index) => {
          if (item.show === true) {
            return <NotiFormat data={item.dataActivities}
                               content={NotiContent(item.dataActivities)}
                               onClick={(id) => {
                                 props.onClick(id)
                               }} key={index}/>
          }
        })}
      </div>
  )
}

ActivityNotification.propTypes = {};
export default ActivityNotification;
