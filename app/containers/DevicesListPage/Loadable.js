/**
 *
 * Asynchronously loads the component for DevicesListPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
