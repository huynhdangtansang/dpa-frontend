/*
 *
 * JobNewPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: Constants.RESET_STATE
  };
}

export function clearErrors() {
  return {
    type: Constants.CLEAR_ERROR
  };
}

export function getAllDataDropdown() {
  return {
    type: Constants.GET_ALL_DATA_DROPDOWN
  };
}

export function getSuccess(key, response) {
  return {
    type: Constants.GET_SUCCESS,
    key,
    response
  }
}

export function updateJobDescription(jobId, updateData) {
  return {
    type: Constants.UPDATE_JOB_DESCRIPTION,
    jobId,
    updateData
  }
}

export function updateError(response) {
  return {
    type: Constants.UPDATE_ERROR,
    response
  }
}

export function addImage(newImage) {
  return {
    type: Constants.ADD_IMAGE,
    newImage
  }
}

export function removeImage(id) {
  return {
    type: Constants.REMOVE_IMAGE,
    id
  }
}

export function changeLocation(newLocation) {
  return {
    type: Constants.CHANGE_LOCATION,
    newLocation
  }
}

export function chooseItem(key, item) {
  return {
    type: Constants.CHOOSE_ITEM,
    key,
    item
  }
}

export function changeAppointment(newAppointment) {
  return {
    type: Constants.CHANGE_APPOINTMENT,
    newAppointment
  }
}

export function addJob(newJob) {
  return {
    type: Constants.ADD_JOB,
    newJob
  }
}

export function addComplete(response) {
  return {
    type: Constants.ADD_COMPLETE,
    response
  }
}

export function getCompanies(serviceId) {
  return {
    type: Constants.GET_COMPANIES,
    serviceId
  }
}

export function getCompaniesComplete(response) {
  return {
    type: Constants.GET_COMPANIES_COMPLETE,
    response
  }
}

export function getEmployees(searchData) {
  return {
    type: Constants.GET_EMPLOYEES,
    searchData
  }
}

export function getEmployeesComplete(response) {
  return {
    type: Constants.GET_EMPLOYEES_COMPLETE,
    response
  }
}