import React from 'react';
import './style.scss';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, } from 'reactstrap';
import Location from 'components/Location';

export default class AddressItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  deleteAddress = (index) => {
    this.props.delete(index);
  };
  handlePropsChange = (index, value) => {
    this.props.handleChange(index, value);
  };

  render() {
    return (
        <div className={this.props.className}>
          <Location
              name={'location'}
              title={"address " + (this.props.index + 1) + (this.props.data.isPrimary == true ? ' (primary address)' : '')}
              id={this.props.id + 1}
              placeholder={'Address'}
              value={this.props.data ? this.props.data.name : ''}
              onChange={() => {
                this.props.deactiveAddress(this.props.id);
              }}
              onSelect={newLocation => {
                this.handlePropsChange(this.props.id, newLocation);
              }}
          />
          <div className="edit-dropdown">
            <UncontrolledDropdown>
              <DropdownToggle>
                <span className="icon-ellypsis"></span>
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => {
                  this.props.setPrimary(this.props.id);
                }}>Set as primary address</DropdownItem>
                <DropdownItem onClick={() => {
                  this.props.delete(this.props.id);
                }}>Remove</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </div>
    )
  }
}