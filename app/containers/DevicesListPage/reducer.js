/*
 *
 * DevicesListPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  CHANGE_IS_SUCCESS,
  CLEAR_ALL_SELECTED_DEVICE,
  GET_DEVICES_LIST,
  GET_SUBURBS_SUCCESS,
  GET_SUCCESS,
  SELECT_ALL_DEVICE,
  SELECT_DEVICE,
  SHOW_MODAL,
  UPDATE_SEARCH_DATA
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  devicesList: [],
  isSent: false,
  selectedDevices: [],
  dataList: [],
  suburbsList: [],
  chosenSuburb: '',
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    suburb: '',
    sort: 'fullName',
    sortType: 'asc'
  },
  currentPage: 1,
  showResetPasswordConfirm: false
});

function devicesListPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_DEVICES_LIST:
      return state
          .set('devicesList', fromJS([]))
          .set('dataList', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('devicesList', action.response.data)
          .set('dataList', action.response.data.data);
      //.set('selectedDevices', fromJS([]))
    case CHANGE_IS_SUCCESS:
      return state
          .set('isSent', action.value)
          .set('selectedDevices', fromJS([]));
    case SELECT_DEVICE:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedDevices'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedDevices', fromJS(state.get('selectedDevices').toJS().filter((device) => device.id !== action.selectItem.id)))
      }
    case SELECT_ALL_DEVICE:
      if (action.isSelected === true) {
        let newArray = state.get('dataList').map((device) => {
          return {
            id: device._id,
            status: device.active
          }
        });
        return state
            .set('selectedDevices', fromJS(newArray))
      } else {
        return state
            .set('selectedDevices', fromJS([]))
      }
    case GET_SUBURBS_SUCCESS:
      return state
          .set('suburbsList', action.response.data);
    case SHOW_MODAL:
      if (action.modal === 'resetPassword') {
        return state
            .set('showResetPasswordConfirm', action.value)
      }
      return state;
    case UPDATE_SEARCH_DATA:
      return state
      //.set('dataList', fromJS([]))
          .set('searchData', fromJS(action.newSearchData))
          .set('currentPage', fromJS(action.newPage ? action.newPage : 1));
    case CLEAR_ALL_SELECTED_DEVICE:
      return state
          .set('selectedDevices', fromJS([]));
    default:
      return state;
  }
}

export default devicesListPageReducer;
