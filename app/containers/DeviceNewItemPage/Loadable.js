/**
 *
 * Asynchronously loads the component for DeviceNewItemPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
