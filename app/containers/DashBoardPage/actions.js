/*
 *
 * DashBoardPage actions
 *
 */
import {
  DEFAULT_ACTION,
  GET_NOTIFICATION,
  GET_NOTIFICATION_COMPLETE,
  GET_SUCCESS,
  GET_USER_PROFILE,
  HIDE_NOTIFICATION,
  SELECT_MENU,
  SELECT_NOTIFICATION,
  SIGN_OUT,
  SIGN_OUT_ERROR,
  SIGN_OUT_SUCCESS
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function signOut() {
  return {
    type: SIGN_OUT
  }
}

export function signOutSuccess() {
  return {
    type: SIGN_OUT_SUCCESS
  }
}

export function signOutError() {
  return {
    type: SIGN_OUT_ERROR
  }
}

export function selectMenu(tabNumber) {
  return {
    type: SELECT_MENU,
    tab: tabNumber
  }
}

export function getUserProfile(resolve, reject) {
  return {
    type: GET_USER_PROFILE,
    resolve,
    reject
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function getNotification() {
  return {
    type: GET_NOTIFICATION,
  }
}

export function getNotificationComplete(response) {
  return {
    type: GET_NOTIFICATION_COMPLETE,
    response
  }
}

export function selectNotification(id) {
  return {
    type: SELECT_NOTIFICATION,
    id
  }
}

export function hideNotification(id) {
  return {
    type: HIDE_NOTIFICATION,
    id
  }
}

