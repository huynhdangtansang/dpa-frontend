import './style.scss';
import React from "react";
import ReactDOM from "react-dom";
import { DropdownToggle, UncontrolledDropdown, UncontrolledTooltip } from 'reactstrap';

class CheckboxTable extends React.Component {
  componentDidMount() {
    this.update(this.props.checked);
  }

  componentWillReceiveProps(props) {
    this.update(props.checked);
  }

  update(checked) {
    ReactDOM.findDOMNode(this).indeterminate = checked === 'indeterminate';
  }

  render() {
    return (
        <input className='react-bs-select-all'
               type='checkbox'
               name={'checkbox' + this.props.rowIndex}
               id={'checkbox' + this.props.rowIndex}
               checked={this.props.checked}
               onChange={this.props.onChange}/>
    );
  }
}

function ActionFormatter(props) {
  return (
      <UncontrolledDropdown className="action-dropdown" onClick={(e) => {
        e.stopPropagation();
      }}>
        <DropdownToggle>
          <i className="icon-ellypsis"></i>
        </DropdownToggle>
        {props.menu}
      </UncontrolledDropdown>
  )
}

function checkPrev(page) {
  if (page) {
    return page <= 1;
  } else
    return true;
}

function checkNext(page, limit, total) {
  // const limit = clientsList.pagination.limit;
  // const total = clientsList.pagination.total;
  let mod = total % limit;
  let endPage = 0;
  if (mod > 0) {
    endPage = parseInt(total / limit) + 1;
  } else {
    endPage = parseInt(total / limit);
  }
  if (page) {
    return page >= endPage;
  } else {
    return true;
  }
}

function TotalFormatter(props) {
  let toOffset;
  if ((props.total - props.offset) >= props.limit) {
    toOffset = props.offset + props.limit;
  } else {
    toOffset = props.offset + (props.total - props.offset);
  }
  return (
      <div className="showed-items ml-auto">
      <span className="counter">
        {props.total === 0 ? 0 : (props.offset + 1) || 0}-{toOffset || 0} of {props.total || 0} items showed
      </span>

        <button type="button" className="btn prev" id="prev-pagination"
                disabled={checkPrev(props.page)}
                onClick={() => {
                  props.changePagination('prev');
                }}>
          <span className="icon-chevron-left"></span>
        </button>
        <UncontrolledTooltip
            container={'prev-pagination'}
            className="fixle-tooltip"
            hideArrow={true}
            placement="bottom"
            target="prev-pagination" delay={{ show: 10, hide: 0 }}>Previous</UncontrolledTooltip>


        <button type="button" className="btn next" id="next-pagination"
                disabled={checkNext(props.page, props.limit, props.total)}
                onClick={() => {
                  props.changePagination('next');
                }}>
          <span className="icon-chevron-right"></span>
        </button>
        <UncontrolledTooltip
            container={'next-pagination'}
            className="fixle-tooltip"
            hideArrow={true}
            placement="bottom"
            target="next-pagination"
            delay={{ show: 10, hide: 0 }}>Next</UncontrolledTooltip>
      </div>
  );
}

function NameAndImageFormatter(props) {
  return (
      <div className="name-and-image">
        <img src={props.image} onError={(e) => {
          e.target.onerror = null;
          e.target.src = './default-user.png'
        }} alt="img"/>
        <span>{props.name}</span>
      </div>
  )
}

function CustomMultiSelect(props) {
  const { type, checked, disabled, onChange, rowIndex } = props;
  /*
   * If rowIndex is 'Header', means this rendering is for header selection column.
   */
  if (rowIndex === 'Header') {
    return (
        <div className='fixle-checkbox-square'>
          <CheckboxTable {...props} />
          <label htmlFor={'checkbox' + rowIndex}>
            <div className='check'></div>
          </label>
        </div>);
  } else {
    return (
        <div className='fixle-checkbox-square'>
          <input
              type={type}
              name={'checkbox' + rowIndex}
              id={'checkbox' + rowIndex}
              checked={checked}
              disabled={disabled}
              onChange={(e) => onChange(e, rowIndex)}
              ref={input => {
                if (input) {
                  input.indeterminate = props.indeterminate;
                }
              }}/>
          <label htmlFor={'checkbox' + rowIndex}>
            <div className='check'></div>
          </label>
        </div>);
  }
}

function StatusFormat(cell) {
  return (
      <div className="status">
        <div
            className={'content ' + (cell == true ? 'active' : 'inactive')}>{cell == true ? 'Active' : 'Inactive'}</div>
      </div>
  )
}

export { ActionFormatter, CustomMultiSelect, NameAndImageFormatter, StatusFormat, TotalFormatter }