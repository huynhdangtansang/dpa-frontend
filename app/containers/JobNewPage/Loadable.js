/**
 *
 * Asynchronously loads the component for JobNewPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
