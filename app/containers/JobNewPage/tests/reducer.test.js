import { fromJS } from 'immutable';
import jobNewPageReducer from '../reducer';

describe('jobNewPageReducer', () => {
  it('returns the initial state', () => {
    expect(jobNewPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
