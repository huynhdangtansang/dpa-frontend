/*
 *
 * UserProfilePage actions
 *
 */
import {
  CHANGE_AVATAR,
  CHANGE_IS_SENT,
  CHANGE_UPDATE_STATE,
  DEFAULT_ACTION,
  GET_SUCCESS,
  GET_USER_PROFILE,
  RESEND_ERROR,
  RESEND_SUCCESS,
  RESEND_VERIFY_CODE,
  RESET_ERROR,
  RESET_STORE,
  SEND_CODE_ERROR,
  SEND_CODE_SUCCESS,
  SEND_EMAIL_CONFIRM,
  SEND_VERIFY_CODE,
  SET_EDIT_MODE,
  SET_VALUE_ERRORS,
  UPDATE_ERROR,
  UPDATE_SUCCESS,
  UPDATE_USER_PROFILE
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function resetError() {
  return {
    type: RESET_ERROR
  };
}

export function resetStore() {
  return {
    type: RESET_STORE
  };
}

export function getProfile() {
  return {
    type: GET_USER_PROFILE
  };
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function updateProfile(status, profile) {
  return {
    type: UPDATE_USER_PROFILE,
    status,
    profile: profile
  }
}

export function setEditMode(value) {
  return {
    type: SET_EDIT_MODE,
    value: value
  }
}

export function changeAvatar(url) {
  return {
    type: CHANGE_AVATAR,
    url: url
  }
}

export function sendEmailConfirm() {
  return {
    type: SEND_EMAIL_CONFIRM
  }
}

export function updateSuccess(response) {
  return {
    type: UPDATE_SUCCESS,
    response
  }
}

export function updateError(response) {
  return {
    type: UPDATE_ERROR,
    response
  }
}

export function changeUpdateState(value) {
  return {
    type: CHANGE_UPDATE_STATE,
    value
  }
}

export function changeIsSent(value) {
  return {
    type: CHANGE_IS_SENT,
    value
  }
}

export function sendVerifyCode(email, code) {
  return {
    type: SEND_VERIFY_CODE,
    email,
    code
  }
}

export function sendCodeSuccess(response) {
  return {
    type: SEND_CODE_SUCCESS,
    response
  }
}

export function sendCodeError(response) {
  return {
    type: SEND_CODE_ERROR,
    response
  }
}

export function resendVerifyCode(email) {
  return {
    type: RESEND_VERIFY_CODE,
    email
  }
}

export function resendSuccess(response) {
  return {
    type: RESEND_SUCCESS,
    response
  }
}

export function resendError(response) {
  return {
    type: RESEND_ERROR,
    response
  }
}

export function setValueErrors(key, value) {
  return {
    type: SET_VALUE_ERRORS,
    key,
    value
  }
}