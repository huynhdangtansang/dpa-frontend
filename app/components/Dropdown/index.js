import React from "react";
import './style.scss';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledButtonDropdown, } from 'reactstrap';

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      label: ''
    };
  }

  componentDidMount() {
    if (this.props.defaultValue) {
      this.setState({
        value: this.props.defaultValue,
        label: this.props.defaultLabel
      });
      this.props.onSelect(this.props.defaultValue);
    } else {
      this.setState({
        label: this.props.defaultLabel
      });
    }
  }

  selectItem = (optionValue, optionLabel) => {
    this.setState({
      value: optionValue,
      label: optionLabel
    });
    this.props.onSelect(optionValue);
  };

  render() {
    return (
        <UncontrolledButtonDropdown className="dropdown-edit">
          <DropdownToggle caret={true} disabled={this.props.disabled}>
            {this.state.label ? this.state.label : ''}
          </DropdownToggle>
          <DropdownMenu>
            {(this.props.options && this.props.options.length > 0) ? this.props.options.map((option, index) => {
              return (
                  <DropdownItem key={index} onClick={(e) => {
                    e.preventDefault();
                    this.selectItem(option.value, option.label);
                  }}>
                    {option.label}
                  </DropdownItem>
              )
            }) : []}
          </DropdownMenu>
        </UncontrolledButtonDropdown>
    );
  }
}