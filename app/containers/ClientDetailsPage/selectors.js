import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the clientDetailsPage state domain
 */

const selectClientDetailsPageDomain = state =>
    state.get("clientDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ClientDetailsPage
 */

const makeSelectClientDetailsPage = () =>
    createSelector(selectClientDetailsPageDomain, substate => substate.toJS());
const makeSelectClientDetails = () =>
    createSelector(selectClientDetailsPageDomain, state => state.get('clientData'));
export { makeSelectClientDetailsPage, makeSelectClientDetails };
export { selectClientDetailsPageDomain };
