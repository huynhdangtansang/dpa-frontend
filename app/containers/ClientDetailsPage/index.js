/**
 *
 * ClientDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectClientDetails, makeSelectClientDetailsPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import './style.scss';
//Lib
import NumberFormat from 'react-number-format';
import moment from 'moment';
import _ from "lodash";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import PurpleRoundButton from 'components/PurpleAddRoundButton';
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import GhostButton from 'components/GhostButton';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ClientDetailsPage extends React.PureComponent {
  editInfo = () => {
    const { clientData } = this.props.clientdetailspage;
    this.props.history.push(urlLink.editClient + '?id=' + clientData._id);
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  changeStatus = () => {
    const { clientData } = this.props.clientdetailspage;
    let listIds = [];
    let data = {};
    let { _id: id, active: currentStatus } = clientData;
    listIds.push(id);
    if (currentStatus === true) {
      data['ids'] = listIds;
      data['reason'] = this.state.reason;
      data['active'] = !currentStatus;
    } else {
      data['ids'] = listIds;
      data['active'] = !currentStatus;
    }
    this.props.changeStatus(id, data);
  };
  removeClient = () => {
    const { clientData } = this.props.clientdetailspage;
    let listIds = [];
    let id = clientData._id;
    listIds.push(id);
    let data = {
      ids: listIds,
      reason: this.state.reason
    };
    this.props.delete(data);
  };
  resetPassword = () => {
    const { clientData } = this.props.clientdetailspage;
    let listIds = [];
    let id = clientData._id;
    listIds.push(id);
    let data = {
      ids: listIds
    };
    this.props.resetPass(data);
  };
  closeSuccessSendMail = () => {
    this.props.changeSuccess(false);
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      reason: '',
      showChangeStatusConfirm: false
    }
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let clientID = temp[1];
    this.props.getClientData(clientID);//load Persional Information, Payment Details, Job assigned
  }

  render() {
    const {
      clientData, paymentDetail, jobAssigned,
      isSent, addressList, showResetPasswordConfirm
    } = this.props.clientdetailspage;
    let infoClient = (
        <div className="info-client">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                <div className="information">
                  <div className="row">

                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Persional Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="company-logo">
                        <img src={clientData && clientData.avatar ? clientData.avatar.fileName : './default-user.png'}
                             onError={(e) => {
                               e.target.onerror = null;
                               e.target.src = './default-user.png'
                             }}
                             alt="logo"/>
                      </div>
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Full Name</span>
                          </div>
                          <div className="data">
                            <span>{clientData && clientData.fullName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Email</span>
                          </div>
                          <div className="data">
                            <span>{clientData && clientData.email}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Phone number</span>
                          </div>
                          <div className="data">
                            <span>{clientData && clientData.phoneNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Status</span>
                          </div>
                          <div className="data">
                            <span>{clientData && clientData.active == true ? 'Active' : 'Inactive'}</span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="icon-contain">
                  <span className="icon-edit" onClick={() => {
                    this.editInfo();
                  }}></span>
                  </div>
                </div>

                <div className="information address-details">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Address Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details address-list">
                        {addressList.length > 0 &&
                        addressList.map((address) => (
                            <div key={address._id} className="info-item">
                              <div className="title">
                              <span>
                                {address.isPrimary && address.isPrimary === true ? 'Primary address' : 'Address'}
                              </span>
                              </div>
                              <div className="data">
                                <span>{address.name}</span>
                              </div>
                            </div>
                        ))
                        }
                        <PurpleAddRoundButton
                            onClick={() => {
                              this.editInfo();
                            }}
                            className={'add-address btn-add-purple-round'}
                            title={'Add Address'}/>
                      </div>
                    </div>
                  </div>
                  <div className="icon-contain">
                  <span className="icon-edit" onClick={() => {
                    this.editInfo();
                  }}></span>
                  </div>
                </div>

                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Payment Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details payment-list">
                        {!_.isEmpty(paymentDetail) && paymentDetail.map((payment, index) => (
                            <div className="info-item" key={index}>
                              <div className="title">
                                <span>Card {index + 1}</span>
                              </div>
                              <div className="data">
                            <span>
                              {!_.isUndefined(payment && payment.cardholderName) ? payment.cardholderName : ''}
                              <br></br>
                              **** **** *** {!_.isUndefined(payment && payment.last4) ? payment.last4 : ''}
                            </span>
                              </div>
                            </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>

                <div className="information jobs-assigned">
                  <div className="left">
                    <div className="title">
                      <span>Jobs assigned</span>
                    </div>
                  </div>
                  {!_.isEmpty(jobAssigned) && (
                      <div className="list table-assigned">
                        <table>
                          <thead className="title-table">
                          <tr>
                            <td className="service"><span>Services</span></td>
                            <td className="status"><span>Status</span></td>
                            <td className="top-priority"><span>Value</span></td>
                          </tr>
                          </thead>

                          <tbody className="content">
                          {jobAssigned.map((job) => (
                              <tr key={job._id}>
                                <td className="service">
                                  <span>{job.name || job.name}</span>
                                </td>
                                <td className="status">
                                  <div className="content">{job.status || job.status}</div>
                                </td>
                                <td className="top-priority">
                                  <span>${job.estimation ? parseFloat(job.estimation.total).toFixed(2) : '0.00'}</span>
                                </td>
                              </tr>
                          ))}
                          </tbody>
                        </table>
                      </div>
                  )}

                </div>

              </div>
              <div className="col-md-4 right-part">
                <div className="information">
                  <div className="title">
                    <span>Paid</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                      <span>{!_.isEmpty(clientData) &&
                      <NumberFormat
                          value={clientData.paid}
                          defaultValue={0}
                          thousandSeparator={true}
                          displayType={'text'}
                          prefix={'$'}
                          decimalScale={2}
                          fixedDecimalScale={true}
                      />}</span>
                      </div>
                      <div className="info">
                        {clientData && _.isNumber(clientData.completed) && (
                            <span>on {clientData.completed} completed project{clientData.completed > 1 ? 's' : ''}</span>)}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information">
                  <div className="title">
                    <span>Unpaid</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                      <span>{!_.isEmpty(clientData) &&
                      <NumberFormat
                          value={clientData.unpaid}
                          defaultValue={0}
                          thousandSeparator={true}
                          displayType={'text'}
                          prefix={'$'}
                          decimalScale={2}
                          fixedDecimalScale={true}
                      />}</span>
                      </div>
                      <div className="info">
                        {clientData && _.isNumber(clientData.beforeCompleted) && (
                            <span>on {clientData.beforeCompleted} active project{clientData.beforeCompleted > 1 ? 's' : ''}</span>)}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information">
                  <div className="title">
                    <span>Number of jobs created</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                        {clientData && _.isNumber(clientData.beforeCompleted) && _.isNumber(clientData.completed) ?
                            <span>{(clientData.beforeCompleted + clientData.completed) || 0}</span> :
                            <span>0</span>
                        }
                      </div>
                      <div className="info">
                        with {clientData && _.isNumber(clientData.cancelled) ?
                          <span>{clientData.cancelled}</span> :
                          <span>0</span>
                      } cancelled project{clientData && clientData.cancelled > 1 ? 's' : ''}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
    return (
        <div className="client-details">
          <div className="header-edit-add-page">
            <div className="action">
              <div className="return" id='return' onClick={() => {
                this.props.history.goBack();
              }}>
                <span className={'icon-arrow-left'}></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
              <div className="btn-group float-right">
                <GhostButton className={'btn-ghost remove'} title={'Remove'} onClick={() => {
                  this.setState({ showRemoveConfirm: true });
                }}/>
                <GhostButton className={'btn-ghost edit'} title={'Edit'} onClick={() => {
                  this.editInfo();
                }}/>
                <GhostButton className={'btn-ghost deactive'}
                             title={clientData && clientData.active == true ? 'Deactivate' : 'Activate'}
                             onClick={() => {
                               this.setState({ showChangeStatusConfirm: true });
                             }}/>
                <GhostButton className={'btn-ghost reset'} title={'Reset Password'} onClick={() => {
                  this.props.showModal('resetPassword', true);
                }}/>
              </div>
            </div>
            <div className="title">
              <span>{clientData && clientData.fullName}</span>
              <div className="joined-date">
                <span>Joined in {moment(clientData && clientData.createdAt).format('ddd DD MMM YYYY')}</span>
              </div>
            </div>

          </div>
          <div className="content">
            {infoClient}
          </div>
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              currentStatus={clientData && clientData.active}
              object={'client'}
              action={'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'client'}
              action={'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeClient}/>
          <Modal isOpen={isSent} className="logout-modal">
            <ModalBody>
              <div className="send-email-success">
                <div className="upper">
                  <div className="title">
                    <span>Reset Passowrd</span>
                  </div>
                  <div className="description">
                    <span>Reset password email has been sent successfully!</span>
                  </div>
                </div>
                <div className="lower">
                  <PurpleRoundButton className="btn-purple-round sign-out"
                                     title={'OK'}
                                     onClick={() => {
                                       this.closeSuccessSendMail();
                                     }}/>
                </div>
              </div>
            </ModalBody>
          </Modal>
          <Modal isOpen={showResetPasswordConfirm} className="logout-modal">
            <ModalBody>
              <div className="reset-password">
                <div className="upper">
                  <div className="title">
                    <span>Reset This Client Password</span>
                  </div>
                  <div className="description">
                    <span>Are you sure want to reset this client password. This action will send reset password email to this client.</span>
                  </div>
                </div>
                <div className="lower">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.showModal('resetPassword', false);
                  }}/>
                  <PurpleRoundButton className="btn-purple-round reset-password"
                                     title={'Reset Password'}
                                     onClick={() => {
                                       this.props.showModal('resetPassword', false);
                                       this.resetPassword();
                                     }}/>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

ClientDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  getClientData: PropTypes.func,
  getJobAssigned: PropTypes.func,
  deactive: PropTypes.func,
  delete: PropTypes.func,
  changeStatus: PropTypes.func,
  resetPass: PropTypes.func,
  changeSuccess: PropTypes.func,
  showModal: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  clientdetailspage: makeSelectClientDetailsPage(),
  clientData: makeSelectClientDetails()
});

function mapDispatchToProps(dispatch) {
  return {
    getClientData: (id) => {
      dispatch(Actions.getClientAllData(id));
    },
    getJobAssigned: (id) => {
      dispatch(Actions.getJobAssigned(id));
    },
    deactive: (id) => {
      dispatch(Actions.deactiveClient(id));
    },
    delete: (data) => {
      dispatch(Actions.deleteClient(data));
    },
    changeStatus: (id, data) => {
      dispatch(Actions.changeClientStatus(id, data));
    },
    resetPass: (data) => {
      dispatch(Actions.resetPassword(data));
    },
    changeSuccess: (value) => {
      dispatch(Actions.changeIsSuccess(value));
    },
    showModal: (modal, value) => {
      dispatch(Actions.showModal(modal, value));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "clientDetailsPage", reducer });
const withSaga = injectSaga({ key: "clientDetailsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ClientDetailsPage);
