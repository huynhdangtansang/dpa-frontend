/**
 *
 * DeviceDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectDeviceDetails, makeSelectDeviceDetailsPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import './style.scss';
//Lib
import NumberFormat from 'react-number-format';
import moment from 'moment';
import _ from "lodash";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import PurpleRoundButton from 'components/PurpleAddRoundButton';
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import GhostButton from 'components/GhostButton';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class DeviceDetailsPage extends React.PureComponent {
  editInfo = () => {
    const { deviceData } = this.props.devicedetailspage;
    this.props.history.push(urlLink.editDevice + '?id=' + deviceData._id);
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  changeStatus = () => {
    const { deviceData } = this.props.devicedetailspage;
    let listIds = [];
    let data = {};
    let { _id: id, active: currentStatus } = deviceData;
    listIds.push(id);
    if (currentStatus === true) {
      data['ids'] = listIds;
      data['reason'] = this.state.reason;
      data['active'] = !currentStatus;
    } else {
      data['ids'] = listIds;
      data['active'] = !currentStatus;
    }
    this.props.changeStatus(id, data);
  };
  removeDevice = () => {
    const { deviceData } = this.props.devicedetailspage;
    let listIds = [];
    let id = deviceData._id;
    listIds.push(id);
    let data = {
      ids: listIds,
      reason: this.state.reason
    };
    this.props.delete(data);
  };
  resetPassword = () => {
    const { deviceData } = this.props.devicedetailspage;
    let listIds = [];
    let id = deviceData._id;
    listIds.push(id);
    let data = {
      ids: listIds
    };
    this.props.resetPass(data);
  };
  closeSuccessSendMail = () => {
    this.props.changeSuccess(false);
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      reason: '',
      showChangeStatusConfirm: false
    }
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let deviceID = temp[1];
    this.props.getdeviceData(deviceID);//load Persional Information, Payment Details, Job assigned
  }

  render() {
    const {
      deviceData, paymentDetail, jobAssigned,
      isSent, addressList, showResetPasswordConfirm
    } = this.props.devicedetailspage;
    let infoDevice = (
        <div className="info-device">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                <div className="information">
                  <div className="row">

                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Persional Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="company-logo">
                        <img src={deviceData && deviceData.avatar ? deviceData.avatar.fileName : './default-user.png'}
                             onError={(e) => {
                               e.target.onerror = null;
                               e.target.src = './default-user.png'
                             }}
                             alt="logo"/>
                      </div>
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Full Name</span>
                          </div>
                          <div className="data">
                            <span>{deviceData && deviceData.fullName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Email</span>
                          </div>
                          <div className="data">
                            <span>{deviceData && deviceData.email}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Phone number</span>
                          </div>
                          <div className="data">
                            <span>{deviceData && deviceData.phoneNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Status</span>
                          </div>
                          <div className="data">
                            <span>{deviceData && deviceData.active == true ? 'Active' : 'Inactive'}</span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="icon-contain">
                  <span className="icon-edit" onClick={() => {
                    this.editInfo();
                  }}></span>
                  </div>
                </div>

                <div className="information address-details">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Address Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details address-list">
                        {addressList.length > 0 &&
                        addressList.map((address) => (
                            <div key={address._id} className="info-item">
                              <div className="title">
                              <span>
                                {address.isPrimary && address.isPrimary === true ? 'Primary address' : 'Address'}
                              </span>
                              </div>
                              <div className="data">
                                <span>{address.name}</span>
                              </div>
                            </div>
                        ))
                        }
                        <PurpleAddRoundButton
                            onClick={() => {
                              this.editInfo();
                            }}
                            className={'add-address btn-add-purple-round'}
                            title={'Add Address'}/>
                      </div>
                    </div>
                  </div>
                  <div className="icon-contain">
                  <span className="icon-edit" onClick={() => {
                    this.editInfo();
                  }}></span>
                  </div>
                </div>

                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Payment Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details payment-list">
                        {!_.isEmpty(paymentDetail) && paymentDetail.map((payment, index) => (
                            <div className="info-item" key={index}>
                              <div className="title">
                                <span>Card {index + 1}</span>
                              </div>
                              <div className="data">
                            <span>
                              {!_.isUndefined(payment && payment.cardholderName) ? payment.cardholderName : ''}
                              <br></br>
                              **** **** *** {!_.isUndefined(payment && payment.last4) ? payment.last4 : ''}
                            </span>
                              </div>
                            </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>

                <div className="information jobs-assigned">
                  <div className="left">
                    <div className="title">
                      <span>Jobs assigned</span>
                    </div>
                  </div>
                  {!_.isEmpty(jobAssigned) && (
                      <div className="list table-assigned">
                        <table>
                          <thead className="title-table">
                          <tr>
                            <td className="service"><span>Services</span></td>
                            <td className="status"><span>Status</span></td>
                            <td className="top-priority"><span>Value</span></td>
                          </tr>
                          </thead>

                          <tbody className="content">
                          {jobAssigned.map((job) => (
                              <tr key={job._id}>
                                <td className="service">
                                  <span>{job.name || job.name}</span>
                                </td>
                                <td className="status">
                                  <div className="content">{job.status || job.status}</div>
                                </td>
                                <td className="top-priority">
                                  <span>${job.estimation ? parseFloat(job.estimation.total).toFixed(2) : '0.00'}</span>
                                </td>
                              </tr>
                          ))}
                          </tbody>
                        </table>
                      </div>
                  )}

                </div>

              </div>
              <div className="col-md-4 right-part">
                <div className="information">
                  <div className="title">
                    <span>Paid</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                      <span>{!_.isEmpty(deviceData) &&
                      <NumberFormat
                          value={deviceData.paid}
                          defaultValue={0}
                          thousandSeparator={true}
                          displayType={'text'}
                          prefix={'$'}
                          decimalScale={2}
                          fixedDecimalScale={true}
                      />}</span>
                      </div>
                      <div className="info">
                        {deviceData && _.isNumber(deviceData.completed) && (
                            <span>on {deviceData.completed} completed project{deviceData.completed > 1 ? 's' : ''}</span>)}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information">
                  <div className="title">
                    <span>Unpaid</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                      <span>{!_.isEmpty(deviceData) &&
                      <NumberFormat
                          value={deviceData.unpaid}
                          defaultValue={0}
                          thousandSeparator={true}
                          displayType={'text'}
                          prefix={'$'}
                          decimalScale={2}
                          fixedDecimalScale={true}
                      />}</span>
                      </div>
                      <div className="info">
                        {deviceData && _.isNumber(deviceData.beforeCompleted) && (
                            <span>on {deviceData.beforeCompleted} active project{deviceData.beforeCompleted > 1 ? 's' : ''}</span>)}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information">
                  <div className="title">
                    <span>Number of jobs created</span>
                  </div>
                  <div className="details">
                    <div className="info-item">
                      <div className="number">
                        {deviceData && _.isNumber(deviceData.beforeCompleted) && _.isNumber(deviceData.completed) ?
                            <span>{(deviceData.beforeCompleted + deviceData.completed) || 0}</span> :
                            <span>0</span>
                        }
                      </div>
                      <div className="info">
                        with {deviceData && _.isNumber(deviceData.cancelled) ?
                          <span>{deviceData.cancelled}</span> :
                          <span>0</span>
                      } cancelled project{deviceData && deviceData.cancelled > 1 ? 's' : ''}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
    return (
        <div className="device-details">
          <div className="header-edit-add-page">
            <div className="action">
              <div className="return" id='return' onClick={() => {
                this.props.history.goBack();
              }}>
                <span className={'icon-arrow-left'}></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
              <div className="btn-group float-right">
                <GhostButton className={'btn-ghost remove'} title={'Remove'} onClick={() => {
                  this.setState({ showRemoveConfirm: true });
                }}/>
                <GhostButton className={'btn-ghost edit'} title={'Edit'} onClick={() => {
                  this.editInfo();
                }}/>
                <GhostButton className={'btn-ghost deactive'}
                             title={deviceData && deviceData.active == true ? 'Deactivate' : 'Activate'}
                             onClick={() => {
                               this.setState({ showChangeStatusConfirm: true });
                             }}/>
                <GhostButton className={'btn-ghost reset'} title={'Reset Password'} onClick={() => {
                  this.props.showModal('resetPassword', true);
                }}/>
              </div>
            </div>
            <div className="title">
              <span>{deviceData && deviceData.fullName}</span>
              <div className="joined-date">
                <span>Joined in {moment(deviceData && deviceData.createdAt).format('ddd DD MMM YYYY')}</span>
              </div>
            </div>

          </div>
          <div className="content">
            {infoDevice}
          </div>
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              currentStatus={deviceData && deviceData.active}
              object={'device'}
              action={'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'device'}
              action={'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeDevice}/>
          <Modal isOpen={isSent} className="logout-modal">
            <ModalBody>
              <div className="send-email-success">
                <div className="upper">
                  <div className="title">
                    <span>Reset Passowrd</span>
                  </div>
                  <div className="description">
                    <span>Reset password email has been sent successfully!</span>
                  </div>
                </div>
                <div className="lower">
                  <PurpleRoundButton className="btn-purple-round sign-out"
                                     title={'OK'}
                                     onClick={() => {
                                       this.closeSuccessSendMail();
                                     }}/>
                </div>
              </div>
            </ModalBody>
          </Modal>
          <Modal isOpen={showResetPasswordConfirm} className="logout-modal">
            <ModalBody>
              <div className="reset-password">
                <div className="upper">
                  <div className="title">
                    <span>Reset This Device Password</span>
                  </div>
                  <div className="description">
                    <span>Are you sure want to reset this device password. This action will send reset password email to this device.</span>
                  </div>
                </div>
                <div className="lower">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.showModal('resetPassword', false);
                  }}/>
                  <PurpleRoundButton className="btn-purple-round reset-password"
                                     title={'Reset Password'}
                                     onClick={() => {
                                       this.props.showModal('resetPassword', false);
                                       this.resetPassword();
                                     }}/>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

DeviceDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  getdeviceData: PropTypes.func,
  getJobAssigned: PropTypes.func,
  deactive: PropTypes.func,
  delete: PropTypes.func,
  changeStatus: PropTypes.func,
  resetPass: PropTypes.func,
  changeSuccess: PropTypes.func,
  showModal: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  devicedetailspage: makeSelectDeviceDetailsPage(),
  deviceData: makeSelectDeviceDetails()
});

function mapDispatchToProps(dispatch) {
  return {
    getdeviceData: (id) => {
      dispatch(Actions.getDeviceAllData(id));
    },
    getJobAssigned: (id) => {
      dispatch(Actions.getJobAssigned(id));
    },
    deactive: (id) => {
      dispatch(Actions.deactiveDevice(id));
    },
    delete: (data) => {
      dispatch(Actions.deleteDevice(data));
    },
    changeStatus: (id, data) => {
      dispatch(Actions.changeDeviceStatus(id, data));
    },
    resetPass: (data) => {
      dispatch(Actions.resetPassword(data));
    },
    changeSuccess: (value) => {
      dispatch(Actions.changeIsSuccess(value));
    },
    showModal: (modal, value) => {
      dispatch(Actions.showModal(modal, value));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "deviceDetailsPage", reducer });
const withSaga = injectSaga({ key: "deviceDetailsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceDetailsPage);
