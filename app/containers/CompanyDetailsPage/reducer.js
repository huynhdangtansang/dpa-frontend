/*
 *
 * CompanyDetailsPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { CHANGE_STATUS_FILTER } from "containers/JobsListPage/constants";
import { LOCATION_CHANGE } from "react-router-redux";
//Library
import moment from 'moment';

export const initialState = fromJS({
  companyData: {},
  employeeData: null,
  jobData: null,
  dataList: [],
  activityList: [],
  activeTab: '1',
  FORMATCURRENCY: Intl.NumberFormat('en-IN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }),
  note: '',
  periodEmployee: '',
  filterPeriod: {
    value: '',
    label: 'All'
  },
  selectedEmployee: [],
  searchDataForEmployee: {
    offset: 0,
    limit: 10,
    search: '',
    sort: '',
    period: '',
    sortType: '',
    active: false
  },
  chosenEmployee: {
    _id: ''
  },
  searchDataForJob: {
    offset: 0,
    limit: 10,
    search: '',
    employeeId: [],
    status: [
      {
        label: 'All',
        value: ''
      }
    ],
    sort: 'jobId',
    period: '',
    sortType: 'desc',
  },
  statusList: [
    {
      label: 'All',
      value: '',
    },
    {
      label: 'New Lead',
      value: 'newLead',
    },
    {
      label: 'Accepted',
      value: 'accepted',
    },
    {
      label: 'On Way',
      value: 'onWay',
    },
    {
      label: 'Paid',
      value: 'paid',
    },
    {
      label: 'Deposit Paid',
      value: 'depositPaid',
    },
    {
      label: 'Completed',
      value: 'completed',
    },
    {
      label: 'Cancelled',
      value: 'cancelled',
    }
  ],
  showConfirmPriority: false,
  selectedServicePriority: null,
  currentPageEmployee: 1,
  currentPageJob: 1,
  //earning
  earningData: {},
  errors: [],
});

function multiCheckboxChange(state, action) {
  let newArray = state.get(action.key).toJS().map((status, index) => {
    return index === action.id ?
        {
          label: status.label,
          value: status.value,
          checked: !status.checked
        } :
        status;
  });
  return state
      .set(action.key, fromJS(newArray))
}

function companyDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_SUCCESS:
      return state
          .set('companyData', fromJS(action.response.data));
    case Constants.TOGGLE_TOP_PRIORITY:
      if (action.index >= 0)
        return state
            .set('showConfirmPriority', action.value)
            .set('selectedServicePriority', state.toJS().companyData.topPriority[action.index]);
      else
        return state
            .set('showConfirmPriority', action.value)
            .set('selectedServicePriority', null);
    case Constants.CHANGE_NOTE:
      return state.set('note', action.noteText);
    case Constants.GET_ERROR:
      return state
          .set('errors', action.response);

      //COMMON
    case Constants.UPDATE_TAB:
      return state
          .set('activeTab', action.newTab);
    case Constants.UPDATE_PAGE:
      return state
          .set(action.key, action.value);
    case CHANGE_STATUS_FILTER:
      return multiCheckboxChange(state, action);
    case Constants.UPDATE_SEARCH_DATA:
      return state
          .set(action.key, action.newSearchData);
    case Constants.CHANGE_STORE_VALUE:
      return state.setIn(action.event.key, action.event.value);
    case Constants.CHOOSE_ITEM:
      return state
          .set(action.key, action.item);

      //EMPLOYEE
    case Constants.GET_EMPLOYEE_DATA:
      return state
          .set('employeeData', fromJS({}));
    case Constants.GET_EMPLOYEE_SUCCESS:
      return state
          .set('employeeData', fromJS(action.response.data))
          .set('dataList', fromJS(action.response.data.data));
    case Constants.UPDATE_SEARCH_EMPLOYEE:
      return state
          .setIn(['searchDataForEmployee', action.key], action.value);
    case Constants.SELECT_EMPLOYEE:
      if (action.isSelected) {
        return state
            .updateIn(['selectedEmployee'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedEmployee', state.get('selectedEmployee').filter((employee) => employee.id !== action.selectItem.id))
      }
    case Constants.SELECT_ALL_EMPLOYEE:
      if (action.isSelected) {
        let newArray = state.get('dataList').map((employee) => {
          return {
            id: employee.toJS()._id,
            status: employee.toJS().active
          }
        });
        return state
            .set('selectedEmployee', fromJS(newArray))
      } else {
        return state
            .set('selectedEmployee', fromJS([]))
      }
    case Constants.GET_PERIOD_EMPLOYEE_SUCCESS: {
      let dateStart = moment(action.period);
      let dateEnd = moment(new Date());//current date
      let timeValues = [];
      while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
        timeValues.push(dateStart.format('MMM YYYY'));
        dateStart.add(1, 'month');
      }
      return state.set('periodEmployee', fromJS(timeValues));
    }
    case Constants.UPDATE_PERIOD_EMPLOYEE:
      return state
          .set('filterPeriod', action.newPeriod)
          .setIn(['searchDataForEmployee', 'period'], action.newPeriod.value ? action.newPeriod.value : []);

      //EARNING
    case Constants.GET_EARNING_COMPANY_SUCCESS:
      return state
          .set('earningData', action.response);


      //JOB
    case Constants.GET_JOB_DATA:
      return state.set('jobData', fromJS({}));
    case Constants.GET_JOB_SUCCESS:
      return state
          .set('jobData', fromJS(action.response.data));
    case Constants.UPDATE_SEARCH_JOB:
      return state
          .setIn(['searchDataForJob', action.key], action.value ? action.value : '');

      //ACTIVITIES
    case Constants.GET_ACTIVITIES_COMPANY:
      return state
          .set('activityList', fromJS([]));
    case Constants.GET_ACTIVITIES_COMPANY_SUCCESS:
      return state
          .set('activityList', fromJS(action.response));
    default:
      return state;
  }
}

export default companyDetailsPageReducer;
