import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the userProfilePage state domain
 */

const selectUserProfilePageDomain = state =>
    state.get("userProfilePage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by UserProfilePage
 */

const makeSelectUserProfilePage = () =>
    createSelector(selectUserProfilePageDomain, substate => substate.toJS());
const makeSelectUserData = () =>
    createSelector(selectUserProfilePageDomain, state => state.get('userData'));
const makeSelectEditMode = () =>
    createSelector(selectUserProfilePageDomain, state => state.get('edit'));
const makeSelectAvatar = () =>
    createSelector(selectUserProfilePageDomain, state => state.get('avatar'));
export { makeSelectUserProfilePage, makeSelectUserData, makeSelectEditMode, makeSelectAvatar };
export { selectUserProfilePageDomain };
