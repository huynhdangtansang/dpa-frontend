/**
 *
 * AreaAndHours
 *
 */
import React from "react";
import _, { debounce, findIndex } from "lodash";
import { placeAutoComplete } from 'helper/exportFunction'
//Lib
import ct from "countries-and-timezones";
// Components
import Selection from "components/Selection";
import { city, country, formatDate, formatTime, suburbs } from "helper/data";

class AreaAndHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      country_code: ''
    }
  }

  componentWillMount() {
    if (!_.isEmpty(this.props.data)) {
      Object.keys(this.props.data).forEach((key) => {
        if (_.isArray(this.props.data[key])) {// Format Array need loot each item is {label,value}
          let list = this.props.data[key].map((item) => {
            return {
              value: _.isEmpty(item) ? '' : item,
              label: _.isEmpty(item) ? '' : item,
            }
          });
          this.props.onChange(key, list);
        } else { // Data format string need change to label and value
          if (key === 'country' && !_.isEmpty(this.props.data[key])) {
            let temp = country.filter((item) => {
              return item.label.toLowerCase().indexOf(this.props.data[key].toLowerCase().split(' ').join('')) > -1
            });
            this.setState({ country_code: _.isArray(temp) && temp.length > 0 ? temp[0].alpha2_code : undefined });
            this.props.onChange('timeZoneList', this.getTimeZoneBaseOnCountry(_.isArray(temp) && temp.length > 0 ? temp[0].alpha2_code : undefined));
          }
          this.props.onChange(key, {
            label: this.props.data[key],
            value: this.props.data[key]
          });
        }
      });
    }
  }

  checkIndexInArr = (value, arr) => {
    return findIndex(arr, val => val.value === value.value)
  };
  // Split this into 3 functions because of debounce
  loadCountryOptions = (inputValue, callback) => {
    placeAutoComplete('country', inputValue).then((rs) => {
      callback(rs);
    })
  };
  loadCityOptions = (inputValue, callback) => {
    placeAutoComplete('city', inputValue, this.state.country_code).then((rs) => {
      callback(rs);
    })
  };
  loadSuburbOptions = (inputValue, callback) => {
    placeAutoComplete('suburbs', inputValue, this.state.country_code).then((rs) => {
      callback(rs);
    })
  };
  formatTimeZoneValue = (list) => {
    if (_.isUndefined(list) || _.isNull(list)) {
      return [];
    }
    return list.map((item) => {
      return {
        value: `${item.name} (UTC${item.offsetStr})`,
        label: `${item.name} (UTC${item.offsetStr})`
      }
    })
  };
  getTimeZoneBaseOnCountry = (code) => {
    return (this.formatTimeZoneValue(ct.getTimezonesForCountry(code)));
  };

  render() {
    const { data } = this.props;
    return (
        <div className="information">
          <div className="row">
            <div className="col-md-4 left">
              <div className="title">
                <span>Region and Hours</span>
              </div>
            </div>
            <div className="col-md-8 right">
              <div className="details">
                <div className="info-item">
                  {}

                  <Selection
                      options={country}
                      title={'country'}
                      name={'country'}
                      value={data && data.country ? data.country : []}
                      isAsyncList={true}
                      loadOptions={debounce(this.loadCountryOptions, 700)}
                      getOptionLabel={option => option.label}
                      getOptionValue={option => option.value}
                      type={'country'}
                      onChange={option => {
                        // Reset all other related value due to change country
                        if (data && data.country && option.label !== data.country.label) {
                          this.props.onChange('timeZone', []);
                          this.props.onChange('city', []);
                          this.props.onChange('suburbs', []);
                        }
                        this.setState({ country_code: option.alpha2_code });
                        // Get timezone base on selected country
                        this.props.onChange('timeZoneList', this.getTimeZoneBaseOnCountry(option.alpha2_code));
                        this.props.onChange('country', option);
                      }}/>
                </div>
                <div className="row">
                  <div className="info-item col-md-6">
                    <Selection
                        options={data && _.isArray(data.timeZoneList) ? data.timeZoneList : []}
                        title={'time zone'}
                        name={'timeZone'}
                        value={data && data.timeZone ? data.timeZone : []}
                        getOptionLabel={option => option.label}
                        getOptionValue={option => option.value}
                        type={'timeZone'}
                        onChange={option => {
                          this.props.onChange('timeZone', option);
                        }}/>

                  </div>
                  <div className="info-item col-md-6">
                    <Selection
                        options={city}
                        name={'city'}
                        title={'city'}
                        value={data && data.city ? data.city : []}
                        isAsyncList={true}
                        loadOptions={debounce(this.loadCityOptions, 700)}
                        getOptionLabel={option => option.label}
                        getOptionValue={option => option.value}
                        type={'city'}
                        onChange={option => {
                          this.props.onChange('city', option);
                        }}/>
                  </div>
                </div>
                <div className="info-item">
                  <Selection
                      options={suburbs}
                      name={'suburbs'}
                      title={'suburbs'}
                      value={data && data.suburbs ? data.suburbs : []}
                      isAsyncList={true}
                      isMulti
                      loadOptions={debounce(this.loadSuburbOptions, 700)}
                      getOptionLabel={option => option.label}
                      getOptionValue={option => option.value}
                      type={'city'}
                      onChange={option => {
                        this.props.onChange('suburbs', option);
                      }}/>
                </div>

                <div className="row">
                  <div className="info-item col-md-6">
                    <Selection
                        options={formatTime}
                        name={'formatTime'}
                        title={'time format'}
                        value={data && data.formatTime ? data.formatTime : []}
                        getOptionLabel={option => option.label}
                        getOptionValue={option => option.value}
                        type={'formatTime'}
                        onChange={option => {
                          this.props.onChange('formatTime', option);
                        }}/>
                  </div>
                  <div className="info-item col-md-6">
                    <Selection
                        options={formatDate}
                        name={'formatDate'}
                        title={'date format'}
                        value={data && data.formatDay ? data.formatDay : []}
                        getOptionLabel={option => option.label}
                        getOptionValue={option => option.value}
                        type={'formatDate'}
                        onChange={option => {
                          this.props.onChange('formatDay', option);
                        }}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

AreaAndHours.propTypes = {};
export default AreaAndHours;
