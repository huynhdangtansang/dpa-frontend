/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
// import FontFaceObserver from 'fontfaceobserver';
import createHistory from 'history/createHashHistory';
// import Q from 'q';
import 'sanitize.css/sanitize.css';

// Import root app
import App from 'containers/App';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Import CSS Libraries
import '!style-loader!css-loader!sass-loader!./css/bootstrap/css/bootstrap.min.css';
import '!style-loader!css-loader!sass-loader!./css/font-awesome/css/all.css';
import '!style-loader!css-loader!sass-loader!./css/icon/style.css';
import '!style-loader!css-loader!sass-loader!./css/react-bootstrap-table/react-bootstrap-table.css';
import '!style-loader!css-loader!sass-loader!./css/react-image-crop/ReactCrop.css';
import '!style-loader!css-loader!sass-loader!./css/react-datepicker/react-datepicker.css';
import '!style-loader!css-loader!sass-loader!./css/rc-timepicker/index.css';
import '!style-loader!css-loader!sass-loader!./css/react-toastify/ReactToastify.css';
// Import JS Libraries

// Load the favicon and the .htaccess file
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import 'file-loader?name=[name].[ext]!./.htaccess'; // eslint-disable-line import/extensions
import '!file-loader?name=[name].[ext]!./images/icon/menu.svg';
import '!file-loader?name=[name].[ext]!./images/icon/arrow-left.svg';
import '!file-loader?name=[name].[ext]!./images/icon/edit.svg';
import '!file-loader?name=[name].[ext]!./images/icon/logo-color.svg';
import '!file-loader?name=[name].[ext]!./images/icon/logo-white-h.svg';
import '!file-loader?name=[name].[ext]!./images/default-user.png';
import '!file-loader?name=[name].[ext]!./images/logo-white.png';
import '!file-loader?name=[name].[ext]!./images/icon/logo-color-white.svg';
import '!file-loader?name=[name].[ext]!./images/web/logo.png';
import '!file-loader?name=[name].[ext]!./images/web/radio-btn.png';
import '!file-loader?name=[name].[ext]!./images/web/checkbox.png';
import '!file-loader?name=[name].[ext]!./images/web/checkbox-checked.png';
import '!file-loader?name=[name].[ext]!./images/web/edit-ico.png';
import '!file-loader?name=[name].[ext]!./images/web/like.png';

/*///////////////////////////////////////////////////////////////
/////                      START LOGO                       /////
///////////////////////////////////////////////////////////////*/

// Android

// iOS

/*///////////////////////////////////////////////////////////////
/////                      STOP LOGO                       /////
///////////////////////////////////////////////////////////////*/

/*///////////////////////////////////////////////////////////////
/////                START SPLASH SCREEN                   /////
/////////////////////////////////////////////////////////////// */
// Android
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-hdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-ldpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-mdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-xhdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-xxhdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-land-xxxhdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-hdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-ldpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-mdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-xhdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-xxhdpi-screen.png';
import '!file-loader?name=[name].[ext]!./images/screen/android/drawable-port-xxxhdpi-screen.png';

// iOS
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default@2x~iphone.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-568h@2x~iphone.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-667h.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-736h.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-Landscape@2x~ipad.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-Landscape-736h.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-Landscape~ipad.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-Portrait@2x~ipad.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default-Portrait~ipad.png';
import '!file-loader?name=[name].[ext]!./images/screen/ios/Default~iphone.png';

/* ///////////////////////////////////////////////////////////////
/////                STOP SPLASH SCREEN                   /////
/////////////////////////////////////////////////////////////// */

import configureStore from './configureStore';

// Import i18n messages
import { translationMessages } from './i18n';

// Import CSS reset and Global Styles
import './global-styles';
import './css/style.scss';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
// const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
// openSansObserver.load().then(() => {
//   document.body.classList.add('fontLoaded');
// });

// Add is-ios to body when current platform is ios
document.addEventListener('deviceready', () => {
  const platformId = window.cordova.platformId;
  if (platformId) {
    document.querySelector('#app').classList.add(`is-${platformId}`);
  }
});

// Create redux store with history
const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

const render = messages => {
  ReactDOM.render(
    <Provider store={store}>
      <LanguageProvider messages={messages}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </LanguageProvider>
    </Provider>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n', 'containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  new Promise(resolve => {
    resolve(import('intl'));
  })
    .then(() =>
      Promise.all([
        import('intl/locale-data/jsonp/en.js'),
        import('intl/locale-data/jsonp/de.js'),
      ]),
    )
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}