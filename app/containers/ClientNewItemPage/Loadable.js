/**
 *
 * Asynchronously loads the component for ClientNewItemPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
