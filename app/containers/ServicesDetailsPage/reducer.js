/*
 *
 * ServicesDetailsPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  GET_SERVICE_DATA,
  GET_SUCCESS,
  SET_EDIT_MODE,
  SHOW_COMMISSION_CONFIRM,
  SHOW_REMOVE_CONFIRM
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  serviceData: {},
  edit: false,
  showRemoveConfirm: false,
  showChangeCommissionConfirm: false
});

function servicesDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_SERVICE_DATA:
      return initialState;
    case GET_SUCCESS:
      return state
          .set('serviceData', fromJS(action.response.data))
          .set('showRemoveConfirm', false)
          .set('showChangeCommissionConfirm', false);
    case SET_EDIT_MODE:
      return state
          .set('edit', action.value);
    case SHOW_REMOVE_CONFIRM:
      return state
          .set('showRemoveConfirm', action.value);
    case SHOW_COMMISSION_CONFIRM:
      return state
          .set('showChangeCommissionConfirm', action.value);
    default:
      return state;
  }
}

export default servicesDetailsPageReducer;
