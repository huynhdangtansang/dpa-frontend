/*
 *
 * ServiceEditPage reducer
 *
 */
import { fromJS } from "immutable";
//Library
import _ from "lodash";
import {
  ADD_AREA,
  ADD_CERT,
  ADD_COUPON,
  ADD_TAG,
  CHANGE_IMAGE,
  DELETE_TAG,
  GET_SERVICE_DETAILS,
  GET_SUCCESS,
  SHOW_MODAL,
  UPDATE_AREA,
  UPDATE_CERT,
  UPDATE_COUPON
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  iconSrc: './default-user.png',
  backgroundSrc: './default-user.png',
  serviceDetails: null,
  couponCodes: [],
  keywords: [],
  certifications: [],
  areas: [],
  deletedKeywords: [],
  showConfirmModal: false,
  showIconModal: false,
  showBackgroundModal: false
});

function serviceEditPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_SERVICE_DETAILS:
      return initialState;
    case GET_SUCCESS:
      return state
          .set('serviceDetails', fromJS(action.response.data))
          .set('keywords',
              fromJS(action.response.data.keywords.map((keyword) => {
                return {
                  id: keyword._id,
                  text: keyword.keyName,
                  action: 'edit'
                }
              })))
          .set('couponCodes',
              fromJS(action.response.data.couponCodes.map((coupon) => {
                return {
                  id: coupon._id,
                  name: coupon.name,
                  code: coupon.code,
                  disCount: coupon.disCount,
                  disCountType: coupon.disCountType,
                  expiredAt: coupon.expiredAt,
                  action: 'edit',
                  error: false
                }
              }))
          )
          .set('certifications',
              fromJS(action.response.data.certifications.map((cert) => {
                return {
                  id: cert._id,
                  content: cert.content,
                  action: 'edit',
                  error: false
                }
              }))
          )
          .set('areas',
              fromJS(action.response.data.areas.map((area) => {
                return {
                  id: area._id,
                  country: area.country,
                  city: area.city,
                  suburbs: area.suburbs.join(","),
                  action: 'edit',
                  error: false
                }
              }))
          )
          .set('iconSrc', !_.isNull(action.response.data.icon) ? action.response.data.icon.fileName : './default-user.png')
          .set('backgroundSrc', !_.isNull(action.response.data.background) ? action.response.data.background.fileName : './default-user.png')
          .set('deletedKeywords', fromJS([]));
    case ADD_COUPON:
      return state
          .updateIn(['couponCodes'], arr => fromJS(arr.push(action.newCoupon)));
    case UPDATE_COUPON:
      if (action.newItem.action === 'delete') {
        if (action.newItem.id === '') {
          let newArray = [];
          newArray = state.get('couponCodes').filter((coupon, index) => index !== action.id).toJS();
          return state
              .set('couponCodes', fromJS(newArray))
        } else {
          return state
              .updateIn(['couponCodes'], arr => fromJS(arr.map((coupon, index) => {
                return (index === action.id) ? action.newItem : coupon
              })))
        }
      } else {
        return state
            .updateIn(['couponCodes'], arr => arr.map((coupon, index) => {
              return (index === action.id) ? action.newItem : coupon
            }))
      }
    case ADD_CERT:
      return state
          .updateIn(['certifications'], arr => arr.push(action.newCert));
    case UPDATE_CERT:
      if (action.newItem.action === 'delete') {
        if (action.newItem.id === '') {
          let newArray = [];
          newArray = state.get('certifications').filter((cert, index) => index !== action.id).toJS();
          return state
              .set('certifications', fromJS(newArray))
        } else {
          return state
              .updateIn(['certifications'], arr => arr.map((cert, index) => {
                return (index === action.id) ? action.newItem : cert
              }))
        }
      } else {
        return state
            .updateIn(['certifications'], arr => arr.map((cert, index) => {
              return (index === action.id) ? action.newItem : cert
            }))
      }
    case ADD_AREA:
      return state
          .updateIn(['areas'], arr => arr.push(action.newArea));
    case UPDATE_AREA:
      if (action.newItem.action === 'delete') {
        if (action.newItem.id === '') {
          let newArray = [];
          newArray = state.get('areas').filter((area, index) => index !== action.id).toJS();
          return state
              .set('areas', fromJS(newArray))
        } else {
          return state
              .updateIn(['areas'], arr => arr.map((area, index) => {
                return (index === action.id) ? action.newItem : area
              }))
        }
      } else {//add
        return state
            .updateIn(['areas'], arr => arr.map((area, index) => {
              return (index === action.id) ? action.newItem : area
            }))
      }
    case ADD_TAG:
      return state
          .updateIn(['keywords'], arr => arr.push(action.newTag));
    case DELETE_TAG: {
      let tag = state.get('keywords').toJS()[action.id];
      if (tag.id !== '') {
        tag.action = 'delete';
        return state
            .updateIn(['deletedKeywords'], arr => arr.push(tag))
            .set('keywords', state.get('keywords').filter((keyword, index) => index !== action.id))
      } else {
        return state
            .set('keywords', state.get('keywords').filter((keyword, index) => index !== action.id))
      }
    }
    case CHANGE_IMAGE:
      return state
          .setIn([action.key], action.value);
    case SHOW_MODAL:
      return state
          .set(action.modal, action.value);
    default:
      return state;
  }
}

export default serviceEditPageReducer;
