/*
 *
 * UserProfilePage constants
 *
 */
export const DEFAULT_ACTION = "app/UserProfilePage/DEFAULT_ACTION";
export const RESET_ERROR = "/UserProfilePage/RESET_ERROR";
export const RESET_STORE = "app/UserProfilePage/RESET_STORE";
export const GET_USER_PROFILE = "/UserProfilePage/GET_USER_PROFILE";
export const GET_SUCCESS = "/UserProfilePage/GET_SUCCESS";
export const UPDATE_USER_PROFILE = "/UserProfilePage/UPDATE_USER_PROFILE";
export const SET_EDIT_MODE = "UserProfilePage/SET_EDIT_MODE";
export const CHANGE_AVATAR = "UserProfilePage/CHANGE_AVATAR";
export const SEND_EMAIL_CONFIRM = "UserProfilePage/SEND_EMAIL_CONFIRM";
export const UPDATE_SUCCESS = "UserProfilePage/UPDATE_SUCCESS";
export const UPDATE_ERROR = "UserProfilePage/UPDATE_ERROR";
export const CHANGE_UPDATE_STATE = "UserProfilePage/CHANGE_UPDATE_STATE";
export const CHANGE_IS_SENT = "UserProfilePage/CHANGE_IS_SENT";
export const SEND_VERIFY_CODE = "UserProfilePage/SEND_VERIFY_CODE";
export const SEND_CODE_SUCCESS = "UserProfilePage/SEND_CODE_SUCCESS";
export const SEND_CODE_ERROR = "UserProfilePage/SEND_CODE_ERROR";
export const RESEND_VERIFY_CODE = "UserProfilePage/RESEND_VERIFY_CODE";
export const RESEND_SUCCESS = "UserProfilePage/RESEND_SUCCESS";
export const RESEND_ERROR = "UserProfilePage/RESEND_ERROR";
export const SET_VALUE_ERRORS = "UserProfilePage/SET_VALUE_ERRORS";
