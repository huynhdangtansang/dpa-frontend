/*
 *
 * TeamListPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  GET_ERROR,
  GET_PERIOD_SUCCESS,
  GET_SUCCESS,
  GET_TEAM_LIST,
  SELECT_ALL_MEMBER,
  SELECT_MEMBER,
  SET_CURRENT_MEMBER,
  SHOW_MODAL,
  UPDATE_DROPDOWN_PERIOD,
  UPDATE_MODIFY_TYPE,
  UPDATE_PAGE,
  UPDATE_SEARCH_DATA
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";
//Library
import moment from 'moment';

export const initialState = fromJS({
  teamList: [],
  errors: [],
  periodList: [],
  filterPeriod: {
    value: '',
    label: ''
  },
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    active: false,
    sort: '',
    sortType: '',
    period: ''
  },
  selectedMember: [],
  chosenMember: {},
  currentPage: 1,
  pagination: {},
  modifyType: 'single',
  showRemoveConfirm: false,
  showChangeStatusConfirm: false
});

function teamListPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_TEAM_LIST:
      return state
          .set('teamList', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('teamList', action.response.data.data)
          .set('pagination', action.response.data.pagination)
          .set('showRemoveConfirm', false)
          .set('showChangeStatusConfirm', false)
          .set('selectedMember', fromJS([]))
          .set('errors', action.response.errors);
    case GET_ERROR:
      return state
          .set('teamList', action.response.data.data)
          .set('errors', action.response.errors);
    case UPDATE_PAGE:
      return state
          .set('currentPage', action.newPage);
    case SET_CURRENT_MEMBER:
      return state
          .set('chosenMember', action.member);
    case UPDATE_SEARCH_DATA:
      return state
          .set('searchData', fromJS(action.newSearchData));
    case SELECT_MEMBER:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedMember'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedMember', fromJS(state.get('selectedMember').toJS().filter((member) => member.id !== action.selectItem.id)))
      }
    case SELECT_ALL_MEMBER:
      if (action.isSelected === true) {
        let newArray = state.get('teamList').map((member) => {
          return {
            id: member._id,
            status: member.active
          }
        });
        return state
            .set('selectedMember', fromJS(newArray))
      } else {
        return state
            .set('selectedMember', fromJS([]))
      }
    case SHOW_MODAL:
      if (action.modal === 'remove') {
        return state.set('showRemoveConfirm', action.value)
      } else if (action.modal === 'changeStatus') {
        return state.set('showChangeStatusConfirm', action.value)
      }
      return state;
    case UPDATE_MODIFY_TYPE:
      return state.set('modifyType', action.value);

      //Period
    case GET_PERIOD_SUCCESS:
      action.resovle(moment(action.list[0].value));
      return state
          .setIn(['searchData', 'period'], action.list[0].value)
          .set('filterPeriod', fromJS({ label: action.list[0].label, value: action.list[0].value }))
          .set('periodList', fromJS(action.list));
    case UPDATE_DROPDOWN_PERIOD:
      return state
          .set('filterPeriod', fromJS(action.newFilterPeriod))
          .setIn(['searchData', 'period'], action.newFilterPeriod.value ? [action.newFilterPeriod.value] : []);
    default:
      return state;
  }
}

export default teamListPageReducer;
