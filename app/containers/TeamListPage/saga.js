import { put, takeLatest } from 'redux-saga/effects';
import { CHANGE_MEMBER_STATUS, DELETE_MEMBER, GET_PERIOD, GET_TEAM_LIST, UPDATE_SEARCH_DATA } from './constants';
import { getError, getSuccess, getTeamList } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import _ from 'lodash';
import axios from 'axios';

export function* apiGetTeamList(data) {
  if (data) {
    let { searchData: params = {}, resovle: resovle = undefined, reject: reject = undefined } = data;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => k + '=' + esc(params[k]))
      .join('&');
    const requestUrl = config.serverUrl + config.api.teamMember + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
      if (_.isFunction(resovle))
        resovle(response.data);
    } catch (error) {
      yield put(getError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
      if (_.isFunction(reject))
        reject(error.response.data);
    }
  }
}

export function* apiChangeMemberStatus(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/deactives';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.dataUpdate);
      yield put(reposLoaded());
      yield put(getTeamList(data.searchData));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteMember(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(getTeamList(data.searchData));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* getListAgain(data) {
  if (data) {
    yield put(getTeamList(data.newSearchData));
  }
}

export function* apiGetPeriod(data) {
  const requestUrl = config.serverUrl + config.api.teamMember + '/getPeriodMaster';
  try {
    const response = yield axios.get(requestUrl);
    //yield put(getPeriodSuccess(response.data));

    if (_.isFunction(data.resolve)) {
      data.resolve(response.data);
    }
  } catch (error) {
    yield put(updateError({
      error: true,
      title: 'Error!!!',
      message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
    }));
    yield put(reposLoaded());
    if (_.isFunction(data.resolve)) {
      data.resolve(error);
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_TEAM_LIST, apiGetTeamList);
  yield takeLatest(CHANGE_MEMBER_STATUS, apiChangeMemberStatus);
  yield takeLatest(DELETE_MEMBER, apiDeleteMember);
  yield takeLatest(UPDATE_SEARCH_DATA, getListAgain);
  //Period
  yield takeLatest(GET_PERIOD, apiGetPeriod);
}
