/*
 *
 * ApprovalAreaListPage constants
 *
 */
export const DEFAULT_ACTION = "app/ApprovalAreaListPage/DEFAULT_ACTION";
export const RESET_STATE = "app/ApprovalAreaListPage/RESET_STATE";
export const GET_APPROVAL_LIST = "/ApprovalAreaListPage/GET_APPROVAL_LIST";
export const GET_SUCCESS = "/ApprovalAreaListPage/GET_SUCCESS";
export const APPROVE_COMPANY = "/ApprovalAreaListPage/APPROVE_COMPANY";
export const REMOVE_COMPANY = "/ApprovalAreaListPage/REMOVE_COMPANY";
export const SELECT_COMPANY = "/ApprovalAreaListPage/SELECT_COMPANY";
export const SELECT_ALL_COMPANY = "/ApprovalAreaListPage/SELECT_ALL_COMPANY";
export const UPDATE_SEARCH_STRING = "/ApprovalAreaListPage/UPDATE_SEARCH_STRING";
export const UPDATE_FILTER_LIST = "/ApprovalAreaListPage/UPDATE_FILTER_LIST";