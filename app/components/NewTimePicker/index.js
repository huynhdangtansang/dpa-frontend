import React from "react";
import moment from 'moment';
import ClassNames from 'classnames';
import _ from "lodash";
import { checkApiError } from "helper/exportFunction";
import './style.scss';

export default class TimePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggle: false,
      isInside: false
    }
  }

  changeHour = (type) => {
    switch (type) {
      case 'add':
        this.props.onChange(moment(this.props.time).add(1, 'hours'));
        break;
      case 'subtract':
        this.props.onChange(moment(this.props.time).subtract(1, 'hours'));
        break;
    }
  };
  changeMinute = (type) => {
    switch (type) {
      case 'add':
        this.props.onChange(moment(this.props.time).add(1, 'm'));
        break;
      case 'subtract':
        this.props.onChange(moment(this.props.time).subtract(1, 'm'));
        break;
    }
  };
  changeSession = () => {
    if (moment(this.props.time).format('A') === 'AM') {
      this.props.onChange(moment(this.props.time).add(12, 'h'));
    } else {
      this.props.onChange(moment(this.props.time).subtract(12, 'h'));
    }
  };
  insideToggler = () => {
    this.setState({ isInside: true });
  };
  outsideToggler = () => {
    this.setState({ isInside: false });
  };
  toggle = () => {
    let { isToggle: status } = this.state;
    const { hoursFromNow, time } = this.props;
    this.setState({ isToggle: !status }, () => {
      if (this.state.isToggle) {
        document.body.addEventListener('click', this.closeToggle);
      }
      if (!_.isUndefined(hoursFromNow)) {
        let chosen = moment(time);
        let diff = chosen.diff(moment(), 'hours');
        if (diff <= hoursFromNow - 1) {
          this.props.onChange(moment().add(hoursFromNow, 'h'));
        }
      }
    });
  };
  closeToggle = () => {
    const { isInside } = this.state;
    const { hoursFromNow, time } = this.props;
    if (isInside === false) {
      this.setState({ isToggle: false }, () => {
        if (!_.isUndefined(hoursFromNow)) {
          let chosen = moment(time);
          let diff = chosen.diff(moment(), 'hours');
          if (diff <= hoursFromNow - 1) {
            this.props.onChange(moment().add(hoursFromNow, 'h'));
          }
        }
      });
      document.body.removeEventListener('click', this.closeToggle);
      event.stopPropagation();
    }
  };
  changeTime = (e, type) => {
    let number = e.deltaY;
    if (number === 100) {
      if (type === 'hour') {
        this.changeHour('add');
      } else {
        this.changeMinute('add');
      }
    } else if (number === -100) {
      if (type === 'hour') {
        this.changeHour('subtract');
      } else {
        this.changeMinute('subtract');
      }
    }
  };
  handleScroll = (type) => {
    let scroll;
    if (type === 'hour') {
      scroll = document.getElementById('hour_scroll');
    } else {
      scroll = document.getElementById('minute_scroll');
    }
    scroll.addEventListener('mousewheel', (e) => {
      this.changeTime(e, type);
    }, { passive: true });
  };
  clearEvent = (type) => {
    let scroll;
    if (type === 'hour') {
      scroll = document.getElementById('hour_scroll');
    } else {
      scroll = document.getElementById('minute_scroll');
    }
    scroll.removeEventListener('mousewheel', (e) => {
      this.changeTime(e, type);
    }, { passive: true });
  };

  render() {
    return (
        <div className="time-picker">
          <input
              className={ClassNames('input-form', ((this.props.touched && this.props.error) || checkApiError(this.props.name, this.props.apiError)) && 'error-form')}
              value={_.isUndefined(this.props.time) ? '' : moment(this.props.time).format('hh:mm A')}
              readOnly={true}
              onClick={this.toggle}
              placeholder={'Select time'}
              onChange={() => {
              }}
          />
          {this.state.isToggle &&
          <div className="toggler" onMouseEnter={this.insideToggler} onMouseLeave={this.outsideToggler}>
            <div className="hour-and-minute">
              <div className="wrapper">
                <div id="hour_scroll" className="time-item hour"
                     onMouseEnter={() => {
                       this.handleScroll('hour');
                     }}
                     onMouseLeave={() => {
                       this.clearEvent('hour');
                     }}>
                  <div className="icon" onClick={(e) => {
                    e.preventDefault();
                    this.changeHour('subtract');
                  }}>
                    <div className="icon-contain up">
                      <span className="icon-chevron-up"></span>
                    </div>
                  </div>
                  <div className="value">
                    <span>{moment(this.props.time).format('hh')}</span>
                  </div>
                  <div className="icon" onClick={(e) => {
                    e.preventDefault();
                    this.changeHour('add');
                  }}>
                    <div className="icon-contain down">
                      <span className="icon-chevron-down"></span>
                    </div>
                  </div>
                </div>
                <div className="time-item divider">
                  <div className="value">
                    <span>:</span>
                  </div>
                </div>
                <div id="minute_scroll" className="time-item minute"
                     onMouseEnter={() => {
                       this.handleScroll('minute');
                     }}
                     onMouseLeave={() => {
                       this.clearEvent('minute')
                     }}>
                  <div className="icon" onClick={() => {
                    this.changeMinute('subtract');
                  }}>
                    <div className="icon-contain up">
                      <span className="icon-chevron-up"></span>
                    </div>
                  </div>
                  <div className="value">
                    <span>{moment(this.props.time).format('mm')}</span>
                  </div>
                  <div className="icon" onClick={() => {
                    this.changeMinute('add');
                  }}>
                    <div className="icon-contain down">
                      <span className="icon-chevron-down"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="session">
              <div className="switcher">
                <div className="value">
                  <span>{moment(this.props.time).format('A')}</span>
                </div>
                <div className="icon" onClick={() => {
                  this.changeSession();
                }}>
                  <span className="icon-rotation"></span>
                </div>
              </div>
            </div>
          </div>
          }
        </div>
    )
  }
}
