/**
 *
 * InputForm
 *
 */
import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import ClassNames from 'classnames';
import './style.scss';
import { listError } from "helper/data";
//Library
import _, { findIndex, indexOf } from "lodash";

function checkApiError(name, error) {
  if (!_.isEmpty(error)) {
    let index = findIndex(listError, val => val.name === name);
    if (index > -1) {
      if (indexOf(listError[index].error, error[0].errorCode) > -1) {
        return true;
      }
    }
    return false;
  }
}

function InputForm(props) {
  return (
      <div className={'input-group form-input'}>
        {props.label && (<div className="form-label" htmlFor={props.name}>{props.label}</div>)}

        {props.prependLabel && (
            <div className="input-group-prepend">
              <div className="input-group-text">
                {props.prependLabel}
              </div>
            </div>
        )}

        <input
            className={ClassNames('input-form',
                ((props.touched && props.error) || checkApiError(props.name, props.apiError)) && 'error-form',
                !props.showPassword && 'hide-password')}
            min={props.min}
            max={props.max}
            name={props.name}
            type={props.type}
            onChange={props.onChange}
            onBlur={props.onBlur}
            icon={props.icon}
            value={props.value}
            placeholder={props.placeholder}
        />

        {(props.value && props.value.length > 0) ? (
            <div className="icon-append" onClick={props.togglePassword}>
              {
                !props.showPassword ? <i className={props.iconClassShow}/> : <i className={props.iconClassHide}/>
              }
            </div>
        ) : []}

        {props.showIcon && (
            <div className="icon-append">
              <i className={props.icon}></i>
            </div>
        )}


        {(_.isBoolean(props.touched) && props.touched && props.error) && (
            <div className={'error-text'}>
              <i className={'icon-error'}/>
              <span>{props.error}</span>
            </div>
        )}
      </div>
  );
}

InputForm.propTypes = {};
export default InputForm;
