import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the companyDetailsPage state domain
 */

const selectCompanyDetailsPageDomain = state =>
    state.get("companyDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by CompanyDetailsPage
 */

const makeSelectCompanyDetailsPage = () =>
    createSelector(selectCompanyDetailsPageDomain, substate => substate.toJS());
export { selectCompanyDetailsPageDomain, makeSelectCompanyDetailsPage };