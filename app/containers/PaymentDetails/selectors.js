import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the paymentDetails state domain
 */

const selectPaymentDetailsDomain = state =>
    state.get("paymentDetails", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by PaymentDetails
 */

const makeSelectPaymentDetails = () =>
    createSelector(selectPaymentDetailsDomain, substate => substate.toJS());
export default makeSelectPaymentDetails;
export { selectPaymentDetailsDomain };
