/*
 *
 * TeamMemberDetailsPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  memberDetails: {},
  activityList: [],
  errors: [],
});

function teamMemberDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_MEMBER_DETAILS:
      return state
          .set('memberDetails', fromJS([]))
          .set('errors', fromJS([]));
    case Constants.GET_SUCCESS:
    case Constants.GET_ERROR:
      return state
          .set('memberDetails', action.response.data)
          .set('errors', action.response.errors);

      //Activities
    case Constants.GET_ACTIVITIES_SUCCESS:
      return state
          .set('activityList', fromJS(action.response.data));
    default:
      return state;
  }
}

export default teamMemberDetailsPageReducer;
