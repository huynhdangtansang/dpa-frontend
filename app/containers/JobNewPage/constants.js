/*
 *
 * JobNewPage constants
 *
 */
export const DEFAULT_ACTION = "app/JobNewPage/DEFAULT_ACTION";
export const RESET_STATE = "app/JobNewPage/RESET_STATE";
export const CLEAR_ERROR = "app/JobNewPage/CLEAR_ERROR";
export const GET_ALL_DATA_DROPDOWN = "JobNewPage/GET_ALL_DATA_DROPDOWN";
export const GET_SUCCESS = "JobNewPage/GET_SUCCESS";
export const UPDATE_JOB_DESCRIPTION = "JobNewPage/UPDATE_JOB_DESCRIPTION";
export const UPDATE_ERROR = "JobNewPage/UPDATE_ERROR";
export const ADD_IMAGE = "JobNewPage/ADD_IMAGE";
export const REMOVE_IMAGE = "JobEdtiDescription/REMOVE_IMAGE";
export const CHANGE_LOCATION = "JobNewPage/CHANGE_LOCATION";
export const CHOOSE_ITEM = "JobNewPage/CHOOSE_ITEM";
export const CHANGE_APPOINTMENT = "JobNewPage/CHANGE_APPOINTMENT";
export const ADD_JOB = "JobNewPage/ADD_JOB";
export const ADD_COMPLETE = "JobNewPage/ADD_COMPLETE";
export const GET_COMPANIES = "JobNewPage/GET_COMPANIES";
export const GET_COMPANIES_COMPLETE = "JobNewPage/GET_COMPANIES_COMPLETE";
export const GET_EMPLOYEES = "JobNewPage/GET_EMPLOYEES";
export const GET_EMPLOYEES_COMPLETE = "JobNewPage/GET_EMPLOYEES_COMPLETE";