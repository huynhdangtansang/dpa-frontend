/*
 *
 * DashBoardPage constants
 *
 */
export const DEFAULT_ACTION = "app/DashBoardPage/DEFAULT_ACTION";
export const SIGN_OUT = "/DashBoardPage/SIGN_OUT";
export const SIGN_OUT_SUCCESS = "/DashBoardPage/SIGN_OUT_SUCCESS";
export const SIGN_OUT_ERROR = "/DashBoardPage/SIGN_OUT_ERROR";
export const SELECT_MENU = "/DashBoardPage/SELECT_MENU";
export const GET_USER_PROFILE = "DashBoardPage/GET_USER_PROFILE";
export const GET_SUCCESS = "DashBoardPage/GET_SUCCESS";
//NOTIFICATION
export const GET_NOTIFICATION = "DashBoardPage/GET_NOTIFICATION";
export const GET_NOTIFICATION_COMPLETE = "DashBoardPage/GET_NOTIFICATION_COMPLETE";
export const SELECT_NOTIFICATION = "DashBoardPage/SELECT_NOTIFICATION";
export const HIDE_NOTIFICATION = "DashBoardPage/HIDE_NOTIFICATION";
