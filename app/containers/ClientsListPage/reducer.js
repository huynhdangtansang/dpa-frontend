/*
 *
 * ClientsListPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  CHANGE_IS_SUCCESS,
  CLEAR_ALL_SELECTED_CLIENT,
  GET_CLIENTS_LIST,
  GET_SUBURBS_SUCCESS,
  GET_SUCCESS,
  SELECT_ALL_CLIENT,
  SELECT_CLIENT,
  SHOW_MODAL,
  UPDATE_SEARCH_DATA
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  clientsList: [],
  isSent: false,
  selectedClients: [],
  dataList: [],
  suburbsList: [],
  chosenSuburb: '',
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    suburb: '',
    sort: 'fullName',
    sortType: 'asc'
  },
  currentPage: 1,
  showResetPasswordConfirm: false
});

function clientsListPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_CLIENTS_LIST:
      return state
          .set('clientsList', fromJS([]))
          .set('dataList', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('clientsList', action.response.data)
          .set('dataList', action.response.data.data);
      //.set('selectedClients', fromJS([]))
    case CHANGE_IS_SUCCESS:
      return state
          .set('isSent', action.value)
          .set('selectedClients', fromJS([]));
    case SELECT_CLIENT:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedClients'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedClients', fromJS(state.get('selectedClients').toJS().filter((client) => client.id !== action.selectItem.id)))
      }
    case SELECT_ALL_CLIENT:
      if (action.isSelected === true) {
        let newArray = state.get('dataList').map((client) => {
          return {
            id: client._id,
            status: client.active
          }
        });
        return state
            .set('selectedClients', fromJS(newArray))
      } else {
        return state
            .set('selectedClients', fromJS([]))
      }
    case GET_SUBURBS_SUCCESS:
      return state
          .set('suburbsList', action.response.data);
    case SHOW_MODAL:
      if (action.modal === 'resetPassword') {
        return state
            .set('showResetPasswordConfirm', action.value)
      }
      return state;
    case UPDATE_SEARCH_DATA:
      return state
      //.set('dataList', fromJS([]))
          .set('searchData', fromJS(action.newSearchData))
          .set('currentPage', fromJS(action.newPage ? action.newPage : 1));
    case CLEAR_ALL_SELECTED_CLIENT:
      return state
          .set('selectedClients', fromJS([]));
    default:
      return state;
  }
}

export default clientsListPageReducer;
