import { fromJS } from 'immutable';
import serviceEditPageReducer from '../reducer';

describe('serviceEditPageReducer', () => {
  it('returns the initial state', () => {
    expect(serviceEditPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
