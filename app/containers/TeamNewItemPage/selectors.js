import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the teamNewItemPage state domain
 */

const selectTeamNewItemPageDomain = state =>
    state.get("teamNewItemPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by TeamNewItemPage
 */

const makeSelectTeamNewItemPage = () =>
    createSelector(selectTeamNewItemPageDomain, substate => substate.toJS());
export default makeSelectTeamNewItemPage;
export { selectTeamNewItemPageDomain };
