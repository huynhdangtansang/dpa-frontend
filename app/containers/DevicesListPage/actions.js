/*
 *
 * DevicesListPage actions
 *
 */
import {
  CHANGE_DEVICE_STATUS,
  CHANGE_IS_SUCCESS,
  CLEAR_ALL_SELECTED_DEVICE,
  DEFAULT_ACTION,
  DELETE_DEVICE,
  GET_DEVICES_LIST,
  GET_SUBURBS_LIST,
  GET_SUBURBS_SUCCESS,
  GET_SUCCESS,
  RESET_PASSWORD,
  RESET_STATE,
  SELECT_ALL_DEVICE,
  SELECT_DEVICE,
  SHOW_MODAL,
  UPDATE_SEARCH_DATA
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: RESET_STATE
  };
}

export function getDevicesList(dataSearch) {
  return {
    type: GET_DEVICES_LIST,
    searchData: dataSearch
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function deleteDevice(deleteData, searchData) {
  return {
    type: DELETE_DEVICE,
    deleteData,
    searchData
  }
}

export function changeDeviceStatus(statusData, searchData) {
  return {
    type: CHANGE_DEVICE_STATUS,
    statusData,
    searchData
  }
}

export function resetPassword(resetData) {
  return {
    type: RESET_PASSWORD,
    resetData
  }
}

export function clearAllSelectedDevice() {
  return {
    type: CLEAR_ALL_SELECTED_DEVICE
  }
}

export function changeIsSuccess(value) {
  return {
    type: CHANGE_IS_SUCCESS,
    value
  }
}

export function selectDevice(isSelected, selectItem) {
  return {
    type: SELECT_DEVICE,
    isSelected,
    selectItem
  }
}

export function selectAllDevice(isSelected) {
  return {
    type: SELECT_ALL_DEVICE,
    isSelected
  }
}

export function getSuburbsList() {
  return {
    type: GET_SUBURBS_LIST
  }
}

export function getSuburbsSuccess(response) {
  return {
    type: GET_SUBURBS_SUCCESS,
    response
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}

export function updateSearchData(newSearchData, newPage) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData,
    newPage
  }
}
