/**
 *
 * ServicesDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectEditMode, makeSelectServiceData, makeSelectServicesDetailsPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  changeShowCommissionConfirm,
  changeShowRemoveConfirm,
  deleteService,
  getPdf,
  getService,
  resetState,
  setEditMode,
  updateCommission
} from "./actions";
import './style.scss';
//lib
import { UncontrolledTooltip } from 'reactstrap';
import _ from "lodash";
import GhostButton from 'components/GhostButton';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
//Data to view
import ServiceDetailsDataView from 'components/ServiceDetailsDataView';
import FixleChargeModal from 'components/FixleChargeModal';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ServicesDetailsPage extends React.PureComponent {
  goToEdit = (sectionRef, action) => {
    //action is option for button add
    //default not required
    const { serviceData } = this.props.servicesdetailspage;
    this.props.history.push({
      pathname: urlLink.editService,
      search: '?id=' + serviceData._id,
      state: {
        sectionRef: sectionRef ? sectionRef : '',
        action: _.isUndefined(action) ? 'edit' : action
      }
    })
  };

  constructor(props) {
    super(props);
    this.state = {}
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let serviceID = temp[1];
    this.props.getServiceData(serviceID);
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const { serviceData, showRemoveConfirm, showChangeCommissionConfirm } = this.props.servicesdetailspage;
    return (
        <div className="service-details">
          <div className="header-edit-add-page service-edit-header">
            <div className="action">
              <div className="return" id="return"
                   onClick={() => {
                     this.props.history.goBack();
                   }}>
                <span className="icon-arrow-left"/>
              </div>

              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return">
                Back</UncontrolledTooltip>

              {!_.isEmpty(serviceData) && (
                  <div className="btn-group float-right" hidden={this.props.edit}>
                    <GhostButton className={'btn-ghost'} title={'Remove'} onClick={() => {
                      this.props.showRemoveConfirm(true);
                    }}/>
                    <GhostButton className={'btn-ghost'} title={'Print'} onClick={() => {
                      this.props.getPdf([serviceData._id]);
                    }}/>
                    <GhostButton className={'btn-ghost'} title={'Edit'} onClick={() => {
                      this.goToEdit();
                    }}/>
                    <GhostButton className={'btn-ghost edit-fixle-charge'} title={'Edit Fixle Charge'} onClick={() => {
                      this.props.showCommissionConfirm(true);
                    }}/>
                  </div>
              )}

            </div>
            <div className="title">
              {serviceData && (<span>{this.props.edit ? 'Edit Service' : serviceData.name}</span>)}
            </div>
          </div>

          {serviceData && !this.props.edit &&
          <ServiceDetailsDataView data={serviceData} goToEdit={this.goToEdit}/>
          }

          <RemoveConfirmModal
              show={showRemoveConfirm}
              reason={'this.state.reason'}
              object={'service'}
              action={'single'}
              handleChange={(e) => {
                this.setState({ reason: e });
              }}
              closeModal={() => {
                this.props.showRemoveConfirm(false);
              }}
              removeObject={() => {
                if (serviceData._id) {
                  let data = {
                    ids: [serviceData._id]
                  };
                  this.props.delete(data);
                }
              }}/>


          <FixleChargeModal
              show={showChangeCommissionConfirm}
              onSubmit={commission => {
                let listIds = [];
                listIds.push(serviceData._id);
                let data = {
                  commission: commission,
                  ids: listIds
                };
                this.props.changeCharge(data);
              }}
              closeModal={() => {
                this.props.showCommissionConfirm(false);
              }}
          />
        </div>
    );
  }
}

ServicesDetailsPage.propTypes = {
  resetState: PropTypes.func,
  dispatch: PropTypes.func,
  getServiceData: PropTypes.func,
  changeEditMode: PropTypes.func,
  delete: PropTypes.func,
  changeCharge: PropTypes.func,
  showRemoveConfirm: PropTypes.func,
  showCommissionConfirm: PropTypes.func,
  getPdf: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  servicesdetailspage: makeSelectServicesDetailsPage(),
  serviceData: makeSelectServiceData(),
  edit: makeSelectEditMode()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(resetState());
    },
    getServiceData: (serviceID) => {
      dispatch(getService(serviceID));
    },
    changeEditMode: (value) => {
      dispatch(setEditMode(value));
    },
    showRemoveConfirm: (value) => {
      dispatch(changeShowRemoveConfirm(value));
    },
    showCommissionConfirm: (value) => {
      dispatch(changeShowCommissionConfirm(value));
    },
    delete: (deleteData) => {
      dispatch(deleteService(deleteData));
    },
    changeCharge: (updateData) => {
      dispatch(updateCommission(updateData));
    },
    getPdf: (serviceId) => {
      dispatch(getPdf(serviceId));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "servicesDetailsPage", reducer });
const withSaga = injectSaga({ key: "servicesDetailsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ServicesDetailsPage);
