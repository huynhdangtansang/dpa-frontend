/*
 *
 * ApprovalAreaListPage actions
 *
 */
import * as constants from "./constants";

export function defaultAction() {
  return {
    type: constants.DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: constants.RESET_STATE
  };
}

export function getApprovalListData(searchData) {
  return {
    type: constants.GET_APPROVAL_LIST,
    searchData: searchData
  };
}

// export function getServiceListData(searchData) {
//   return {
//     type: constants.GET_SERVICE_DATA,
//     searchData: searchData
//   };
// }
export function approveCompany(companyIds, searchData) {
  return {
    type: constants.APPROVE_COMPANY,
    companyIds,
    searchData
  };
}

export function removeCompany(dataDelete, searchData) {
  return {
    type: constants.REMOVE_COMPANY,
    dataDelete,
    searchData
  };
}

export function updateSearchString(textSearch) {
  return {
    type: constants.UPDATE_SEARCH_STRING,
    textSearch
  }
}

export function updateFilter(newSearchData, newPage) {
  return {
    type: constants.UPDATE_FILTER_LIST,
    newSearchData,
    newPage
  }
}

//get list approval success
export function getSuccess(response) {
  return {
    type: constants.GET_SUCCESS,
    response: response
  }
}

export function selectCompany(isSelected, selectItem) {
  return {
    type: constants.SELECT_COMPANY,
    isSelected,
    selectItem
  }
}

export function selectAllCompany(isSelected) {
  return {
    type: constants.SELECT_ALL_COMPANY,
    isSelected
  }
}