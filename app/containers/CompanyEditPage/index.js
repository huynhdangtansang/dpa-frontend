/**
 *
 * EditCompanyPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import _, { debounce, findIndex, indexOf } from "lodash";
import uuidv1 from 'uuid';
import config from 'config';
//readux saga
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectEditCompanyPage } from "./selectors";
import * as Actions from "./actions";
import { loadRepos, updateError } from 'containers/App/actions';
import reducer from "./reducer";
import saga from "./saga";
import './style.scss';
//Component
import Location from 'components/Location';
import { Scrollbars } from 'react-custom-scrollbars';
import InputForm from "components/InputForm";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import SubmitButton from 'components/SubmitButton';
import TextareaCounter from 'components/TextareaCounter';
import CheckBoxSquare from 'components/CheckBoxSquare';
import Switch from 'react-switch';
import UploadProofModal from 'components/UploadProofModal';
import CropImage from 'components/CropImage';
import Selection from 'components/Selection';
import AreaAndHours from 'components/AreaAndHours';
//Support for auto complete
import { loadCountries, placeAutoComplete, removePlus, removeZero } from 'helper/exportFunction';
import InputFile from "components/InputFile";
//data
import { country, listError } from 'helper/data';
//Name link
import { urlLink } from 'helper/route';
//Lib
import moment from "moment";
//Formik
import { Input, Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

const validateForm = Yup.object().shape({
  'companyName': Yup.string()
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (from 6 - 30 and no special characters)')
    .min(6, 'Invalid name (from 6 - 30 and no special characters)')
    .max(30, 'Invalid name (from 6 - 30 and no special characters)'),
  'abnNumber': Yup.string()
    .matches(/^[0-9]*$/, 'Invalid ABN number'),
  'contactPersonName': Yup.string()
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (from 6 - 30 and no special characters)')
    .min(6, 'Invalid name (from 6 - 30 and no special characters)')
    .max(30, 'Invalid name (from 6 - 30 and no special characters)'),
  'bankAccountName': Yup.string()
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid account name (no special characters)')
    .min(6, 'Invalid account name (from 6 - 30 and no special characters)')
    .max(30, 'Invalid account name (from 6 - 30 and no special characters)'),
  'bankName': Yup.string()
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid bank name (no special characters)')
    .min(6, 'Invalid bank name (from 6 - 30 and no special characters)')
    .max(30, 'Invalid bank name (from 6 - 30 and no special characters)'),
  'bsbNumber': Yup.string()
    .matches(/^[0-9]*$/, 'Invalid BSB number (format xxxxxx)')
    .min(6, 'Invalid BSB number (format xxxxxx)')
    .max(6, 'Invalid BSB number (format xxxxxx)'),
  'accountNumber': Yup.string()
    .matches(/^[0-9]*$/, 'Invalid account number'),
});

/* eslint-disable react/prefer-stateless-function */
export class EditCompanyPage extends React.PureComponent {
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onload = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeLogo(src);
    this.setState({
      cropSrc: '',
      avatarFile: file,
    });
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e);
    }
  };
  closeModal = () => {
    this.setState({ showAvatarModal: false, cropSrc: '' });
  };
  getContactPerson = (value) => {
    this.setState({ contactPerson: value });
  };
  getContactCountryValue = (value) => {
    this.setState({ countryName: value });
  };
  getStatusValue = (value) => {
    this.setState({ status: value });
  };
  handleSearchTextChange = debounce(text => {
    this.props.updateSearchDataService('search', text);
    const { companyData, searchDataForService } = this.props.editcompanypage;
    this.props.getServiceData(companyData, searchDataForService);
  }, 1000);
  openUploadProof = (index) => {
    this.props.selectLicense(index);
  };
  updateLicenseItem = (key, value, index) => {
    this.props.updateLicenseItem(key, value, index);
  };
  cancelUploadProofConfirm = () => {
    this.props.clearLicenseImage();
  };
  addLicenseImage = (image) => {
    this.props.addLicenseImage(image)
  };
  removeLicenseImage = (index) => {
    this.props.removeLicenseImage(index)
  };
  updateDateExpiredTemp = (date) => {
    this.props.updateDateExpiredTemp(date)
  };
  // Edit company data
  onHandleChangeService = (type, itemService, index) => {
    let promise = new Promise((resolve, reject) => {
      if (index >= 0) {
        resolve(this.props.editCompanyListServiceData(type, !itemService.isCheck, index));
      } else {
        reject("Promise rejected");
      }
    });
    //that need do after chose checkbox service data
    promise.then(result => {
      const { companyData, myServiceData, dataBackup } = this.props.editcompanypage;
      this.props.selectService(myServiceData[index], companyData, dataBackup);
      return result;
    }, function (error) {
      return error;
    });
  };
  debounceAfterSelectService = debounce((index) => {
    //that need do after chose checkbox service data
    const { companyData, myServiceData, dataBackup } = this.props.editcompanypage;
    this.props.selectService(myServiceData[index], companyData, dataBackup);
  }, 1000);
  // Split this into 3 functions because of debounce
  loadCountryOptions = (inputValue, callback) => {
    placeAutoComplete('country', inputValue).then((rs) => {
      callback(rs);
    })
  };
  loadCityOptions = (inputValue, callback) => {
    placeAutoComplete('city', inputValue, this.props.data.country.alpha2_code).then((rs) => {
      callback(rs);
    })
  };
  loadSuburbOptions = (inputValue, callback) => {
    placeAutoComplete('suburbs', inputValue, this.props.data.country.alpha2_code).then((rs) => {
      callback(rs);
    })
  };
  //Clear api error
  clearApiError = (type) => {
    const { updateErrors } = this.props.editcompanypage;
    if (updateErrors && updateErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, updateErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };
  //Handle response error from api
  handleErrorResponse = (error) => {
    if (!_.isEmpty(error)) {
      if (!_.isEmpty(error.errors)) {
        if (findIndex(listError, item => {
          return indexOf(item.error, error.errors[0].errorCode) > -1
        }) > -1) {
          //this.props.onChangeStore({ key: ['formError'], value: error.errors });
        } else {
          this.props.updateError({
            error: true,
            title: 'Error!!!',
            message: error.errors[0].errorMessage
          });
        }
      } else {
        this.props.updateError({
          error: true,
          title: 'Error!!!',
          message: 'Internal server error. Please contact with your administrator to resolve this.'
        });
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      showAvatarModal: false,
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let companyID = temp[1];
    const { searchDataForService } = this.props.editcompanypage;
    this.props.getAllDataSetup(companyID, searchDataForService);
    // let now = moment(); // This returns the Date/Time of right now.
    // moment.tz.add('America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0');
    // let nowNz = moment.tz.names();
  }

  componentWillUnmount() {
    this.props.resetDefault();
  }

  _disableButton(value, error) {
    //Loop through validation fields

    //const { companyData: { address } } = this.props.editcompanypage;
    const keys = [
      'companyName',
      'abnNumber',
      'address',
      'contactPersonName',
      'contactNumber',
      'aboutUs',
      'bankAccountName',
      'bankName',
      'bsbNumber',
      'accountNumber',
      'country',
      'timeZone',
      'city',
      'suburbs',
      'formatTime',
      'formatDay',
    ];
    for (let key of keys) {
      if (key === 'address') {//address need have name
        if (!_.isObject(value[key]) || _.isUndefined(value[key].name) || _.isEmpty(value[key].geoInfo)) {
          return true;
        }
      }
      else {
        if (_.isEmpty(value[key]) || !_.isEmpty(error[key])) {
          //If this field has error or
          return true;
        }
      }
    }
    return false;
  }

  _isEmptyService() {
    const { myServiceData } = this.props.editcompanypage;
    let tempArray = myServiceData;
    tempArray = tempArray.filter((item) => {
      return item.isCheck === true
    });
    if (tempArray.length > 0) {
      return false;
    } else
      return true;
  }

  render() {
    const {
      companyData, companyLogo, myServiceData,
      areas,
      indexLicenseData, listLicense, listAcceptRateCards,
      tempListLicenseImage, tempDateExpired, proofPopup, searchDataForService,
      companyData: {
        name: name = '',
        abnNumber: abnNumber = '',
        address: address = { name: '' },
      }
    } = this.props.editcompanypage;


    return (
      <div className="edit-company company-details">
        {!_.isEmpty(companyData) && (
          <div>
            <div className="header-edit-add-page">
              <div className="action">
                <div className="return" id="return" onClick={() => this.props.history.goBack()}>
                  <span className="icon-arrow-left"></span>
                </div>
                <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return"
                  container='return'>
                  Back</UncontrolledTooltip>
              </div>
              <div className="title">
                <span>Edit Company</span>
              </div>
            </div>
            <input type="file" id="file" ref="fileUploader" style={{ display: "none" }}
              onChange={(e) => {
                this.uploadImage(e);
                e.target.value = null;
              }} />

            <Formik ref={ref => (this.formik = ref)}
              initialValues={{
                //Company information
                companyName: name,
                abnNumber: abnNumber,
                address: address,
                contactPersonName: companyData && companyData.contactPerson ? companyData.contactPerson : (companyData.createdBy ? companyData.createdBy.fullName : ''),
                contactArea: {
                  value: !_.isEmpty(companyData) ? companyData.countryCode : '',
                  label: !_.isEmpty(companyData) ? companyData.countryName : ''
                },
                contactNumber: companyData ? companyData.phoneNumber : '',
                aboutUs: companyData ? companyData.description : '',
                //status: companyData ? (companyData.active ? true : false) : false,
                //Licenses
                //bank details
                bankAccountName: !_.isEmpty(companyData.banks) && _.isObject(companyData.banks[0]) ? companyData.banks[0].accountName : '',
                accountNumber: !_.isEmpty(companyData.banks) && _.isObject(companyData.banks[0]) ? companyData.banks[0].accountNumber : '',
                bankName: !_.isEmpty(companyData.banks) && _.isObject(companyData.banks[0]) ? companyData.banks[0].bankName : '',
                bsbNumber: !_.isEmpty(companyData.banks) && _.isObject(companyData.banks[0]) ? companyData.banks[0].bsbNumber : '',
                //areas
                country: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].country : '',
                timeZone: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].timeZone : '',
                city: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].city : '',
                suburbs: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].suburbs : '',
                formatTime: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].formatTime : '',
                formatDay: !_.isEmpty(companyData.areas) && _.isObject(companyData.areas[0]) ? companyData.areas[0].formatDay : '',
              }}
              enableReinitialize={true}
              validationSchema={validateForm}
              onSubmit={(e) => {
                //This will save to store when click Save in popup confirm modal
                //Or clear all data when click Cancle in popup confirm modal
                let formDataInfo = new FormData();
                let objectService = {
                  services: []
                };
                let objectBank = {};
                let objectAcceptRateCard = {
                  "acceptRateCards": []
                };
                let objectListLicenses = [];
                let objectRegionHours = {};
                //Company Info
                if (this.state.avatarFile) {
                  formDataInfo.append('file', this.state.avatarFile);
                }
                formDataInfo.append('name', e.companyName);
                formDataInfo.append('abnNumber', e.abnNumber);
                
                let addressTemp = _.pick(e.address, 'name', 'geoInfo');//just get field name, geoInfo
                
                formDataInfo.append('address', JSON.stringify(addressTemp));

                formDataInfo.append('contactPerson', e.contactPersonName);
                formDataInfo.append('countryName', e.contactArea.label);
                formDataInfo.append('countryCode', e.contactArea.value);
                formDataInfo.append('phoneNumber', removeZero(e.contactNumber));
                formDataInfo.append('description', e.aboutUs);
                //Service
                const { companyData } = this.props.editcompanypage;
                companyData.services.map((item) => {
                  objectService.services.push(item._id);
                });
                //Accept Rate Card
                const { listAcceptRateCards } = this.props.editcompanypage;
                listAcceptRateCards.map((item) => {
                  objectAcceptRateCard.acceptRateCards.push({
                    "isAccept": item.isAccept,
                    "serviceId": item.serviceId._id
                  })
                });
                //List license
                const { listLicense } = this.props.editcompanypage;
                listLicense.map((item) => {
                  let formDataLicense = new FormData();
                  formDataLicense.append('serviceId', item.serviceId ? item.serviceId._id : null);
                  formDataLicense.append('certificationId', item.certificationId ? item.certificationId._id : null);
                  if (item.expiredAt) {
                    formDataLicense.append('expiredAt', item.expiredAt.toString());
                  }
                  let arrayImageServerTemp = [];
                  if (!_.isEmpty(item.proofs)) {
                    item.proofs.map((proofImage) => {
                      //this is file upload from image
                      if (proofImage.file)
                        formDataLicense.append('files', proofImage.file);
                      else//this is file in server
                        arrayImageServerTemp.push(proofImage._id);
                    });
                  }
                  let temp = JSON.stringify(arrayImageServerTemp);
                  formDataLicense.append('imageIds', temp);
                  objectListLicenses.push(formDataLicense);
                });
                //Bank
                objectBank = {
                  "accountName": e.bankAccountName,
                  "bankName": e.bankName,
                  "bsbNumber": e.bsbNumber,
                  "accountNumber": e.accountNumber,
                  "action": _.isArray(companyData.banks) && companyData.banks.length > 0 ? "edit" : "add",
                  "id": _.isArray(companyData.banks) && companyData.banks.length ? companyData.banks[0]._id : ""
                };
                //Region and Hours
                objectRegionHours = {
                  "timeZone": areas.timeZone.value,
                  "country": areas.country.label,
                  "city": areas.city.value,
                  "suburbs": areas.suburbs.map((item) => {
                    return item['value']
                  }),
                  "formatTime": areas.formatTime.value,
                  "formatDay": areas.formatDay.value,
                  "action": areas._id.value ? "edit" : "add",
                  "id": _.isString(areas._id.value) ? areas._id.value : ''
                };
                this.props.saveDataUpdateCompany(
                  {
                    formDataInfo,
                    objectService,
                    objectAcceptRateCard,
                    objectListLicenses,
                    objectBank,
                    objectRegionHours
                  }, true);
                this.setState({ showUpdateConfirm: true });
                //this.props.updateAllCompany(companyData._id, { formDataInfo, objectService, listLicenses, objectBank, objectListLicenses, objectRegionHours });
              }}>
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue,
              }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="content content-add-edit">
                      {/* Company information */}
                      <div className="information">
                        <div className="row">
                          <div className="col-md-4 left">
                            <div className="title">
                              <span>Company Information</span>
                            </div>
                          </div>
                          <div className="col-md-8 right">
                            <div className='company-logo'>
                              <div className="image-wrapper">
                                <img src={companyLogo} alt="logo" onError={(e) => {
                                  e.target.onerror = null;
                                  e.target.src = './default-user.png'
                                }} />

                                <InputFile
                                  name={'companyLogo'}
                                  src={companyLogo}
                                  onBlur={handleBlur}
                                  btnText={companyLogo ? 'change logo' : 'upload logo'}
                                  inlineErrorMessge={false}
                                  onChange={evt => {
                                    this._handleImageChange(evt);
                                  }} />
                              </div>
                            </div>
                            <div className="details">
                              <InputForm
                                label="company name"
                                name={'companyName'}
                                placeholder={'Company name'}
                                value={values.companyName}
                                error={errors.companyName}
                                touched={touched.companyName}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              <InputForm
                                label="abn number"
                                name='abnNumber'
                                placeholder={'ABN Number'}
                                value={values.abnNumber}
                                error={errors.abnNumber}
                                touched={touched.abnNumber}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              {/* <InputForm
                                label="company address"
                                name='address'
                                value={values.address.name}
                                error={errors.address}
                                touched={touched.address}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} /> */}
                              <Location
                                name={'address'}
                                title={'Company address'}
                                placeholder={'Address'}
                                country={config.country}
                                value={values.address.name}
                                onSelect={newAddress => {
                                  setFieldValue('address', newAddress);
                                }}
                                onChange={evt => {
                                  handleChange(evt);
                                  setFieldValue('address', { name: '' });
                                }}
                              />
                              <InputForm
                                label="Company Contact Person"
                                name='contactPersonName'
                                placeholder={'Company Contact Person'}
                                value={values.contactPersonName}
                                error={errors.contactPersonName}
                                touched={touched.contactPersonName}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              <div className="info-item contact-number">

                                <Selection
                                  options={country}
                                  title={'Contact number'}
                                  name={'contactArea'}
                                  value={values.contactArea}
                                  isAsyncList={true}
                                  loadOptions={debounce(loadCountries, 700)}
                                  getOptionLabel={option => option.label}
                                  getOptionValue={option => option.value}
                                  type={'contactArea'}
                                  onBlur={handleBlur}
                                  onChange={option => {
                                    setFieldValue('contactArea', {
                                      value: option.country_code,
                                      label: option.label
                                    });
                                    this.clearApiError('phoneNumber');
                                  }} />
                                <InputForm
                                  name={'contactNumber'}
                                  prependLabel={'+' + removePlus(values.contactArea.value)}
                                  value={values.contactNumber}
                                  error={errors.contactNumber}
                                  touched={touched.contactNumber}
                                  placeholder={'Phone number'}
                                  onChange={evt => {
                                    handleChange(evt);
                                  }}
                                  onBlur={handleBlur} />
                              </div>

                              <div className="info-item">
                                <div className="title">
                                  <span>About us</span>
                                </div>
                                <div className="data">
                                  <TextareaCounter
                                    name='aboutUs'
                                    placeholder={'Describe your business'}
                                    value={values.aboutUs}
                                    error={errors.aboutUs}
                                    touched="true"
                                    maxLength={300}
                                    onChange={evt => {
                                      handleChange(evt);
                                    }}
                                    onBlur={handleBlur}
                                  />

                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Service */}
                      <div className="information">
                        <div className="row">
                          <div className="col-md-4 left">
                            <div className="title">
                              <span>Services</span>
                            </div>
                          </div>
                          <div className="col-md-8 right">
                            <div className="form-input">
                              <div className="search-bar services-search">
                                <Input
                                  type="text"
                                  placeholder="Search type of service"
                                  defaultValue={searchDataForService.search}
                                  onKeyPress={(e) => {
                                    if (e.key === 'Enter') e.preventDefault();
                                    this.handleSearchTextChange(e.target.value);
                                  }}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    this.handleSearchTextChange(e.target.value);
                                  }} />

                                <span className="icon-search"></span>

                              </div>
                            </div>
                            <div className='list-service'>
                              {myServiceData && (
                                <Scrollbars
                                  // This will activate auto hide
                                  autoHide
                                  // Hide delay in ms
                                  autoHideTimeout={1000}
                                  autoHeight
                                  autoHeightMin={0}
                                  autoHeightMax={448}>

                                  {myServiceData.map((item, index) => (
                                    <div className='service-item' key={uuidv1()}>
                                      <CheckBoxSquare
                                        value={item.isCheck}
                                        onChange={() => {
                                          this.onHandleChangeService('isCheck', item, index);
                                        }}>
                                      </CheckBoxSquare>
                                      <img className='logo-service'
                                        src={item.icon ? item.icon.fileName : './default-user.png'}
                                        alt="logo" onError={(e) => {
                                          e.target.onerror = null;
                                          e.target.src = './default-user.png'
                                        }} />
                                      <span className="name-service">{item.name}</span>
                                    </div>
                                  ))}

                                </Scrollbars>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Accept Rate Card */}
                      <div className="information">
                        <div className="title">
                          <span>Accept Rate Card</span>
                        </div>
                        <div className="list table-accept-rate-card">
                          <table>
                            <thead className="title">
                              <tr>
                                <td className="service"><span>Services</span></td>
                                <td className=""><span>hourly rate</span></td>
                                <td className=""><span>minimum hours</span></td>
                                <td className=""><span>minimum charge</span></td>
                                <td className=""><span>fixle charge</span></td>
                                <td className="top-priority"><span>Acceptance</span></td>
                              </tr>
                            </thead>

                            <tbody className="content">
                              {listAcceptRateCards && listAcceptRateCards.map((item, index) => (
                                <tr key={uuidv1()}>
                                  <td className="service">
                                    <span>{item.serviceId ? item.serviceId.name : '-'}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.serviceId.rateHours.unit + parseFloat(item.serviceId.rateHours.value).toFixed(2)}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.serviceId ? item.serviceId.minHours.value : '-'}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.serviceId.minCharge.unit + parseFloat(item.serviceId.minCharge.value).toFixed(2)}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.serviceId.commission.value}{item.serviceId.commission.unit}</span>
                                  </td>
                                  <td className="acceptance">
                                    <Switch
                                      onChange={() => {
                                        this.props.toogleAcceptance('isAccept', !item.isAccept, index);
                                      }}
                                      checked={item.isAccept ? item.isAccept : false}
                                      onColor="#917bd5"
                                      offColor="#212121"
                                      height={15}
                                      width={27}
                                      handleDiameter={10}
                                      uncheckedIcon={false}
                                      checkedIcon={false}
                                      className="react-switch"
                                    />
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                      </div>

                      {/* Lisence */}
                      <div className="information">
                        <div className="title">
                          <span>Licenses and Certifications</span>
                        </div>
                        <div className="list table-lisence">
                          <table>
                            <thead className="title">
                              <tr>
                                <td className="items"><span>items</span></td>
                                <td className="certifications"><span>certifications required</span></td>
                                <td className="expiry-date"><span>expiry date</span></td>
                                <td className="proof"><span>proof</span></td>

                              </tr>
                            </thead>

                            <tbody className="content">
                              {listLicense && listLicense.map((item, index) => (
                                <tr key={index}>
                                  <td className="">
                                    <span>{item.serviceId ? item.serviceId.name : '-'}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.certificationId ? item.certificationId.content : '-'}</span>
                                  </td>
                                  <td className="">
                                    <span>{item.expiredAt ? moment(new Date(item.expiredAt)).format('ddd D MMM YYYY') : '-'}</span>
                                  </td>
                                  <td className="column-img">
                                    <div className="fixle-list-image-overflow">
                                      {_.isEmpty(item.proofs) ? '-' : item.proofs.map((image) => (
                                        <img key={uuidv1()} src={image.fileName} onError={(e) => {
                                          e.target.onerror = null;
                                          e.target.src = './default-user.png'
                                        }} />
                                      ))}
                                    </div>

                                    <span className="edit-proof filxe-link" onClick={() => {
                                      this.openUploadProof(index);
                                    }}>
                                      {item.proofs && item.proofs.length > 0 ? 'Change' : 'Upload'}
                                    </span>
                                  </td>
                                </tr>
                              ))}

                            </tbody>
                          </table>
                        </div>
                      </div>

                      {/* Bank Details */}
                      <div className="information">
                        <div className="row">
                          <div className="col-md-4 left">
                            <div className="title">
                              <span>Bank Details</span>
                            </div>
                          </div>
                          <div className="col-md-8 right">
                            <div className="details">
                              <InputForm
                                label="Account Name"
                                name='bankAccountName'
                                value={values.bankAccountName}
                                error={errors.bankAccountName}
                                touched={touched.bankAccountName}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              <InputForm
                                label="bank name"
                                name='bankName'
                                value={values.bankName}
                                error={errors.bankName}
                                touched={touched.bankName}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                              <InputForm
                                label="bsb number"
                                type="text"
                                name='bsbNumber'
                                value={values.bsbNumber}
                                error={errors.bsbNumber}
                                touched={touched.bsbNumber}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />

                              <InputForm
                                label="account number"
                                type="text"
                                name='accountNumber'
                                value={values.accountNumber}
                                error={errors.accountNumber}
                                touched={touched.accountNumber}
                                onChange={evt => {
                                  handleChange(evt);
                                }}
                                onBlur={handleBlur} />
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Area and Hours */}
                      <AreaAndHours
                        data={areas}
                        onChange={(name, option) => {
                          this.props.onChangeAreaAndHours({
                            name: name,
                            value: option
                          });
                        }}
                      />
                    </div>
                    <div className="footer">
                      <GhostButton type="button" className="btn-ghost cancel" title={'Cancel'}
                        onClick={() => {
                          this.props.saveDataUpdateCompany({});
                          this.props.history.goBack();
                        }} />
                      <SubmitButton
                        type={'submit'}
                        disabled={this._disableButton(values, errors) || this._isEmptyService()}
                        content={'Save'} />
                    </div>
                  </form>
                )}
            </Formik>
            {(this.state.cropSrc && this.state.cropSrc !== '') ?
              <CropImage
                show={this.state.showAvatarModal}
                cropSrc={this.state.cropSrc}
                closeModal={this.closeModal}
                changeImage={this.changeImage}
              />
              : []
            }

            {indexLicenseData >= 0 && (
              <UploadProofModal
                key={indexLicenseData}
                show={proofPopup}
                expiredAt={tempDateExpired}
                itemLicense={listLicense[indexLicenseData]}
                dataProof={tempListLicenseImage}
                addImage={(image) => this.addLicenseImage(image)}
                removeImage={(index) => this.removeLicenseImage(index)}
                updateDateExpiredTemp={(date) => this.props.updateDateExpiredTemp(date)}
                cancelModal={this.cancelUploadProofConfirm}
                updateLicenseItem={(key, value) => this.updateLicenseItem(key, value, indexLicenseData)}
              />
            )}

            <Modal isOpen={this.state.showUpdateConfirm} className="logout-modal update-confirm">
              <ModalBody>
                <div className="remove-confirm">
                  <div className="upper">
                    <div className="title">
                      <span>Save This Change</span>
                    </div>
                    <div className="description">
                      <span>Are you sure to want to save this change? This action could influence on all data involved.</span>
                    </div>
                  </div>
                  <div className="lower">
                    <GhostButton type="button" className="btn-ghost cancel" title={'cancel'}
                      onClick={() => {
                        this.setState({ showUpdateConfirm: false });
                        //const { dataUpdateCompany } = this.props.editcompanypage;
                        this.props.saveDataUpdateCompany({});
                      }} />
                    <PurpleRoundButton className="btn-purple-round save" title={'save'}
                      onClick={() => {
                        this.setState({ showUpdateConfirm: false });
                        const { companyData, dataUpdateCompany } = this.props.editcompanypage;
                        this.props.updateAllCompany(companyData._id, dataUpdateCompany).then(() => {
                          if (companyData.isApproved)
                            this.props.history.push(urlLink.viewCompany + '?id=' + companyData._id);
                          else
                            this.props.history.push(urlLink.viewApproval + '?id=' + companyData._id);
                        }).catch(error => {
                          this.handleErrorResponse(error);
                        });
                      }}
                    />
                  </div>
                </div>
              </ModalBody>
            </Modal>

          </div>
        )}
      </div>
    );
  }
}

EditCompanyPage.propTypes = {
  dispatch: PropTypes.func,
  getCompanyData: PropTypes.func,
  getServiceData: PropTypes.func,
  updateCompanyProfile: PropTypes.func,
  getAllDataSetup: PropTypes.func,
  startLoad: PropTypes.func,
  editCompanyData: PropTypes.func,
  updateLicenseItem: PropTypes.func,
  updateDateExpiredTemp: PropTypes.func,
  changeLogo: PropTypes.func,
  updateSearchDataService: PropTypes.func,
  updateError: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  editcompanypage: makeSelectEditCompanyPage(),
  //companyData: makeSelectCompanyData(),
  //companyLogo: makeSelectLogo(),
  //serviceData: makeSelectServiceData(),
});

function mapDispatchToProps(dispatch) {
  return {
    resetDefault: () => {
      dispatch(Actions.defaultAction());
    },
    resetError: () => {
      dispatch(Actions.resetError());
    },
    getCompanyData: (companyID) => {
      dispatch(Actions.getCompany(companyID));
    },
    getAllDataSetup: (companyID, searchDataForService) => {
      dispatch(Actions.getAllDataSetup(companyID, searchDataForService));
    },
    getServiceData: (companyData, searchDataForService) => {
      dispatch(Actions.getService(companyData, searchDataForService));
    },
    updateAllCompany: (companyID, data) => {
      return new Promise((resolve, reject) => {
        dispatch(Actions.updateAllCompany(companyID, data, resolve, reject));
      });
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    editCompanyListServiceData: (type, value, index) => {
      dispatch(Actions.editCompanyListServiceData(type, value, index));
      //return index >= 0 ? true : false;
    },
    selectService: (itemService, companyData, dataBackup) => {
      let serviceIds = [];
      let newMyServiceData = companyData.services;
      if (itemService.isCheck) {
        newMyServiceData.push(itemService);
      } else {
        newMyServiceData = newMyServiceData.filter((item) => item._id !== itemService._id);
      }
      newMyServiceData.map((item) => {
        serviceIds.push(item._id);
      });
      //call api update Licenses and Certifications, Accept Rate Card
      dispatch(Actions.selectService(newMyServiceData, companyData._id, serviceIds, dataBackup));
      //call api update Accept Rate Card
      //dispatch(Actions.selectService(companyID, serviceIds));
    },
    selectLicense: (index) => {
      dispatch(Actions.selectLicense(index));
    },
    updateLicenseItem: (key, value, index) => {
      dispatch(Actions.updateItemProof(key, value, index));
    },
    updateDateExpiredTemp: (date) => {
      dispatch(Actions.updateDateExpiredTemp(date));
    },
    changeLogo: (src) => {
      dispatch(Actions.changeLogo(src));
    },
    addLicenseImage: (image) => {
      dispatch(Actions.addLicenseImage(image));
    },
    removeLicenseImage: (index) => {
      dispatch(Actions.removeLicenseImage(index));
    },
    clearLicenseImage: () => {
      dispatch(Actions.clearLicenseImage());
    },
    updateSearchDataService: (key, value) => {
      dispatch(Actions.updateSearchDataService(key, value));
    },
    toogleAcceptance: (key, value, index) => {
      dispatch(Actions.toogleAcceptance(key, value, index));
    },
    onChangeAreaAndHours: (event) => {
      dispatch(Actions.changeAreaAndHours(event));
    },
    saveDataUpdateCompany: (data) => {
      //option true will save to dataUpdateCompany in store
      //option false will clear dataUpdateCompany in store
      dispatch(Actions.saveDataUpdateCompany(data));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "editCompanyPage", reducer });
const withSaga = injectSaga({ key: "editCompanyPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(EditCompanyPage);
