/**
 *
 * EmployeeDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import * as Actions from "./actions";
import './style.scss';
//Lib
import { TabContent, TabPane, UncontrolledTooltip } from 'reactstrap';
import moment from 'moment';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectEmployeeDetailsPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
//Component
import GhostButton from 'components/GhostButton';
import EmployeeTabControl from 'components/EmployeeTabControl';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import EmployeeTabOverview from 'components/EmployeeTabOverview';
import EmployeeTabReviews from 'components/EmployeeTabReviews';

/* eslint-disable react/prefer-stateless-function */
export class EmployeeDetailsPage extends React.PureComponent {
  changeStatus = () => {
    const { memberDetails } = this.props.employeedetailspage;
    let listIds = [];
    let data = {};
    listIds.push(memberDetails._id);
    if (memberDetails.active == true) {
      data['ids'] = listIds;
      data['reason'] = this.state.reason;
      data['active'] = !memberDetails.active;
    } else {
      data['ids'] = listIds;
      data['active'] = !memberDetails.active;
    }
    this.props.changeStatus(memberDetails._id, data);
  };
  deleteMember = () => {
    const { memberDetails } = this.props.employeedetailspage;
    let listIds = [];
    listIds.push(memberDetails._id);
    let data = {
      ids: listIds,
      reason: this.state.reason
    };
    this.props.deleteEmployee(data);
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeStatusConfirm = () => {
    this.setState({ showChangeStatusConfirm: false });
  };
  closeRemoveConfirm = () => {
    this.setState({ showRemoveConfirm: false });
  };

  constructor(props) {
    super(props);
    let url = window.location.href;
    let temp = url.split('?id=');
    this.state = {
      memberID: temp[1] ? temp[1] : '',
      activeTab: '1',
      showRemoveConfirm: false,
      showChangeStatusConfirm: false,
      reason: '',
    };
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this.props.getDetails(this.state.memberID);
    this.toggle(this.state.activeTab);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
    switch (tab) {
      case '1': {
        //Overview
        const { periodJob } = this.props.employeedetailspage;
        this.props.getActivitiesEmployee(this.state.memberID);
        this.props.getJobAssigned(this.state.memberID, { dateTime: moment(periodJob).format('MM/YYYY') });
        this.props.getEmployeeReview(this.state.memberID);
        this.props.getEmployeeEarning(this.state.memberID, moment().format('MM/YYYY'));
        break;
      }
      case '2'://Reviews
        this.props.getEmployeeReview(this.state.memberID);
        break;
      default:
        return false;
    }
  }

  render() {
    const {
      memberDetails: {
        _id: id = '',
        active: active = 'Deactivate',
        fullName: fullName = '',
        createdAt: createdAt = null
      }
    } = this.props.employeedetailspage;
    return (
      <div className="member-profile">
        <div>
          <div className="header-edit-add-page">
            <div className="action">
              <div className="return" id="return" onClick={() => {
                this.props.history.goBack();
              }}>
                <span className='icon-arrow-left'></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return">
                Back</UncontrolledTooltip>
              <div className="btn-group float-right">
                <GhostButton className={'btn-ghost remove'} title={'Remove'}
                  onClick={() => {
                    this.setState({ showRemoveConfirm: true, reason: '' });
                  }} />
                <GhostButton className={'btn-ghost deactive'}
                  title={active ? 'Deactivate' : 'Activate'}
                  onClick={() => {
                    this.setState({ showChangeStatusConfirm: true, reason: '' });
                  }} />
              </div>
            </div>
            <div className="title">
              <span>{fullName}</span>
              <div className="joined-date">
                <span>Joined in {moment(createdAt).format('ddd D MMM YYYY')}</span>
              </div>
            </div>
            <EmployeeTabControl activeTab={this.state.activeTab} toggle={this.toggle} />
          </div>
          <div className="content employee-detail-content">
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1" className="fixel-tab-content">
                <EmployeeTabOverview key={id} {...this.props} toggle={this.toggle} />
              </TabPane>
              <TabPane tabId="2" className="fixel-tab-content reviews">
                <EmployeeTabReviews key={id} {...this.props} />
              </TabPane>
            </TabContent>
          </div>

          <ChangeStatusConfirmModal
            show={this.state.showChangeStatusConfirm}
            reason={this.state.reason}
            currentStatus={active}
            action='single'
            object={'employee'}
            handleChange={this.handleReasonChange}
            closeModal={this.closeStatusConfirm}
            changeStatus={this.changeStatus} />
          <RemoveConfirmModal
            show={this.state.showRemoveConfirm}
            reason={this.state.reason}
            action='single'
            object={'employee'}
            handleChange={this.handleReasonChange}
            closeModal={this.closeRemoveConfirm}
            removeObject={this.deleteMember} />
        </div>

      </div>
    );
  }
}

EmployeeDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  getDetails: PropTypes.func,
  getActivitiesEmployee: PropTypes.func,
  changeStatus: PropTypes.func,
  deleteEmployee: PropTypes.func,
  getEmployeeReview: PropTypes.func,
  getEmployeeEarning: PropTypes.func,
  onChangeStore: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  employeedetailspage: makeSelectEmployeeDetailsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    getDetails: (id) => {
      dispatch(Actions.getMemberDetails(id));
    },
    changeStatus: (id, data) => {
      dispatch(Actions.changeMemberStatus(id, data));
    },
    deleteEmployee: (data) => {
      dispatch(Actions.deleteMember(data));
    },
    onChangeStore(event) {
      dispatch(Actions.changeEmployeeStore(event));
    },
    //Review
    getEmployeeReview(id) {
      return new Promise((resolve, reject) => {
        dispatch(Actions.getEmployeeReview({ id, resolve, reject }));
      });
    },
    //Earning
    getEmployeeEarning(id, time) {
      return new Promise((resolve, reject) => {
        dispatch(Actions.getEmployeeEarning({ id, time, resolve, reject }));
      });
    },
    //Job
    getJobAssigned: (id, searchData) => {
      dispatch(Actions.getJobAssigned(id, searchData));
    },
    //Activities employee
    getActivitiesEmployee: (id) => {
      dispatch(Actions.getActivitiesEmployee(id));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "employeeDetailsPage", reducer });
const withSaga = injectSaga({ key: "employeeDetailsPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(EmployeeDetailsPage);
