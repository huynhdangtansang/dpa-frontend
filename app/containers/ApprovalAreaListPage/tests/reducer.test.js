import { fromJS } from 'immutable';
import approvalAreaListPageReducer from '../reducer';

describe('approvalAreaListPageReducer', () => {
  it('returns the initial state', () => {
    expect(approvalAreaListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
