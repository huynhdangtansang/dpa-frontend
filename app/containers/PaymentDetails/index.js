/**
 *
 * PaymentDetails
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPaymentDetails from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import './style.scss';
//lib
import moment from 'moment';
import { UncontrolledTooltip } from 'reactstrap';
import NumberFormat from 'react-number-format';
import _ from "lodash";
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
//Component
import GhostButton from 'components/GhostButton';
import SubmitButton from 'components/SubmitButton';
import TextareaCounter from 'components/TextareaCounter';
import ProofUpload from "components/ProofUpload";
import RemoveConfirmModal from 'components/RemoveConfirmModal';
/* eslint-disable react/prefer-stateless-function */
const MAX_NUMBER = 4;
const MAX_FILE_SIZE = 2 * 1024 * 1024;//2MB
const validateForm = Yup.object().shape({
  'note': Yup.string()
});

export class PaymentDetails extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errorUpload: false,
      chosenPayment: {},
      reason: '',
      modifyType: 'single',
      showRemoveConfirm: false
    };
  }

  componentDidMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let clientID = temp[1];
    this.props.getPaymentData(clientID, true);
  }

  componentWillUnmount() {
    this.props.setDefault();
  }

  _handleImageChange(e) {
    const { images } = this.props.paymentdetails;
    e.preventDefault();
    this.setState({ errorUpload: false });
    let currentSize = e.target.files.length + images.length;
    //upload over MAX_PROOFS images will not do
    if (0 <= currentSize && currentSize <= MAX_NUMBER) {
      Array.from(e.target.files).forEach(item => {
        let image = { file: '', fileName: '' };
        let reader = new FileReader();
        let file = item;
        let type = file.name.split('.');
        if (type[1] === 'jpeg' || type[1] === 'jpg' || type[1] === 'png' || type[1] === 'gif') {
          reader.onloadend = () => {
            //file size need lower than 2MB
            if (file.size <= MAX_FILE_SIZE) {
              image.file = file;
              image.fileName = reader.result;
              this.props.addImage(image);
            }
          };
          reader.readAsDataURL(file);
        } else {
          this.setState({ errorUpload: true });
        }
      });
      e.target.value = null;
    } else {
      this.setState({ errorUpload: true });
    }
  }

  openRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: true,
      reason: ''
    });
  };

  closeRemoveConfirm = () => {
    this.setState({ showRemoveConfirm: false });
  };

  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };

  removePayment = () => {
    const { paymentData: { _id: id = '' } } = this.props.paymentdetails;
    let data = {
      paymentIds: [id],
      reason: this.state.reason
    };
    this.props.removePayment(data);
  }

  render() {
    const { paymentData, images } = this.props.paymentdetails;
    return (
      <div className="payment-details">
        <div className="header-edit-add-page payment-details-header">
          <div className="action">
            <div className="return" id="return" onClick={() => {
              this.props.history.goBack();
            }}>
              <span className="icon-arrow-left" />
            </div>

            <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return">
              Back</UncontrolledTooltip>

            <div className="btn-group float-right" hidden={this.props.edit}>
              <GhostButton className={'btn-ghost'} title={'Remove'} onClick={() => {
                this.openRemoveConfirm();
              }} />
              <GhostButton className={'btn-ghost'} title={'Print'} onClick={() => {
                this.props.getPdf([paymentData._id]);
              }} />
              <GhostButton className={'btn-ghost'}
                title={'Status: ' + (!_.isUndefined(paymentData.status) ? paymentData.status : 'None')} />


            </div>
          </div>
          <div className="title payment-id">
            {!_.isEmpty(paymentData) && !_.isUndefined(paymentData.company) && !_.isUndefined(paymentData.payoutId) && (paymentData.company.name + ' #' + paymentData.payoutId)}
          </div>
        </div>
        <div className="content content-details-with-activities">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                <div className="information transaction">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Transaction Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Amount</span>
                          </div>
                          <div className="data price">
                            <NumberFormat
                              className={'price'}
                              value={(!_.isEmpty(paymentData) && !_.isUndefined(paymentData.balance) && paymentData.balance.value) ? paymentData.balance.value : ''}
                              className="price"
                              displayType={'text'}
                              thousandSeparator={true}
                              prefix={'$'}
                              decimalScale={2}
                              fixedDecimalScale={true}
                            />
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Sent By</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isUndefined(paymentData.createdBy) && paymentData.createdBy.fullName}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information bank">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Bank Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Account Name</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && !_.isEmpty(paymentData.company.banks) && paymentData.company.banks[0].accountName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Bank Name</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && !_.isEmpty(paymentData.company.banks) && paymentData.company.banks[0].bankName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Bsb Number</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && !_.isEmpty(paymentData.company.banks) && paymentData.company.banks[0].bsbNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Account Number</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && !_.isEmpty(paymentData.company.banks) && paymentData.company.banks[0].accountNumber}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="information company">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Company Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="company-logo">
                        <img
                          src={!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) ? paymentData.company.logo.fileName : './default-user.png'}
                          alt="logo"
                          onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }} />
                      </div>
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Company Name</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && paymentData.company.name}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Abn Number</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && paymentData.company.abnNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Company Address</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && paymentData.company.address.name}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Company Contact Person</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && paymentData.company.contactPerson ? paymentData.company.contactPerson : (!_.isUndefined(paymentData.company) && paymentData.company.createdBy ? paymentData.company.createdBy.fullName : '')}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Contact Number</span>
                          </div>
                          <div className="data">
                            <span>{!_.isEmpty(paymentData) && !_.isEmpty(paymentData.company) && paymentData.company.phoneNumber}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 right-part">
                <div className="information note">
                  <Formik
                    ref={ref => (this.formik = ref)}
                    initialValues={{
                      note: ''
                    }}
                    enableReinitialize={true}
                    validationSchema={validateForm}
                    onSubmit={values => {
                      let formData = new FormData();
                      formData.append('content', values.note);
                      if (!_.isEmpty(images)) {
                        images.map((image) => {
                          if (_.isObject(image.file)) {
                            formData.append('files', image.file);
                          }
                        })
                      }
                      this.props.addNote(paymentData._id, formData);
                    }}>
                    {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                    }) => (
                        <Form onSubmit={handleSubmit}>
                          <div className="title">
                            <span>Document</span>
                          </div>
                          <div className="comment-images">
                            <div className="label">
                              <span>Your note</span>
                            </div>
                            <div>
                              <TextareaCounter
                                name={'note'}
                                placeholder={'Type something'}
                                rows={5}
                                maxLength={50}
                                value={values.note}
                                error={errors.note}
                                onChange={handleChange}
                                touched={touched.note}
                                onBlur={handleBlur} />
                              <div className="images">
                                <ProofUpload
                                  images={_.isArray(images) ? images : []}
                                  removeItem={index => {
                                    this.props.removeImage(index);
                                  }}
                                  onChange={evt => {
                                    if (images.length < MAX_NUMBER) {
                                      this._handleImageChange(evt);
                                    }
                                  }} />
                              </div>
                            </div>
                          </div>
                          <SubmitButton
                            className="btn-purple-round submit"
                            type="submit"
                            content={'Submit'}
                            disabled={Object.keys(errors).length > 0 || values.note === '' || images.length === 0}
                          />
                        </Form>
                      )}
                  </Formik>
                </div>
                <div className="information bank"
                  hidden={(!_.isArray(paymentData.notes) || paymentData.notes.length === 0)}>
                  {(_.isArray(paymentData.notes) && paymentData.notes.length > 0) &&
                    paymentData.notes.map((note, index) => (
                      <div className="note-item" key={index}>
                        <div className="notification">
                          <span>{note.content}</span>
                        </div>
                        <div className="images-list">
                          <div className="image-item">
                            <div className="image">
                              {!_.isUndefined(note.images) && <img className="bank-image"
                                src={note.images && note.images.fileName ? note.images.fileName : './default-user.png'}
                                onError={(e) => {
                                  e.target.onerror = null;
                                  e.target.src = './default-user.png'
                                }} />}

                              <div className="icon-contain" onClick={() => {
                                this.props.deleteNote(paymentData._id, note._id);
                              }}>
                                <span className="icon-bin"></span>
                              </div>
                            </div>
                            <div className="time-owner">
                              <span
                                className="time">{moment(note.createdAt).format('HH:mm A, DD MMM YYYY')} {'by'}</span>
                              <span
                                className="owner"> {(note.createdBy && note.createdBy.fullName) ? note.createdBy.fullName : ''}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
        </div>

        <RemoveConfirmModal
          show={this.state.showRemoveConfirm}
          reason={this.state.reason}
          object={'payment'}
          action={'single'}
          handleChange={this.handleReasonChange}
          closeModal={this.closeRemoveConfirm}
          removeObject={this.removePayment} />
      </div>
    );
  }
}

PaymentDetails.propTypes = {
  setDefault: PropTypes.func,
  dispatch: PropTypes.func,
  getPaymentData: PropTypes.func,
  removePayment: PropTypes.func,
  getPdf: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  paymentdetails: makeSelectPaymentDetails()
});

function mapDispatchToProps(dispatch) {
  return {
    setDefault: () => {
      dispatch(Actions.defaultAction());
    },
    getPaymentData: (id, loadSpinner) => {
      dispatch(Actions.getPaymentDetails(id, loadSpinner));
    },
    addImage: (newImage) => {
      dispatch(Actions.addImage(newImage));
    },
    removeImage: (id) => {
      dispatch(Actions.removeImage(id));
    },
    addNote: (id, noteData) => {
      dispatch(Actions.addNote(id, noteData));
    },
    deleteNote: (paymentId, noteId) => {
      dispatch(Actions.deleteNote(paymentId, noteId));
    },
    removePayment: (deleteData, searchData) => {
      dispatch(Actions.removePayment(deleteData, searchData));
    },
    getPdf: (paymentId) => {
      dispatch(Actions.getPdf(paymentId));
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "paymentDetails", reducer });
const withSaga = injectSaga({ key: "paymentDetails", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(PaymentDetails);
