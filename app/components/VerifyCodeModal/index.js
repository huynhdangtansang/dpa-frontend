import React from "react";
import InputForm from "components/InputForm";
import SubmitButton from 'components/SubmitButton';
import PageInfo from "components/PageInfo";
import { Formik } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';

const validateForm = Yup.object().shape({
  'code': Yup.number()
      .test('len', 'Invalid code', value => value.toString().length === 6)
      // .required('Please enter your code')
      .required('')
      .typeError('Code must be a number'),
});
export default class VerifyCodeModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resendTime: 0,
      modal: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentDidMount() {
    this.setState({ resendTime: 30 }, () => {
      this.countDown();
    })
  }

  _disableButton(value, error) {
    const keys = [
      'code'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  countDown = () => {
    if (this.state.resendTime > 0) {
      let countdown = setInterval(() => {
        let time = this.state.resendTime;
        this.setState({ resendTime: time - 1 });
        if (--time === 0) {
          clearInterval(countdown);
        }
      }, 1000);
    }
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="verify-modal">
          <ModalBody>
            <PageInfo title={'Security Code'}
                      content={'Please check your phone for the securiry code'}/>
            <Formik
                ref={ref => (this.formik = ref)}
                initialValues={{
                  code: ''
                }}
                enableReinitialize={true}
                validationSchema={validateForm}
                onSubmit={evt => {
                  this.props.onSubmit(evt.code);
                }}
            >
              {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  /* and other goodies */
                }) => (
                  <div>
                    <form onSubmit={handleSubmit}>
                      <InputForm label={'code'}
                                 name={'code'}
                                 type={'number'}
                                 error={errors.code}
                                 value={values.code}
                                 touched={touched.code}
                                 onChange={evt => {
                                   evt.preventDefault();
                                   this.props.clearErrors();
                                   handleChange(evt);
                                 }}
                                 maxlength={6}
                                 apiError={this.props.errors}
                                 onBlur={handleBlur}
                                 placeholder={'6 numbers sent through your phone'}
                                 showIcon={!this._disableButton(values, errors) && _.isEmpty(this.props.errors)}
                                 icon={'icon-checkmark'}/>
                      <SubmitButton type="submit"
                                    disabled={this._disableButton(values, errors) || (this.props.errors && this.props.errors.length > 0)}
                                    content={'verify'}/>
                      {(this.props.errors && this.props.errors.length > 0) ? this.props.errors.map((error) => {
                        return (
                            <div key={error.errorCode} className="errors">
                              <span className="icon-error"></span>
                              <div key={error.errorCode} className="error-item">
                                <span>{error.errorMessage}</span>
                              </div>
                            </div>
                        );
                      }) : []}

                    </form>
                  </div>
              )}
            </Formik>
            <div className="text-center">
              <a className={this.state.resendTime !== 0 ? 'btn forgot-style security disabled' : 'btn forgot-style security'}
                 onClick={() => {
                   if (this.state.resendTime == 0) {
                     this.setState({ resendTime: 30 }, () => {
                       this.countDown();
                       this.props.onResend();
                     });
                   }
                 }}>resend code</a>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}