/**
 *
 * Asynchronously loads the component for StatisticsPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
