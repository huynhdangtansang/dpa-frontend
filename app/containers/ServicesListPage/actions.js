/*
 *
 * ServicesListPage actions
 *
 */
import {
  DELETE_ERROR,
  DELETE_SERVICE,
  DELETE_SUCCESS,
  GET_PDF,
  GET_SERVICES_LIST,
  GET_SUCCESS,
  SELECT_ALL_SERVICE,
  SELECT_SERVICE,
  SET_CURRENT_SERVICE,
  SHOW_COMMISSION_CONFIRM,
  SHOW_REMOVE_CONFIRM,
  UPDATE_COMMISSION,
  UPDATE_MODIFY_TYPE,
  UPDATE_PAGE,
  UPDATE_SEARCH_DATA
} from "./constants";

export function getServicesList(searchData) {
  return {
    type: GET_SERVICES_LIST,
    searchData: searchData
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function deleteService(deleteData) {
  return {
    type: DELETE_SERVICE,
    deleteData
  }
}

export function deleteSuccess(response) {
  return {
    type: DELETE_SUCCESS,
    response: response
  }
}

export function deleteError(response) {
  return {
    type: DELETE_ERROR,
    response: response
  }
}

export function selectService(isSelected, selectItem) {
  return {
    type: SELECT_SERVICE,
    isSelected,
    selectItem
  }
}

export function selectAllService(isSelected) {
  return {
    type: SELECT_ALL_SERVICE,
    isSelected
  }
}

export function updateSearchData(newSearchData) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData
  }
}

export function updatePage(newPage) {
  return {
    type: UPDATE_PAGE,
    newPage
  }
}

export function changeShowRemoveConfirm(value) {
  return {
    type: SHOW_REMOVE_CONFIRM,
    value
  }
}

export function changeShowCommissionConfirm(value) {
  return {
    type: SHOW_COMMISSION_CONFIRM,
    value
  }
}

export function updateModifyType(value) {
  return {
    type: UPDATE_MODIFY_TYPE,
    value
  }
}

export function udpateCurrentService(service) {
  return {
    type: SET_CURRENT_SERVICE,
    service
  }
}

export function updateCommission(updateData) {
  return {
    type: UPDATE_COMMISSION,
    updateData
  }
}

export function getPdf(serviceId) {
  return {
    type: GET_PDF,
    serviceId
  }
}

