/*
 *
 * TeamListPage actions
 *
 */
import {
  CHANGE_MEMBER_STATUS,
  DELETE_MEMBER,
  GET_ERROR,
  GET_PERIOD,
  GET_PERIOD_SUCCESS,
  GET_SUCCESS,
  GET_TEAM_LIST,
  RESET_STATE,
  SELECT_ALL_MEMBER,
  SELECT_MEMBER,
  SET_CURRENT_MEMBER,
  SHOW_MODAL,
  UPDATE_DROPDOWN_PERIOD,
  UPDATE_MODIFY_TYPE,
  UPDATE_PAGE,
  UPDATE_SEARCH_DATA
} from "./constants";

export function resetState() {
  return {
    type: RESET_STATE
  };
}

export function getTeamList(searchData, resovle, reject) {
  return {
    type: GET_TEAM_LIST,
    searchData,
    resovle, reject
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: GET_ERROR,
    response
  }
}

export function changeMemberStatus(dataUpdate, searchData) {
  return {
    type: CHANGE_MEMBER_STATUS,
    dataUpdate,
    searchData
  }
}

export function deleteMember(deleteData, searchData) {
  return {
    type: DELETE_MEMBER,
    deleteData,
    searchData
  }
}

export function updateSearchData(newSearchData) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData
  }
}

export function selectMember(isSelected, selectItem) {
  return {
    type: SELECT_MEMBER,
    isSelected,
    selectItem
  }
}

export function selectAllMember(isSelected) {
  return {
    type: SELECT_ALL_MEMBER,
    isSelected
  }
}

export function updatePage(newPage) {
  return {
    type: UPDATE_PAGE,
    newPage
  }
}

export function updateCurrentMember(member) {
  return {
    type: SET_CURRENT_MEMBER,
    member
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}

export function updateModifyType(value) {
  return {
    type: UPDATE_MODIFY_TYPE,
    value
  }
}

export function getPeriod(resolve, reject) {
  return {
    type: GET_PERIOD,
    resolve,
    reject
  }
}

export function getPeriodSuccess(list, resovle, reject) {
  return {
    type: GET_PERIOD_SUCCESS,
    list,
    resovle,
    reject
  }
}

export function updateDropdownPeriod(newFilterPeriod) {
  return {
    type: UPDATE_DROPDOWN_PERIOD,
    newFilterPeriod
  }
}
