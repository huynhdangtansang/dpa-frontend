/*
 *
 * ClientsListPage constants
 *
 */
export const DEFAULT_ACTION = "app/ClientsListPage/DEFAULT_ACTION";
export const RESET_STATE = "app/ClientsListPage/RESET_STATE";
export const GET_CLIENTS_LIST = "/ClientsListPage/GET_CLIENTS_LIST";
export const GET_SUCCESS = "/ClientsListPage/GET_SUCCESS";
export const DELETE_CLIENT = "/ClientsListPage/DELETE_CLIENT";
export const CHANGE_CLIENT_STATUS = "/ClientsListPage/CHANGE_CLIENT_STATUS";
export const RESET_PASSWORD = "/ClientListPage/RESET_PASSWORD";
export const CHANGE_IS_SUCCESS = "/ClientListPage/CHANGE_IS_SUCCESS";
export const CLEAR_ALL_SELECTED_CLIENT = "/ClientListPage/CLEAR_ALL_SELECTED_CLIENT";
export const SELECT_CLIENT = "ClientListPage/SELECT_CLIENT";
export const SELECT_ALL_CLIENT = "ClientListPage/SELECT_ALL_CLIENT";
export const GET_SUBURBS_LIST = "ClientListPage/GET_SUBURBS_LIST";
export const GET_SUBURBS_SUCCESS = "ClientListPage/GET_SUBURBS_SUCCESS";
export const SHOW_MODAL = "ClientListPage/SHOW_MODAL";
export const UPDATE_SEARCH_DATA = "ClientListPage/UPDATE_SEARCH_DATA";
