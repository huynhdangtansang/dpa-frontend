/**
 *
 * LoginPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import * as Yup from 'yup';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectErrors, makeSelectLoginPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import FormGroup from "components/FormGroup";
import InputForm from "components/InputForm";
import { Formik } from 'formik';
import './style.scss';
import Checkbox from "components/Checkbox";
import SubmitButton from "components/SubmitButton";
import { defaultAction, getLoginData, setErrors } from "./actions";
import { urlLink } from 'helper/route';
//lib
import _ from "lodash";
/* eslint-disable react/prefer-stateless-function */
const validateForm = Yup.object().shape({
  'email': Yup.string()
      .email(" Invalid email"),
  //.required('Please enter email'),//No value but need put here for change if need display required
  'password': Yup.string()
      .min(6, 'Invalid password (At least 6 and no special characters)'),
  //.required('Please enter password'),//No value but need put here for change if need display required
});

export class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this._disableButton = this._disableButton.bind(this);
    this.state = {
      showPassword: false
    }
  }

  componentWillMount() {
    this.props.default();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'email',
      'password',
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { apiError } = this.props.loginpage;
    return (
        <FormGroup>
          <Formik
              ref={ref => (this.formik = ref)}
              initialValues={{ email: '', password: '', isRemember: false }}
              enableReinitialize={true}
              validationSchema={validateForm}
              onSubmit={evt => {
                this.props.onSubmit(evt);
              }}
          >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                /* and other goodies */
              }) => (
                <div>
                  <form onSubmit={handleSubmit} className="form-login">
                    <InputForm label={'email'}
                               name={'email'}
                               type={'email'}
                               value={values.email}
                               error={errors.email}
                               apiError={apiError}
                               touched={touched.email}
                               onChange={evt => {
                                 handleChange(evt);
                                 this.props.setErrors([]);
                               }}
                               onBlur={handleBlur}
                               placeholder={'you@example.com'}/>
                    <InputForm label={'password'}
                               name={'password'}
                               value={values.password}
                               error={errors.password}
                               apiError={apiError}
                               touched={touched.password}
                               onChange={evt => {
                                 handleChange(evt);
                                 this.props.setErrors([]);
                               }}
                               onBlur={handleBlur}
                               type={!this.state.showPassword ? 'password' : 'text'}
                        //type='text'
                               placeholder={'At least 6 and no special characters'}
                               iconClassShow={'icon-invisible'}
                               iconClassHide={'icon-visible'}
                               togglePassword={() => {
                                 this.setState({
                                   showPassword: !this.state.showPassword
                                 });
                               }}
                               showPassword={this.state.showPassword}/>

                    <Checkbox
                        name={'isRemember'}
                        label={'remember password'}
                        checked={values.isRemember}
                        onChange={evt => {
                          handleChange(evt);
                        }}
                    />
                    <SubmitButton type={'submit'}
                                  disabled={this._disableButton(values, errors) || (apiError && apiError.length > 0)}
                                  content={'log in'}/>
                  </form>
                </div>
            )}
          </Formik>
          <div className={'forgot-style'} onClick={() => {
            this.props.history.push(urlLink.forgotPassword);
          }}>Forgot Password?
          </div>

          {(apiError && apiError.length > 0) ? apiError.map((error) => {
            return (
                <div key={error.errorCode} className="errors">
                  <span className="icon-error"></span>
                  <div className="error-item">
                    <span>{error.errorMessage}</span>
                  </div>
                </div>
            );
          }) : []}
        </FormGroup>
    );
  }
}

LoginPage.propTypes = {
  dispatch: PropTypes.func,
  onSubmit: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  loginpage: makeSelectLoginPage(),
  errors: makeSelectErrors()
});

function mapDispatchToProps(dispatch) {
  return {
    default: () => {
      dispatch(defaultAction());
    },
    onSubmit: evt => {
      if (!_.isUndefined(evt)) {
        dispatch(getLoginData(evt.email, evt.password, evt.isRemember));
      }
    },
    setErrors: (data) => {
      dispatch(setErrors(data));
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "loginPage", reducer });
const withSaga = injectSaga({ key: "loginPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(LoginPage);
