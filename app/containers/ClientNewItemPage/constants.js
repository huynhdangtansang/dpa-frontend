/*
 *
 * ClientNewItemPage constants
 *
 */
export const ADD_CLIENT = "ClientNewItemPage/ADD_CLIENT";
export const ADD_SUCCESS = "ClientNewItemPage/ADD_SUCCESS";
export const ADD_ERROR = "ClientNewItemPage/ADD_ERROR";
export const RESET_ERROR = "ClientNewItemPage/SET_ERROR";
export const CHANGE_AVATAR = "ClientNewItemPage/CHANGE_AVATAR";
