import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the servicesDetailsPage state domain
 */

const selectServicesDetailsPageDomain = state =>
    state.get("servicesDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ServicesDetailsPage
 */

const makeSelectServicesDetailsPage = () =>
    createSelector(selectServicesDetailsPageDomain, substate => substate.toJS());
const makeSelectServiceData = () =>
    createSelector(selectServicesDetailsPageDomain, state => state.get('serviceData'));
const makeSelectEditMode = () =>
    createSelector(selectServicesDetailsPageDomain, state => state.get('edit'));
export { makeSelectServicesDetailsPage, makeSelectServiceData, makeSelectEditMode };
export { selectServicesDetailsPageDomain };