/**
 *
 * ModalWrapper
 *
 */
import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import GhostButton from 'components/GhostButton';
import './style.scss';

function ErrorPopup(props) {
  return (
      <Modal className={'modal-wrapper popup-wrapper'} isOpen={props.visible}>
        <ModalBody>
          <div className={'popup-contain'}>
            <div className={'popup-title'}>{props.title}</div>
            {props.content && <div className={'popup-content'}>{props.content}</div>}
            <div>{props.children}</div>
            {props.btnText && (
                <GhostButton
                    title={props.btnText}
                    type={props.type}
                    className={`btn btn-ghost btn-popup error-btn`}
                    onClick={props.onSubmit}
                    disabled={props.disabled}/>
            )}
          </div>
        </ModalBody>
      </Modal>
  );
}

ErrorPopup.propTypes = {};
export default ErrorPopup;
