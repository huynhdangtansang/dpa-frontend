import io from 'socket.io-client';
import config from 'config';

const socket = io(config.serverUrl, { autoConnect: false });
// const socket = io("192.168.1.91:5052", { autoConnect: false });

function connectSocket() {
  socket.on('connect', () => {
    socket && socket.emit("init:userInfo", localStorage.getItem('token'));
  });
}

function sendEvent(eventName) {
  if (!socket.connected) {
    openSocket();
  }
  socket.emit(eventName, localStorage.getItem('token'));
}

function getEvent(eventName, callback) {
  if (!socket.connected) {
    openSocket();
  }

  socket.on(eventName, response => {
    callback(response);
  })
}

function openSocket() {
  socket.open();
}

function closeSocket() {
  socket.close();
}

export { getEvent, sendEvent, closeSocket, openSocket, connectSocket }