import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import Dropdown from 'components/Dropdown';
import "./style.scss";

export default class ReAssignSpecificModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  companies = (list) => {
    let array = [];
    list.map((company, index) => {
      let data = {
        label: company.name,
        value: company._id
      };
      array.push(data);
    });
    return array;
  };
  employees = (list) => {
    let array = [];
    list.map((employee, index) => {
      let data = {
        label: employee.fullName,
        value: employee._id
      };
      array.push(data);
    });
    return array;
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="reassign-specific-modal">
          <ModalBody>
            <div className="upper">
              <div className="title">
                <span>Reassign a Specific Company</span>
              </div>
              <div className="description">
                <span>The company selected will be accepted without priority. Provider could be allocated by admin or the company.</span>
              </div>
            </div>
            <div className="middle">
              <div className="info-item">
                <div className="label">
                  <span>Company</span>
                </div>
                <Dropdown
                    defaultLabel={'Select a company'}
                    options={this.companies(this.props.companiesList)}
                    onSelect={(value) => {
                      this.props.select('company', value);
                    }}
                />
              </div>
              <div className="info-item">
                <div className="label">
                  <span>Employee</span>
                </div>
                <Dropdown
                    defaultLabel={'Select an employee'}
                    options={this.employees(this.props.employeesList)}
                    onSelect={(value) => {
                      this.props.select('employee', value);
                    }}
                />
              </div>
            </div>
            <div className="lower">
              <GhostButton className="btn-ghost" title={'Cancel'} onClick={this.props.closeModal}/>
              <PurpleRoundButton className="btn-purple-round" title={'Assign'} onClick={this.props.reAssign}/>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}