/*
 *
 * ServiceNewItemPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  errorsApi: [],
  isSuccess: false,
  couponsList: [],
  keywords: [],
  backgroundSrc: '',
  iconSrc: '',
  certifications: [],
  areas: [],
  showConfirmModal: false,
  showIconModal: false,
  showBackgroundModal: false
});

function serviceNewItemPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case Constants.ADD_SUCCESS:
      return state
          .set('errorsApi', action.response.errors)
          .set('isSuccess', true);
    case Constants.ADD_ERROR:
      return state
          .set('errorsApi', action.response.errors);
    case Constants.SET_SUCCESS:
      return state
          .set('isSuccess', action.value);
    case Constants.ADD_COUPON:
      return state
          .updateIn(['couponsList'], arr => arr.push(action.newItem));
    case Constants.DELETE_COUPON:
      return state
          .set('couponsList', state.get('couponsList').filter((coupon, id) => id !== action.index));
    case Constants.UPDATE_COUPON:
      return state
          .set('couponsList', state.get('couponsList').map((coupon, index) => {
            return (index === action.id) ? action.updatedItem : coupon
          }));
    case Constants.ADD_CERT:
      return state
          .updateIn(['certifications'], arr => arr.push(action.newItem));
    case Constants.DELETE_CERT:
      return state
          .set('certifications', state.get('certifications').filter((item, id) => id !== action.index));
    case Constants.UPDATE_CERT:
      return state
          .set('certifications', state.get('certifications').map((item, index) => {
            return (index === action.id) ? action.updatedItem : item
          }));
    case Constants.ADD_AREA:
      return state
          .updateIn(['areas'], arr => arr.push(action.newItem));
    case Constants.DELETE_AREA:
      return state
          .set('areas', state.get('areas').filter((item, id) => id !== action.index));
    case Constants.UPDATE_AREA:
      return state
          .set('areas', state.get('areas').map((item, index) => {
            return (index === action.id) ? action.updatedItem : item
          }));
    case Constants.SHOW_MODAL:
      return state
          .set(action.modal, action.value);
    case Constants.SET_INIT:
      return initialState;
      // .set('errorsApi', [])
      // .set('couponsList', fromJS([]))
      // .set('certifications', fromJS([]))
      // .set('areas', fromJS([]))
    case Constants.CHANGE_IMAGE:
      return state
          .setIn([action.key], action.value);
    default:
      return state;
  }
}

export default serviceNewItemPageReducer;
