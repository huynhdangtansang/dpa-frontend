import React from "react";
import ReactCrop from 'react-image-crop';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import './style.scss';
import _ from "lodash";

const cropSquare = {
  x: 25,
  y: 25,
  width: 50,
  height: 50,
  aspect: 1
};
export default class CropImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cropImageSrc: this.props.cropSrc,
      croppedImageUrl: '',
      crop: cropSquare
    };
  }

  componentWillMount() {
    if (this.props.crop) {
      this.setState({ crop: this.props.crop });
    } else {
      this.setState({ crop: cropSquare });
    }
  }

  onImageLoaded = (image) => {
    this.imageRef = image;
  };
  onCropChange = (crop) => {
    if (_.isObject(crop)) {
      this.setState({ crop });
    }
  };
  onCropComplete = async (crop, pixelCrop) => {
    const croppedImageUrl = await this.getCroppedImg(this.imageRef, pixelCrop, 'newFile.jpeg');
    this.setState({
      croppedImageUrl: croppedImageUrl
    });
  };

  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(
        image,
        pixelCrop.x,
        pixelCrop.y,
        pixelCrop.width,
        pixelCrop.height,
        0,
        0,
        pixelCrop.width,
        pixelCrop.height
    );
    return new Promise((resolve) => {
      canvas.toBlob(file => {
        if (!_.isNull(file)) {
          file.name = _.isNull(fileName) ? 'newFile.jpeg' : fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(file);
          resolve(this.fileUrl);
        }
      }, "image/jpeg");
    });
  }

  toDataURL = (url, callback) => {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
      let reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  };
  dataURItoBlob = (dataURI) => {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);
    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    let ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  };
  saveCropImage = () => {
    if (this.state.croppedImageUrl) {
      let imageUrl = this.state.croppedImageUrl;
      //Convert blobUrl to base64data
      this.toDataURL(imageUrl, (base64data) => {
        //Convert base64data to Blob
        let blob = this.dataURItoBlob(base64data);
        //Convert Blob to File
        this.handleChange(base64data, blob);
      });
    }
  };
  cancelImageCrop = () => {
    this.props.closeModal();
  };
  handleChange = (src, file) => {
    this.props.changeImage(src, file);
    this.cancelImageCrop();
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="change-avatar-modal">
          <ModalHeader>Crop Image</ModalHeader>
          <ModalBody>
            <div className="image">
              <ReactCrop
                  src={this.state.cropImageSrc}
                  crop={this.state.crop}
                  onImageLoaded={this.onImageLoaded}
                  onComplete={this.onCropComplete}
                  onChange={this.onCropChange}
                  keepSelection={true}
                  minWidth={5}
                  minHeight={5}
              />
            </div>
          </ModalBody>
          <ModalFooter>
            <GhostButton className={'btn-ghost upload'} title={'Cancel'} onClick={() => {
              this.cancelImageCrop();
            }}/>
            <PurpleRoundButton className={'btn-purple-round save'} title={'Crop & Save'} onClick={() => {
              this.saveCropImage();
            }}/>
          </ModalFooter>
        </Modal>
    )
  }
}