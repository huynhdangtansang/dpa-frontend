import { put, takeLatest } from 'redux-saga/effects';
import { ADD_DEVICE } from './constants';
import { addError, addSuccess } from './actions';
import { reposLoaded } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import { goBack } from 'react-router-redux';

export function* apiAddNewDevice(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.device;
    try {
      const response = yield axios.post(requestUrl,
          data.profile);
      yield put(addSuccess(response.data));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(addError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ADD_DEVICE, apiAddNewDevice)
}
