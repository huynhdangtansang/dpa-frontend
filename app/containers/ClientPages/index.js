/**
 *
 * ClientPages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import ClientsListPage from '../ClientsListPage/Loadable';
import ClientDetailsPage from '../ClientDetailsPage/Loadable';
import ClientEditPage from '../ClientEditPage/Loadable';
import ClientNewItemPage from '../ClientNewItemPage/Loadable';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ClientPages extends React.PureComponent {
  render() {
    return (
        <div className="client-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.clients} component={ClientsListPage}/>
              <Route path={urlLink.viewClient} component={ClientDetailsPage}/>
              <Route path={urlLink.editClient} component={ClientEditPage}/>
              <Route path={urlLink.addClient} component={ClientNewItemPage}/>
            </Switch>
          </Router>
        </div>
    );
  }
}

ClientPages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(ClientPages);
