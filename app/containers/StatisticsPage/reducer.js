/*
 *
 * StatisticsPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";
import { monthLabel } from "helper/data";
//Lib
import _ from "lodash";
import moment from "moment";

export const initialState = fromJS({
  //-------CHART DATA-------
  averageJobData: {
    value: 0,
    unit: '$'
  },
  averageFeeData: {
    value: 0,
    unit: '$'
  },
  chart: {
    appoinmentTime: {
      filters: {
        'showWithType': {
          label: 'Daily',
        }
      },
      searchData: {
        showWithType: 'daily'
      }
    },
    job: {
      filters: {
        'companyId': {
          label: 'All',
        },
        'serviceId': {
          label: 'All',
        },
        'suburb': {
          label: 'All',
        },
        'period': {
          label: moment().format('YYYY'),
        },
      },
      searchData: {
        companyId: '',
        serviceId: '',
        suburb: '',
        period: moment().format('YYYY')
      }
    },
    region: {
      filters: {
        'month': {
          label: 'All',
        },
        'year': {
          label: moment().format('YYYY')
        },
      },
      searchData: {
        month: '',
        year: moment().format('YYYY')
      }
    },
    service: {
      filters: {
        'month': {
          label: 'All',
        },
        'year': {
          label: moment().format('YYYY')
        },
      },
      searchData: {
        month: '',
        year: moment().format('YYYY')
      }
    },
    user: {
      filters: {
        'period': {
          label: moment().format('YYYY'),
        },
      },
      searchData: {
        period: moment().format('YYYY')
      }
    },
    revenue: {
      filters: {
        'companyId': {
          label: 'All',
        },
        'serviceId': {
          label: 'All',
        },
        'suburb': {
          label: 'All',
        },
        'period': {
          label: moment().format('YYYY'),
        },
      },
      searchData: {
        companyId: '',
        serviceId: '',
        suburb: '',
        period: moment().format('YYYY')
      }
    },
    profit: {
      filters: {
        'companyId': {
          label: 'All',
        },
        'serviceId': {
          label: 'All',
        },
        'suburb': {
          label: 'All',
        },
        'period': {
          label: moment().format('YYYY'),
        },
      },
      searchData: {
        companyId: '',
        serviceId: '',
        suburb: '',
        period: moment().format('YYYY')
      }
    },
    averageJob: {
      filters: {
        'serviceId': {
          label: 'All',
        },
      },
      searchData: {
        serviceId: ''
      }
    },
    averageFee: {
      filters: {
        'serviceId': {
          label: 'All',
        },
      },
      searchData: {
        serviceId: ''
      }
    }
  },
  serviceData: {},
  regionData: {},
  userData: {},
  jobData: {},
  revenueData: {},
  appoinmentTimeData: {
    labels: ['12:00AM'],
    datasets: [
      {
        label: 'apoinmentTime',
        data: [1]
      }
    ]
  },
  pieDataDefault: {
    labels: [
      'No data'
    ],
    datasets: [{
      data: [1],
      borderWidth: 0, //this will hide border
      backgroundColor: [
        '#4285F4',
        '#DB4437',
        '#F4B400',
        '#0F9D58',
        '#FF6D00',
        '#46BDC6'
      ],
      hoverBackgroundColor: [
        '#4285F4',
        '#DB4437',
        '#F4B400',
        '#0F9D58',
        '#FF6D00',
        '#46BDC6'
      ],
    }]
  },
  pieDataDefaultNodata: {
    labels: [
      'No data',
    ],
    datasets: [{
      data: [1],
      borderWidth: 0, //this will hide border
      backgroundColor: '#e5e5e5',
      hoverBackgroundColor: '#e5e5e5'
    }],
  },
  barDataDefault: {
    labels: monthLabel,
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: '#4285F4',
        borderColor: '#4285F4',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
        label: 'My second dataset',
        backgroundColor: '#DB4437',
        borderColor: '#DD5246',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
        label: 'My three dataset',
        backgroundColor: '#F4B400',
        borderColor: '#F4B400',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
    ]
  },
  barDataOpacityDefault: {
    labels: monthLabel,
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: 'rgba(66, 133, 244, 0.25)',
        borderColor: '#4285F4',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
        label: 'My second dataset',
        backgroundColor: 'rgba(219, 68, 55, 0.2)',
        borderColor: '#DD5246',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
        label: 'My three dataset',
        backgroundColor: 'rgba(244, 180, 0, 0.3)',
        borderColor: '#F4B400',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
    ]
  },
  barDataOpacityDefaultOneDatasets: {
    labels: ['1'],
    datasets: [
      {
        label: 'Appointments Created',
        backgroundColor: 'rgba(66, 133, 244, 0.25)',
        borderColor: '#4285F4',
        lineTension: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
    ]
  },
  //--------------------
  //DROPDOWN DATA
  serviceListDropdown: [],
  companyListDropdown: [],
  suburbListDropdown: [],
});

function customizer(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return _.merge(objValue, srcValue);
  }
}

function renderArrayTime(number) {
  //7 records is weekly
  //24 records is daily
  //28,29,30,31 records is monthly
  //12 records is yearly
  let indents = [];
  switch (number) {
    case 24:
      indents = [];
      for (let i = 0; i < number; i++) {
        indents.push(moment().hour(i).format('hha').toUpperCase());
      }
      return indents;
    case 7:
      return ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    case 28:
    case 29:
    case 30:
    case 31:
      indents = [];
      for (let i = 1; i <= number; i++) {
        indents.push(i.toString());
      }
      return indents;
    case 12:
      return monthLabel;
    default:
      return number;
  }
}

function formatLabel(label) {
  switch (label) {
    case 'jobsBooked':
    case 'jobsCancel':
    case 'jobsCompleted':
      return label.slice(0, 1).toUpperCase() + label.slice(1, 4) + ' ' + label.slice(4);
    default:
      return label.slice(0, 1).toUpperCase() + label.slice(1);
  }
}

function formatData(state, key, data) {
  let formatedData = {
    datasets: []
  };
  let objTemp1 = {};
  let objTemp2 = {};
  let allRules = {};
  switch (key) {
    case 'userData':
    case 'revenueData':
    case 'profitData':
    case 'jobData': {
      Object.keys(data).map(key => {
        let arrayTemp = [];
        if (_.isArray(data[key])) {
          data[key].map(item => {
            arrayTemp.push(item.value);
          });
        }
        return formatedData.datasets.push({ label: formatLabel(key), data: arrayTemp });
      });
      objTemp2 = state.get('barDataDefault').toJS();
      if (key === 'jobData') {
        objTemp2 = state.get('barDataOpacityDefault').toJS();
      }
      //remove datasets is redundancy (default is 3) base on length datasets
      let lengTemp = objTemp2.datasets.length;
      for (var i = 0; i < (lengTemp - formatedData.datasets.length); i++) {
        objTemp2.datasets.pop();
      }
      allRules = {};
      _.mergeWith(allRules, objTemp2, customizer);
      _.mergeWith(allRules, formatedData, customizer);
      formatedData = allRules;
      break;
    }
    case 'regionData':
      if (_.isEmpty(data)) {
        formatedData = state.get('pieDataDefaultNodata').toJS();
      } else {
        if (_.isArray(data)) {
          formatedData.labels = data.map(item => {
            return _.isUndefined(item.suburb) ? '' : item.suburb;
          });
        }
        formatedData.datasets = [];
        objTemp1 = data.map(item => {
          return (_.isUndefined(item.count) ? 0 : item.count);
        });
        formatedData.datasets.push({
          data: objTemp1
        });
        objTemp2 = state.get('pieDataDefault').toJS();
        allRules = {};
        _.mergeWith(allRules, objTemp2, customizer);
        _.mergeWith(allRules, formatedData, customizer);
        formatedData = allRules;
      }
      break;
    case 'serviceData':
      if (_.isEmpty(data)) {
        formatedData = state.get('pieDataDefaultNodata').toJS();
      } else {
        if (_.isArray(data)) {
          formatedData.labels = data.map(item => {
            return _.isUndefined(item.service) ? '' : item.service;
          });
        }
        formatedData.datasets = [];
        objTemp1 = data.map(item => {
          return (_.isUndefined(item.count) ? 0 : item.count);
        });
        formatedData.datasets.push({
          data: objTemp1
        });
        objTemp2 = state.get('pieDataDefault').toJS();
        allRules = {};
        _.mergeWith(allRules, objTemp2, customizer);
        _.mergeWith(allRules, formatedData, customizer);
        formatedData = allRules;
      }
      break;
    case 'appoinmentTimeData':
      if (_.isArray(data)) {
        formatedData.labels = renderArrayTime(data.length);
        formatedData.datasets = [];
        objTemp1 = data.map(item => {
          return (_.isUndefined(item.value) ? 0 : item.value);
        });
        formatedData.datasets.push({
          data: objTemp1
        });
        objTemp2 = state.get('barDataOpacityDefaultOneDatasets').toJS();
        //process label of formattedData
        //7 records is weekly
        //24 records is daily
        //28,29,30,31 records is monthly
        //12 records is yearly
        allRules = {};
        _.mergeWith(allRules, objTemp2, customizer);
        _.mergeWith(allRules, formatedData, customizer);
        formatedData = allRules;
      }
      break;
    default:
      formatedData = data;
  }
  return formatedData;
}

function statisticsPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_ALL_SUCCESS: {
      let dataFormated = formatData(state, action.key, action.response);
      return state.set(action.key, fromJS(dataFormated));
    }
    case Constants.GET_DROPDOWN_SUCCESS:
      return state.set(action.key, fromJS(action.response));
    case Constants.UPDATE_FILTER_CHART:
      return state.setIn(action.position, action.value);
    default:
      return state;
  }
}

export default statisticsPageReducer;
