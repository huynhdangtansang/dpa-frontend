/**
 *
 * Asynchronously loads the component for DeviceDetailsPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
