import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';

export default class ChangeTopPriorityConfirmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  title = (currentStatus) => {
    if (currentStatus) {
      return "Disable Top Priority";
    } else {
      return "Change To Top Priority"
    }
  };
  description = (currentStatus) => {
    let firstString = "Are you sure to want to " + (currentStatus ? "disable Top Priority of this service? " : "change this service into Top Priority? ");
    let secondString = "So this company will " + (currentStatus ? "not" : "") + " be the high priority to get the job regarding to this service.";
    return firstString + secondString;
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="logout-modal">
          <ModalBody>
            <div className="change-status">
              <div className="upper">
                <div className="title">
                  <span>{this.title(this.props.currentStatus)}</span>
                </div>
                <div className="description">
                  <span>{this.description(this.props.currentStatus)}</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton
                    className="btn-ghost cancel"
                    title={'Cancel'}
                    onClick={() => {
                      this.props.closeModal();
                    }}/>

                <PurpleRoundButton className="btn-purple-round sign-out"
                                   type="button"
                                   title={'Save'}
                                   onClick={() => {
                                     this.props.changeStatus();
                                   }}/>
              </div>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}