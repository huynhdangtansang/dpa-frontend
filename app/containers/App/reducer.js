/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import { LOAD_REPOS, LOAD_REPOS_ERROR, LOAD_REPOS_SUCCESS, UPDATE_ERROR } from './constants';
// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  currentUser: false,
  userData: {
    repositories: false,
  },
  globalError: {
    error: false,
    title: 'Error!!!',
    message: 'Message'
  }
});

function AppReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_REPOS:
      return state
          .set('loading', true)
          .set('error', false)
          .setIn(['userData', 'repositories'], false);
    case LOAD_REPOS_SUCCESS:
      return state
      // .setIn(['userData', 'repositories'], action.repos)
          .set('loading', false);
      // .set('currentUser', action.username);
    case LOAD_REPOS_ERROR:
      return state.set('error', action.error).set('loading', false);
    case UPDATE_ERROR:
      return state.setIn(['globalError', 'error'], action.data.error)
          .setIn(['globalError', 'title'], action.data.title)
          .setIn(['globalError', 'message'], action.data.message);
    default:
      return state;
  }
}

export default AppReducer;
