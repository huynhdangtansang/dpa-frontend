/**
 *
 * ServicePages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
//Service Pages
import ServicesListPage from '../ServicesListPage/Loadable';
import ServicesDetailsPage from '../ServicesDetailsPage/Loadable';
import ServiceNewItemPage from '../ServiceNewItemPage/Loadable';
import ServiceEditPage from '../ServiceEditPage/Loadable';
import { urlLink } from 'helper/route';
import CustomRoute from 'components/PrivateRoute';

/* eslint-disable react/prefer-stateless-function */
export class ServicePages extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className="service-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.services} component={ServicesListPage}/>
              <Route path={urlLink.viewService} component={ServicesDetailsPage}/>
              <Route path={urlLink.addService} component={ServiceNewItemPage}/>
              <CustomRoute path={urlLink.editService} component={ServiceEditPage} {...this.props}/>
            </Switch>
          </Router>
        </div>
    )
  }
}

ServicePages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(ServicePages);
