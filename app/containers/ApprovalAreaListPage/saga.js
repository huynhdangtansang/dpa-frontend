// import { take, call, put, select } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import config from 'config';
import axios from 'axios';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import { GET_SERVICE_DATA } from "containers/CompaniesListPage/constants";
import { apiGetServiceList } from "containers/CompaniesListPage/saga";
import _ from "lodash";

export function* apiGetApprovalList(data) {
  if (data) {
    let { searchData: params } = data;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
        // k =>
        // k + '=' + esc(params[k])
        //this function to process sort by service ID
        function (keys) {
          if (keys === 'serviceIds') {
            if (params[keys].length > 0) {
              return (keys + '=' + encodeURIComponent('["' + params[keys] + '"]'));
            } else {
              return (keys + '=' + esc(params[keys]));
            }
          } else {
            return (keys + '=' + esc(params[keys]));
          }
        }
    ).join('&');
    const requestUrl = config.serverUrl + config.api.company.approval_list + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiApproveCompany(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.approvalCompanies;
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.companyIds);
      yield put(reposLoaded());
      yield put(Actions.getApprovalListData(data.searchData));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiRemoveCompany(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.companies_details + 'deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.dataDelete });
      yield put(reposLoaded());
      yield put(Actions.getApprovalListData(data.searchData));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_APPROVAL_LIST, apiGetApprovalList);
  yield takeLatest(Constants.APPROVE_COMPANY, apiApproveCompany);
  yield takeLatest(Constants.REMOVE_COMPANY, apiRemoveCompany);
  yield takeLatest(GET_SERVICE_DATA, apiGetServiceList);
}
