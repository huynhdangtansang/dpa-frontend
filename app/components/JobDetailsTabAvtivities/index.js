/**
 *
 * JobDetailsTabAvtivities
 *
 */
import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import moment from 'moment';
import uuidv1 from 'uuid';
import _ from 'lodash';
import queryString from 'query-string';
//Activities
import Activity from 'components/Activities';
//Modal
import ReAssignModal from 'components/ReAssignModal';
import RescheduleModal from 'components/RescheduleModal';
import ReAssignSpecificModal from 'components/ReAssignSpecificModal';
import RefundModal from 'components/RefundModal';
import TextareaCounter from 'components/TextareaCounter';
//Other
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import { getEvent } from 'helper/socketConnection';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
class JobDetailsTabAvtivities extends React.Component {
  showReassign = () => {
    this.props.showModal('reassign', true);
  };
  showReschedule = () => {
    this.props.showModal('reschedule', true);
  };
  showCancelJob = () => {
    this.setState({ reason: '' });
    this.props.showModal('cancelJob', true);
  };
  showReassignSpecific = () => {
    this.props.showModal('reassignSpecific', true);
  };
  showEditEstimate = () => {
    this.props.showModal('editEstimate', true);
  };
  showRefund = () => {
    this.props.showModal('refund', true);
    this.props.reloadJob();
  };
  reAssign = () => {
    if (this.props.assignedObject === 'specificCompany') {
      this.props.showModal('reassign', false);
      this.showReassignSpecific();
    } else if (this.props.assignedObject === 'company') {
      this.props.reAssign();
    } else if (this.props.assignedObject === 'provider') {
      this.props.reAssign();
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      reason: ''
    };
    getEvent('notification:newActivities', (response) => {
      const { dataActivities } = response;
      let { pathname } = this.props.history.location;
      let { id } = queryString.parse(this.props.history.location.search);
      let jobId = dataActivities.jobId;
      let actsList = this.props.list;
      if (!_.isEmpty(id) && id.toString().includes(jobId.toString()) && urlLink.viewJob === pathname) {
        let activityData = dataActivities;
        let date = moment(dataActivities.createdAt).format('DD MMM YYYY');
        if (!_.includes(actsList, activityData)) {
          this.props.addNew(date, activityData);
        }
      }
    })
  }

  render() {
    return (
        <div className="activities">
          <div className="content-add-edit activities-content">
            <div className="information">
              <div className="time-line">
                {!_.isEmpty(this.props.list) && this.props.list.length > 0 && this.props.list.map((date, index) => {
                  let diff = moment().isSame(moment(date.day), 'day');
                  return (
                      <div key={date.day} className="row date">
                        <div className="col left">
                          <div className="title">
                            <span>{diff ? 'Today' : moment(date.day).format('DD MMM YYYY')}</span>
                          </div>
                        </div>
                        <div className="col right">
                          <div className="activities-list">
                            {!_.isUndefined(date.job) && date.job.map((act, id) => {
                              return (
                                  <Activity
                                      key={uuidv1()}
                                      isTop={(index === 0 && id === 0) ? true : false}
                                      activity={act}
                                      showReassign={this.showReassign}
                                      showReschedule={this.showReschedule}
                                      showCancelJob={this.showCancelJob}
                                      showRefund={this.showRefund}
                                      submitEstimate={(data) => {
                                        this.props.submitEstimate(data);
                                      }}
                                      updateEstimate={(data) => {
                                        this.props.updateEstimate(data);
                                      }}
                                      completeAndPay={this.props.completeAndPay}
                                  />
                              )
                            })}
                          </div>
                        </div>
                      </div>
                  )
                })}

              </div>
            </div>
          </div>
          <ReAssignModal
              show={this.props.showReassignModal}
              closeModal={() => {
                this.props.showModal('reassign', false);
              }}
              assignedObject={this.props.assignedObject}
              onChange={type => {
                this.props.handleChangeObjectType(type);
              }}
              reAssign={this.reAssign}/>
          <Modal isOpen={this.props.showCancelJobConfirm} className="canceljob-modal">
            <ModalBody>
              <div className="upper">
                <div className="title">
                  <span>Cancel This Job?</span>
                </div>
                <div className="description">
                  <span>Are you sure you want to cancel this job?</span>
                </div>
                <div className="label">
                  <span>Note</span>
                </div>
                <div className="modify-item">
                  <TextareaCounter
                      placeholder={'Describe the reason why cancel this job'}
                      rows={8}
                      maxLength={50}
                      value={this.state.reason}
                      onChange={(e) => {
                        e.preventDefault();
                        this.setState({ reason: e.target.value });
                      }}/>
                </div>
              </div>
              <div className="lower">
                <GhostButton
                    className="btn-ghost cancel"
                    title={'Cancel'}
                    onClick={() => {
                      this.props.showModal('cancelJob', false);
                    }}
                />
                <PurpleRoundButton
                    className="btn-purple-round yes"
                    title={'Yes'}
                    disabled={_.isEmpty(this.state.reason)}
                    onClick={() => {
                      this.props.cancelJob(this.state.reason);
                    }}/>
              </div>
            </ModalBody>
          </Modal>
          <RescheduleModal
              show={this.props.showRescheduleModal}
              appointment={this.props.appointment}
              changeAppointment={this.props.changeAppointment}
              apiError={this.props.apiErrors}
              reschedule={reason => {
                this.props.reschedule(reason);
              }}
              closeModal={() => {
                this.props.showModal('reschedule', false);
              }}/>
          <ReAssignSpecificModal
              show={this.props.showReassignSpecificModal}
              companiesList={this.props.companiesList}
              employeesList={this.props.employeesList}
              select={(selector, value) => {
                this.props.specificSelect(selector, value);
              }}
              reAssign={this.props.reAssignSpecific}
              closeModal={() => {
                this.props.showModal('reassignSpecific', false);
              }}/>
          <RefundModal
              show={this.props.showRefundModal}
              paid={this.props.paid}
              closeModal={() => {
                this.props.showModal('refund', false);
              }}
              refund={(amount, unit, reason) => {
                this.props.refund(amount, unit, reason);
                this.props.showModal('refund', false);
              }}
          />
        </div>
    )
  }
}

JobDetailsTabAvtivities.propTypes = {};
export default JobDetailsTabAvtivities;
