import { fromJS } from 'immutable';
import clientDetailsPageReducer from '../reducer';

describe('clientDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(clientDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
