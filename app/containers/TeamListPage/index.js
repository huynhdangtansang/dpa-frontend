/**
 *
 * TeamListPage
 *
 */
//Lib
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import _ from "lodash";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { DropdownItem, DropdownMenu, Form, Input } from 'reactstrap';
import classnames from 'classnames';
import moment from 'moment';
//Saga, redux, action, reducer, selector
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectTeamListPage from "./selectors";
import reducer from "./reducer";
import { loadRepos } from 'containers/App/actions';
import saga from "./saga";
import {
  changeMemberStatus,
  deleteMember,
  getPeriod,
  getPeriodSuccess,
  getTeamList,
  resetState,
  selectAllMember,
  selectMember,
  showModal,
  updateCurrentMember,
  updateDropdownPeriod,
  updateModifyType,
  updatePage,
  updateSearchData
} from "./actions";
//CSS
import './style.scss';
// Components
import GhostButton from 'components/GhostButton';
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import SingleSelectDropdown from 'components/SingleSelectDropdown';
import Checkbox from 'components/Checkbox';
import { ActionFormatter, CustomMultiSelect, StatusFormat, TotalFormatter } from 'components/TableFormatter';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import debounce from 'components/Debounce';
import { getEvent } from 'helper/socketConnection';
import { urlLink } from 'helper/route';
// -------------------
/* eslint-disable react/prefer-stateless-function */
export class TeamListPage extends React.PureComponent {
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  handleSearchChange = debounce(value => {
    const { searchData } = this.props.teamlistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.search = value;
    this.props.changePage(1);
    this.props.updateSearch(temp);
  }, 500);
  handleChangeOnlyActive = (value) => {
    const { searchData } = this.props.teamlistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.active = value;
    this.props.changePage(1);
    this.props.updateSearch(temp);
  };
  sortData = (field, order) => {
    const { searchData } = this.props.teamlistpage;
    let temp = searchData;
    temp.offset = 0;
    temp.sort = field;
    temp.sortType = order;
    this.props.changePage(1);
    this.props.updateSearch(temp);
  };
  changePagination = (type) => {
    const { currentPage } = this.props.teamlistpage;
    if (type == 'prev') {
      let newPage = currentPage - 1;
      this.props.changePage(newPage);
    } else {
      let newPage = currentPage + 1;
      this.props.changePage(newPage);
    }
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { searchData, pagination, currentPage } = this.props.teamlistpage;
    const limit = pagination.limit;
    let temp = searchData;
    let newOffset = limit * (currentPage - 1);
    temp.offset = newOffset;
    this.props.updateSearch(temp);
  }, 700);
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
      status: row.active
    };
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  checkChangeStatusButton = () => {
    const { selectedMember } = this.props.teamlistpage;
    let list = selectedMember;
    if (list.length > 0) {
      const compareStatus = list[0].status;
      let differentItems = list.filter((item) => item.status !== compareStatus);
      if (differentItems.length === 0) {
        return {
          isHide: false,
          title: compareStatus === true ? 'Deactive' : 'Active'
        };
      } else {
        return {
          isHide: true,
          title: 'Deactive'
        };
      }
    }
  };
  changeStatusModal = () => {
    const { showChangeStatusConfirm, chosenMember, selectedMember, modifyType } = this.props.teamlistpage;
    if (modifyType === 'single') {
      return (
        <ChangeStatusConfirmModal
          show={showChangeStatusConfirm}
          reason={this.state.reason}
          currentStatus={chosenMember.status}
          object={'member'}
          action={modifyType}
          handleChange={this.handleReasonChange}
          closeModal={() => {
            this.setState({ reason: '' });
            this.props.showModal('changeStatus', false);
          }}
          changeStatus={() => {
            this.singleAction('changeStatus');
          }} />
      )
    } else {
      const changeStatusButton = this.checkChangeStatusButton();
      if (selectedMember.length > 0) {
        return (
          <ChangeStatusConfirmModal
            show={showChangeStatusConfirm}
            reason={this.state.reason}
            currentStatus={changeStatusButton.title === 'Deactive' ? true : false}
            object={'member'}
            action={selectedMember.length === 1 ? 'single' : 'multi'}
            handleChange={this.handleReasonChange}
            closeModal={() => {
              this.setState({ reason: '' });
              this.props.showModal('changeStatus', false);
            }}
            changeStatus={() => {
              this.multiAction('changeStatus');
            }} />
        )
      }
    }
  };
  singleAction = (type) => {
    const { chosenMember, searchData } = this.props.teamlistpage;
    let listIds = [];
    listIds.push(chosenMember.id);
    let data = {
      ids: listIds,
    };
    if (type === 'remove') {
      data['reason'] = this.state.reason;
      this.props.delete(data, searchData);
    } else if (type === 'changeStatus') {
      data['active'] = !chosenMember.status;
      if (chosenMember.status === true) {
        data['reason'] = this.state.reason;
      }
      this.props.changeStatus(data, searchData);
    }
    this.refs.table_member.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  multiAction = (type) => {
    const { selectedMember, searchData } = this.props.teamlistpage;
    let list = selectedMember;
    let i = 0;
    let listIds = [];
    list.forEach((client) => {
      listIds.push(client.id);
      if (i == list.length - 1) {
        let data = {
          ids: listIds
        };
        if (type === 'remove') {
          data['reason'] = this.state.reason;
          this.props.delete(data, searchData);
        } else if (type === 'changeStatus') {
          const changeStatusButton = this.checkChangeStatusButton();
          if (changeStatusButton.title === 'Deactive') {
            data['active'] = false;
          } else {
            data['active'] = true;
          }
          this.props.changeStatus(data, searchData);
        }
      } else {
        i++;
      }
    });
    this.refs.table_member.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  handleChangeFilterPeriod = debounce(newfilterPeriod => {
    this.props.updateDropdownPeriod(newfilterPeriod);
    const { searchData } = this.props.teamlistpage;
    this.props.getList(searchData);
  }, 0);

  constructor(props) {
    super(props);
    this.state = {
      reason: '',
    };
    getEvent('notification:newUserActivity', () => {
      let url = window.location.hash;
      url = url.replace('#', '');
      if (url === urlLink.team) {
        const { searchData } = this.props.teamlistpage;
        this.props.getList(searchData);
      }
    })
  }

  componentDidMount() {
    this.props.startLoad();
    this.props.getPeriod().then((res) => {
      let dateStart = moment(!_.isUndefined(res.data.period) ? res.data.period : null);
      let dateEnd = moment(new Date());//current date
      let timeValues = [];
      while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
        timeValues.push({
          label: dateStart.format('MMM YYYY'),
          value: dateStart.format('MM/YYYY'),
          _id: dateStart.format('MM/YYYY')
        });
        dateStart.add(1, 'month');
      }
      this.props.getPeriodSuccess(timeValues).then(() => {
        const { searchData } = this.props.teamlistpage;
        this.props.getList(searchData);
      });
    });
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const {
      teamList, selectedMember, pagination, modifyType, showRemoveConfirm,
      searchData, currentPage,
      periodList, filterPeriod
    } = this.props.teamlistpage;
    const changeStatusButton = this.checkChangeStatusButton();
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    const options = {
      withoutNoDataText: true,
      //defaultSortName: 'fullName',
      //defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    const actionFormat = (cell, row) => {
      const editMenu = (
        <DropdownMenu>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.viewMember + '?id=' + row._id);
          }}>View Member</DropdownItem>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.editMember + "?id=" + row._id);
          }}>Edit Member</DropdownItem>
          <DropdownItem onClick={() => {
            this.props.setMember({ id: row._id, status: row.active });
            this.props.changeModifyType('single');
            this.props.showModal('changeStatus', true);
          }}>
            {row.active == true ? 'Deactivate Member' : 'Activate Member'}
          </DropdownItem>
          <DropdownItem onClick={() => {
            this.props.setMember({ id: row._id, status: row.active });
            this.props.changeModifyType('single');
            this.props.showModal('remove', true);
          }}>Remove Member</DropdownItem>
        </DropdownMenu>
      );
      return (
        <ActionFormatter menu={editMenu} />
      );
    };
    const avatarFormat = (cell, row) => {
      return (
        <img src={cell && cell.fileName ? cell.fileName : './default-user.png'}
          onError={(e) => {
            e.target.onerror = null;
            e.target.src = './default-user.png'
          }}
          alt="avatar"
          onClick={() => {
            this.props.history.push(urlLink.viewMember + '?id=' + row._id);
          }} />
      )
    };
    const nameFormat = (cell, row) => {
      return (
        <div onClick={() => {
          this.props.history.push(urlLink.viewMember + '?id=' + row._id);
        }}>{cell}</div>
      )
    };
    return (
      <div className="team-management">
        <div className="header-list-page team-management-header">
          <Form inline className="align-items-center" onSubmit={(e) => {
            e.preventDefault();
            this.props.getList(searchData);
          }}>
            <PurpleAddRoundButton className={'btn-add-purple-round'} title={'New Member'} iconClassName={'icon-plus'}
              onClick={() => {
                this.props.history.push(urlLink.addMember);
              }} type="button" />

            <div className="search-bar services-search">
              <Input placeholder="Search employee" onChange={(e) => {
                this.handleSearchChange(e.target.value);
              }} />
              <span className="icon-search" onClick={this.sortData}></span>
            </div>

            <div className="period">

              <SingleSelectDropdown
                title={'Period: ' + (_.isEmpty(periodList) ? 'loading...' : '')}
                nameField={'label'}
                chosenItemLabel={filterPeriod.label}
                chosenItemID={filterPeriod.value}
                options={_.isEmpty(periodList) ? [] : periodList}
                onSelect={(item) => {
                  this.handleChangeFilterPeriod(item);
                }}
              />
            </div>

            <div className="show-active">
              <Checkbox name={'only-show-active'}
                label={'Only show active'}
                onChange={(e) => {
                  this.handleChangeOnlyActive(e.target.checked);
                }}
              />
            </div>
            {teamList && teamList.length > 0 && pagination && (
              <TotalFormatter
                offset={pagination.offset}
                limit={pagination.limit}
                total={pagination.total}
                page={currentPage}
                changePagination={this.changePagination}
              />
            )}
          </Form>
        </div>
        <div className={classnames("content content-list-page", selectedMember.length > 0 ? 'selecting' : '')}>
          <BootstrapTable
            ref="table_member"
            data={teamList}
            options={options}
            bordered={false}
            selectRow={selectRowProp}
            containerClass='table-fixle'
            tableHeaderClass='table-fixle-header'
            tableBodyClass='table-fixle-content'>
            <TableHeaderColumn dataField='_id' isKey hidden>Member ID</TableHeaderColumn>
            <TableHeaderColumn dataField="avatar" dataFormat={avatarFormat}
              width={46 / 14 + 'rem'}></TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='fullName' width={152 / 14 + 'rem'} columnClassName={'name'}
              dataFormat={nameFormat}>Member Name</TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='email' width={224 / 14 + 'rem'}>Email</TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='location' width={224 / 14 + 'rem'}>Location</TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='phoneNumber' width={218 / 14 + 'rem'}>Contact
                Number</TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='subRole' dataAlign='middle'>Position</TableHeaderColumn>
            <TableHeaderColumn dataSort dataField='active' dataFormat={StatusFormat} columnClassName={'status'}
              width={84 / 14 + 'rem'}>Status</TableHeaderColumn>
            <TableHeaderColumn dataField='data' dataFormat={actionFormat} columnClassName={'edit-dropdown'}
              dataAlign='right' width={84 / 14 + 'rem'}>Action</TableHeaderColumn>
          </BootstrapTable>
        </div>
        {selectedMember.length > 0 &&
          <div className="footer member-list-footer">
            <GhostButton hidden={changeStatusButton.isHide} title={changeStatusButton.title}
              className="btn-ghost btn-edit-charge"
              onClick={() => {
                if (selectedMember.length === 1) {
                  this.props.setMember({ id: selectedMember[0].id, status: selectedMember[0].status });
                  this.props.changeModifyType('single');
                } else {
                  this.props.changeModifyType('multi');
                }
                this.props.showModal('changeStatus', true);
              }} />
            <GhostButton title={'Remove'} className="btn-ghost btn-remove"
              onClick={() => {
                if (selectedMember.length === 1) {
                  this.props.setMember({ id: selectedMember[0].id, status: selectedMember[0].status });
                  this.props.changeModifyType('single');
                } else {
                  this.props.changeModifyType('multi');
                }
                this.props.showModal('remove', true);
              }} />
          </div>
        }
        {this.changeStatusModal(modifyType)}

        <RemoveConfirmModal
          show={showRemoveConfirm}
          reason={this.state.reason}
          action={modifyType}
          object={'member'}
          handleChange={this.handleReasonChange}
          closeModal={() => {
            this.props.showModal('remove', false);
            this.setState({ reason: '' });
          }}
          removeObject={() => {
            modifyType === 'single' ? this.singleAction('remove') : this.multiAction('remove')
          }} />
      </div>
    );
  }
}

TeamListPage.propTypes = {
  dispatch: PropTypes.func,
  getList: PropTypes.func,
  getPeriod: PropTypes.func,
  changeStatus: PropTypes.func,
  delete: PropTypes.func,
  updateSearch: PropTypes.func,
  select: PropTypes.func,
  selectAll: PropTypes.func,
  changePage: PropTypes.func,
  setMember: PropTypes.func,
  showModal: PropTypes.func,
  changeModifyType: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  teamlistpage: makeSelectTeamListPage()
});

function mapDispatchToProps(dispatch) {
  return {
    startLoad: () => {
      dispatch(loadRepos());
    },
    resetState: () => {
      dispatch(resetState());
    },
    changePage: (newPage) => {
      dispatch(updatePage(newPage));
    },
    getList: (dataSearch) => {
      return new Promise((resovle, reject) => {
        dispatch(getTeamList(dataSearch, resovle, reject));
      });
    },
    getPeriod: () => {
      return new Promise((resovle, reject) => {
        dispatch(getPeriod(resovle, reject));
      });
    },
    getPeriodSuccess: (list) => {
      return new Promise((resovle, reject) => {
        dispatch(getPeriodSuccess(list, resovle, reject));
      });
    },
    changeStatus: (data, searchData) => {
      dispatch(changeMemberStatus(data, searchData));
    },
    delete: (deleteData, searchData) => {
      dispatch(deleteMember(deleteData, searchData));
    },
    updateSearch: (searchData) => {
      dispatch(updateSearchData(searchData));
    },
    select: (isSelected, selectItem) => {
      dispatch(selectMember(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(selectAllMember(isSelected));
    },
    setMember: (member) => {
      dispatch(updateCurrentMember(member));
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    changeModifyType: (value) => {
      dispatch(updateModifyType(value));
    },
    updateDropdownPeriod: (newfilterPeriod) => {
      dispatch(updateDropdownPeriod(newfilterPeriod));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "teamListPage", reducer });
const withSaga = injectSaga({ key: "teamListPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(TeamListPage);
