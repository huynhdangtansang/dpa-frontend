/**
 *
 * CompanyDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectCompanyDetailsPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import { changeStatusFilter } from "containers/JobsListPage/actions";
//Library
import _ from "lodash";
import moment from 'moment';
import './style.scss';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
// Modal
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { Modal, ModalBody, TabContent, TabPane, UncontrolledTooltip } from 'reactstrap';
//Socket
import { getEvent } from 'helper/socketConnection';
//Tab component import
import CompanyTabControl from 'components/CompanyTabControl';
import CompanyTabOverview from 'components/CompanyTabOverview';
import CompanyTabEmployee from 'components/CompanyTabEmployee';
import CompanyTabJob from 'components/CompanyTabJob';
import CompanyTabEarning from 'components/CompanyTabEarning';
//Name link
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class CompanyDetailsPage extends React.PureComponent {
  handleChangeReason = (value) => {
    this.setState({ reason: value });
  };
  editInfo = () => {
    this.props.companydetailspage.companyData.isApproved ?
        this.props.history.push(urlLink.editCompany + '?id=' + this.props.companydetailspage.companyData._id)
        :
        this.props.history.push(urlLink.editApproval + '?id=' + this.props.companydetailspage.companyData._id)
  };
  closeStatusConfirm = () => {
    this.setState({
      activeModal: false,
      reason: ''
    });
  };
  changeStatus = () => {
    this.setState({
      activeModal: false,
      reason: ''
    });
    const { companyData, activeTab } = this.props.companydetailspage;
    this.props.activeCompany(companyData._id, this.state.reason, !companyData.active).then(() => {
      this.props.getCompanyData(companyData._id).then(() => {
        this.toggle(activeTab);
      });
    });
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeRemoveConfirm = () => {
    this.setState({
      removeModal: false,
      reason: ''
    });
  };
  removeCompany = () => {
    this.setState({
      removeModal: false,
      reason: ''
    });
    const { companyData } = this.props.companydetailspage;
    this.props.removeCompany(companyData._id, this.state.reason);
  };

  constructor(props) {
    super(props);
    this.state = {
      reason: "",
      activeModal: false,
      removeModal: false
    };
    //remove this line maybe make error FE
    this.toggle = this.toggle.bind(this);
    getEvent('notification:newUserActivity', (response) => {
      let url = window.location.hash;
      let temp = url.split('?id=');
      let companyID = temp[1] ? temp[1] : '';
      if (url.includes(urlLink.viewCompany)) {
        if (!_.isUndefined(response.companyId) && response.companyId === companyID) {
          const { activeTab } = this.props.companydetailspage;
          if (activeTab === '1') {
            this.props.getActivitiesCompany(companyID);
          }
          if (activeTab === '2' || activeTab === '3') {
            this.props.getEmployeeCompany(companyID, this.props.companydetailspage.searchDataForEmployee);
          }
        }
      }
    });
    getEvent('notification:newActivities', (response) => {
      let url = window.location.hash;
      let temp = url.split('?id=');
      let companyID = temp[1] ? temp[1] : '';
      if (url.includes(urlLink.viewCompany)) {
        if (!_.isUndefined(response.companyId) && response.companyId === companyID) {
          const { activeTab } = this.props.companydetailspage;
          if (activeTab === '1')
            this.props.getActivitiesCompany(companyID);
          if (activeTab === '2' || activeTab === '3') {
            this.props.getEmployeeCompany(companyID, this.props.companydetailspage.searchDataForEmployee);
          }
        }
      }
    });
    getEvent('notification:newJob', () => {
      let url = window.location.hash;
      let temp = url.split('?id=');
      let companyID = temp[1] ? temp[1] : '';
      if (url.includes(urlLink.viewCompany)) {
        const { activeTab } = this.props.companydetailspage;
        if (activeTab === '3') {
          this.props.getJobCompany(companyID, this.props.companydetailspage.searchDataForJob);
        }
      }
    });
    getEvent('notification:deleteJob', () => {
      let url = window.location.hash;
      let temp = url.split('?id=');
      let companyID = temp[1] ? temp[1] : '';
      if (url.includes(urlLink.viewCompany)) {
        const { activeTab } = this.props.companydetailspage;
        if (activeTab === '3') {
          this.props.getJobCompany(companyID, this.props.companydetailspage.searchDataForJob);
        }
      }
    });
  }

  componentWillMount() {
  }

  componentDidMount() {
    const { activeTab } = this.props.companydetailspage;
    this.toggle(activeTab);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.search !== this.props.location.search) {
      const { activeTab } = this.props.companydetailspage;
      this.toggle(activeTab);
    }
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  toggle(tab) {
    const { activeTab, searchDataForEmployee, searchDataForJob } = this.props.companydetailspage;
    let url = window.location.href;
    let temp = url.split('?id=');
    let companyID = temp[1] ? temp[1] : '';
    if (activeTab !== tab) {
      this.props.updateTab(tab);
    }
    switch (tab) {
      case '1':
        this.props.getCompanyData(companyID).then((res) => {
          if (!_.isEmpty(res)) {
            if (res.isApproved) {
              //this is company is approved so need get activities
              //this.props.history.replace(urlLink.viewCompany + '?id=' + companyID);
              // location = window.location.origin + window.location.pathname + '#' + urlLink.viewCompany + '?id=' + companyID;
              this.props.getActivitiesCompany(companyID);
            } else {
              //this is company in approval so not need get activities
              //this.props.history.replace(urlLink.viewApproval + '?id=' + companyID);
              // location = window.location.origin + window.location.pathname + '#' + urlLink.viewApproval + '?id=' + companyID;
            }
          }
        });
        return true;
      case '2':
        this.props.getEmployeeCompany(companyID, searchDataForEmployee);
        this.props.getPeriodEmployee(companyID);
        return true;
      case '3':
        this.props.getJobCompany(companyID, searchDataForJob);
        this.props.getEmployeeCompany(companyID);
        return true;
      case '4':
        this.props.getEarningCompany(companyID);
        return true;
      default:
        return false;
    }
  }

  render() {
    const { companyData, activeTab } = this.props.companydetailspage;
    return (
        <div className="company-details">
          {!_.isEmpty(companyData) && (
              <div>
                <div className="header-edit-add-page">
                  <div className="action">
                    <div className="return" id="return" onClick={() => this.props.history.goBack()}>
                      <span className="icon-arrow-left"></span>
                    </div>

                    <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return"
                                         container='return'>
                      Back</UncontrolledTooltip>

                    <div className="btn-group float-right">
                      <GhostButton className={'btn-ghost remove'} title={'Remove'} onClick={() => {
                        this.setState({ removeModal: true, reason: '' });
                      }}/>
                      <GhostButton className={'btn-ghost edit'} title={'Edit'} onClick={() => {
                        this.editInfo();
                      }}/>
                      {companyData && !companyData.isApproved ?
                          (
                              <GhostButton className={'btn-ghost edit'} title={'Approve'} onClick={() => {
                                this.setState({ showApproveConfirm: true })
                              }}/>
                          ) : (
                              <GhostButton onClick={() => {
                                this.setState({ activeModal: true, reason: '' });
                              }} className={'btn-ghost edit'}
                                           title={companyData && companyData.active ? 'Deactivate' : 'Activate'}/>
                          )}

                    </div>
                  </div>

                  <div className="title company-name">
                    <span>{companyData && companyData.name ? companyData.name : 'Company name'}</span>
                    <div className="joined-date">
                      <span>{companyData.isApproved ? 'Joined in' : 'Submission date:'} {companyData && moment(companyData.createdAt).format('ddd D MMM YYYY')}</span>
                    </div>
                  </div>

                  {companyData && companyData.isApproved && (
                      <CompanyTabControl activeTab={activeTab} toggle={this.toggle}/>)}

                </div>

                <div className="content">
                  <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                      <CompanyTabOverview {...this.props} />
                    </TabPane>
                    <TabPane tabId="2">
                      <CompanyTabEmployee {...this.props} />
                    </TabPane>
                    <TabPane tabId="3">
                      <CompanyTabJob {...this.props} />
                    </TabPane>
                    <TabPane tabId="4">
                      <CompanyTabEarning {...this.props} />
                    </TabPane>
                  </TabContent>
                </div>

                <ChangeStatusConfirmModal
                    show={this.state.activeModal}
                    reason={this.state.reason}
                    currentStatus={_.isEmpty(companyData) ? false : companyData.active}
                    object={'company'}
                    action={'single'}
                    handleChange={this.handleReasonChange}
                    closeModal={this.closeStatusConfirm}
                    changeStatus={this.changeStatus}
                />

                <RemoveConfirmModal
                    show={this.state.removeModal}
                    reason={this.state.reason}
                    object={companyData.isApproved ? 'company' : "approval company"}
                    action={'single'}
                    handleChange={this.handleReasonChange}
                    closeModal={this.closeRemoveConfirm}
                    removeObject={this.removeCompany}/>

                <Modal isOpen={this.state.showApproveConfirm} className="remove-confirm-modal update-confirm">
                  <ModalBody>
                    <div className="remove-confirm">
                      <div className="upper">
                        <div className="title">
                          <span>Approval This Company</span>
                        </div>
                        <div className="description">
                          <span>Are you sure to want to approve this company? After approval, company profile will move to companies section</span>
                        </div>
                      </div>
                      <div className="lower">
                        <GhostButton type="button" className="btn-ghost cancel" title={'cancel'}
                                     onClick={() => {
                                       this.setState({ showApproveConfirm: false });
                                     }}/>
                        <PurpleRoundButton className="btn-purple-round save" title={'approve'}
                                           onClick={() => {
                                             this.setState({ showApproveConfirm: false });
                                             this.props.approveCompany(companyData._id);
                                           }}
                        />
                      </div>
                    </div>
                  </ModalBody>
                </Modal>
              </div>
          )}

        </div>
    )
  }
}

CompanyDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  //get
  getCompanyData: PropTypes.func,
  getEmployeeCompany: PropTypes.func,
  getEarningCompany: PropTypes.func,
  //update data
  approveCompany: PropTypes.func,
  activeCompany: PropTypes.func,
  removeCompany: PropTypes.func,
  changeStatusEmployee: PropTypes.func,
  updateSearchEmployee: PropTypes.func,
  selectAllEmployee: PropTypes.func,
  toggleTopPriorityConfirmModal: PropTypes.func,
  setTopPriority: PropTypes.func,
  changeStatusFilter: PropTypes.func,
  chooseItem: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  companydetailspage: makeSelectCompanyDetailsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    getCompanyData: (companyID) => {
      return new Promise((resolve, reject) => {
        dispatch(Actions.getCompany(companyID, resolve, reject));
      });
    },
    activeCompany: (companyID, reason, isActive) => {
      return new Promise((resolve, reject) => {
        dispatch(Actions.setActiveCompany(companyID, reason, isActive, resolve, reject));
      });
    },
    removeCompany: (companyID, reason) => {
      dispatch(Actions.setRemoveCompany(companyID, reason));
    },
    approveCompany: (companyID) => {
      dispatch(Actions.approveCompany(companyID));
    },
    toggleTopPriorityConfirmModal: (value, index) => {
      dispatch(Actions.toggleTopPriorityConfirmModal(value, index));
    },
    setTopPriority: (companyID, serviceID, value) => {
      dispatch(Actions.setTopPriority(companyID, serviceID, value));
    },
    handleChangeNote: (noteText) => {
      dispatch(Actions.changeNote(noteText));
    },
    createNote: (companyID, noteText) => {
      dispatch(Actions.createNote(companyID, noteText));
    },
    deleteNote: (companyID, noteID) => {
      dispatch(Actions.deleteNote(companyID, noteID));
    },
    //Common
    resetState: () => {
      dispatch(Actions.resetState());
    },
    updateTab: (newTab) => {
      dispatch(Actions.updateTab(newTab));
    },
    updatePage: (key, value) => {
      dispatch(Actions.updatePage(key, value));
    },
    updateSearchData: (key, newSearchData) => {
      dispatch(Actions.updateSearchData(key, newSearchData));
    },
    onChangeStoreData(event) {
      dispatch(Actions.changeStoreValue(event));
    },
    chooseItem: (key, item) => {
      dispatch(Actions.chooseItem(key, item));
    },
    //Employee
    getEmployeeCompany: (companyID, searchData) => {
      dispatch(Actions.getEmployeeCompany(companyID, searchData));
    },
    selectEmployee: (isSelected, selectItem) => {
      dispatch(Actions.selectEmployee(isSelected, selectItem));
    },
    selectAllEmployee: (isSelected) => {
      dispatch(Actions.selectAllEmployee(isSelected));
    },
    changeStatusEmployee: (companyID, data, searchData) => {
      dispatch(Actions.setActiveEmployee(companyID, data, searchData));
    },
    removeEmployee: (companyID, data, searchData) => {
      dispatch(Actions.removeEmployee(companyID, data, searchData));
    },
    updateSearchEmployee: (key, value) => {
      dispatch(Actions.updateSearchEmployee(key, value));
    },
    getPeriodEmployee: (companyID) => {
      dispatch(Actions.getPeriodEmployee(companyID));
    },
    updateDropdownPeriod: (newfilterPeriod) => {
      dispatch(Actions.updateDropdownPeriod(newfilterPeriod));
    },
    //Job
    getJobCompany: (companyID, searchData) => {
      dispatch(Actions.getJobCompany(companyID, searchData));
    },
    updateSearchJob: (key, value) => {
      dispatch(Actions.updateSearchJob(key, value));
    },
    changeStatusFilter: (key, id) => {
      dispatch(changeStatusFilter(key, id));
    },
    //Earning
    getEarningCompany: (companyID, searchData) => {
      dispatch(Actions.getEarningCompany(companyID, searchData));
    },
    //Activities
    getActivitiesCompany: (companyID) => {
      dispatch(Actions.getActivitiesCompany(companyID));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "companyDetailsPage", reducer });
const withSaga = injectSaga({ key: "companyDetailsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(CompanyDetailsPage);
