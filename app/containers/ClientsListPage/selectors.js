import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the clientsListPage state domain
 */

const selectClientsListPageDomain = state =>
    state.get("clientsListPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ClientsListPage
 */

const makeSelectClientsListPage = () =>
    createSelector(selectClientsListPageDomain, substate => substate.toJS());
const makeSelectClientsList = () =>
    createSelector(selectClientsListPageDomain, clientsListPage => clientsListPage.get('clientsList'));
export { makeSelectClientsListPage, makeSelectClientsList };
export { selectClientsListPageDomain };
