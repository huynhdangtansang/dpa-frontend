/*
 *
 * ServicesDetailsPage actions
 *
 */
import {
  DELETE_SERVICE,
  GET_PDF,
  GET_SERVICE_DATA,
  GET_SUCCESS,
  RESET_STATE,
  SET_EDIT_MODE,
  SHOW_COMMISSION_CONFIRM,
  SHOW_REMOVE_CONFIRM,
  UPDATE_COMMISSION
} from "./constants";

export function resetState() {
  return {
    type: RESET_STATE,
  };
}

export function getService(serviceID) {
  return {
    type: GET_SERVICE_DATA,
    id: serviceID
  };
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function setEditMode(value) {
  return {
    type: SET_EDIT_MODE,
    value: value
  }
}

export function deleteService(deleteData) {
  return {
    type: DELETE_SERVICE,
    deleteData
  }
}

export function updateCommission(updateData) {
  return {
    type: UPDATE_COMMISSION,
    updateData
  }
}

export function changeShowRemoveConfirm(value) {
  return {
    type: SHOW_REMOVE_CONFIRM,
    value
  }
}

export function changeShowCommissionConfirm(value) {
  return {
    type: SHOW_COMMISSION_CONFIRM,
    value
  }
}

export function getPdf(serviceId) {
  return {
    type: GET_PDF,
    serviceId
  }
}