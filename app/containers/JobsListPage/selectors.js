import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the jobsListPage state domain
 */

const selectJobsListPageDomain = state =>
    state.get("jobsListPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by JobsListPage
 */

const makeSelectJobsListPage = () =>
    createSelector(selectJobsListPageDomain, substate => substate.toJS());
export default makeSelectJobsListPage;
export { selectJobsListPageDomain };
