/**
 *
 * DashBoardPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectDashBoardPage, makeSelectTab, makeSelectUserData } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { getNotification, getUserProfile, hideNotification, selectMenu, selectNotification, signOut } from './actions';
//Style
import './style.scss';
//Library
import _ from "lodash";
import axios from 'axios';
import moment from 'moment';
import Classnames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import { HashRouter as Router, Link, Route, Switch } from 'react-router-dom';
import { DropdownItem, DropdownMenu, DropdownToggle, Modal, ModalBody, UncontrolledButtonDropdown } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';
import CustomRoute from 'components/PrivateRoute';
import { updateError } from 'containers/App/actions';
//link url
import { urlLink } from 'helper/route';
import PurpleRoundButton from 'components/PurpleRoundButton';
import GhostButton from 'components/GhostButton';
//Children pages
import UserProfilePage from 'containers/UserProfilePage/Loadable';
import ChangePasswordPage from 'containers/ChangePasswordPage/Loadable';
import StatisticsPage from 'containers/StatisticsPage/Loadable';
import ClientPages from 'containers/ClientPages/Loadable';
import DevicePages from 'containers/DevicePages/Loadable';
import ApprovalAreaPages from 'containers/ApprovalAreaPages/Loadable';
import BotMessagesPages from 'containers/BotMessagesPages/Loadable';
import JobPages from 'containers/JobPages/Loadable';
import PaymentPages from 'containers/PaymentPages';
import ServicePages from 'containers/ServicePages/Loadable';
//Team Pages
import TeamListPage from 'containers/TeamListPage/Loadable';
import TeamMemberDetailsPage from 'containers/TeamMemberDetailsPage/Loadable';
import EmployeeDetailsPage from 'containers/EmployeeDetailsPage/Loadable';
import TeamNewItemPage from 'containers/TeamNewItemPage/Loadable';
import TeamEditMemberPage from 'containers/TeamEditMemberPage/Loadable';
import { connectSocket, getEvent } from 'helper/socketConnection';

const TIME_CLOSE_TOAST = 10000;
/* eslint-disable react/prefer-stateless-function */
export class DashBoardPage extends React.PureComponent {
  redirect = (noti) => {
    switch (noti.icon) {
      case 'user':
        this.props.history.push(urlLink.userProfile);
        break;
      default:
        this.props.history.push(urlLink.login);
    }
  };
  toggleMenu = () => {
    this.setState({ showMenu: !this.state.showMenu })
  };
  dateFormat = (date) => {
    let now = moment().format('MMM DD YYYY');
    let temp = moment(date).format('MMM DD YYYY');
    if (now === temp) {
      return moment(date).format('hh:mm A');
    } else {
      return moment(date).format('MMM DD YYYY hh:mm A');
    }
  };
  iconFormat = (className) => {
    return 'icon-' + className;
  };
  notification = (content) => {
    toast(content, {
      position: "top-right",
      hideProgressBar: true,
      closeOnClick: true,
      className: 'noti'
    })
  };

  constructor(props) {
    super(props);
    this.state = {
      signOut: false,
      showMenu: true
    };
    connectSocket();
    getEvent('notification:newNotifications', (response) => {
      this.props.getNotification();
      let content = response.dataActivities.content ? response.dataActivities.content : '';
      this.notification(content);
    });
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      let tokenExpired = localStorage.getItem('tokenExpired');
      if (!_.isEmpty(tokenExpired) && !_.isNull(tokenExpired)) {
        if (moment(tokenExpired).isBefore(moment())) {//date token has been expired
          this.props.logOut();
        }
      }
      return config;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });
  }

  componentWillMount() {
    this.props.getUserData().then(() => {
      this.props.getNotification();
    }, (e) => {
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.response ? e.response.data.errors ? e.response.data.errors[0].errorMessage : 'Internal server error. Please contact with your administrator to resolve this.' : 'Internal server error. Please contact with your administrator to resolve this.'
      });
    });
  }

  render() {
    let url = window.location.href;
    let temp = url.split('#');
    let [, page] = temp;//get temp[1]
    const { notification, unRead } = this.props.dashboardpage;
    return (
      <div className="dashboard">
        <div className={Classnames("wrapper", this.state.showMenu ? 'visible-menu' : 'hidden-menu')}>
          <div className="left-side">
            <div className="side-bar slideInLeft">
              <div className="item-wrapper">
                <div className="upper-menu">
                  <div className="menu-item">
                    <button type='button' id="sidebarCollapse" className="btn btn-secondary"
                      onClick={this.toggleMenu.bind(this)}>
                      <span className="icon-main icon-hamburger"></span>
                    </button>
                  </div>
                  {/* <div className="menu-item">
                     <button type="button" className="btn btn-secondary">
                     <span className="icon-main icon-search"></span>
                     </button>
                     </div> */}
                  <div className="menu-item">
                    <UncontrolledButtonDropdown direction="right">
                      <DropdownToggle caret={false}>
                        <span className="icon-main icon-plus"></span>
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.addJob);
                        }}>
                          <span className="icon-job"></span>New Job
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.addClient);
                        }}>
                          <span className="icon-client"></span>New Client
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.addService);
                        }}>
                          <span className="icon-service"></span>New Service
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.addMember);
                        }}>
                          <span className="icon-team"></span>New Team Member
                          </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </div>

                  <div className="second-menu-mini" hidden={this.state.showMenu}>
                    {/* <div className='menu-item' onClick={() => {
                      this.props.history.push(urlLink.statistics);
                    }}>
                      <button type="button"
                        className={"btn btn-secondary " + (page.includes(urlLink.statistics) ? 'active' : '')}>
                        <span className="icon-main icon-dashboard"></span>
                      </button>
                    </div> */}
                    <div className='menu-item' onClick={() => {
                      this.props.history.push(urlLink.clients);
                    }}>
                      <button type="button"
                        className={"btn btn-secondary " + (page.includes(urlLink.clients) ? 'active' : '')}>
                        <span className="icon-main icon-client"></span>
                      </button>
                    </div>
                    <div className='menu-item' onClick={() => {
                      this.props.history.push(urlLink.devices);
                    }}>
                      <button type="button"
                        className={"btn btn-secondary " + (page.includes(urlLink.devices) ? 'active' : '')}>
                        <span className="icon-main icon-client"></span>
                      </button>
                    </div>
                    {/* <div className='menu-item' onClick={() => {
                      this.props.history.push(urlLink.approvals);
                    }}>
                      <button type="button"
                        className={"btn btn-secondary " + (page.includes(urlLink.approvals) ? 'active' : '')}>
                        <span className="icon-main icon-approval-area"></span>
                      </button>
                    </div>
                    <div className='menu-item' onClick={() => {
                      this.props.history.push(urlLink.payments);
                    }}>
                      <button type="button"
                        className={"btn btn-secondary " + (page.includes(urlLink.payments) ? 'active' : '')}>
                        <span className="icon-main icon-payout"></span>
                      </button>
                    </div> */}
                  </div>

                </div>
                <div className="lower-menu">
                  <div className="menu-item avatar">
                    <UncontrolledButtonDropdown direction="right">
                      <DropdownToggle caret={false}>
                        <img className="avatar"
                          src={(this.props.userData && this.props.userData.avatar) ? this.props.userData.avatar.fileName : "./default-user.png"}
                          alt="avatar" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }} />
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.userProfile);
                        }}>
                          <span className="icon-profile"></span>Profile
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.setState({ signOut: true });
                        }}>
                          <span className="icon-sign-out"></span>Log Out
                          </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </div>
                  <div className="menu-item">
                    <UncontrolledButtonDropdown direction="right">
                      <DropdownToggle caret={false}>
                        <span className="icon-main icon-settings"></span>
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.services);
                        }}>
                          <span className="icon-service"></span>Service Management
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.botMess);
                        }}>
                          <span className="icon-bot"></span>Bot message
                          </DropdownItem>
                        <DropdownItem onClick={() => {
                          this.props.history.push(urlLink.team);
                        }}>
                          <span className="icon-team"></span>Team Management
                          </DropdownItem>
                        {/* <DropdownItem>
                           <span className="icon-profile"></span>Unknow Keywords
                           </DropdownItem> */}
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>

                  </div>
                  <div className="menu-item notification">
                    <UncontrolledButtonDropdown direction="right">
                      <DropdownToggle caret={false}>
                        <span className="icon-main icon-notification"></span>
                        {unRead > 0 &&
                          <div className="unread">
                            <span>{unRead}</span>
                          </div>
                        }
                      </DropdownToggle>
                      <DropdownMenu>
                        <Scrollbars
                          // This will activate auto hide
                          autoHide
                          // Hide delay in ms
                          autoHideTimeout={1000}
                          autoHeight
                          autoHeightMin={0}
                          autoHeightMax={570}>
                          {(notification && notification.length > 0) ? (notification.map((item, index) => (
                            <DropdownItem toggle={true}
                              key={index} className={item.isRead ? '' : 'unread'}
                              onClick={(e) => {
                                e.stopPropagation();
                                if (!item.isRead) {
                                  this.props.selectNotification(item._id);
                                }
                                this.redirect(item);
                              }}>
                              <div className="d-flex flex-row">
                                <div className="type">
                                  <div className="icon">
                                    <span className={this.iconFormat(item.icon)}></span>
                                  </div>
                                </div>
                                <div className="info">
                                  <div className='content'>{!_.isUndefined(item.content) ? item.content : ''}</div>
                                  <span className='time'>{this.dateFormat(item.createdAt)}</span>
                                </div>
                                <div className="delete align-self-center ml-auto" onClick={(e) => {
                                  e.stopPropagation();
                                  this.props.hideNotification(item._id);
                                }}>
                                  <span className='icon-cancel'></span>
                                </div>
                              </div>
                            </DropdownItem>
                          ))) : <DropdownItem toggle={false} className="no-noti">Have no notification</DropdownItem>}
                        </Scrollbars>

                      </DropdownMenu>
                    </UncontrolledButtonDropdown>

                  </div>
                  <div className="menu-item" onClick={() => {
                    this.props.history.push(urlLink.clients);
                  }}>
                    <img className="logo-white" src="./logo-white-h.svg" alt="logowhite" onError={(e) => {
                      e.target.onerror = null;
                      e.target.src = './default-user.png'
                    }} />
                  </div>
                </div>
              </div>
            </div>

            <div className="second-menu" id='secondMenu'>
              <div className="menu-wrapper">
                <ul className="menu">
                  {/* <li className={page.includes(urlLink.statistics) ? 'active' : ''}>
                    <Link to={urlLink.statistics}>
                      <span className="icon-dashboard"></span><span>Statistics</span>
                    </Link>
                  </li> */}
                  <li className={page.includes(urlLink.clients) ? 'active' : ''}>
                    <Link to={urlLink.clients}>
                      <span className="icon-client"></span><span>Clients</span>
                    </Link>
                  </li>
                  <li className={page.includes(urlLink.devices) ? 'active' : ''}>
                    <Link to={urlLink.devices}>
                      <span className="icon-client"></span><span>Devices</span>
                    </Link>
                  </li>
                  {/* <li className={page.includes(urlLink.approvals) ? 'active' : ''}>
                    <Link to={urlLink.approvals}>
                      <span className="icon-approval-area"></span><span>Approval Area</span>
                    </Link>
                  </li>
                  <li className={page.includes(urlLink.payments) ? 'active' : ''}>
                    <Link to={urlLink.payments}>
                      <span className="icon-payout"></span><span>Payment</span>
                    </Link>
                  </li> */}
                </ul>
              </div>
            </div>

          </div>
          <div className="display animated">
            <Router>
              <Switch>
                <Route path={urlLink.userProfile} component={UserProfilePage} />
                <Route path={urlLink.changePassword} component={ChangePasswordPage} />
                <Route path={urlLink.clients} component={ClientPages} />
                <Route path={urlLink.devices} component={DevicePages} />
                {/* <Route path={urlLink.jobs} component={JobPages} />
                <Route path={urlLink.statistics} component={StatisticsPage} /> */}
                <CustomRoute path={urlLink.services} component={ServicePages}
                  scrollBarRef={this.props.scrollBarRef ? this.props.scrollBarRef : null} />
                <Route path={urlLink.approvals} component={ApprovalAreaPages} />
                <Route path={urlLink.payments} component={PaymentPages} />
                <Route path={urlLink.team} component={TeamListPage} />
                <Route path={urlLink.viewMember} component={TeamMemberDetailsPage} />
                <Route path={urlLink.addMember} component={TeamNewItemPage} />
                <Route path={urlLink.editMember} component={TeamEditMemberPage} />
                <Route path={urlLink.employee} component={EmployeeDetailsPage} />
                <Route path={urlLink.botMess} component={BotMessagesPages} />
              </Switch>
            </Router>
          </div>
        </div>
        <ToastContainer
          newestOnTop
          closeButton={<span className="icon-cancel"></span>}
          autoClose={TIME_CLOSE_TOAST}
        />
        <Modal isOpen={this.state.signOut} className="logout-modal logout-modal-v2">
          <ModalBody>
            <div className="log-out">
              <div className="upper">
                <div className="title">
                  <span>Sign Out?</span>
                </div>
                <div className="description">
                  <span>Are you sure you want to sign out?</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.setState({ signOut: false });
                }} />
                <PurpleRoundButton className="btn-purple-round sign-out" title={'Sign Out'}
                  onClick={() => {
                    this.props.logOut();
                  }} />
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

DashBoardPage.propTypes = {
  dispatch: PropTypes.func,
  logOut: PropTypes.func,
  selectMenu: PropTypes.func,
  getUserData: PropTypes.func,
  getNotification: PropTypes.func,
  selectNotification: PropTypes.func,
  hideNotification: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  dashboardpage: makeSelectDashBoardPage(),
  tab: makeSelectTab(),
  userData: makeSelectUserData(),
});

function mapDispatchToProps(dispatch) {
  return {
    logOut: () => {
      dispatch(signOut(localStorage.getItem('token')));
    },
    updateError: (data) => {
      dispatch(updateError(data))
    },
    selectMenu: (tabNumber) => {
      if (tabNumber) {
        dispatch(selectMenu(tabNumber));
      }
    },
    getUserData: () => {
      return new Promise((resolve, reject) => {
        dispatch(getUserProfile(resolve, reject));
      });
    },
    getNotification: () => {
      dispatch(getNotification());
    },
    selectNotification: (id) => {
      dispatch(selectNotification(id));
    },
    hideNotification: (id) => {
      dispatch(hideNotification(id));
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "dashBoardPage", reducer });
const withSaga = injectSaga({ key: "dashBoardPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(DashBoardPage);
