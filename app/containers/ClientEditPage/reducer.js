/*
 *
 * ClientEditPage reducer
 *
 */
import { fromJS } from "immutable";
import {
  ADD_ADDRESS,
  CHANGE_AVATAR,
  DEFAULT_ACTION,
  EDIT_ADDRESS,
  GET_CLIENT_DETAILS,
  GET_ERROR,
  GET_SUCCESS,
  SHOW_MODAL,
  UPDATE_ERROR,
  UPDATE_SUCCESS
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  clientData: {},
  errors: [],
  addressList: [],
  avatar: './default-user.png',
  showConfirmModal: false
});

function clientEditPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case GET_CLIENT_DETAILS:
      return initialState;
    case GET_SUCCESS:
      return state
          .set('clientData', action.response.data)
          .set('addressList', action.response.data.address ? fromJS(action.response.data.address.map((address) => {
            return {
              id: address._id,
              name: address.name,
              suburb: address.suburb,
              city: address.city,
              country: address.country,
              geoInfo: address.geoInfo,
              isPrimary: address.isPrimary,
              action: address.action,
              error: false
            }
          })) : fromJS([]))
          .set('errors', action.response.errors)
          .set('avatar', action.response.data.avatar && action.response.data.avatar.fileName ? action.response.data.avatar.fileName : './default-user.png');
    case GET_ERROR:
      return state
          .set('clientData', action.response.data)
          .set('errors', action.response.errors);
    case ADD_ADDRESS:
      return state
          .updateIn(['addressList'], arr => arr.push(action.newAddress));
    case EDIT_ADDRESS:
      if (action.newAddress.action === 'delete') {
        let newArray = [];
        if (action.newAddress.id === '') {
          newArray = state.get('addressList').filter((address, index) => index !== action.id).toJS();
        } else {
          newArray = state.get('addressList').map((address, index) => {
            return (index === action.id) ? action.newAddress : address
          }).toJS();
        }
        if (action.newAddress.isPrimary === true) {
          let check = false;
          let filteredArray = newArray.map((address) => {
            if (address.action !== 'delete' && check === false) {
              address.isPrimary = true;
              check = true;
              return address;
            } else {
              return address;
            }
          });
          return state
              .set('addressList', fromJS(filteredArray));
        } else {
          return state
              .set('addressList', fromJS(newArray));
        }
      } else {
        if (action.newAddress.isPrimary === true) {
          let newArray = state.get('addressList').toJS().map((address, index) => {
            if (index === action.id) {
              return action.newAddress
            } else {
              address.isPrimary = false;
              return address;
            }
          });
          return state
              .set('addressList', fromJS(newArray));
        } else {
          return state
              .updateIn(['addressList'], arr => arr.map((address, index) => {
                return (index === action.id) ? action.newAddress : address
              }))
        }
      }
    case CHANGE_AVATAR:
      return state
          .set('avatar', action.src);
    case UPDATE_SUCCESS:
    case UPDATE_ERROR:
      return state
          .set('errors', action.response.errors);
    case SHOW_MODAL:
      if (action.modal === 'updateConfirm') {
        return state
            .set('showConfirmModal', action.value)
      }
      return state;
    default:
      return state;
  }
}

export default clientEditPageReducer;
