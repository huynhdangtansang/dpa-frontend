import { fromJS } from 'immutable';
import paymentListReducer from '../reducer';

describe('paymentListReducer', () => {
  it('returns the initial state', () => {
    expect(paymentListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
