import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the teamEditMemberPage state domain
 */

const selectTeamEditMemberPageDomain = state =>
    state.get("teamEditMemberPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by TeamEditMemberPage
 */

const makeSelectTeamEditMemberPage = () =>
    createSelector(selectTeamEditMemberPageDomain, substate => substate.toJS());
export default makeSelectTeamEditMemberPage;
export { selectTeamEditMemberPageDomain };
