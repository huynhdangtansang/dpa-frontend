import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the clientEditPage state domain
 */

const selectClientEditPageDomain = state =>
    state.get("clientEditPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ClientEditPage
 */

const makeSelectClientEditPage = () =>
    createSelector(selectClientEditPageDomain, substate => substate.toJS());
const makeSelectClientDetails = () =>
    createSelector(selectClientEditPageDomain, state => state.get('clientData'));
const makeSelectErrors = () =>
    createSelector(selectClientEditPageDomain, state => state.get('errors'));
export { makeSelectClientEditPage, makeSelectClientDetails, makeSelectErrors };
export { selectClientEditPageDomain };
