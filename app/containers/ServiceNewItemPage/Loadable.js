/**
 *
 * Asynchronously loads the component for ServiceNewItemPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
