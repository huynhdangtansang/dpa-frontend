/*
 *
 * PaymentDetails reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  paymentData: {},
  images: []
});

function paymentDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return initialState;
    case LOCATION_CHANGE:
      return initialState;
    case Constants.GET_PAYMENT_DETAILS:
      if (action.loadSpinner === true) {
        return state
            .set('paymentData', fromJS([]))
            .set('images', fromJS([]))
      } else {
        return state
            .set('images', fromJS([]))
      }
    case Constants.GET_SUCCESS:
      return state
          .set('paymentData', fromJS(action.response.data));
    case Constants.ADD_IMAGE:
      return state
          .updateIn(['images'], arr => arr.push(action.newImage));
    case Constants.REMOVE_IMAGE:
      return state
          .set('images', state.get('images').filter((image, index) => index !== action.id));
    default:
      return state;
  }
}

export default paymentDetailsReducer;
