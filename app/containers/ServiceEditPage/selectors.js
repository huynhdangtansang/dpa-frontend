import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the serviceEditPage state domain
 */

const selectServiceEditPageDomain = state =>
    state.get("serviceEditPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ServiceEditPage
 */

const makeSelectServiceEditPage = () =>
    createSelector(selectServiceEditPageDomain, substate => substate.toJS());
export default makeSelectServiceEditPage;
export { selectServiceEditPageDomain };
