/**
 *
 * InputFormNumberFormat
 *
 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import ClassNames from 'classnames';
import './style.scss';
import { listError } from "helper/data";
import NumberFormat from 'react-number-format';
//Library
import _, { findIndex, indexOf } from "lodash";

function checkApiError(name, error) {
  if (!_.isEmpty(error)) {
    let index = findIndex(listError, val => val.name === name);
    if (index > -1) {
      if (indexOf(listError[index].error, error[0].errorCode) > -1) {
        return true;
      }
    }
    return false;
  }
}

function InputFormNumberFormat(props) {
  return (
      <div className={'input-group form-input'}>
        {props.label && (<div className="form-label" htmlFor={props.name}>{props.label}</div>)}

        {props.prependLabel && (
            <div className="input-group-prepend">
              <div className="input-group-text">
                {props.prependLabel}
              </div>
            </div>
        )}

        <NumberFormat
            decimalScale={props.decimalScale}
            name={props.name}
            className={ClassNames('input-form', ((props.touched && props.error) || checkApiError(props.name, props.apiError)) && 'error-form')}
            value={props.value}
            touched={props.touched}
            displayType={props.type}
            prefix={props.prefix}
            suffix={props.suffix}
            onBlur={props.onBlur}
            placeholder={props.placeholder}
            thousandSeparator={true}
            onValueChange={(values) => {
              props.onChange(values);
            }}
            fixedDecimalScale={true}

        />
        {/* <NumberFormat
         className={ClassNames('input-form',
         ((props.touched && props.error) || checkApiError(props.name, props.apiError)) && 'error-form',
         !props.showPassword && 'hide-password')}
         name={props.name}
         type={props.type}
         onChange={props.onChange}
         onBlur={props.onBlur}
         icon={props.icon}
         prefix={'$'}
         value={props.value}
         placeholder={props.placeholder}
         /> */}

        {(props.value && props.value.length > 0) ? (
            <div className="icon-append" onClick={props.togglePassword}>
              {
                !props.showPassword ? <i className={props.iconClassShow}/> : <i className={props.iconClassHide}/>
              }
            </div>
        ) : []}

        {props.showIcon && (
            <div className="icon-append">
              <i className={props.icon}></i>
            </div>
        )}


        {(_.isBoolean(props.touched) && props.touched && props.error) && (
            <div className={'error-text'}>
              <i className={'icon-error'}/>
              <span>{props.error}</span>
            </div>
        )}
      </div>
  );
}

InputFormNumberFormat.propTypes = {};
export default InputFormNumberFormat;
