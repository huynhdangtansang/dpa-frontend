import React from "react";
import TextareaCounter from 'components/TextareaCounter';
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
//Lib
import debounce from 'components/Debounce';

const REMOVE_TIME = 3;
export default class RemoveConfirmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      removeTime: 0,
    };
    this.interval = 0;
  }

  tick() {
    this.setState(prevState => ({
      removeTime: prevState.removeTime - 1
    }));
  }

  startRemoveTimer = debounce(() => {
    if (this.interval === 0 && this.state.removeTime > 0)
      this.interval = setInterval(() => this.tick(), 1000);
  }, 1000);
  resetTimer = () => {
    this.setState({ removeTime: REMOVE_TIME }, () => {
      clearInterval(this.interval);
      this.interval = 0;
    });
  };

  componentDidMount() {
    this.setState({ removeTime: REMOVE_TIME });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidUpdate() {
    if (this.state.removeTime <= 0) {
      clearInterval(this.interval);
      this.interval = 0;
    } else {
      if ((this.props.object === 'job' || this.props.object === 'service') && this.props.show === true) {//if remove job not need countdown
        if (this.interval === 0 && this.state.removeTime > 0)
          this.interval = setInterval(() => this.tick(), 1000);
      }
    }
  }

  title = (object, action) => {
    let objectString = '';
    if (object === 'company' || object === "approval company") {
      (action === 'single') ? objectString = 'company' : objectString = 'companies';
    } else {
      (action === 'single') ? objectString = object : objectString = object + 's';
    }
    return objectString.charAt(0).toUpperCase() + objectString.slice(1);
  };
  description = (object, action) => {
    let firstString = '';
    let objectString = '';
    let secondString = '';

    switch (object) {
      case "company":
      case "approval company":
        action === 'single' ? objectString = 'company' : objectString = 'companies';
        break;
      default:
        action === 'single' ? objectString = object : objectString = object + 's';
        break;
    }

    firstString = 'Are you sure to want to' + ' remove ' + (action === 'single' ? 'this ' : 'these ') + objectString;

    switch (object) {
      case 'service':
      case 'member':
        secondString = '? This action could influence on all datas involved such as jobs, companies, support ticket.';
        break;

      case 'payment':
        secondString = '? This action will send an email to the company.';
        break;

      default:
        secondString = '? All data of ' + (action === 'single' ? ' this ' : ' these ') + objectString + ' will not able to be reversed.';
        break;
    }

    return firstString + secondString;
  };
  placeholder = (object, action) => {
    let objectString = '';
    if (object === 'company' || object === "approval company") {
      (action === 'single') ? objectString = 'company' : objectString = 'companies';
    } else {
      (action === 'single') ? objectString = object : objectString = object + 's';
    }
    return (action === 'single' ? 'Why is this ' : 'Why are these ') + objectString + ' removed?';
  };

  render() {
    return (
      <Modal isOpen={this.props.show} className="logout-modal remove-modal">
        <ModalBody>
          <div className="change-status">
            <div className="upper">
              <div className="title">
                <span>Remove {this.props.action === 'single' ? 'This' : 'These'} {this.title(this.props.object, this.props.action)}</span>
              </div>
              <div className="description">
                <p className="question">
                  {this.description(this.props.object, this.props.action)}
                </p>

                <TextareaCounter
                  hidden={this.props.object === 'job' || this.props.object === 'service'}
                  placeholder={this.placeholder(this.props.object, this.props.action)}
                  title={'Reason'}
                  rows={8}
                  maxLength={50}
                  value={this.props.reason}
                  onChange={(e) => {
                    e.preventDefault();
                    this.props.handleChange(e.target.value);
                    this.startRemoveTimer();
                  }} />

              </div>
            </div>
            <div className="lower">
              <GhostButton type="button" className="btn-ghost cancel" title={'Cancel'}
                onClick={() => {
                  this.props.closeModal();
                  this.resetTimer();
                }} />
              <PurpleRoundButton className="btn-purple-round sign-out"
                title={'Remove' + '(' + this.state.removeTime + ')'}
                disabled={this.state.removeTime > 0}
                onClick={() => {
                  this.props.closeModal();
                  this.resetTimer();
                  this.props.removeObject();
                }} />
            </div>
          </div>
        </ModalBody>
      </Modal>
    )
  }
}