/**
 *
 * Asynchronously loads the component for JobDetailsTabControl
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
