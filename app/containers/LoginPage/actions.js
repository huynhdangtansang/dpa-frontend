/*
 *
 * LoginPage actions
 *
 */
import { DEFAULT_ACTION, GET_LOGIN_DATA, LOGIN_FAIL, LOGIN_SUCCESS, SET_ERRORS } from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getLoginData(email, password, remember) {
  return {
    type: GET_LOGIN_DATA,
    email: email,
    password: password,
    remember: remember
  }
}

export function loginSuccess(response, email) {
  return {
    type: LOGIN_SUCCESS,
    response: response,
    email: email
  }
}

export function loginFail(response) {
  return {
    type: LOGIN_FAIL,
    response: response
  }
}

export function setErrors(data) {
  return {
    type: SET_ERRORS,
    data
  }
}