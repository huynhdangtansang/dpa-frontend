/*
 *
 * JobDetailsPage actions
 *
 */
import {
  ADD_NEW_ACT,
  CANCEL_JOB,
  CHANGE_APPOINTMENT,
  CHANGE_SPECIFIC_SELECTION,
  CHANGE_TAB,
  COMPLETE_PAY,
  GET_ACTS,
  GET_ACTS_ERROR,
  GET_ACTS_SUCCESS,
  GET_COMPANIES,
  GET_COMPANIES_ERROR,
  GET_COMPANIES_SUCCESS,
  GET_EMPLOYEES,
  GET_EMPLOYEES_ERROR,
  GET_EMPLOYEES_SUCCESS,
  GET_ERROR,
  GET_JOB,
  GET_SUCCESS,
  REASSIGN_JOB,
  REASSIGN_OBJECT,
  REASSIGN_SPECIFIC,
  REFUND,
  RESCHEDULE_JOB,
  RESET_STATE,
  SHOW_MODAL,
  SUBMIT_ESTIMATE,
  UPDATE_ESTIMATE
} from "./constants";

export function resetState(tab) {
  return {
    type: RESET_STATE,
  }
}

export function changeTab(tab) {
  return {
    type: CHANGE_TAB,
    tab
  }
}

export function getJob(id, resolve, reject) {
  return {
    type: GET_JOB,
    id,
    resolve,
    reject
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: GET_ERROR,
    response
  }
}

export function changeReassignObject(object) {
  return {
    type: REASSIGN_OBJECT,
    object
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}

export function getActs(jobId) {
  return {
    type: GET_ACTS,
    jobId
  }
}

export function getActsSuccess(response) {
  return {
    type: GET_ACTS_SUCCESS,
    response
  }
}

export function getActsError(response) {
  return {
    type: GET_ACTS_ERROR,
    response
  }
}

export function reassignJob(reAssignType, reAssignData) {
  return {
    type: REASSIGN_JOB,
    reAssignType,
    reAssignData
  }
}

export function reAssignSpecific(jobId, assignData) {
  return {
    type: REASSIGN_SPECIFIC,
    jobId,
    assignData
  }
}

export function cancelJob(jobId, reason) {
  return {
    type: CANCEL_JOB,
    jobId,
    reason
  }
}

export function getCompanies(serviceId) {
  return {
    type: GET_COMPANIES,
    serviceId
  }
}

export function getCompaniesSuccess(response) {
  return {
    type: GET_COMPANIES_SUCCESS,
    response
  }
}

export function getCompaniesError(response) {
  return {
    type: GET_COMPANIES_ERROR,
    response
  }
}

export function getEmployees(companyId) {
  return {
    type: GET_EMPLOYEES,
    companyId
  }
}

export function getEmployeesSuccess(response) {
  return {
    type: GET_EMPLOYEES_SUCCESS,
    response
  }
}

export function getEmployeesError(response) {
  return {
    type: GET_EMPLOYEES_ERROR,
    response
  }
}

export function changeSpecificSelection(selector, value, appointmentTime, serviceId) {
  return {
    type: CHANGE_SPECIFIC_SELECTION,
    selector,
    value,
    searchData: {
      companyId: value,
      appointmentTime,
      serviceId
    }
  }
}

export function addNewAct(dateType, date, actData) {
  return {
    type: ADD_NEW_ACT,
    dateType,
    date,
    actData
  }
}

export function changeAppointment(value) {
  return {
    type: CHANGE_APPOINTMENT,
    value
  }
}

export function rescheduleJob(jobId, appointmentTime, reason) {
  return {
    type: RESCHEDULE_JOB,
    jobId,
    appointmentTime,
    reason
  }
}

export function submitEstimate(jobId, updateData) {
  return {
    type: SUBMIT_ESTIMATE,
    jobId,
    updateData
  }
}

export function updateEstimate(jobId, updateData) {
  return {
    type: UPDATE_ESTIMATE,
    jobId,
    updateData
  }
}

export function refund(jobId, refundData) {
  return {
    type: REFUND,
    jobId,
    refundData
  }
}

export function completePay(jobId) {
  return {
    type: COMPLETE_PAY,
    jobId
  }
}