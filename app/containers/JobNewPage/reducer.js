/*
 *
 * JobNewPage reducer
 *
 */
import { fromJS } from "immutable";
import * as Constants from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  images: [],
  description: '',
  location: {},
  serviceListDropdown: [],
  clientListDropdown: [],
  companyListDropdown: [],
  employeeListDropdown: [],
  chosenService: {
    _id: ''
  },
  chosenClient: {
    _id: ''
  },
  chosenCompany: {
    _id: ''
  },
  chosenEmployee: {
    _id: ''
  },
  appointment: undefined,
  apiErrors: []
});

function jobNewPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;

      //JOB
    case Constants.GET_SUCCESS:
      return state
          .set(action.key, action.response);
    case Constants.ADD_IMAGE:
      return state
          .updateIn(['images'], arr => arr.push(action.newImage));
    case Constants.REMOVE_IMAGE:
      return state
          .set('images', state.get('images').filter((image, index) => index !== action.id));
    case Constants.CHANGE_LOCATION:
      return state
          .set('location', action.newLocation);
    case Constants.CHOOSE_ITEM:
      return state
          .set(action.key, action.item);
    case Constants.CHANGE_APPOINTMENT:
      return state
          .set('appointment', action.newAppointment);
    case Constants.GET_COMPANIES_COMPLETE:
      return state
          .set('companyListDropdown', action.response.data)
          .set('employeeListDropdown', fromJS([]));
    case Constants.GET_EMPLOYEES_COMPLETE:
      return state
          .set('employeeListDropdown', action.response.data.data);
    case Constants.CLEAR_ERROR:
      return state
          .set('apiErrors', fromJS([]));
    case Constants.ADD_COMPLETE:
      return state
          .set('apiErrors', action.response.errors);
    default:
      return state;
  }
}

export default jobNewPageReducer;
