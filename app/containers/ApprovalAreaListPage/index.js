/**
 *
 * ApprovalAreaListPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as Selectors from "./selectors";
import * as Actions from "./actions";
import { getServiceListData, updateDropdownService } from 'containers/CompaniesListPage/actions';
import reducer from "./reducer";
import saga from "./saga";
//Library
import _ from "lodash";
import moment from 'moment';
import debounce from 'components/Debounce';
import classnames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import PurpleRoundButton from 'components/PurpleRoundButton';
import './style.scss';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  Input,
  Modal,
  ModalBody,
  UncontrolledDropdown
} from 'reactstrap';
import GhostButton from 'components/GhostButton';
// Combo import table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
// Modal
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ApprovalAreaListPage extends React.PureComponent {
  //-----------------------------TABLE FUNCTION----------------------------
  changePagination = (type) => {
    const { approvalList, searchData, currentPage } = this.props.approvalarealistpage;
    const { limit } = approvalList.pagination;
    let newSearchData = searchData;
    let newPage, newOffset;
    if (type == 'prev') {
      newPage = currentPage - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    } else {//next
      newPage = currentPage + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    }
    this.props.updateFilter(newSearchData, newPage);
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { searchData } = this.props.approvalarealistpage;
    this.props.getApprovalList(searchData);
  }, 1000);
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
      status: row.active
    };
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  sortData = (field, order) => {
    const { searchData } = this.props.approvalarealistpage;
    let temp = searchData;
    temp.sort = field;
    temp.sortType = order;
    this.props.updateFilter(temp, 1);
    this.props.getApprovalList(searchData);
  };
  //--------------------FUNCTION HANDLE DATA-----------------------------
  checkChangeStatusButton = () => {
    const { selectedCompanies } = this.props.approvalarealistpage;
    let list = selectedCompanies;
    if (list.length > 0) {
      const compareStatus = list[0].status;
      let differentItems = list.filter((item) => item.status !== compareStatus);
      if (differentItems.length === 0) {
        return {
          isHide: false,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      } else {
        return {
          isHide: true,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      }
    }
    return {
      isHide: false,
      title: 'Deactive',
    }
  };
  singleAction = (type, companyData) => {
    if (type === 'changeStatus') {
      this.setState({ chosenCompany: companyData }, () => {
        this.openStatusConfirm('single');
      });
    } else if (type === 'remove') {
      this.setState({ chosenCompany: companyData }, () => {
        this.openRemoveConfirm('single');
      })
    } else if (type === 'approve') {
      this.setState({ chosenCompany: companyData }, () => {
        this.openApproveConfirm('single');
      })
    } else {
      let listIds = [];
      listIds.push(companyData.id);
      let data = {
        ids: listIds
      };
      this.props.resetPass(data);
    }
  };
  //---------------------------------------------------------------------
  multiAction = (type) => {
    if (type === 'changeStatus') {
      this.openStatusConfirm('multi');
    } else if (type === 'remove') {
      this.openRemoveConfirm('multi');
    } else if (type === 'approve') {
      this.openApproveConfirm('multi');
    } else {
      const { selectedCompanies } = this.props.approvalarealistpage;
      let listIds = [];
      let list = selectedCompanies;
      let i = 0;
      list.forEach((company) => {
        listIds.push(company.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds
          };
          this.props.resetPass(data);
        } else {
          i++;
        }
      });
    }
  };
  openStatusConfirm = (type) => {
    this.setState({
      modifyType: type,
      showChangeStatusConfirm: true
    });
  };
  openApproveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showApproveConfirm: true
    });
  };
  openRemoveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showRemoveConfirm: true
    });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  handleSearchTextChange = debounce(text => {
    this.props.updateSearchString(text);
    const { searchData } = this.props.approvalarealistpage;
    this.props.getApprovalList(searchData);
  }, 1000);
  handleChangeFilterService = debounce(newfilterService => {
    this.props.updateDropdownService(newfilterService);
    const { searchData } = this.props.approvalarealistpage;
    this.props.getApprovalList(searchData);
  }, 400);
  approveCompany = () => {
    this.refs.table_approve.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType === 'single') {
      const { searchData } = this.props.approvalarealistpage;
      let { id } = this.state.chosenCompany;
      let listIds = [];
      listIds.push(id);
      let data = {
        ids: listIds
      };
      this.props.approveCompany(data, searchData);
    } else { //multi
      const { selectedCompanies, searchData } = this.props.approvalarealistpage;
      let companyIds = [];
      selectedCompanies.map(item => {
        companyIds.push(item.id)
      });
      this.props.approveCompany({ ids: companyIds }, searchData);
    }
    this.setState({ showApproveConfirm: false });
  };
  removeCompany = () => {
    this.refs.table_approve.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType === 'single') {
      const { searchData } = this.props.approvalarealistpage;
      let id = this.state.chosenCompany.id;
      let listIds = [];
      listIds.push(id);
      let data = {
        ids: listIds,
        reason: this.state.reason
      };
      this.props.removeCompany(data, searchData);
    } else {
      let listIds = [];
      const { selectedCompanies, searchData } = this.props.approvalarealistpage;
      let list = selectedCompanies;
      let i = 0;
      list.forEach((company) => {
        listIds.push(company.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds,
            reason: this.state.reason
          };
          this.props.removeCompany(data, searchData);
        } else {
          i++;
        }
      });
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      showChangeStatusConfirm: false,
      selectedCompanies: [],
      isActive: true,
      reason: '',
      chosenCompany: {},
      modifyType: 'single',
      hideChangeStatusButton: false,
      changeStatusType: 'Deactive',
      page: 1
    }
  }

  componentWillMount() {
    const { searchData, searchDataForService } = this.props.approvalarealistpage;
    this.props.getApprovalList(searchData);
    this.props.getServiceList(searchDataForService);
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const { approvalList, selectedCompanies, searchData, serviceList, filterService, currentPage } = this.props.approvalarealistpage;
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll,
    };
    const options = {
      withoutNoDataText: true,
      defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    const actionFormat = (col, row) => {
      let editMenu = (
          <DropdownMenu>
            <DropdownItem onClick={(e) => {
              e.preventDefault();
              this.props.history.push(urlLink.viewApproval + '?id=' + row._id);
            }}>View</DropdownItem>
            <DropdownItem onClick={(e) => {
              e.preventDefault();
              this.singleAction('approve', { id: row._id, status: row.active });
            }}>Approval</DropdownItem>
            <DropdownItem onClick={(e) => {
              e.preventDefault();
              this.props.history.push(urlLink.editApproval + '?id=' + row._id);
            }}>Edit</DropdownItem>
            <DropdownItem
                onClick={(e) => {
                  e.preventDefault();
                  this.singleAction('remove', { id: row._id, status: row.active });
                }}
            > Remove</DropdownItem>
          </DropdownMenu>
      );
      return (
          <ActionFormatter menu={editMenu}/>
      );
    };
    const iconFormat = (cell, row) => {
      return (
          <img
              className='avatar-company'
              src={row.logo ? row.logo.fileName : './default-user.png'}
              onError={(e) => {
                e.target.onerror = null;
                e.target.src = './default-user.png'
              }}
              onClick={(e) => {
                e.preventDefault();
                this.props.history.push(urlLink.viewApproval + '?id=' + row._id);
              }}>
          </img>
      )
    };
    const nameFormat = (cell, row) => {
      return (
          <span onClick={(e) => {
            e.preventDefault();
            this.props.history.push(urlLink.viewApproval + '?id=' + row._id);
          }}>{cell}</span>
      );
    };
    const servicesFormat = (cell) => {
      return (
          <div className="list-logo-services">
            {!_.isEmpty(cell) && cell.map(item => (
                <img
                    key={item._id}
                    className='logo-service'
                    src={item.icon ? item.icon.fileName : './default-user.png'}
                    onError={(e) => {
                      e.target.onerror = null;
                      e.target.src = './default-user.png';
                    }}>
                </img>
            ))}
          </div>);
    };
    const surburFomat = (cell) => {
      return (
          <div className="surbur">
            {!_.isEmpty(cell) ? <span>{cell[0].suburbs}</span> : '-'}
          </div>
      );
    };
    const notesFormat = (cell) => {
      return (
          <div className="list-notes">
            {cell && cell[0] && <span>{cell[0].content}</span>}
          </div>);
    };
    const trClassFormat = (row) => {
      // row is the current row data
      return _.isEmpty(row.notes) ? "tr-bold" : "tr-normal";  // return class name.
    };
    return (
        <div className="approval-area-list">
          <div className="header-list-page">
            <Form inline inline onSubmit={(e) => this.handleSearch(e)} className="align-items-center">

              <div className="search-bar services-search">
                <Input placeholder="Search company and phone number"
                       defaultValue={searchData.search}
                       onChange={(e) => {
                         e.preventDefault();
                         this.handleSearchTextChange(e.target.value);
                       }}/>
                <span className="icon-search"></span>
              </div>

              <UncontrolledDropdown className="services-dropdown">
                <DropdownToggle>
                  <span className="title">Services:&nbsp;</span>
                  <span className="content">{filterService.label}</span>
                  <i className="fixle-caret icon-triangle-down"></i>
                </DropdownToggle>

                <DropdownMenu>
                  <Scrollbars
                      // This will activate auto hide
                      autoHide
                      // Hide delay in ms
                      autoHideTimeout={1000}
                      autoHeight
                      autoHeightMin={0}
                      autoHeightMax={200}>
                    <DropdownItem onClick={(e) => {
                      e.preventDefault();
                      this.handleChangeFilterService({
                        value: null, label: 'All'
                      });
                    }}>All</DropdownItem>
                    {!_.isEmpty(serviceList) && (serviceList.map((item) => (
                        <DropdownItem key={item._id}
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.handleChangeFilterService({
                                          value: item._id, label: item.name
                                        })
                                      }}
                        >{item.name}
                        </DropdownItem>
                    )))}
                  </Scrollbars>
                </DropdownMenu>
              </UncontrolledDropdown>

              {approvalList.data && approvalList.data.length > 0 && approvalList.pagination && (
                  <TotalFormatter
                      offset={approvalList.pagination.offset}
                      limit={approvalList.pagination.limit}
                      total={approvalList.pagination.total}
                      changePagination={this.changePagination}
                      page={currentPage}
                  />
              )}

            </Form>
          </div>
          <div className={classnames("content content-list-page", selectedCompanies.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_approve"
                data={approvalList.data instanceof Array ? approvalList.data : []}
                options={options}
                bordered={false}
                containerClass='table-fixle approvals-table'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'
                bodyContainerClass='table-fixle-body'
                selectRow={selectRowProp}
                trClassName={trClassFormat}>
              <TableHeaderColumn dataField='_id' isKey hidden>Company ID</TableHeaderColumn>
              <TableHeaderColumn dataField='icon' width={46 / 14 + 'rem'} dataFormat={iconFormat}></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='name' width={150 / 14 + 'rem'} dataFormat={nameFormat}
                                 columnClassName='name'>Company Name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='services' width={186 / 14 + 'rem'} dataFormat={servicesFormat}
                                 headerAlign={'center'}>Types of services</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='areas' dataAlign='left' dataFormat={surburFomat}
                                 headerAlign={'left'}
                                 dataAlign={'center'}>Surbur</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='phoneNumber' dataAlign='center'>Phone number</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='createdAt' dataFormat={(cell) => {
                return moment(cell).format('ddd D MMM YYYY')
              }}>Submission date</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='note' width={164 / 14 + 'rem'} dataField='notes'
                                 dataFormat={notesFormat}>Note</TableHeaderColumn>
              <TableHeaderColumn dataFormat={actionFormat} width={90 / 14 + 'rem'} columnClassName={'edit-dropdown'}
                                 dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div className="footer" hidden={selectedCompanies.length === 0}>
            <GhostButton title={'Approve'} className="btn-ghost btn-edit-charge"
                         onClick={() => {
                           if (selectedCompanies.length === 1) {
                             this.singleAction('approve', selectedCompanies[0]);
                           } else
                             this.multiAction('approve');
                         }}/>
            <GhostButton title={'Remove'} className="btn-ghost btn-remove"
                         onClick={() => {
                           if (selectedCompanies.length === 1) {
                             this.singleAction('remove', selectedCompanies[0]);
                           } else
                             this.multiAction('remove');
                         }}/>
          </div>

          <Modal isOpen={this.state.showApproveConfirm} className="remove-confirm-modal update-confirm">
            <ModalBody>
              <div className="remove-confirm">
                <div className="upper">
                  <div className="title">
                    <span>Approve {this.state.modifyType === 'single' ? 'This Company' : 'These Companies'}</span>
                  </div>
                  <div className="description">
                    <span>Are you sure to want to approve {this.state.modifyType === 'single' ? 'this company' : 'these companies'}? After approval, company {this.state.modifyType === 'single' ? 'profile' : 'profiles'} will move to companies section</span>
                  </div>
                </div>
                <div className="lower">
                  <GhostButton type="button" className="btn-ghost cancel" title={'cancel'}
                               onClick={() => {
                                 this.setState({ showApproveConfirm: false });
                               }}/>
                  <PurpleRoundButton className="btn-purple-round save" title={'approve'}
                                     onClick={() => {
                                       this.approveCompany();
                                     }}
                  />
                </div>
              </div>
            </ModalBody>
          </Modal>

          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              timer={3}
              object={'approval company'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeCompany}/>
        </div>
    );
  }
}

ApprovalAreaListPage.propTypes = {
  dispatch: PropTypes.func,
  getApprovalList: PropTypes.func,
  approveCompany: PropTypes.func,
  getServiceList: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  approvalarealistpage: Selectors.makeSelectApprovalAreaListPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(Actions.resetState());
    },
    getApprovalList: (searchData) => {
      dispatch(Actions.getApprovalListData(searchData));
    },
    getServiceList: (searchData) => {
      dispatch(getServiceListData(searchData));
    },
    select: (isSelected, selectItem) => {
      dispatch(Actions.selectCompany(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(Actions.selectAllCompany(isSelected));
    },
    approveCompany: (companyIds, searchData) => {
      dispatch(Actions.approveCompany(companyIds, searchData));
    },
    removeCompany: (dataDelete, searchData) => {
      dispatch(Actions.removeCompany(dataDelete, searchData));
    },
    updateSearchString: (text) => {
      dispatch(Actions.updateSearchString(text));
    },
    updateFilter: (newSearchData, newPage) => {
      dispatch(Actions.updateFilter(newSearchData, newPage));
    },
    updateDropdownService: (newfilterService) => {
      dispatch(updateDropdownService(newfilterService));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "approvalAreaListPage", reducer });
const withSaga = injectSaga({ key: "approvalAreaListPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ApprovalAreaListPage);
