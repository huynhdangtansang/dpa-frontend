/**
 *
 * StatisticsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectStatisticsPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import config from 'config';
import * as Actions from "./actions";
import { monthList, showWithType } from "helper/data";
//Lib
import { Bar, Line, Pie } from 'react-chartjs-2';
import _, { debounce } from 'lodash';
import uuidv1 from 'uuid';
import { Scrollbars } from 'react-custom-scrollbars';
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, } from 'reactstrap';
//Components
import { getEvent } from 'helper/socketConnection';
//Name link
import { urlLink } from 'helper/route';
//Style
import './style.scss';
import moment from 'moment';

/* eslint-disable react/prefer-stateless-function */
export class StatisticsPage extends React.PureComponent {
  //Get chart after change search data
  getChartAfterChangeData = debounce((key) => {
    const { chart } = this.props.statisticspage;
    this.props.getDataChartByType({
      typeChart: config.api.chart[key] ? config.api.chart[key] : '',
      searchData: chart[key.toString()].searchData ? chart[key.toString()].searchData : {},
      key: key ? key : ''
    });
  }, 0);
  renderPeriod = (currentPeriod, chartType, key) => {
    let list = [];
    for (let i = parseInt(currentPeriod); i <= moment().year(); i++) {
      list.push(
          <DropdownItem key={uuidv1()}
                        onClick={(e) => {
                          e.preventDefault();
                          this.handleChangeFilterChart(['chart', chartType], key, {
                            value: i,
                            label: i.toString()
                          });
                        }}>
            {i}
          </DropdownItem>)
    }
    return list;
  };

  constructor(props) {
    super(props);
    getEvent('init:updateChart', (response) => {
      let url = window.location.hash;
      if (url.includes(urlLink.statistics) && response.type) {
        const { chart } = this.props.statisticspage;
        switch (response.type) {
          case 'chartAppointmentTime':
            this.props.getDataChartByType({
              typeChart: config.api.chart['appointmentTime'] ? config.api.chart['appointmentTime'] : '',
              searchData: chart['appointmentTime'].searchData ? chart['appointmentTime'].searchData : {},
              key: 'appointmentTime'
            });
            break;
          case 'chartJobBooking':
            this.props.getDataChartByType({
              typeChart: config.api.chart['job'] ? config.api.chart['job'] : '',
              searchData: chart['region'].searchData ? chart['region'].searchData : {},
              key: 'region'
            });
            break;
          case 'chartRevenue':
            this.props.getDataChartByType({
              typeChart: config.api.chart['region'] ? config.api.chart['region'] : '',
              searchData: chart['region'].searchData ? chart['region'].searchData : {},
              key: 'region'
            });
            break;
          case 'chartRegion':
            this.props.getDataChartByType({
              typeChart: config.api.chart['region'] ? config.api.chart['region'] : '',
              searchData: chart['region'].searchData ? chart['region'].searchData : {},
              key: 'region'
            });
            break;
          case 'chartNumberOfUser':
            this.props.getDataChartByType({
              typeChart: config.api.chart['user'] ? config.api.chart['user'] : '',
              searchData: chart['user'].searchData ? chart['user'].searchData : {},
              key: 'user'
            });
            break;
          default:
            this.props.getAllDataStatistic();
        }
      }
    });
  }

  componentWillMount() {
    this.props.getAllDataStatistic();
  }

  componentWillUnmount() {
    this.props.resetStore();
  }

  handleChangeFilterChart(position, key, obj) {
    //update label filter, and value in searchData in key
    let positionFilter = position.concat(['filters', key, 'label']);
    this.props.updateFilterChart(positionFilter, obj.label);
    let positionsearchData = position.concat(['searchData', key]);
    this.props.updateFilterChart(positionsearchData, obj.value);
    this.getChartAfterChangeData(!_.isUndefined(position[1]) ? position[1] : '');
  }

  render() {
    const {
      //data dropdown
      currentPeriod, serviceListDropdown, suburbListDropdown, companyListDropdown,
      //data chart
      chart,
      averageJobData, averageFeeData,
      serviceData, regionData,
      userData, revenueData, jobData, profitData, appoinmentTimeData,
      barDataDefault
    } = this.props.statisticspage;
    const legendDotOpts = {
      display: true,
      position: 'bottom',
      fullWidth: true,
      reverse: false,
      labels: {
        fontSize: 9,
        fontColor: '#9b9b9b',
        usePointStyle: true,
        padding: 5,
        boxWidth: 10,
      },
    };
    const legendSquareOpts = {
      display: true,
      position: 'bottom',
      fullWidth: true,
      reverse: false,
      labels: {
        fontSize: 9,
        fontColor: '#9b9b9b',
        boxWidth: 9,
        padding: 12,
      },
    };
    const optionsPieChart = {
      elements: {
        arc: {
          borderWidth: 0
        }
      },
      responsive: true,
      maintainAspectRatio: true,
      responsiveAnimationDuration: 1000,
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            return data.labels[0] === 'No data' ? data.labels[0] :
                data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']];
          }
        }
      },
    };
    const optionBarChart = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 1000,
      scales: {
        xAxes: [{
          barThickness: 16,
          gridLines: {
            display: false
          }
        }]
      }
    };
    const optionBarStackedChart = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 1000,
      scales: {
        xAxes: [{
          barThickness: 52,
          gridLines: {
            display: false
          },
          stacked: true
        }]
      }
    };
    const optionLineChart = {
      responsive: true,
      maintainAspectRatio: false,
      responsiveAnimationDuration: 1000,
      scales: {
        xAxes: [{
          barThickness: 50,
          gridLines: {
            display: false
          },
          stacked: true
        }]
      }
    };
    return (
        <div className="statistic-page">
          <div className="content">
            <div className="container-fluid">

              {/* Average TOP */}
              <div className="row">
                <div className="col-4">
                  <div className="information average-info">

                    <div className="row">
                      <div className="col">
                        <div className="title">
                          <span>Average Job Size</span>
                        </div>
                      </div>
                      <div className="col">
                        <div className="number-average">
                          <span className="unit">{averageJobData.unit}</span><span
                            className="natural-part">{parseInt(averageJobData.value || 0, 10)}</span><span
                            className="decimal-fraction">{(averageJobData || 0).value.toFixed(2).toString().slice((averageJobData.value || 0).toFixed(2).toString().indexOf('.'))}</span>
                        </div>
                      </div>
                    </div>


                    <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                      <DropdownToggle>
                        <span className="title">Service:&nbsp;</span>
                        <span className="content">{chart.averageJob.filters['serviceId'].label}</span>
                        <i className="fixle-caret icon-triangle-down"></i>
                      </DropdownToggle>
                      <DropdownMenu>
                        <Scrollbars
                            // This will activate auto hide
                            autoHide
                            // Hide delay in ms
                            autoHideTimeout={1000}
                            autoHeight
                            autoHeightMin={0}
                            autoHeightMax={300}>
                          <DropdownItem key={uuidv1()}
                                        onClick={(e) => {
                                          e.preventDefault();
                                          this.handleChangeFilterChart(['chart', 'averageJob'], 'serviceId', {
                                            value: '', label: 'All'
                                          });
                                        }}>All</DropdownItem>

                          {_.isArray(serviceListDropdown) ? serviceListDropdown.map((item) => (
                              <DropdownItem key={uuidv1()}
                                            onClick={(e) => {
                                              e.preventDefault();
                                              this.handleChangeFilterChart(['chart', 'averageJob'], 'serviceId', {
                                                value: item._id ? item._id : '',
                                                label: item.name ? item.name : ''
                                              });
                                            }}>
                                {item.name}
                              </DropdownItem>
                          )) : []}
                        </Scrollbars>
                      </DropdownMenu>
                    </UncontrolledDropdown>


                  </div>
                  <div className="information average-info">

                    <div className="row">
                      <div className="col">
                        <div className="title">
                          <span>Average Fixle Fee</span>
                        </div>
                      </div>
                      <div className="col">
                        <div className="number-average">
                          <span className="unit">{averageFeeData.unit}</span><span
                            className="natural-part">{parseInt(averageFeeData.value || 0, 10)}</span><span
                            className="decimal-fraction">{(averageFeeData.value || 0).toFixed(2).toString().slice((averageFeeData.value || 0).toFixed(2).toString().indexOf('.'))}</span>
                        </div>
                      </div>
                    </div>

                    <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                      <DropdownToggle>
                        <span className="title">Service:&nbsp;</span>
                        <span className="content">{chart.averageFee.filters['serviceId'].label}</span>
                        <i className="fixle-caret icon-triangle-down"></i>
                      </DropdownToggle>
                      <DropdownMenu>
                        <Scrollbars
                            // This will activate auto hide
                            autoHide
                            // Hide delay in ms
                            autoHideTimeout={1000}
                            autoHeight
                            autoHeightMin={0}
                            autoHeightMax={300}>
                          <DropdownItem key={uuidv1()}
                                        onClick={(e) => {
                                          e.preventDefault();
                                          this.handleChangeFilterChart(['chart', 'averageFee'], 'serviceId', {
                                            value: '', label: 'All'
                                          });
                                        }}>All</DropdownItem>

                          {_.isArray(serviceListDropdown) ? serviceListDropdown.map((item) => (
                              <DropdownItem key={uuidv1()}
                                            onClick={(e) => {
                                              e.preventDefault();
                                              this.handleChangeFilterChart(['chart', 'averageFee'], 'serviceId', {
                                                value: item._id ? item._id : '',
                                                label: item.name ? item.name : ''
                                              });
                                            }}>
                                {item.name}
                              </DropdownItem>
                          )) : []}
                        </Scrollbars>
                      </DropdownMenu>
                    </UncontrolledDropdown>

                  </div>

                </div>

                {/* SERVICE BREAK DOWN */}
                <div className="col-4">
                  <div className="information">
                    <div className="title">
                      <span>Service Breakdown</span>
                    </div>

                    <div className="chart pie-chart">
                      <Pie
                          data={serviceData}
                          options={optionsPieChart}
                          legend={legendDotOpts}
                          width={264}
                          height={224}
                      />

                    </div>
                    <div className="row filter-dropdown">
                      <div className="col-6" key={uuidv1()}>
                        <UncontrolledDropdown className="period-dropdown">
                          <DropdownToggle>
                            <span className="title">Month:&nbsp;</span>
                            <span className="content">{chart.service.filters['month'].label}</span>
                            <i className="fixle-caret icon-triangle-down"></i>
                          </DropdownToggle>
                          <DropdownMenu>
                            <Scrollbars
                                // This will activate auto hide
                                autoHide
                                // Hide delay in ms
                                autoHideTimeout={1000}
                                autoHeight
                                autoHeightMin={0}
                                autoHeightMax={300}>

                              {monthList.map(item => (
                                  <DropdownItem key={uuidv1()}
                                                onClick={(e) => {
                                                  e.preventDefault();
                                                  this.handleChangeFilterChart(['chart', 'service'], 'month', {
                                                    value: item ? item.value : '',
                                                    label: item ? item.label : ''
                                                  });
                                                }}>{item.label}</DropdownItem>
                              ))}

                            </Scrollbars>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>

                      <div className="col-6" key={uuidv1()}>
                        <UncontrolledDropdown className="period-dropdown">
                          <DropdownToggle>
                            <span className="title">Year:&nbsp;</span>
                            <span className="content">{chart.service.filters['year'].label}</span>
                            <i className="fixle-caret icon-triangle-down"></i>
                          </DropdownToggle>
                          <DropdownMenu>
                            <Scrollbars
                                // This will activate auto hide
                                autoHide
                                // Hide delay in ms
                                autoHideTimeout={1000}
                                autoHeight
                                autoHeightMin={0}
                                autoHeightMax={300}>

                              {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'service', 'year'))}

                            </Scrollbars>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>
                    </div>
                  </div>
                </div>

                {/* USER BY REGION */}
                <div className="col-4">
                  <div className="information">
                    <div className="title">
                      <span>Users by Region</span>
                    </div>

                    <div className="chart pie-chart">
                      <Pie data={regionData}
                           options={optionsPieChart}
                           legend={legendDotOpts}
                           width={264}
                           height={224}
                      />
                    </div>
                    <div className="row filter-dropdown">

                      <div className="col-6" key={uuidv1()}>
                        <UncontrolledDropdown className="period-dropdown">
                          <DropdownToggle>
                            <span className="title">Month:&nbsp;</span>
                            <span className="content">{chart.region.filters['month'].label}</span>
                            <i className="fixle-caret icon-triangle-down"></i>
                          </DropdownToggle>
                          <DropdownMenu>
                            <Scrollbars
                                // This will activate auto hide
                                autoHide
                                // Hide delay in ms
                                autoHideTimeout={1000}
                                autoHeight
                                autoHeightMin={0}
                                autoHeightMax={300}>

                              {monthList.map(item => (
                                  <DropdownItem key={uuidv1()}
                                                onClick={(e) => {
                                                  e.preventDefault();
                                                  this.handleChangeFilterChart(['chart', 'region'], 'month', {
                                                    value: item ? item.value : '',
                                                    label: item ? item.label : ''
                                                  });
                                                }}>{item.label}</DropdownItem>
                              ))}

                            </Scrollbars>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>

                      <div className="col-6" key={uuidv1()}>
                        <UncontrolledDropdown className="period-dropdown">
                          <DropdownToggle>
                            <span className="title">Year:&nbsp;</span>
                            <span className="content">{chart.region.filters['year'].label}</span>
                            <i className="fixle-caret icon-triangle-down"></i>
                          </DropdownToggle>
                          <DropdownMenu>
                            <Scrollbars
                                // This will activate auto hide
                                autoHide
                                // Hide delay in ms
                                autoHideTimeout={1000}
                                autoHeight
                                autoHeightMin={0}
                                autoHeightMax={300}>

                              {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'region', 'year'))}

                            </Scrollbars>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>

                    </div>

                  </div>
                </div>
              </div>

              {/* Number of Users */}
              <div className="row-info">
                <div className="information">
                  <div className="row">
                    <div className="col-2">
                      <div className="title">
                      <span>
                        Number of Users
                      </span>
                      </div>
                    </div>
                    <div className="col-10 text-right dropdown-group">

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Period:&nbsp;</span>
                          <span className="content">{chart.user.filters['period'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'user', 'period'))}

                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>

                  </div>
                  <div className='row'>
                    <div className="chart">
                      <Bar
                          data={_.isEmpty(userData) ? barDataDefault : userData}
                          height={316}
                          options={optionBarChart}
                          legend={legendSquareOpts}
                      />
                    </div>
                  </div>
                </div>
              </div>

              {/* Revenue */}
              <div className="row-info">
                <div className="information">
                  <div className="row">
                    <div className="col-2">
                      <div className="title">
                      <span>
                        Revenue
                      </span>
                      </div>
                    </div>
                    <div className="col-10 text-right dropdown-group">

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Company:&nbsp;</span>
                          <span className="content">{chart.revenue.filters['companyId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'revenue'], 'companyId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(companyListDropdown) ? companyListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'revenue'], 'companyId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Service:&nbsp;</span>
                          <span className="content">{chart.revenue.filters['serviceId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'revenue'], 'serviceId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(serviceListDropdown) ? serviceListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'revenue'], 'serviceId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Suburb:&nbsp;</span>
                          <span className="content">{chart.revenue.filters['suburb'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'revenue'], 'suburb', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(suburbListDropdown) ? suburbListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'revenue'], 'suburb', {
                                                  value: item.suburb ? item.suburb : '',
                                                  label: item.suburb ? item.suburb : ''
                                                });
                                              }}>
                                  {item.suburb}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Period:&nbsp;</span>
                          <span className="content">{chart.revenue.filters['period'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'revenue', 'period'))}

                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>

                  </div>
                  <div className='row'>
                    <div className="chart">
                      <Bar
                          data={_.isEmpty(revenueData) ? barDataDefault : revenueData}
                          height={316}
                          options={optionBarStackedChart}
                          legend={legendSquareOpts}
                      />

                    </div>
                  </div>
                </div>
              </div>

              {/* Fixle Profit */}
              <div className="row-info">
                <div className="information">
                  <div className="row">
                    <div className="col-2">
                      <div className="title">
                      <span>
                        Fixle Profit
                      </span>
                      </div>
                    </div>
                    <div className="col-10 text-right dropdown-group">
                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Company:&nbsp;</span>
                          <span className="content">{chart.profit.filters['companyId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'profit'], 'companyId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(companyListDropdown) ? companyListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'profit'], 'companyId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Service:&nbsp;</span>
                          <span className="content">{chart.profit.filters['serviceId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'profit'], 'serviceId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(serviceListDropdown) ? serviceListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'profit'], 'serviceId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Suburb:&nbsp;</span>
                          <span className="content">{chart.profit.filters['suburb'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'profit'], 'suburb', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(suburbListDropdown) ? suburbListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'profit'], 'suburb', {
                                                  value: item.suburb ? item.suburb : '',
                                                  label: item.suburb ? item.suburb : ''
                                                });
                                              }}>
                                  {item.suburb}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Period:&nbsp;</span>
                          <span className="content">{chart.profit.filters['period'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'profit', 'period'))}

                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>

                  </div>
                  <div className='row'>
                    <div className="chart">
                      <Bar
                          data={_.isEmpty(profitData) ? barDataDefault : profitData}
                          height={316}
                          options={optionBarStackedChart}
                          legend={legendSquareOpts}
                      />

                    </div>
                  </div>
                </div>
              </div>

              {/* Jobs */}
              <div className="row-info">
                <div className="information">
                  <div className="row">
                    <div className="col-2">
                      <div className="title">
                      <span>
                        Jobs
                      </span>
                      </div>
                    </div>
                    <div className="col-10 text-right dropdown-group">

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Company:&nbsp;</span>
                          <span className="content">{chart.job.filters['companyId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'job'], 'companyId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(companyListDropdown) ? companyListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'job'], 'companyId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Service:&nbsp;</span>
                          <span className="content">{chart.job.filters['serviceId'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'job'], 'serviceId', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(serviceListDropdown) ? serviceListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'job'], 'serviceId', {
                                                  value: item._id ? item._id : '',
                                                  label: item.name ? item.name : ''
                                                });
                                              }}>
                                  {item.name}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Suburb:&nbsp;</span>
                          <span className="content">{chart.job.filters['suburb'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>
                            <DropdownItem key={uuidv1()}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.handleChangeFilterChart(['chart', 'job'], 'suburb', {
                                              value: '',
                                              label: 'All'
                                            });
                                          }}>All</DropdownItem>

                            {_.isArray(suburbListDropdown) ? suburbListDropdown.map((item) => (
                                <DropdownItem key={uuidv1()}
                                              onClick={(e) => {
                                                e.preventDefault();
                                                this.handleChangeFilterChart(['chart', 'job'], 'suburb', {
                                                  value: item.suburb ? item.suburb : '',
                                                  label: item.suburb ? item.suburb : ''
                                                });
                                              }}>
                                  {item.suburb}
                                </DropdownItem>
                            )) : []}
                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Period:&nbsp;</span>
                          <span className="content">{chart.job.filters['period'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>

                            {!_.isUndefined(currentPeriod) && (this.renderPeriod(currentPeriod, 'job', 'period'))}

                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>

                  </div>
                  <div className='row'>
                    <div className="chart">
                      <Line
                          data={_.isEmpty(jobData) ? barDataDefault : jobData}
                          height={316}
                          options={optionLineChart}
                          legend={legendSquareOpts}
                      />

                    </div>
                  </div>
                </div>
              </div>

              {/* Appointments Created */}
              <div className="row-info">
                <div className="information">
                  <div className="row">
                    <div className="col-3">
                      <div className="title">
                      <span>
                        Appointments Created
                      </span>
                      </div>
                    </div>
                    <div className="col-9 text-right dropdown-group">

                      <UncontrolledDropdown className="period-dropdown" key={uuidv1()}>
                        <DropdownToggle>
                          <span className="title">Time:&nbsp;</span>
                          <span className="content">{chart.appoinmentTime.filters['showWithType'].label}</span>
                          <i className="fixle-caret icon-triangle-down"></i>
                        </DropdownToggle>
                        <DropdownMenu>
                          <Scrollbars
                              // This will activate auto hide
                              autoHide
                              // Hide delay in ms
                              autoHideTimeout={1000}
                              autoHeight
                              autoHeightMin={0}
                              autoHeightMax={300}>

                            {showWithType.map(item => (
                                <DropdownItem key={uuidv1()} onClick={() => {
                                  this.handleChangeFilterChart(['chart', 'appoinmentTime'], 'showWithType', {
                                    value: item ? item.value : '',
                                    label: item ? item.label : ''
                                  });
                                }}>{item.label}</DropdownItem>
                            ))}

                          </Scrollbars>
                        </DropdownMenu>
                      </UncontrolledDropdown>

                    </div>

                  </div>
                  <div className='row'>
                    <div className="chart">
                      <Line
                          data={_.isEmpty(appoinmentTimeData) ? barDataDefault : appoinmentTimeData}
                          height={316}
                          options={optionLineChart}
                          legend={{ display: false }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>);
  }
}

StatisticsPage.propTypes = {
  dispatch: PropTypes.func,
  getAllDataStatistic: PropTypes.func,
  updateFilterChart: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  statisticspage: makeSelectStatisticsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    getAllDataStatistic: () => {
      dispatch(Actions.getAllDataStatistic());
    },
    getDataChartByType: (data) => {
      dispatch(Actions.getCharByType(data.key, data.typeChart, data.searchData));
    },
    updateFilterChart: (position, value) => {
      dispatch(Actions.updateFilterChart(position, value));
    },
    resetStore: () => {
      dispatch(Actions.resetStore());
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "statisticsPage", reducer });
const withSaga = injectSaga({ key: "statisticsPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(StatisticsPage);
