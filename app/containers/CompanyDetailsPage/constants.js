/*
 *
 * CompanyDetailsPage constants
 *
 */
export const RESET_STATE = "/CompanyDetailsPage/RESET_STATE";
export const DEFAULT_ACTION = "app/CompanyDetailsPage/DEFAULT_ACTION";
export const GET_COMPANY_DATA = "/CompanyDetailsPage/GET_COMPANY_DATA";
export const GET_SUCCESS = "/CompanyDetailsPage/GET_SUCCESS";
export const GET_ERROR = "/CompanyDetailsPage/GET_ERROR";
export const SET_ACTIVE_COMPANY = "/CompanyDetailsPage/SET_ACTIVE_COMPANY";
export const SET_REMOVE_COMPANY = "/CompanyDetailsPage/SET_REMOVE_COMPANY";
export const GET_EMPLOYEE_DATA = "/CompanyDetailsPage/GET_EMPLOYEE_DATA";
export const TOGGLE_TOP_PRIORITY = "/CompanyDetailsPage/TOGGLE_TOP_PRIORITY";
export const SET_TOP_PRIORITY = "/CompanyDetailsPage/SET_TOP_PRIORITY";
export const CREATE_NOTE = "/CompanyDetailsPage/CREATE_NOTE";
export const CHANGE_NOTE = "/CompanyDetailsPage/CHANGE_NOTE";
export const DELETE_NOTE = "/CompanyDetailsPage/DELETE_NOTE";
//COMMON
export const UPDATE_PAGE = "/CompanyDetailsPage/UPDATE_PAGE";
export const UPDATE_TAB = "/CompanyDetailsPage/UPDATE_TAB";
export const UPDATE_SEARCH_DATA = "/CompanyDetailsPage/UPDATE_SEARCH_DATA";
export const CHANGE_STORE_VALUE = "/CompanyDetailsPage/CHANGE_STORE_VALUE";
export const CHOOSE_ITEM = "/CompanyDetailsPage/CHOOSE_ITEM";
//EMPLOYEE
export const GET_EMPLOYEE_SUCCESS = "/CompanyDetailsPage/GET_EMPLOYEE_SUCCESS";
export const SET_ACTIVE_EMPLOYEE = "/CompanyDetailsPage/SET_ACTIVE_EMPLOYEE";
export const DELETE_EMPLOYEE = "/CompanyDetailsPage/DELETE_EMPLOYEE";
export const APPROVE_COMPANY = "/CompanyDetailsPage/APPROVE_COMPANY";
export const UPDATE_SEARCH_EMPLOYEE = "/CompanyDetailsPage/UPDATE_SEARCH_EMPLOYEE";
export const SELECT_EMPLOYEE = "/CompanyDetailsPage/SELECT_EMPLOYEE";
export const SELECT_ALL_EMPLOYEE = "/CompanyDetailsPage/SELECT_ALL_EMPLOYEE";
export const GET_PERIOD_EMPLOYEE = "/CompanyDetailsPage/GET_PERIOD_EMPLOYEE";
export const GET_PERIOD_EMPLOYEE_SUCCESS = "/CompanyDetailsPage/GET_PERIOD_EMPLOYEE_SUCCESS";
export const UPDATE_PERIOD_EMPLOYEE = "/CompanyDetailsPage/UPDATE_PERIOD_EMPLOYEE";
//JOB
export const GET_JOB_DATA = "/CompanyDetailsPage/GET_JOB_DATA";
export const GET_JOB_SUCCESS = "/CompanyDetailsPage/GET_JOB_SUCCESS";
export const UPDATE_SEARCH_JOB = "/CompanyDetailsPage/UPDATE_SEARCH_JOB";
//EARNING
export const GET_EARNING_COMPANY = "/CompanyDetailsPage/GET_EARNING_COMPANY";
export const GET_EARNING_COMPANY_SUCCESS = "/CompanyDetailsPage/GET_EARNING_COMPANY_SUCCESS";
export const GET_EARNING_COMPANY_ERROR = "/CompanyDetailsPage/GET_EARNING_COMPANY_ERROR";
//ACTIVITIES
export const GET_ACTIVITIES_COMPANY = "/CompanyDetailsPage/GET_ACTIVITIES_COMPANY";
export const GET_ACTIVITIES_COMPANY_SUCCESS = "/CompanyDetailsPage/GET_ACTIVITIES_COMPANY_SUCCESS";