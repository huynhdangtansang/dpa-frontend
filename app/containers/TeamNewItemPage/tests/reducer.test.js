import { fromJS } from 'immutable';
import teamNewItemPageReducer from '../reducer';

describe('teamNewItemPageReducer', () => {
  it('returns the initial state', () => {
    expect(teamNewItemPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
