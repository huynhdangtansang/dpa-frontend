/**
 *
 * Asynchronously loads the component for EditCompanyPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
