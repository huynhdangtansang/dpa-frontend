/**
 *
 * CompanyTabEarning
 *
 */
import React from 'react';
//Library
import moment from 'moment';
import debounce from 'components/Debounce';
import classnames from 'classnames';
import _ from "lodash";
// Combo import table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

// -------------------
class CompanyTabEarning extends React.PureComponent {
  dateFormat = (cell) => {
    return (
      <div className="date">
        {moment(cell).format('h:mm A, ddd DD MMM YYYY')}
      </div>
    )
  };
  amountFormat = (cell, row) => {
    const { FORMATCURRENCY }
      = this.props.companydetailspage;
    return (
      <div className="amount">
        {(row.type === 'payout' ? '-' : '') + (!_.isUndefined(row.balance && row.balance) ? row.balance.unit : '$')}
        {FORMATCURRENCY.format(!_.isUndefined(row.balance && row.balance) ? row.balance.value : 0)}
      </div>
    )
  };
  descriptionFormat = (cell, row) => {
    let strFormat = '';
    switch (row.type) {
      case 'payment':
        strFormat = 'Payment - ' + (!_.isUndefined(row.jobId && row.jobId.name) ? row.jobId.name : '') + ' #' + (!_.isUndefined(row.jobId) ? row.jobId.jobId : '');
        break;
      case 'payout':
        strFormat = 'Payout request ' + ' #' + (!_.isUndefined(row.payoutId && row.payoutId) ? row.payoutId : '');
        break;
      default:
        return 'Data payment not correct';
    }
    return strFormat;
  };
  statusFormat = (cell) => {
    return <div className="content active">{cell}</div>
  };
  sortData = (field, order) => {
    const { searchDataForJob, companyData } = this.props.companydetailspage;
    let newSearchData = searchDataForJob;
    newSearchData.sort = field;
    newSearchData.sortType = order;
    this.props.updatePage('currentPageJob', 1);
    this.props.updateSearchData('searchDataForJob', newSearchData);
    this.props.getJobCompany(companyData._id, newSearchData);
    //this.getListAfterChangeData('job');
  };
  //-----------------------PAGINATION-----------------------
  changePagination = (type) => {
    const { jobData, searchDataForJob, currentPageJob } = this.props.companydetailspage;
    const limit = jobData.pagination.limit;
    let newSearchData = searchDataForJob;
    let newPage, newOffset;
    if (type === 'prev') {
      newPage = currentPageJob - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    } else {//next
      newPage = currentPageJob + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    }
    this.props.updatePage('currentPageJob', newPage);
    this.props.updateSearchJob('offset', newOffset);
    //this.setState(state);
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { companyData, searchDataForJob } = this.props.companydetailspage;
    this.props.getJobCompany(companyData._id, searchDataForJob);
  }, 1000);

  constructor(props) {
    super(props);
  }

  //-------------------------------------------------------
  render() {
    const { earningData, FORMATCURRENCY }
      = this.props.companydetailspage;
    
    const options = {
      withoutNoDataText: true,
      //defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    return (
      <div className="earning-tab">
        <div className={classnames("content content-list-page")}>
          <div className="information earning-balance">
            <div className="row">
              <div className="col-6 left">
                <div className="title">
                  <span>Earning Balance</span>
                </div>
              </div>
              <div className="col-6 right">
                {!_.isEmpty(earningData) && earningData.earning ?
                  (<div
                    className="value">{earningData.earning.unit}{FORMATCURRENCY.format(earningData.earning.value)}</div>)
                  :
                  (<div className="value">${FORMATCURRENCY.format(0)}</div>)
                }
              </div>
            </div>
          </div>
          <div className="information earning-history">
            <div className="title">
              <span>Earning History</span>
            </div>

            <BootstrapTable
              data={!_.isEmpty(earningData) ? earningData.data : []}
              options={options}
              bordered={false}
              containerClass='table-fixle'
              tableHeaderClass='table-fixle-header'
              tableBodyClass='table-fixle-content'>
              <TableHeaderColumn dataField="_id" isKey hidden>Payment ID</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="description" columnClassName="description"
                dataFormat={this.descriptionFormat}>Description</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="status" dataAlign={'center'} columnClassName="status" headerAlign={'left'}
                dataFormat={this.statusFormat} width={106 / 14 + 'rem'}>Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="amount" dataFormat={this.amountFormat} width={92 / 14 + 'rem'}>Amount</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="createdAt" dataFormat={this.dateFormat}
                width={215 / 14 + 'rem'}>Date</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
      </div>
    );
  }
}

CompanyTabEarning.propTypes = {};
export default CompanyTabEarning;
