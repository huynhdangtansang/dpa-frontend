import React from "react";
import NumberFormat from 'react-number-format';
import _ from "lodash";
import ChargeItem from 'components/ChargeItem';
import EstimateConfirmModal from 'components/EstimateConfirmModal';
import './style.scss';

export default class EstimateForm extends React.Component {
  emptyCharge = () => {
    return {
      value: 0,
      unit: '$',
      active: false
    }
  };
  reCalculate = (data) => {
    let temp = 0;
    for (let key in data) {
      let chargeItem = data[key];
      if (chargeItem.active === true) {
        if (key === 'deposit') {
          temp -= chargeItem.value;
        } else {
          if (key !== 'coupon' && key !== 'gst') {//not plus coupon and gst value
            temp += chargeItem.value
          }
        }
      }
    }
    if (!_.isUndefined(data['gst']) && data['gst'].active === true) {
      data.gst.value = temp * (this.gstPercent() / 100);
      temp += data.gst.value;//add gst value
    }
    //temp in this time minus coupon
    this.setState({
      total: temp > 0 ? temp : 0
    });
    temp -= (!_.isUndefined(data['coupon']) && data['coupon'].active === true) ? data['coupon'].value : 0;//minus coupon value
    temp -= this.getValue('deposit');//minus deposit value
    this.setState({
      totalDisplay: temp > 0 ? temp : 0,
      estimateData: data
    });
  };
  handleChangeValue = (type, newValue) => {
    let data = this.state.estimateData;
    data[type].value = !_.isUndefined(newValue) ? newValue : 0;
    data[type].active = true;
    this.setState({ estimateData: data }, () => {
      this.reCalculate(data);
    });
  };
  handleChangePercentValue = (type, newValue) => {
    let value = !_.isUndefined(newValue) ? newValue * (this.state.total) / 100 : 0;
    this.handleChangeValue(type, value);
  };
  handleChangeStatus = (type, newValue) => {
    let data = this.state.estimateData;
    data[type].active = _.isBoolean(newValue) ? newValue : false;
    this.setState({ estimateData: data }, () => {
      this.reCalculate(data);
    });
  };
  getValue = (chargeType) => {
    let estimate = this.state.estimateData;
    return (estimate[chargeType] && estimate[chargeType].value) ? estimate[chargeType].value : 0;
  };
  getStatus = (chargeType) => {
    let estimate = this.state.estimateData;
    return (_.isObject(estimate[chargeType]) && _.isBoolean(estimate[chargeType].active)) ? estimate[chargeType].active : true;
  };
  submitEstimate = () => {
    let data = this.state.estimateData;
    data['total'] = this.state.total;
    this.props.onSubmit(data);
  };
  gstPercent = () => {
    let data = this.state.estimateData;
    return _.isObject(data['gst']) ? data['gst'].percent : 0
  };

  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      totalDisplay: 0,
      estimateData: {},
      showEstimateConfirm: false
    };
  }

  componentDidMount() {
    const data = this.props.data;
    let initData = {
      minCharge: (data && data.minCharge) ? data.minCharge : this.emptyCharge(),
      insuranceCover: (data && data.insuranceCover) ? data.insuranceCover : this.emptyCharge(),
      afterHourFee: (data && data.afterHourFee) ? data.afterHourFee : this.emptyCharge(),
      additionalHours: (data && data.additionalHours) ? data.additionalHours : this.emptyCharge(),
      additionalParts: (data && data.additionalParts) ? data.additionalParts : this.emptyCharge(),
      coupon: (data && data.coupon) ? data.coupon : this.emptyCharge(),
      gst: (data && data.gst) ? data.gst : this.emptyCharge(),
      deposit: (data && data.deposit) ? data.deposit : this.emptyCharge()
    };
    this.setState({
      total: data.total ? data.total : 0,
      estimateData: initData
    });
  }

  render() {
    return (
        <div className="estimate-content not-confirm">
          <div className="section">
            <div className="title">Estimate</div>
            <div className="modify-items">
              <ChargeItem
                  label={'Minimum charge'}
                  disabled={true}
                  type={'single'}
                  value={this.getValue('minCharge')}
                  status={this.getStatus('minCharge')}
                  onChange={value => {
                    this.handleChangeValue('minCharge', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('minCharge', value);
                  }}
              />
              <ChargeItem
                  label={'Insurance Cover'}
                  disabled={true}
                  type={'single'}
                  value={this.getValue('insuranceCover')}
                  status={this.getStatus('insuranceCover')}
                  onChange={value => {
                    this.handleChangeValue('insuranceCover', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('insuranceCover', value);
                  }}
              />
              <ChargeItem
                  key={'After'}
                  label={'After Hour Fee'}
                  type={'single'}
                  value={this.getValue('afterHourFee')}
                  status={this.getStatus('afterHourFee')}
                  onChange={(value) => {
                    this.handleChangeValue('afterHourFee', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('afterHourFee', value);
                  }}
              />
              <ChargeItem
                  key={'hours'}
                  label={'Additional hours'}
                  type={'single'}
                  value={this.getValue('additionalHours')}
                  status={this.getStatus('additionalHours')}
                  onChange={(value) => {
                    this.handleChangeValue('additionalHours', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('additionalHours', value);
                  }}
              />
              <ChargeItem
                  key={'parts'}
                  label={'Additional parts'}
                  type={'single'}
                  value={this.getValue('additionalParts')}
                  status={this.getStatus('additionalParts')}
                  onChange={(value) => {
                    this.handleChangeValue('additionalParts', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('additionalParts', value);
                  }}
              />
              <ChargeItem
                  label={'Coupon'}
                  type={'double'}
                  value={this.getValue('coupon')}
                  status={this.getStatus('coupon')}
                  total={this.state.total}
                  onChange={(value) => {
                    this.handleChangeValue('coupon', value);
                  }}
                  onChangePercent={(value) => {
                    this.handleChangePercentValue('coupon', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('coupon', value);
                  }}
              />
              <ChargeItem
                  label={`${this.gstPercent()}% GST`}
                  type={'single'}
                  disabled={true}
                  value={this.getValue('gst')}
                  status={this.getStatus('gst')}
                  onChange={(value) => {
                    this.handleChangeValue('gst', value);
                  }}
                  onSelect={value => {
                    this.handleChangeStatus('gst', value);
                  }}
              />
            </div>
          </div>
          <div className="section">
            <ChargeItem
                label={'Deposit'}
                disabled={true}
                type={'double'}
                value={this.getValue('deposit')}
                status={false}
                total={this.state.total}
                onChange={value => {
                  this.handleChangeValue('deposit', value);
                }}
                onSelect={value => {
                  this.handleChangeStatus('deposit', value);
                }}
            />
          </div>
          <div className="total">
            <div className="total-fee">
              <NumberFormat
                  value={this.state.totalDisplay}
                  className="price"
                  displayType={'text'}
                  prefix={'$'}
                  decimalScale={2}
                  fixedDecimalScale={true}
              />
              <span className="description">{this.getStatus('deposit') ? ' In Balance' : ' In Total'}</span>
            </div>
            <div className="submit">
              <button onClick={() => {
                this.setState({ showEstimateConfirm: true });
              }} disabled={this.props.disabled}><span className="icon-checkmark"></span>Submit
              </button>
            </div>
          </div>
          <EstimateConfirmModal
              show={this.state.showEstimateConfirm}
              charges={this.state.estimateData}
              total={this.state.totalDisplay}
              closeModal={() => {
                this.setState({ showEstimateConfirm: false });
              }}
              submit={() => {
                this.submitEstimate();
              }}/>
        </div>
    )
  }
}