/**
 *
 * PaymentList
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPaymentList from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import _, { debounce } from "lodash";
import * as Actions from './actions';
//lib
import { DropdownItem, DropdownMenu, Input, } from 'reactstrap';
import classnames from 'classnames';
import NumberFormat from 'react-number-format';
//Table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
import './style.scss';
import GhostButton from 'components/GhostButton';
import { urlLink } from 'helper/route';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import moment from 'moment';
import { getEvent } from 'helper/socketConnection';

/* eslint-disable react/prefer-stateless-function */
export class PaymentList extends React.PureComponent {
  handleSearchTextChange = debounce(value => {
    const { searchData } = this.props.paymentlist;
    let temp = searchData;
    temp.offset = 0;
    temp.search = value;
    this.props.changePage(1);
    this.props.updateSearchData(temp);
  }, 700);
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
    };
    this.props.select(isSelected, selectItem);
  };
  //Table Formatter
  nameFormat = (cell) => {
    return (
        <div className="name-company">
          <span>{!_.isUndefined(cell) ? cell.name : '-'}</span>
        </div>
    )
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  sortData = (field, order) => {
    const { searchData } = this.props.paymentlist;
    let temp = searchData;
    temp.sort = field;
    temp.sortType = order;
    this.props.changePage(1);
    this.props.updateSearchData(temp);
  };
  changePagination = (type) => {
    const { currentPage, searchData, pagination } = this.props.paymentlist;
    let newSearchData = searchData;
    let limit = pagination.limit;
    let newPage, newOffset;
    if (type === 'prev') {
      newPage = currentPage - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
      this.props.changePage(newPage);
    } else {
      newPage = currentPage + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
      this.props.changePage(newPage);
    }
    this.actionAfterChangePage(newSearchData);
  };
  actionAfterChangePage = debounce(searchData => {
    this.props.updateSearchData(searchData);
  }, 700);
  trClassFormat = () => {
    // return (_.isBoolean(row.isRead) && row.isRead === false) ? 'unread' : 'read';  // return class name.
    return 'read';
  };
  iconFormat = (cell, row) => {
    return (
        <img className='avatar-company'
             src={row.company ? row.company.logo.fileName : './default-user.png'}
             onError={(e) => {
               e.target.onerror = null;
               e.target.src = './default-user.png'
             }}/>
    )
  };
  actionFormat = (cell, row) => {
    const editMenu = (
        <DropdownMenu>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.viewPayment + '?id=' + row._id);
          }}>View</DropdownItem>
          <DropdownItem onClick={() => {
            this.props.getPdf([row._id]);
          }}>Print</DropdownItem>
          <DropdownItem onClick={() => {
            this.setState({
              chosenPayment: {
                id: row._id,
              }
            }, () => {
              this.openRemoveConfirm('single');
            })
          }}>Remove</DropdownItem>
        </DropdownMenu>
    );
    return (
        <ActionFormatter menu={editMenu}/>
    );
  };
  statusFormat = (cell) => {
    return (
        <div className="content active">{cell}</div>
    )
  };
  submissionFormat = (cell) => {
    return moment(cell).format('ddd D MMM YYYY');
  };
  amountFormat = (cell, row) => {
    return this.priceFormat(row && !_.isUndefined(row.balance) ? row.balance : { value: 0, unit: '$' });
  };
  priceFormat = (cell) => {
    return (
        <NumberFormat
            className={'price'}
            value={cell && !_.isUndefined(cell.value) ? cell.value : 0}
            className="price"
            displayType={'text'}
            prefix={cell && !_.isUndefined(cell.unit) ? cell.unit : '$'}
            decimalScale={2}
            fixedDecimalScale={cell.value || 0 > 0 ? true : false}
        />
    )
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeRemoveConfirm = () => {
    this.setState({ showRemoveConfirm: false });
  };
  onRowClick = (row) => {
    this.props.history.push(urlLink.viewPayment + '?id=' + row._id);
  };
  removePayment = () => {
    const { searchData } = this.props.paymentlist;
    if (this.state.modifyType === 'single') {
      let id = this.state.chosenPayment.id;
      let listIds = [];
      listIds.push(id);
      let data = {
        paymentIds: listIds,
        reason: this.state.reason
      };
      this.props.removePayment(data, searchData);
    } else {//multi
      let listIds = [];
      let { selectedPayments } = this.props.paymentlist;
      let list = selectedPayments;
      let i = 0;
      list.forEach((payment) => {
        listIds.push(payment.id);
        if (i === list.length - 1) {
          let data = {
            paymentIds: listIds,
            reason: this.state.reason
          };
          this.props.removePayment(data, searchData);
        } else {
          i++;
        }
      });
    }
    this.props.selectAll(false);//unselect all payment
    this.refs.table_payment.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  openRemoveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showRemoveConfirm: true,
      reason: ''
    });
  };

  constructor(props) {
    super(props);
    this.state = {
      chosenPayment: {},
      reason: '',
      modifyType: 'single',
      showRemoveConfirm: false
    };
    getEvent('notification:newPayout', () => {
      let url = window.location.hash;
      url = url.replace('#', '');
      if (url === urlLink.payments) {
        const { searchData } = this.props.paymentlist;
        this.props.getPaymentList(searchData);
      }
    });
  }

  componentDidMount() {
    const { searchData } = this.props.paymentlist;
    this.props.getPaymentList(searchData);
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  render() {
    const { paymentList, pagination, currentPage, selectedPayments } = this.props.paymentlist;
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll,
    };
    const options = {
      withoutNoDataText: true,
      defaultSortName: 'payoutId',
      defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData,
      onRowClick: this.onRowClick
    };
    return (
        <div className="payment-list">
          <div className="header-list-page payments-list-header">
            <div className="form-inline align-items-center">
              <div className="search-bar services-search">
                <Input placeholder="Search company"
                       onChange={(e) => {
                         this.handleSearchTextChange(e.target.value);
                       }}/>
                <span className="icon-search"></span>
              </div>

              <TotalFormatter
                  offset={pagination.offset}
                  limit={pagination.limit}
                  total={pagination.total}
                  page={currentPage}
                  changePagination={this.changePagination}
              />
            </div>
          </div>

          <div
              className={classnames("content content-list-page payment-management-content", selectedPayments.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_payment"
                data={paymentList}
                options={options}
                selectRow={selectRowProp}
                bordered={false}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'
                trClassName={this.trClassFormat}>
              <TableHeaderColumn dataField="_id" isKey hidden>Payment ID</TableHeaderColumn>
              <TableHeaderColumn dataField="company" dataFormat={this.iconFormat} width={46 / 14 + 'rem'}
                                 dataAlign='center' columnClassName="name"></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="company" width={'38%'} dataFormat={this.nameFormat}
                                 columnClassName="name">Company Name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="payoutId" width={'150px'} dataAlign={'center'}>Payout
                Id</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="status" width={'118px'} dataFormat={this.statusFormat}
                                 columnClassName="status">Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="createAt" dataFormat={this.submissionFormat}>Submission
                Date</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="amount" dataFormat={this.amountFormat}
                                 dataAlign={'center'}>Amount</TableHeaderColumn>
              <TableHeaderColumn dataField='data' dataFormat={this.actionFormat} columnClassName={'edit-dropdown'}
                                 dataAlign='right' width={84 / 14 + 'rem'}>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>

          {selectedPayments.length > 0 &&
          <div className="footer payments-list-footer">
            <GhostButton title={'Print'} className="btn-ghost btn-print"
                         onClick={(e) => {
                           e.preventDefault();
                           let array = selectedPayments.map((payment) => {
                             return payment.id;
                           });
                           this.props.getPdf(array);
                         }}/>
            <GhostButton title={'Remove'} className="btn-ghost btn-remove"
                         onClick={() => {
                           if (selectedPayments.length === 1) {
                             this.setState({
                               chosenPayment: {
                                 id: selectedPayments[0].id,
                               }
                             }, () => {
                               this.openRemoveConfirm('single');
                             })
                           } else {
                             this.openRemoveConfirm('multi');
                           }
                         }}/>
          </div>
          }

          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'payment'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removePayment}/>
        </div>
    );
  }
}

PaymentList.propTypes = {
  dispatch: PropTypes.func,
  getPaymentList: PropTypes.func,
  updateSearchData: PropTypes.func,
  changePage: PropTypes.func,
  select: PropTypes.func,
  selectAll: PropTypes.func,
  removePayment: PropTypes.func,
  getPdf: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  paymentlist: makeSelectPaymentList()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(Actions.resetState());
    },
    getPaymentList: (searchData) => {
      dispatch(Actions.getList(searchData));
    },
    updateSearchData: (newSearchData) => {
      dispatch(Actions.updateSearchData(newSearchData));
    },
    changePage: (newPage) => {
      dispatch(Actions.changePage(newPage));
    },
    select: (isSelected, selectItem) => {
      dispatch(Actions.selectPayment(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(Actions.selectAllPayment(isSelected));
    },
    removePayment: (deleteData, searchData) => {
      dispatch(Actions.removePayment(deleteData, searchData));
    },
    getPdf: (paymentId) => {
      dispatch(Actions.getPdf(paymentId));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "paymentList", reducer });
const withSaga = injectSaga({ key: "paymentList", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(PaymentList);
