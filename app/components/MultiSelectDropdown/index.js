import React from "react";
import { DropdownItem, DropdownMenu, DropdownToggle, Input, UncontrolledDropdown } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import ClassNames from 'classnames';
import _, { findIndex } from "lodash";
import "./style.scss";
//Lib
import uuidv1 from 'uuid';
import CheckBoxSquare from 'components/CheckBoxSquare';

export default class MultiSelectDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      searchText: '',
    }
  }

  toggle = () => {
    let status = this.state.isOpen;
    this.setState({ isOpen: !status });
  };
  statusContent = (listOption) => {
    let lengthSelect = listOption.length - (_.isUndefined(this.props.value) ? 0 : this.props.value.length);
    if (lengthSelect === 0 || lengthSelect === listOption.length) {
      return 'All';
    } else {
      if (this.props.value.length === 1) {
        return this.props.value[0].label;
      } else {
        return this.props.value.length + ' items selected';
      }
    }
  };
  handleScrollBottom = () => {
    const { scrollbars } = this.refs;
    const isBottomScroll = scrollbars.getValues().top;
    //Scroll is in bottom
    if (isBottomScroll >= 1) {
      //start load next page in dropdown company
      if (_.isBoolean(this.props.searchOption) && this.props.searchOption) {
        this.props.handleScrollBottom();
      }
    } else {
      //start load prev page in dropdown company
    }
  };

  render() {
    return (
        <UncontrolledDropdown className="multi-selection">
          <DropdownToggle>
            <span className="title">{this.props.title}:&nbsp;</span>
            <span className="content">{this.statusContent(this.props.options)}</span>
            <span className="fixle-caret icon-triangle-down"></span>
          </DropdownToggle>
          <DropdownMenu>
            <Scrollbars
                ref="scrollbars"
                // This will activate auto hide
                autoHide
                // Hide delay in ms
                autoHideTimeout={1000}
                autoHeight
                autoHeightMin={0}
                autoHeightMax={456}
                onScroll={() => {
                  this.handleScrollBottom();
                }}>
              {_.isBoolean(this.props.searchOption) && this.props.searchOption && (
                  <DropdownItem key={uuidv1()} toggle={false} header>
                    <div className="search-bar-dropdown">
                      <Input placeholder="Search"
                             autoFocus
                             value={this.state.searchText}
                             onChange={(e) => {
                               this.setState({ searchText: e.target.value });
                               this.props.handleSearchChange(e.target.value);
                             }}
                      />
                      <span className="icon-search"></span>
                    </div>
                  </DropdownItem>
              )}

              {this.props.options && this.props.options.map((option, index) => {
                return (
                    <DropdownItem
                        key={index}
                        toggle={false}
                        className={ClassNames(findIndex(this.props.value, item => item.value === option.value) !== -1 ? 'selected' : '')}
                        onClick={(e) => {
                          //important not remove this
                          e.preventDefault();
                          //-------------------------
                          this.props.onSelect(option);
                        }}>
                      <CheckBoxSquare
                          value={findIndex(this.props.value, item => item.value === option.value) !== -1}
                          onChange={() => {
                          }}
                      />
                      <div className="label">{option.label}</div>
                    </DropdownItem>
                );
              })}
            </Scrollbars>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
}
