import { fromJS } from 'immutable';
import serviceNewItemPageReducer from '../reducer';

describe('serviceNewItemPageReducer', () => {
  it('returns the initial state', () => {
    expect(serviceNewItemPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
