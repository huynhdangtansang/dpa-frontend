/*
 *
 * BotMessagesPages constants
 *
 */
export const DEFAULT_ACTION = "app/BotMessagesPages/DEFAULT_ACTION";
export const GET_MESSAGE_LIST = "app/BotMessagesPages/DEFAULT_ACTION";
export const GET_MESSAGE_LIST_SUCCESS = "app/BotMessagesPages/GET_MESSAGE_LIST_SUCCESS";