import React from "react";
import NumberFormat from 'react-number-format';
import _ from "lodash";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledButtonDropdown, } from 'reactstrap';
import "./style.scss";

export default class ChargeItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: true
    };
  }

  render() {
    const dropdownOptions = [
      {
        label: 'Active',
        value: true
      },
      {
        label: 'Inactive',
        value: false
      }
    ];
    const singleInput = (
        <NumberFormat
            value={this.props.value}
            disabled={this.props.disabled}
            prefix={'$'}
            decimalScale={2}
            fixedDecimalScale={this.props.value > 0 ? true : false}
            onValueChange={(values) => {
              this.props.onChange(values.floatValue);
            }}
        />
    );
    const doubleInput = (
        <div className="double-input">
          <NumberFormat
              className="price"
              value={this.props.value}
              disabled={this.props.disabled}
              prefix={'$'}
              decimalScale={2}
              fixedDecimalScale={this.props.value > 0 ? true : false}
              onValueChange={(values) => {
                this.props.onChange(values.floatValue);
              }}
          />
          <NumberFormat
              className="percentage"
              value={(this.props.value / this.props.total) * 100}
              disabled={this.props.disabled}
              suffix={'%'}
              decimalScale={2}
              fixedDecimalScale={this.props.value > 0 ? true : false}
              onValueChange={(values) => {
                //console.log('auto change from price');
                if (_.isFunction(this.props.onChangePercent))
                  this.props.onChangePercent(values.floatValue);
              }}
          />
        </div>
    );
    return (
        <div className="charge-item">
          <div className="label">
            <span>{this.props.label}</span>
          </div>
          <div className="details">
            <div className="part part-left">
              {this.props.type === 'single' ? singleInput : doubleInput}
            </div>
            <div className="part part-right">
              <UncontrolledButtonDropdown className="estimate-dropdown">
                <DropdownToggle caret={true} disabled={this.props.disabled}>
                  {this.props.status ? 'Active' : 'Inactive'}
                </DropdownToggle>
                <DropdownMenu>
                  {dropdownOptions.map((option, index) => {
                    return (
                        <DropdownItem key={index} onClick={(e) => {
                          e.preventDefault();
                          this.props.onSelect(option.value);
                        }}>
                          {option.label}
                        </DropdownItem>
                    )
                  })}
                </DropdownMenu>
              </UncontrolledButtonDropdown>
            </div>
          </div>
        </div>
    )
  }
}