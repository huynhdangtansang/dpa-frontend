/**
 *
 * ForgotPasswordPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectErrors, makeSelectForgotPasswordPage, makeSelectStatus } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import FormGroup from "components/FormGroup";
import { Formik } from "formik";
import InputForm from "components/InputForm";
import * as Yup from "yup";
import PageInfo from "components/PageInfo";
import SubmitButton from "components/SubmitButton";
import { sendEmail, setIsSent } from "./actions";
import { urlLink } from 'helper/route';
import './style.scss';
//lib
import _ from "lodash";

const validateForm = Yup.object().shape({
  'email': Yup.string()
      .email("Invalid Email")
  //.required('Please enter your E-mail'),
});

/* eslint-disable react/prefer-stateless-function */
export class ForgotPasswordPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReset: false,
    };
    this._disableButton = this._disableButton.bind(this);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'email',
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    // let { isSent } = this.props.isSent;
    return (
        <FormGroup footer={'need to login?'} link={' login'} onClick={() => {
          this.props.setIsSent();
          this.props.history.push(urlLink.login);
        }}>
          <PageInfo title={'Forgot Password'}
                    isReset={this.props.isSent}
                    content={'Please provide us your e-mail address and we will send you a link where you can choose a new password.'}
                    alternative={'Password reset sent! We have just emailed you instructions on how to reset your password.'}/>

          {!this.props.isSent && (
              <Formik
                  ref={ref => (this.formik = ref)}
                  initialValues={{ email: '' }}
                  enableReinitialize={true}
                  validationSchema={validateForm}
                  onSubmit={(evt) => {
                    this.props.onSubmit(evt);
                  }}
              >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    /* and other goodies */
                  }) => (
                    <div className="forgot-password-page">
                      <form onSubmit={handleSubmit}>
                        <InputForm label={'email'}
                                   name={'email'}
                                   type={'email'}
                                   error={errors.email}
                                   value={values.email}
                                   touched={touched.email}
                                   onChange={evt => {
                                     handleChange(evt);
                                   }}
                                   onBlur={handleBlur}
                                   placeholder={'you@example.com'}/>
                        <SubmitButton type={"submit"}
                                      disabled={this._disableButton(values, errors)}
                                      className={'btn-submit'} content={'submit'}/>

                        {(this.props.errors && this.props.errors.length > 0) ? this.props.errors.map((error) => {
                          return (
                              <div className="errors">
                                <span className="icon-error"></span>
                                <div key={error.errorCode} className="error-item">
                                  <span>{error.errorMessage}</span>
                                </div>
                              </div>
                          );
                        }) : []}

                      </form>
                    </div>
                )}
              </Formik>
          )}
        </FormGroup>
    );
  }
}

ForgotPasswordPage.propTypes = {
  dispatch: PropTypes.func,
  onSubmit: PropTypes.func,
  setIsSent: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  forgotpasswordpage: makeSelectForgotPasswordPage(),
  errors: makeSelectErrors(),
  isSent: makeSelectStatus()
});

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: evt => {
      if (!_.isUndefined(evt) && !_.isUndefined(evt.email)) {
        dispatch(sendEmail(evt.email));
      }
    },
    setIsSent: () => {
      dispatch(setIsSent());
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "forgotPasswordPage", reducer });
const withSaga = injectSaga({ key: "forgotPasswordPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ForgotPasswordPage);
