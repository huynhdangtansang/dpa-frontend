/**
 *
 * ServiceDetailsDataView
 *
 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import moment from 'moment';
import _ from "lodash";
import PurpleRoundButton from 'components/PurpleRoundButton';

function ServiceDetailsDataView(props) {
  return (
      <div className='content service-details-content'>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-8 left-part">
              <div className="information service-info">
                <div className="row details-wrapper">
                  <div className="col-md-4 left">
                    <div className="title">
                      <span>Service information</span>
                    </div>
                  </div>
                  <div className="col-md-5 right">
                    <div className="avatar-image service-icon">
                      {props.data.icon ? <img src={props.data.icon.fileName} className="icon-wrapper" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}></img> :
                          <img src='./default-user.png' className="icon-wrapper"></img>}
                    </div>
                    <div className="details">
                      <div className="form-input">
                        <div className="form-label">service name</div>
                        <span className="text-span">{props.data.name}</span>
                      </div>
                      <div className="form-input">
                        <div className="form-label">Hour rate</div>
                        {props.data.rateHours && props.data.rateHours.unit && (
                            <span className="text-span">{props.data.rateHours.unit}{new Intl.NumberFormat('en-IN', {
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2
                            }).format(props.data.rateHours.value)}</span>
                        )}

                      </div>
                      <div className="form-input">
                        <div className="form-label">minimum hours</div>
                        {props.data.minHours && (<span className="text-span">{props.data.minHours.value}</span>)}
                      </div>
                      <div className="form-input">
                        <div className="form-label">minimum charge</div>
                        {props.data.minCharge && (<span
                            className="text-span">{props.data.minCharge.unit}{new Intl.NumberFormat('en-IN', {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2
                        }).format(props.data.minCharge.value)}</span>)}
                      </div>
                    </div>
                    <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                    <span className='icon-edit' onClick={() => {
                      props.goToEdit('info');
                    }}></span>
                    </div>
                  </div>
                  <div className='col-md-3 service-background'>
                    <div className="title">
                      <span>Background Image</span>
                    </div>
                    <div className="background-container">
                      {props.data.background ?
                          <img className="background-uploaded" src={props.data.background.fileName} onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }}></img> : <img className="background-uploaded" src='./default-user.png'></img>}
                    </div>
                    <div className="upload-background" onClick={() => {
                      props.goToEdit('info');
                    }}>
                      <span>Change Bg Image</span>
                    </div>
                  </div>
                </div>

              </div>

              <div className='information fixle-charge'>
                <div className='row'>
                  <div className="col-md-4 left">
                    <div className="title">
                      <span>Fixle charge</span>
                    </div>
                  </div>
                  <div className="col-md-8 right">

                    <div className="details">
                      <div className="form-input">
                        <div className="form-label">Commission per booking</div>
                        {props.data.commission && (
                            <span
                                className='text-span'>{props.data.commission.value}{props.data.commission.unit}</span>)}
                      </div>

                    </div>
                  </div>
                  <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                  <span className='icon-edit' onClick={() => {
                    props.goToEdit('charge');
                  }}></span>
                  </div>
                </div>
              </div>
              <div className="information list-add-item">
                <div className='row'>
                  <div className="col-md-4 left">
                    <div className="title">
                      <span>Coupon code</span>
                    </div>
                  </div>
                  {props.data.couponCodes && (<div className="col-md-8 right">
                        {props.data.couponCodes.map((item, i) => (
                            <div className='coupon' key={i}>
                              <div className='label-coupon'>{item.name}</div>
                              <div className='item-add'>
                                <div className='form-input'>
                                  <div className='form-label'>Coupon code</div>
                                  <span className='text-span'>{item.code}</span>
                                </div>
                                <div className='form-input'>
                                  <div className='form-label'>Discount</div>
                                  <span className='text-span'>{item.disCount}{item.disCountType}</span>
                                </div>
                                <div className='form-input'>
                                  <div className='form-label'>Expiry date</div>
                                  <span className='text-span'>
                            {moment(item.expiredAt).format('ddd D MMM YYYY')}
                          </span>
                                </div>
                              </div>
                            </div>
                        ))}

                        <div className='addition-action'>
                          <PurpleRoundButton title={'Add Coupon'} className="btn-purple-round add-coupon"
                                             onClick={() => {
                                               props.goToEdit('coupon', 'add');
                                             }}/>
                        </div>
                      </div>
                  )}
                  <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                  <span className='icon-edit' onClick={() => {
                    props.goToEdit('coupon');
                  }}></span>
                  </div>
                </div>
              </div>

              {props.data.keywords && (<div className="information keyword">
                <div className='row'>
                  <div className="col-md-4 left">
                    <div className="title">
                      <span>Keywords</span>
                    </div>
                  </div>
                  <div className="col-md-8 right">
                    <div className='list-keywords'>
                      {props.data.keywords.map((item, i) => (<span className='item' key={i}>{item.keyName}</span>))}
                    </div>
                  </div>
                </div>
                <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                <span className='icon-edit' onClick={() => {
                  props.goToEdit('keyword');
                }}></span>
                </div>
              </div>)}

              {props.data.certifications && (
                  <div className="information certifications">
                    <div className='row'>
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Certifications required</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        {props.data.certifications.map((item, i) => (
                            <div className='item-add' key={i}>
                              <div className='form-input'>
                                <span className='text-span'>{item.content}</span>
                              </div>
                            </div>
                        ))}
                        <div className='addition-action'>
                          <PurpleRoundButton title={'Add Certification'} className="btn-purple-round add-coupon"
                                             onClick={() => {
                                               props.goToEdit('cert', 'add');
                                             }}/>
                        </div>
                      </div>
                      <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                    <span className='icon-edit' onClick={() => {
                      props.goToEdit('cert');
                    }}></span>
                      </div>
                    </div>
                  </div>)}

              {props.data.areas && (<div className="information areas">
                <div className='row'>
                  <div className="col-md-4 left">
                    <div className="title">
                      <span>Areas supplied</span>
                    </div>
                  </div>
                  <div className="col-md-8 right">
                    {props.data.areas.map((item, i) => {
                      // let suburbs = item.suburbs.map((surburb, index) => {
                      //   return surburb + ',';
                      // });
                      let suburbs = item.suburbs.join(', ');
                      return (
                          <div className='item-add' key={i}>
                            <div className='form-input'>
                              <div className='form-label'>city and country</div>
                              <span className='text-span'>{item.country}</span>
                            </div>
                            <div className='form-input'>
                              <div className='form-label'>suburbs</div>
                              <span className='text-span'>{suburbs}</span>
                            </div>
                          </div>
                      );
                    })}
                    <div className='addition-action'>
                      <PurpleRoundButton title={'Add Area'} className="btn-purple-round add-coupon" onClick={() => {
                        props.goToEdit('area', 'add');
                      }}/>
                    </div>
                  </div>
                  <div className='icon-contain' hidden={_.isEmpty(props.data)}>
                  <span className='icon-edit' onClick={() => {
                    props.goToEdit('area');
                  }}></span>
                  </div>
                </div>
              </div>)}
            </div>
            <div className="col-md-4 right-part">
              <div className="information service-booking">
                <div className="service-item service-item-top">
                  <div className="name">Percentage of Bookings</div>
                </div>
                <div className="service-item service-item-bottom">
                  <div className="percent">
                    {props.data && props.data.infoJobByService ? 0 || props.data.infoJobByService.percentage : '0'}%
                  </div>
                  <div className="total">
                    with {props.data && props.data.infoJobByService ? 0 || props.data.infoJobByService.totalJob : '0'} bookings
                    in total
                  </div>
                </div>
              </div>
              <div className="information service-booking">
                <div className="service-item service-item-top">
                  <div className="name">Total Bookings</div>
                </div>
                <div className="service-item service-item-bottom">
                  <div className="percent">
                    {props.data && props.data.infoJobByService ?
                        (
                            <span>{props.data.infoJobByService || 0 ? 0 || props.data.infoJobByService.jobsOfService : '0'}</span>) :
                        (<span>0</span>)
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
  );
}

ServiceDetailsDataView.propTypes = {};
export default ServiceDetailsDataView;
