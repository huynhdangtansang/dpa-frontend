import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the DeviceEditPage state domain
 */

const selectDeviceEditPageDomain = state =>
    state.get("deviceEditPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by DeviceEditPage
 */

const makeSelectDeviceEditPage = () =>
    createSelector(selectDeviceEditPageDomain, substate => substate.toJS());
const makeSelectDeviceDetails = () =>
    createSelector(selectDeviceEditPageDomain, state => state.get('deviceData'));
const makeSelectErrors = () =>
    createSelector(selectDeviceEditPageDomain, state => state.get('errors'));
export { makeSelectDeviceEditPage, makeSelectDeviceDetails, makeSelectErrors };
export { selectDeviceEditPageDomain };
