/**
 *
 * CompanyTabEmployee
 *
 */
import React from "react";
//Button
import Checkbox from 'components/Checkbox';
import GhostButton from 'components/GhostButton';
//Library
import _ from "lodash";
import moment from 'moment';
import debounce from 'components/Debounce';
import classnames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import uuidv1 from 'uuid';
// Modal
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
// Combo import table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
import { DropdownItem, DropdownMenu, DropdownToggle, Form, Input, InputGroup, UncontrolledDropdown, } from 'reactstrap';
import { urlLink } from 'helper/route';

// -------------------
class CompanyTabEmployee extends React.PureComponent {
  actionFormat = (cell, row) => {
    let editMenu = (
        <DropdownMenu key={uuidv1()}>
          <DropdownItem onClick={() => {
            this.props.history.push(urlLink.employee + '?id=' + row._id);
          }}>View</DropdownItem>
          <DropdownItem onClick={() => {
            this.singleAction('changeStatus', { id: row._id, status: row.active });
          }}>
            {row.active ? 'Deactivate' : 'Activate'}
          </DropdownItem>
          <DropdownItem onClick={() => {
            this.singleAction('remove', { id: row._id, status: row.active });
          }}>Remove</DropdownItem>
        </DropdownMenu>);
    return (
        <ActionFormatter menu={editMenu}/>
    );
  };
  statusFormat = (cell) => {
    return (
        <div className="status">
          <div className={classnames("content", cell ? 'active' : 'inactive')}>{cell ? 'Active' : 'Inactive'}</div>
        </div>
    )
  };
  iconFormat = (cell, row) => {
    return (
        <img className='avatar-employee' src={row.avatar ? row.avatar.fileName : './default-user.png'}
             onError={(e) => {
               e.target.onerror = null;
               e.target.src = './default-user.png'
             }}
             onClick={() => {
               this.props.history.push(urlLink.employee + '?id=' + row._id);
             }}></img>
    )
  };
  nameFormat = (cell, row) => {
    return (
        <span onClick={() => {
          this.props.history.push(urlLink.employee + '?id=' + row._id);
        }}>{cell}</span>
    )
  };
  roleFormat = (cell) => {
    return (
        <span>{cell ? cell.charAt(0).toUpperCase() + cell.substr(1) : 'Employee'}</span>
    )
  };
  ratingFormat = (cell) => {
    return (
        cell.averageValue > 0 ?
            <div className="rating">
              <span>{parseFloat(cell.averageValue).toFixed(1)}</span>
              <span className="icon-star-full"></span>
              <span className={cell.increment === true ? 'icon-arrow-up' : 'icon-arrow-down'}></span>
            </div> :
            <div className="rating">-</div>
    )
  };
  handleChangeReason = (value) => {
    this.setState({ reason: value });
  };
  handleChangeSearchFilter = debounce((key, value) => {
    this.props.updateSearchEmployee(key, value);
    const { companyData, searchDataForEmployee } = this.props.companydetailspage;
    this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
  }, 600);
  handleSearch = (e) => {
    e.preventDefault();
    const { companyData, searchDataForEmployee } = this.props.companydetailspage;
    this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  singleAction = (type, employeeData) => {
    if (type == 'changeStatus') {
      this.setState({ chosenEmployee: employeeData }, () => {
        this.openStatusConfirm('single');
      });
    } else if (type == 'remove') {
      this.setState({ chosenEmployee: employeeData }, () => {
        this.openRemoveConfirm('single');
      })
    } else {
      let listIds = [];
      listIds.push(employeeData.id);
      let data = {
        ids: listIds
      };
      this.props.resetPass(data);
    }
  };
  multiAction = (type) => {
    if (type == 'changeStatus') {
      this.openStatusConfirm('multi');
    } else if (type == 'remove') {
      this.openRemoveConfirm('multi');
    } else {
      let listIds = [];
      let list = this.state.selectedEmployees;
      let i = 0;
      list.forEach((employee) => {
        listIds.push(employee.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds
          };
          this.props.resetPass(data);
        } else {
          i++;
        }
      });
    }
  };
  //---------------PAGINATION-------------------------
  openStatusConfirm = (type) => {
    this.setState({
      modifyType: type,
      showChangeStatusConfirm: true
    });
  };
  openRemoveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showRemoveConfirm: true
    });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  changeStatus = () => {
    const { companyData, searchDataForEmployee, selectedEmployee } = this.props.companydetailspage;
    this.refs.table_employee.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType == 'single') {
      let id = this.state.chosenEmployee.id;
      let status = this.state.chosenEmployee.status;
      let listIds = [];
      listIds.push(id);
      let data = {};
      if (status == true) {
        data['ids'] = listIds;
        data['reason'] = this.state.reason;
        data['active'] = !status;
      } else {
        data['ids'] = listIds;
        data['active'] = !status;
      }
      this.props.changeStatusEmployee(companyData._id, data, searchDataForEmployee);
    } else {//multi
      const changeStatusButton = this.checkChangeStatusButton();
      let listIds = [];
      let list = selectedEmployee;
      list.forEach((employee) => {
        listIds.push(employee.id);
      });
      let data = {
        ids: listIds,
        active: changeStatusButton.title === 'Deactive' ? false : true
      };
      data['reason'] = this.state.reason;
      this.props.changeStatusEmployee(companyData._id, data, searchDataForEmployee);
    }
  };
  changeStatusModal = () => {
    if (this.state.modifyType === 'single') {
      let currentEmployee = this.state.chosenEmployee;
      return (
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              currentStatus={currentEmployee.status}
              object={'employee'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
      )
    } else {
      const changeStatusButton = this.checkChangeStatusButton();
      const { selectedEmployee } = this.props.companydetailspage;
      return (
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              currentStatus={changeStatusButton.title === 'Deactive' ? true : false}
              object={'employee'}
              action={selectedEmployee.length > 1 ? 'multi' : 'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
      )
    }
  };
  removeModal = () => {
    if (this.state.modifyType === 'single') {
      return (
          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'employee'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeEmployee}/>
      )
    } else {
      const { selectedEmployee } = this.props.companydetailspage;
      return (
          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'employee'}
              action={selectedEmployee.length > 1 ? 'multi' : 'single'}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeEmployee}/>
      )
    }
  };
  removeEmployee = () => {
    const { companyData, searchDataForEmployee, selectedEmployee } = this.props.companydetailspage;
    this.refs.table_employee.cleanSelected();  // this.refs.table is a ref for BootstrapTable
    if (this.state.modifyType == 'single') {
      let id = this.state.chosenEmployee.id;
      let listIds = [];
      listIds.push(id);
      let data = {
        ids: listIds,
        reason: this.state.reason
      };
      this.props.removeEmployee(companyData._id, data, searchDataForEmployee);
    } else {
      let listIds = [];
      let list = selectedEmployee;
      let i = 0;
      list.forEach((employee) => {
        listIds.push(employee.id);
        if (i === list.length - 1) {
          let data = {
            ids: listIds,
            reason: this.state.reason
          };
          this.props.removeEmployee(companyData._id, data, searchDataForEmployee);
        } else {
          i++;
        }
      });
    }
  };
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
      status: row.active
    };
    this.props.selectEmployee(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAllEmployee(isSelected);
  };
  checkChangeStatusButton = () => {
    const { selectedEmployee } = this.props.companydetailspage;
    let list = selectedEmployee;
    if (list.length > 0) {
      const compareStatus = list[0].status;
      let differentItems = list.filter((item) => item.status !== compareStatus);
      if (differentItems.length === 0) {
        return {
          isHide: false,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      } else {
        return {
          isHide: true,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      }
    }
    return {
      isHide: false,
      title: 'Deactive',
    }
  };
  handleChangeFilterPeriod = debounce(newfilterPeriod => {
    this.props.updateDropdownPeriod(newfilterPeriod);
    const { companyData, searchDataForEmployee } = this.props.companydetailspage;
    this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
  }, 0);
  //-----------------------PAGINATION-----------------------
  changePagination = (type) => {
    const { employeeData, currentPageEmployee } = this.props.companydetailspage;
    const limit = employeeData.pagination.limit;
    if (type === 'prev') {
      let newPage = currentPageEmployee - 1;
      let newOffset = limit * (newPage - 1);
      this.props.updatePage('currentPageEmployee', newPage);
      this.props.updateSearchEmployee('offset', newOffset);
    } else {//next
      let newPage = currentPageEmployee + 1;
      let newOffset = limit * (newPage - 1);
      this.props.updatePage('currentPageEmployee', newPage);
      this.props.updateSearchEmployee('offset', newOffset);
    }
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { companyData, searchDataForEmployee } = this.props.companydetailspage;
    this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
  }, 1000);
  sortData = (field, order) => {
    const { searchDataForEmployee, companyData } = this.props.companydetailspage;
    let newSearchData = searchDataForEmployee;
    newSearchData.sort = field;
    newSearchData.sortType = order;
    this.props.updatePage('currentPageEmployee', 1);
    this.props.updateSearchData('searchDataForEmployee', newSearchData);
    //this.getListAfterChangeData('employee');
    this.props.getEmployeeCompany(companyData._id, newSearchData);
    //this.handleChangeSearchFilter('sort', order);
  };
  //Get list after change search data
  getListAfterChangeData = debounce((key) => {
    this.props.updatePage('currentPageEmployee', 1);
    if (key === 'employee') {
      const { searchDataForEmployee, companyData } = this.props.companydetailspage;
      this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
    }
  }, 1000);

  //-------------------------------------------------------
  constructor(props) {
    super(props);
    this.state = {
      activeModal: false,
      removeModal: false,
      selectedEmployees: [],
      isActive: false,
      reason: '',
      chosenEmployee: {},
      modifyType: 'single',
      hideChangeStatusButton: false,
      changeStatusType: 'Deactive',
      page: 1
    };
    this.checkChangeStatusButton = this.checkChangeStatusButton.bind(this);
  }

  render() {
    const { employeeData, searchDataForEmployee, currentPageEmployee, selectedEmployee, periodEmployee, filterPeriod } = this.props.companydetailspage;
    const changeStatusButton = this.checkChangeStatusButton();
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll,
    };
    const options = {
      withoutNoDataText: true,
      //defaultSortOrder: 'desc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    return (
        <div className="employees">
          <div className="header-list-page">
            <Form inline className="align-items-center"
                  onSubmit={(e) => this.handleSearch(e)}>

              <InputGroup className="search-bar">
                <Input placeholder="Search employee"
                       defaultValue={searchDataForEmployee.search}
                       onChange={(e) => {
                         e.preventDefault();
                         this.handleChangeSearchFilter('search', e.target.value);
                       }}/>
                <span className="icon-search"></span>
              </InputGroup>

              <UncontrolledDropdown className="period-dropdown">
                <DropdownToggle>
                  <span className="title">Period:&nbsp;</span>
                  <span className="content">{filterPeriod.label}</span>
                  <span className="fixle-caret icon-triangle-down"></span>
                </DropdownToggle>
                <DropdownMenu>
                  <Scrollbars
                      // This will activate auto hide
                      autoHide
                      // Hide delay in ms
                      autoHideTimeout={1000}
                      autoHeight
                      autoHeightMin={0}
                      autoHeightMax={200}>
                    <DropdownItem key={uuidv1()}
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.handleChangeFilterPeriod({
                                      value: '', label: 'All'
                                    });
                                  }}>All</DropdownItem>
                    {periodEmployee && (periodEmployee.map((item) => (
                        <DropdownItem key={uuidv1()}
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.handleChangeFilterPeriod({
                                          value: moment(item).format('MM/YYYY'), label: item
                                        })
                                      }}
                        >{item}
                        </DropdownItem>
                    )))}
                  </Scrollbars>
                </DropdownMenu>
              </UncontrolledDropdown>

              <div className="only-show-active">
                <Checkbox name={'onlyShowActive'}
                          checked={searchDataForEmployee.active}
                          onChange={(e) => {
                            e.preventDefault();
                            this.handleChangeSearchFilter('active', !searchDataForEmployee.active);
                          }}
                          label={'Only show active'}
                />
              </div>
              {!_.isEmpty(employeeData) && (
                  <TotalFormatter
                      offset={employeeData.pagination.offset}
                      limit={employeeData.pagination.limit}
                      total={employeeData.pagination.total}
                      page={currentPageEmployee}
                      changePagination={this.changePagination}
                  />
              )}

            </Form>
          </div>


          <div className={classnames("content content-list-page", selectedEmployee.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_employee"
                data={employeeData ? employeeData.data : []}
                options={options}
                bordered={false}
                selectRow={selectRowProp}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'>
              <TableHeaderColumn dataField='_id' isKey hidden>Client ID</TableHeaderColumn>
              <TableHeaderColumn dataField='icon' width={46 / 14 + 'rem'}
                                 dataFormat={this.iconFormat}></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='fullName' width={130 / 14 + 'rem'} columnClassName='name'
                                 dataFormat={this.nameFormat}>Employee</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='email' width={200 / 14 + 'rem'}>Email</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='phoneNumber' width={176 / 14 + 'rem'}>Contact
                Number</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='subRole' width={88 / 14 + 'rem'} columnClassName={'subRole'}
                                 dataFormat={this.roleFormat}>Position</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='assigned' width={118 / 14 + 'rem'}
                                 dataAlign='center'>Assigned</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='active' width={84 / 14 + 'rem'} dataFormat={this.statusFormat}
                                 columnClassName={'status'}>Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='earning' dataAlign='center' dataFormat={(cell) => {
                return cell.unit + parseFloat(cell.value).toFixed(2)
              }}>Earning</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='rate' dataAlign='center'
                                 dataFormat={this.ratingFormat}>Rating</TableHeaderColumn>
              <TableHeaderColumn dataFormat={this.actionFormat} dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div className="footer" hidden={selectedEmployee.length === 0}>
            <GhostButton hidden={changeStatusButton.isHide}
                         title={changeStatusButton.title === 'Deactive' ? 'Deactivate' : 'Activate'}
                         className="btn-ghost btn-edit-charge"
                         onClick={() => {
                           this.multiAction('changeStatus');
                         }}
            />
            <GhostButton title={'Remove'} className="btn-ghost btn-remove" onClick={() => {
              this.multiAction('remove');
            }}/>
          </div>

          {this.changeStatusModal()}

          {this.removeModal()}
        </div>
    );
  }
}

CompanyTabEmployee.propTypes = {};
export default CompanyTabEmployee;
