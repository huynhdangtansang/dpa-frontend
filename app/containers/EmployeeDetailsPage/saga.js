// import { take, call, put, select } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import config from 'config';
import axios from 'axios';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import { goBack } from 'react-router-redux';

export function* apiGetMemberDetails(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.members + data.id;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(Actions.getError(error.response.data));
    }
  }
}

export function* apiChangeMemberStatus(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.teamMember + '/deactives';
    try {
      yield axios.put(requestUrl, data.dataUpdate);
      yield put(Actions.getMemberDetails(data.employeeID));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteMember(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.teamMember + '/deletes';
    try {
      yield axios.delete(requestUrl, { data: data.data });
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiGetJobAssigned(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(k =>
        k + '=' + esc(params[k])
    ).join('&');
    const requestUrl = config.serverUrl + config.api.admin + data.id + '/getJobAssigned' + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getJobAssignedSuccess(response.data));
    } catch (error) {
      yield put(Actions.getError(error.response.data));
    }
  }
}

export function* apiGetActivitiesEmployee(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.members + data.id + '/getActivity';
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getActivitiesEmployeeSuccess(response.data));
    } catch (error) {
      yield put(Actions.getError(error.response.data));
    }
  }
}

export function* getEmployeeEarning(data) {
  const { id, time, resolve, reject } = data.value;
  const requestUrl = config.serverUrl + config.api.admin + id + '/getEarningByUserId?time=' + encodeURIComponent(time);
  yield put(loadRepos());
  try {
    //Call API
    const response = yield axios.get(requestUrl);
    yield put(reposLoaded());
    yield put(Actions.getEmployeeEarningSuccess(response.data.data));
    resolve(response.data.data);
  } catch (err) {
    yield put(reposLoaded());
    yield put(Actions.getEmployeeEarningError(err));
    reject(err);
  }
}

export function* getEmployeeReview(data) {
  const { id, resolve, reject } = data.value;
  const requestUrl = config.serverUrl + config.api.admin + id + '/getReview';
  yield put(loadRepos());
  try {
    //Call API
    const response = yield axios.get(requestUrl);
    yield put(reposLoaded());
    yield put(Actions.getEmployeeReviewSuccess(response.data.data));
    resolve(response.data.data);
  } catch (err) {
    yield put(reposLoaded());
    yield put(Actions.getEmployeeReviewError(err));
    reject(err);
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_MEMBER_DETAILS, apiGetMemberDetails);
  yield takeLatest(Constants.CHANGE_MEMBER_STATUS, apiChangeMemberStatus);
  yield takeLatest(Constants.DELETE_MEMBER, apiDeleteMember);
  //Earning
  yield takeLatest(Constants.GET_EMPLOYEE_EARNING, getEmployeeEarning);
  //Preview
  yield takeLatest(Constants.GET_EMPLOYEE_REVIEW, getEmployeeReview);
  //Job
  yield takeLatest(Constants.GET_JOB_ASSIGNED, apiGetJobAssigned);
  //Activities
  yield takeLatest(Constants.GET_ACTIVITIES, apiGetActivitiesEmployee);
}
