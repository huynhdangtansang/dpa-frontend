/*
 *
 * TeamListPage constants
 *
 */
export const RESET_STATE = "TeamListPage/RESET_STATE";
export const GET_TEAM_LIST = "TeamListPage/GET_TEAM_LIST";
export const GET_SUCCESS = "TeamListPage/GET_SUCCESS";
export const GET_ERROR = "TeamListPage/GET_ERROR";
export const CHANGE_MEMBER_STATUS = "TeamListPage/CHANGE_MEMBER_STATUS";
export const DELETE_MEMBER = "TeamListPage/DELETE_MEMBER";
export const UPDATE_SEARCH_DATA = "TeamListPage/UPDATE_SEARCH_DATA";
export const SELECT_MEMBER = "TeamListPage/SELECT_MEMBER";
export const SELECT_ALL_MEMBER = "TeamListPage/SELECT_ALL_MEMBER";
export const UPDATE_PAGE = "TeamListPage/UPDATE_PAGE";
export const SET_CURRENT_MEMBER = "TeamListPage/SET_CURRENT_MEMBER";
export const SHOW_MODAL = "TeamListPage/SHOW_MODAL";
export const UPDATE_MODIFY_TYPE = "TeamListPage/UPDATE_MODIFY_TYPE";
//Period
export const GET_PERIOD = "TeamListPage/GET_PERIOD";
export const GET_PERIOD_SUCCESS = "TeamListPage/GET_PERIOD_SUCCESS";
export const UPDATE_DROPDOWN_PERIOD = "TeamListPage/UPDATE_DROPDOWN_PERIOD";