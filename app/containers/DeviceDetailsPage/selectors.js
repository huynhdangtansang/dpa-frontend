import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the DeviceDetailsPage state domain
 */

const selectDeviceDetailsPageDomain = state =>
    state.get("deviceDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by DeviceDetailsPage
 */

const makeSelectDeviceDetailsPage = () =>
    createSelector(selectDeviceDetailsPageDomain, substate => substate.toJS());
const makeSelectDeviceDetails = () =>
    createSelector(selectDeviceDetailsPageDomain, state => state.get('deviceData'));
export { makeSelectDeviceDetailsPage, makeSelectDeviceDetails };
export { selectDeviceDetailsPageDomain };
