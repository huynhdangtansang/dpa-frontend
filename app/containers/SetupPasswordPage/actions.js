/*
 *
 * SetupPasswordPage actions
 *
 */
import { DEFAULT_ACTION, RESET_ERROR, RESET_PASSWORD, RESET_SUCCESS } from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function resetPassword(token, password) {
  return {
    type: RESET_PASSWORD,
    token: token,
    password: password
  }
}

export function resetSuccess(response) {
  return {
    type: RESET_SUCCESS,
    response: response
  }
}

export function resetError(response) {
  return {
    type: RESET_ERROR,
    response: response
  }
}