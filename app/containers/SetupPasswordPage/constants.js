/*
 *
 * SetupPasswordPage constants
 *
 */
export const DEFAULT_ACTION = "app/SetupPasswordPage/DEFAULT_ACTION";
export const RESET_PASSWORD = "/SetupPasswordPage/RESET_PASSWORD";
export const RESET_SUCCESS = "/SetupPasswordPage/RESET_SUCCESS";
export const RESET_ERROR = "/SetupPasswordPage/RESET_ERROR";