import { put, takeLatest } from 'redux-saga/effects';
import {
  GET_USER_PROFILE,
  RESEND_VERIFY_CODE,
  SEND_EMAIL_CONFIRM,
  SEND_VERIFY_CODE,
  UPDATE_USER_PROFILE
} from './constants';
import {
  getProfile,
  getSuccess,
  resendError,
  resendSuccess,
  sendCodeError,
  sendCodeSuccess,
  updateError,
  updateSuccess
} from './actions';
import { getSuccess as getSuccessProfileInDashboard, signOut } from '../DashBoardPage/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";
import { loadRepos, reposLoaded, updateError as updateErrorApi } from 'containers/App/actions';

export function* apiGetProfile() {
  yield put(loadRepos());
  const requestUrl = config.serverUrl + config.api.user_profile.get;
  try {
    const response = yield axios.get(requestUrl);
    yield put(getSuccess(response.data));
    yield put(getSuccessProfileInDashboard(response.data));
    yield put(reposLoaded());
  } catch (error) {
    yield put(reposLoaded());
    localStorage.removeItem('token');
    if (localStorage.getItem('isRemember')) {
      localStorage.removeItem('isRemember');
    }
    window.location.reload();
  }
}

export function* apiUpdateProfile(data) {
  const requestUrl = config.serverUrl + config.api.user_profile.update;
  yield put(loadRepos());
  try {
    const response = yield axios.put(
        requestUrl,
        data.profile
    );
    if (data.status === false) {
      yield put(signOut());
      window.location.reload();
    }
    // yield put(setEditMode(false));
    yield put(updateSuccess(response.data));
    yield put(getProfile());
  } catch (error) {
    yield put(updateError(error.response.data));
    yield put(reposLoaded());
  }
}

export function* apiSendMailConfirm(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.auth.resend_mail_update;
    yield put(loadRepos());
    try {
      yield axios.post(requestUrl);
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateErrorApi({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiSendVerifyCode(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.auth.verify_code_update;
    yield put(loadRepos());
    try {
      let apiData = {
        email: data.email,
        code: data.code
      };
      const response = yield axios.post(requestUrl, apiData);
      yield put(sendCodeSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(sendCodeError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

export function* apiResendSecurityCode(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.auth.resend_code_update;
    yield put(loadRepos());
    try {
      let apiData = {
        email: data.email
      };
      const response = yield axios.post(requestUrl, apiData);
      yield put(resendSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(resendError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_USER_PROFILE, apiGetProfile);
  yield takeLatest(UPDATE_USER_PROFILE, apiUpdateProfile);
  yield takeLatest(SEND_EMAIL_CONFIRM, apiSendMailConfirm);
  yield takeLatest(SEND_VERIFY_CODE, apiSendVerifyCode);
  yield takeLatest(RESEND_VERIFY_CODE, apiResendSecurityCode);
}
