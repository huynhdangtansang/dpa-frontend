export const urlLink = {
	//DASHBOARD
	dashboard: '/dashboard',

	//STATISTICS
	statistics: '/dashboard/statistics',

	//CLIENTS
	clients: '/dashboard/clients',
	viewClient: '/dashboard/clients/client-details',
	addClient: '/dashboard/clients/new-client',
	editClient: '/dashboard/clients/client-edit-profile',

	//DEVICES
	devices: '/dashboard/devices',
	viewDevice: '/dashboard/devices/device-details',
	addDevice: '/dashboard/devices/new-device',
	editDevice: '/dashboard/devices/edit-device',

	//EMPLOYEE
	employee: '/dashboard/employee-profile',

	//BOT MESSAGES
	botMess: '/dashboard/bot-messages',

	//USER PROFILE
	userProfile: '/dashboard/user-profile',

	//CHANGE PASSWORD
	changePassword: '/dashboard/change-password',

	//AUTHEN
	login: '/',
	forgotPassword: '/forgot-password',
	resetPassword: '/reset-password',
	resetPasswordSuccess: '/reset-password-success',
	setupPassword: '/setup-password',
	verifyCode: '/verify-code'
};
