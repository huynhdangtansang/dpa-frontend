import { fromJS } from 'immutable';
import jobDetailsPageReducer from '../reducer';

describe('jobDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(jobDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
