/*
 *
 * CompaniesListPage reducer
 *
 */
import { fromJS } from "immutable";
//Library
import moment from 'moment';
import {
  CLEAR_ALL_SELECTED_COMPANY,
  DEFAULT_ACTION,
  GET_COMPANIES_LIST,
  GET_PERIOD_SUCCESS,
  GET_SERVICE_SUCCESS,
  GET_SUCCESS,
  SELECT_ALL_COMPANY,
  SELECT_COMPANY,
  UPDATE_DROPDOWN_PERIOD,
  UPDATE_DROPDOWN_SERVICE,
  UPDATE_FILTER_LIST,
  UPDATE_SEARCH_DATA,
  UPDATE_SEARCH_STRING
} from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  companiesList: [],
  dataList: [],
  serviceList: [],
  selectedCompanies: [],
  periodList: [],
  currentPage: 1,
  searchData: {
    offset: 0,
    limit: 10,
    search: '',
    period: '',
    serviceIds: [],
    sort: '',
    sortType: '',
    isTopCompany: false
  },
  filterService: {
    value: '',
    label: 'All'
  },
  filterPeriod: {
    value: '',
    label: 'All'
  },
  searchDataForService: {
    search: '',
    sort: '',
    sortType: ''
  },
});

function companiesListPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
    case LOCATION_CHANGE:
      return initialState;
    case GET_COMPANIES_LIST:
      return state
          .set('companiesList', fromJS([]))
          .set('dataList', fromJS([]));
    case GET_SUCCESS:
      return state
          .set('companiesList', action.response.data)
          .set('dataList', action.response.data.data);
    case GET_SERVICE_SUCCESS:
      return state
          .set('serviceList', action.response.data);
    case GET_PERIOD_SUCCESS: {
      let dateStart = moment(action.response.data);
      let dateEnd = moment(new Date());//current date
      let timeValues = [];
      while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
        timeValues.push(dateStart.format('MMM YYYY'));
        dateStart.add(1, 'month');
      }
      return state
          .set('periodList', fromJS(timeValues));
    }
    case CLEAR_ALL_SELECTED_COMPANY:
      return state
          .set('selectedCompanies', fromJS([]));
    case SELECT_COMPANY:
      if (action.isSelected === true) {
        return state
            .updateIn(['selectedCompanies'], arr => arr.push(action.selectItem))
      } else {
        return state
            .set('selectedCompanies', state.get('selectedCompanies').filter((company) => company.id !== action.selectItem.id))
      }
    case SELECT_ALL_COMPANY:
      if (action.isSelected === true) {
        let newArray = state.get('dataList').map((company) => {
          return {
            id: company._id,
            status: company.active
          }
        });
        return state
            .set('selectedCompanies', newArray)
      } else {
        return state
            .set('selectedCompanies', fromJS([]))
      }
    case UPDATE_SEARCH_STRING:
      return state
          .setIn(['searchData', 'search'], action.textSearch);
    case UPDATE_DROPDOWN_SERVICE:
      return state
          .set('filterService', action.newFilterService)
          .setIn(['searchData', 'serviceIds'], action.newFilterService.value ? [action.newFilterService.value] : []);
    case UPDATE_DROPDOWN_PERIOD:
      return state
          .set('filterPeriod', action.newFilterPeriod)
          .setIn(['searchData', 'period'], action.newFilterPeriod.value ? [action.newFilterPeriod.value] : []);
    case UPDATE_FILTER_LIST:
      return state
          .set('searchData', fromJS(action.newSearchData))
          .set('currentPage', fromJS(action.newPage));
    case UPDATE_SEARCH_DATA:
      return state
          .set('searchData', fromJS(action.newSearchData));
    default:
      return state;
  }
}

export default companiesListPageReducer;
