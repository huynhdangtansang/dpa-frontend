/**
 *
 * Asynchronously loads the component for JobDetailsTabOverview
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
