/*
 *
 * JobEditDescription constants
 *
 */
export const RESET_STATE = "JobEditDescription/RESET_STATE";
export const GET_JOB = "JobEditDescription/GET_JOB";
export const GET_SUCCESS = "JobEditDescription/GET_SUCCESS";
export const UPDATE_JOB_DESCRIPTION = "JobEditDescription/UPDATE_JOB_DESCRIPTION";
export const UPDATE_ERROR = "JobEditDescription/UPDATE_ERROR";
export const ADD_IMAGE = "JobEditDescription/ADD_IMAGE";
export const REMOVE_IMAGE = "JobEdtiDescription/REMOVE_IMAGE";
export const CHANGE_LOCATION = "JobEditDescription/CHANGE_LOCATION";
export const CHANGE_COMMENT = "JobEditDescription/CHANGE_LOCATION";