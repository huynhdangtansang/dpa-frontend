import React from 'react';
import './styles.scss';
import PropTypes from "prop-types";
import classnames from 'classnames';

export default class TextareaCounter extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onChange(event.target.value);
  }

  render() {
    return (
        <div className={classnames('fixle-textarea-counter', this.props.className)} {...this.props}>
          <span className='title'>{this.props.title}</span>
          <textarea
              className="form-control"
              {...this.props}
              onBlur={this.props.onBlur}
              placeholder={this.props.placeholder}
              value={this.props.value}
              onChange={this.props.onChange}
          />
          <span
              className='counter'>{this.props.value ? this.props.maxLength - this.props.value.length : this.props.maxLength}/{this.props.maxLength}</span>
          {(this.props.touched && this.props.error) && (
              <div className={'error-text'}>
                <i className={'icon-error'}/>
                <span>{this.props.error}</span>
              </div>
          )}
        </div>
    );
  }
}
TextareaCounter.propTypes = {
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string
};