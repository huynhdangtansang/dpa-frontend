/**
 *
 * DeviceEditPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
//lib
import _, { debounce, findIndex, indexOf } from "lodash";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectDeviceDetails, makeSelectDeviceEditPage, makeSelectErrors } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { addAddress, changeAvatar, editAddress, getDeviceDetails, showModal, updateDevice } from './actions';
import { updateError } from 'containers/App/actions';
import './style.scss';
//components
import InputForm from "components/InputForm";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import SubmitButton from 'components/SubmitButton';
import CropImage from 'components/CropImage';
import AddressItem from 'components/AddressItem';
import Selection from 'components/Selection';
import InputFile from "components/InputFile";
//data
import { country, listError } from 'helper/data';
import { loadCountries, removePlus } from 'helper/exportFunction';

const validateForm = Yup.object().shape({
  'name': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (no special characters)')
      .min(6, 'Invalid name (At least 6 characters)')
      .max(30, 'Invalid name (Maximum at 30 characters)'),
  'email': Yup.string()
      .email('Invalid email'),
  'phoneNumber': Yup.string()
      .matches(/^[0-9]*$/, 'Invalid phone number')
      .min(5, 'Invalid phone number'),
});

/* eslint-disable react/prefer-stateless-function */
export class DeviceEditPage extends React.PureComponent {
  getPhoneCode = () => {
    const COUNTRYNAME = {
      "Vietnam": '+84',
      "Australia": '+61'
    };
    return COUNTRYNAME[this.state.contactCountry];
  };
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeAva(src);
    this.setState({
      cropSrc: '',
      avatarFile: file
    });
  };
  getContactCountryValue = (value) => {
    this.setState({ contactCountry: value });
  };
  closeModal = () => {
    this.setState({
      showAvatarModal: false,
      cropSrc: ''
    });
  };
  addressListView = (list) => {
    let view = [];
    let count = 0;
    list.forEach((address, index) => {
      let addressView = (
          <AddressItem
              className={address.action === 'delete' ? 'address-item deleted' : 'address-item'}
              key={index}
              index={count}
              id={index}
              data={address}
              delete={index => {
                let newAddress = address;
                newAddress.action = 'delete';
                this.props.edit(index, newAddress);
              }}
              setPrimary={index => {
                let newAddress = address;
                newAddress.isPrimary = !address.primary;
                if (address.id === '') {
                  newAddress.action = 'add';
                } else {
                  newAddress.action = 'edit';
                }
                this.props.edit(index, newAddress);
              }}
              handleChange={(index, value) => {
                let newAddress = { ...address, ...value };
                if (address.id === '') {
                  newAddress.action = 'add';
                } else {
                  newAddress.action = 'edit';
                }
                newAddress.error = false;
                this.props.edit(index, newAddress)
              }}
              deactiveAddress={(index) => {
                let newAddress = address;
                newAddress.error = true;
                this.props.edit(index, newAddress);
              }}
          />
      );
      view.push(addressView);
      if (address.action !== 'delete') {
        count++;
      }
    });
    return view;
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e);
    }
  };
  invalidAddress = () => {
    const { addressList } = this.props.deviceeditpage;
    let list = addressList.filter(address => address.action !== 'delete');
    if (list.length > 0) {
      let errorList = list.filter(address => address.error === true);
      if (errorList.length === 0) {
        return false;
      }
    }
    return true;
  };
  disableSubmit = (values, errors) => {
    return this._disableButton(values, errors) || this.invalidAddress();
  };
  //Clear api error
  clearApiError = (type) => {
    const { apiErrors } = this.props.deviceeditpage;
    if (apiErrors && apiErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, apiErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      showAvatarModal: false,
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let deviceID = temp[1];
    this.props.getDeviceData(deviceID);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'name',
      'email',
      'phoneNumber'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { addressList, avatar, showConfirmModal, deviceData, apiErrors } = this.props.deviceeditpage;
    return (
        <div className="device-edit-profile">
          <div className="header-edit-add-page edit-device-header">
            <div className="action">
              <div className="return" id='return' onClick={() => {
                this.props.history.goBack();
              }}>
                <span className="icon-arrow-left"></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
            </div>
            <div className="title">
              <span>Edit Device</span>
            </div>
          </div>
          {}
          <Formik ref={ref => {
            this.formik = ref
          }}
                  initialValues={{
                    name: !_.isEmpty(deviceData) ? deviceData.fullName : '',
                    email: !_.isEmpty(deviceData) ? deviceData.email : '',
                    phoneNumber: !_.isEmpty(deviceData) ? deviceData.phoneNumber : '',
                    contactArea: {
                      value: !_.isEmpty(deviceData) ? deviceData.countryCode : '',
                      label: !_.isEmpty(deviceData) ? deviceData.countryName : ''
                    },
                  }}
                  enableReinitialize={true}
                  validationSchema={validateForm}
                  onSubmit={(e) => {
                    let formData = new FormData();
                    if (this.state.avatarFile) {
                      formData.append('file', this.state.avatarFile);
                    }
                    formData.append('fullName', e.name);
                    formData.append('email', e.email);
                    formData.append('countryName', e.contactArea.label);
                    formData.append('countryCode', '+' + removePlus(e.contactArea.value));
                    if (e.phoneNumber.substring(0, 1) === '0') {
                      e.phoneNumber = e.phoneNumber.substring(1);
                    }
                    formData.append('phoneNumber', e.phoneNumber);
                    formData.append('addresses', JSON.stringify(addressList));
                    this.setState({ updateData: formData });
                    this.props.showModal('updateConfirm', true);
                  }}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="content-add-edit edit-device-content">
                    <div className="information">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Personal Information</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          <div className="avatar-image device-avatar">
                            <div className="avatar-wrapper">
                              <img src={avatar}
                                   onError={(e) => {
                                     e.target.onerror = null;
                                     e.target.src = './default-user.png'
                                   }}
                                   alt="avatar"/>
                            </div>
                            {}

                            <InputFile name={'avatar'}
                                       src={avatar}
                                       onBlur={handleBlur}
                                       btnText={avatar ? 'change avatar' : 'upload avatar'}
                                       onChange={evt => {
                                         this._handleImageChange(evt);
                                       }}/>
                          </div>
                          <div className="details">
                            <InputForm
                                label="name"
                                name={'name'}
                                value={values.name}
                                error={errors.name}
                                touched={touched.name}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('name');
                                }}
                                onBlur={handleBlur}
                                placeholder={'Full name'}
                            />

                            {/* EMAIL */}
                            <InputForm
                                label="email"
                                name='email'
                                value={values.email}
                                error={errors.email}
                                touched={touched.email}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('email');
                                }}
                                onBlur={handleBlur}
                                placeholder={'you@example.com'}
                            />

                            <div className='input-group form-input'>
                              {}
                              <div className="info-item contact-number">
                                <Selection options={country}
                                           title={'Contact number'}
                                           name={'contactArea'}
                                           value={values.contactArea}
                                           isAsyncList={true}
                                           loadOptions={debounce(loadCountries, 700)}
                                           getOptionLabel={option => option.label}
                                           getOptionValue={option => option.value}
                                           type={'contactArea'}
                                           onBlur={handleBlur}
                                           onChange={option => {
                                             setFieldValue('contactArea', {
                                               value: option.country_code,
                                               label: option.label
                                             });
                                           }}/>

                                <InputForm
                                    name='phoneNumber'
                                    prependLabel={'+' + removePlus(values.contactArea.value)}
                                    value={values.phoneNumber}
                                    error={errors.phoneNumber}
                                    touched={touched.phoneNumber}
                                    onChange={evt => {
                                      handleChange(evt);
                                      this.clearApiError('phoneNumber');
                                    }}
                                    onBlur={handleBlur}
                                    placeholder={'Phone number'}
                                />
                              </div>
                            </div>
                          </div>
                          {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                            return (
                                <div key={error.errorCode} className="errors">
                                  <span className="icon-error"></span>
                                  <div className="error-item">
                                    <span>{error.errorMessage}</span>
                                  </div>
                                </div>
                            );
                          }) : []}
                        </div>
                      </div>
                    </div>
                    <div className="information address-details">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Address Details</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          {this.addressListView(addressList)}
                          <div className="addition-action">
                            <PurpleRoundButton
                                title={'New Address'}
                                className="btn-purple-round"
                                type="button"
                                onClick={() => {
                                  this.props.add(addressList);
                                }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="footer edit-device-footer">
                    <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                      this.props.history.goBack();
                    }}/>
                    <SubmitButton type={'submit'}
                                  disabled={this.disableSubmit(values, errors)}
                                  content={'Save'}/>
                  </div>
                </form>
            )}
          </Formik>
          {(this.state.cropSrc && this.state.cropSrc !== '') ?
              <CropImage
                  show={this.state.showAvatarModal}
                  cropSrc={this.state.cropSrc}
                  closeModal={this.closeModal}
                  changeImage={this.changeImage}
              />
              : []
          }
          <Modal isOpen={showConfirmModal} className="update-confirm">
            <ModalBody>
              <div className="upper">
                <div className="title">
                  <span>Save This Device</span>
                </div>
                <div className="description">
                  <span>Are you sure to want to save this change? This action could influence on all data influence.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.props.showModal('updateConfirm', false);
                }}/>
                <PurpleRoundButton className="btn-purple-round sign-out"
                                   title={'Save'}
                                   onClick={() => {
                                     if (this.state.updateData) {
                                       this.props.showModal('updateConfirm', false);
                                       this.props.updateDeviceProfile(deviceData._id, this.state.updateData);
                                     }
                                   }}/>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

DeviceEditPage.propTypes = {
  dispatch: PropTypes.func,
  getDeviceData: PropTypes.func,
  updateDeviceProfile: PropTypes.func,
  add: PropTypes.func,
  edit: PropTypes.func,
  changeAva: PropTypes.func,
  updateError: PropTypes.func,
  showModal: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  deviceeditpage: makeSelectDeviceEditPage(),
  deviceData: makeSelectDeviceDetails(),
  apiErrors: makeSelectErrors()
});

function mapDispatchToProps(dispatch) {
  return {
    getDeviceData: (id) => {
      dispatch(getDeviceDetails(id));
    },
    updateDeviceProfile: (id, profile) => {
      dispatch(updateDevice(id, profile));
    },
    add: (addressList) => {
      dispatch(addAddress(addressList));
    },
    edit: (id, newAddress) => {
      dispatch(editAddress(id, newAddress));
    },
    changeAva: (src) => {
      dispatch(changeAvatar(src));
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "deviceEditPage", reducer });
const withSaga = injectSaga({ key: "deviceEditPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(DeviceEditPage);
