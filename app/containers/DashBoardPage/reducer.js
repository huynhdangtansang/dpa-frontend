/*
 *
 * DashBoardPage reducer
 *
 */
import { fromJS } from "immutable";
import { DEFAULT_ACTION, GET_NOTIFICATION_COMPLETE, GET_SUCCESS, SELECT_MENU } from "./constants";

export const initialState = fromJS({
  tab: 1,
  notification: [],
  unRead: 0
});

function dashBoardPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SELECT_MENU:
      return state
          .set('tab', action.tab);
    case GET_SUCCESS:
      return state
          .set('userData', action.response.data);
    case GET_NOTIFICATION_COMPLETE:
      return state
          .set('notification', action.response.data.data)
          .set('unRead', action.response.data.unRead);
    default:
      return state;
  }
}

export default dashBoardPageReducer;
