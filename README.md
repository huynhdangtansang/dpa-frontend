### Steps to build
##### Step 1: Clone this repo and navigate into project folder
##### Step 2: Installing dependencies, run this command
```
npm install
```
##### Step 3:
-  If you want to run this website on localhost, run this command:

```
npm start
```

The website is hosted at http://localhost:3000

- And if you host this website in somewhere on server, run this command at the root project:
```
npm run build:standalone
```

The build files are served at `<root>/build` folder
