/*
 *
 * TeamNewItemPage reducer
 *
 */
import { fromJS } from "immutable";
import { ADD_ERROR, ADD_SUCCESS, CHANGE_AVATAR, DEFAULT_ACTION, RESET_ERROR } from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  avatar: '',
  apiErrors: []
});

function teamNewItemPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOCATION_CHANGE:
      return initialState;
    case CHANGE_AVATAR:
      return state
          .set('avatar', action.url);
    case ADD_SUCCESS:
    case ADD_ERROR:
      return state
          .set('apiErrors', action.response.errors);
    case RESET_ERROR:
      return state
          .set('apiErrors', []);
    default:
      return state;
  }
}

export default teamNewItemPageReducer;
