/*
 *
 * DeviceNewItemPage actions
 *
 */
import { ADD_DEVICE, ADD_ERROR, ADD_SUCCESS, CHANGE_AVATAR, RESET_ERROR } from "./constants";

export function addDevice(profile) {
  return {
    type: ADD_DEVICE,
    profile
  }
}

export function addSuccess(response) {
  return {
    type: ADD_SUCCESS,
    response
  }
}

export function addError(response) {
  return {
    type: ADD_ERROR,
    response
  }
}

export function resetError() {
  return {
    type: RESET_ERROR
  }
}

export function changeAvatar(url) {
  return {
    type: CHANGE_AVATAR,
    url: url
  }
}