/**
 *
 * Asynchronously loads the component for JobDetailsPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
