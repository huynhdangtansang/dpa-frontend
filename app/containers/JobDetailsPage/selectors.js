import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the jobDetailsPage state domain
 */

const selectJobDetailsPageDomain = state =>
    state.get("jobDetailsPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by JobDetailsPage
 */

const makeSelectJobDetailsPage = () =>
    createSelector(selectJobDetailsPageDomain, substate => substate.toJS());
export default makeSelectJobDetailsPage;
export { selectJobDetailsPageDomain };
