import React from "react";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { listError } from "helper/data";
import './style.scss';

export default class Timepicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  checkApiError = (name, error) => {
    if (!_.isEmpty(error)) {
      let index = findIndex(listError, val => val.name === name);
      if (index > -1) {
        if (indexOf(listError[index].error, error[0].errorCode) > -1) {
          return true;
        }
      }
      return false;
    }
  };

  render() {
    return (
        <div className="time-picker">
          <DatePicker
              showTimeSelect
              showTimeSelectOnly
              dateFormat="hh:mm aa"
              timeIntervals={1}
              selected={this.props.value ? moment(this.props.value).toDate() : moment().toDate()}
              onChange={this.props.onChange}
          />
        </div>
    )
  }
}