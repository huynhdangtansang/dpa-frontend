/**
 *
 * DevicePages
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import DevicesListPage from '../DevicesListPage/Loadable';
import DeviceDetailsPage from '../DeviceDetailsPage/Loadable';
import DeviceEditPage from '../DeviceEditPage/Loadable';
import DeviceNewItemPage from '../DeviceNewItemPage/Loadable';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class DevicePages extends React.PureComponent {
  render() {
    return (
        <div className="device-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.devices} component={DevicesListPage}/>
              <Route path={urlLink.viewDevice} component={DeviceDetailsPage}/>
              <Route path={urlLink.editDevice} component={DeviceEditPage}/>
              <Route path={urlLink.addDevice} component={DeviceNewItemPage}/>
            </Switch>
          </Router>
        </div>
    );
  }
}

DevicePages.propTypes = {
  dispatch: PropTypes.func.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
    null,
    mapDispatchToProps
);
export default compose(withConnect)(DevicePages);
