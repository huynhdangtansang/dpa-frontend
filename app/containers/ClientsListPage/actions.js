/*
 *
 * ClientsListPage actions
 *
 */
import {
  CHANGE_CLIENT_STATUS,
  CHANGE_IS_SUCCESS,
  CLEAR_ALL_SELECTED_CLIENT,
  DEFAULT_ACTION,
  DELETE_CLIENT,
  GET_CLIENTS_LIST,
  GET_SUBURBS_LIST,
  GET_SUBURBS_SUCCESS,
  GET_SUCCESS,
  RESET_PASSWORD,
  RESET_STATE,
  SELECT_ALL_CLIENT,
  SELECT_CLIENT,
  SHOW_MODAL,
  UPDATE_SEARCH_DATA
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: RESET_STATE
  };
}

export function getClientsList(dataSearch) {
  return {
    type: GET_CLIENTS_LIST,
    searchData: dataSearch
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response: response
  }
}

export function deleteClient(deleteData, searchData) {
  return {
    type: DELETE_CLIENT,
    deleteData,
    searchData
  }
}

export function changeClientStatus(statusData, searchData) {
  return {
    type: CHANGE_CLIENT_STATUS,
    statusData,
    searchData
  }
}

export function resetPassword(resetData) {
  return {
    type: RESET_PASSWORD,
    resetData
  }
}

export function clearAllSelectedClient() {
  return {
    type: CLEAR_ALL_SELECTED_CLIENT
  }
}

export function changeIsSuccess(value) {
  return {
    type: CHANGE_IS_SUCCESS,
    value
  }
}

export function selectClient(isSelected, selectItem) {
  return {
    type: SELECT_CLIENT,
    isSelected,
    selectItem
  }
}

export function selectAllClient(isSelected) {
  return {
    type: SELECT_ALL_CLIENT,
    isSelected
  }
}

export function getSuburbsList() {
  return {
    type: GET_SUBURBS_LIST
  }
}

export function getSuburbsSuccess(response) {
  return {
    type: GET_SUBURBS_SUCCESS,
    response
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}

export function updateSearchData(newSearchData, newPage) {
  return {
    type: UPDATE_SEARCH_DATA,
    newSearchData,
    newPage
  }
}
