import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the devicesListPage state domain
 */

const selectDevicesListPageDomain = state =>
    state.get("devicesListPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by devicesListPage
 */

const makeSelectDevicesListPage = () =>
    createSelector(selectDevicesListPageDomain, substate => substate.toJS());
const makeSelectDevicesList = () =>
    createSelector(selectDevicesListPageDomain, devicesListPage => devicesListPage.get('devicesList'));
export { makeSelectDevicesListPage, makeSelectDevicesList };
export { selectDevicesListPageDomain };
