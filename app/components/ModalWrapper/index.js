/**
 *
 * ModalWrapper
 *
 */
import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';
import ClassName from 'classnames';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
export default class ModalWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
        <Modal
            className={ClassName('modal-wrapper', this.props.popUp && 'popup-wrapper', this.props.loading && 'loading-wrapper')}
            visible={this.props.visible}>
          <ModalBody>
            {!this.props.loading && !this.props.popUp && (
                <div>
                  <header>
                    <div>{props.title}</div>
                  </header>
                  <div className={'children-wrapper'}>{props.children}</div>
                  <footer>
                    <GhostButton text={'cancel'} onClick={props.onCancelClick}/>
                    <PurpleRoundButton text={props.btnText} className={'btn-purple'} onClick={props.onSubmit}
                                       disabled={props.disabled}/>
                  </footer>
                </div>
            )}

            {!this.props.loading && this.props.popUp && (
                <div className={'popup-contain'}>
                  <div className={'popup-title'}>{this.props.title}</div>
                  <div className={'popup-content'}>{this.props.content}</div>
                  <div>{this.props.children}</div>
                  {this.props.cancel && (
                      <GhostButton text={'cancel'} type={'button'} className={'btn-popup'}
                                   onClick={this.props.onCancelClick}/>
                  )}
                  {this.props.btnText && (
                      <PurpleRoundButton text={this.props.btnText} type={this.props.type}
                                         className={`btn-purple btn-popup ${this.props.className}`}
                                         onClick={this.props.onSubmit}
                                         disabled={this.props.disabled}/>
                  )}
                </div>
            )}
            {this.props.loading && (
                <div className={'loading-contain'}>
                  <div>{this.props.children}</div>
                </div>
            )}
          </ModalBody>
        </Modal>
    );
  }
}
