import React from "react";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import InputForm from "components/InputForm";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';

const validateForm = Yup.object().shape({
  commission: Yup.string()
      .matches(/^[0-9]*$/, 'Commission must be a number')
      .test('val', 'Commission must be from 0 to 100', val => 0 <= parseInt(val) && parseInt(val) <= 100)
      .typeError('Commission must be a number'),
});
export default class FixleChargeModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this._disableButton = this._disableButton.bind(this);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'commission'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    return (
        < Modal isOpen={this.props.show} className="logout-modal">
          <ModalBody>
            <Formik
                ref={ref => (this.formik = ref)}
                initialValues={{
                  commission: ''
                }}
                enableReinitialize={true}
                validationSchema={validateForm}
                onSubmit={(e) => {
                  this.props.onSubmit(e.commission);
                }}>
              {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="edit-fixle-charge">
                      <div className="upper">
                        <div className="title">
                          <span>Edit Fixle Charge</span>
                        </div>
                        <div className="description">
                          <InputForm
                              label="commission per booking"
                              name="commission"
                              value={values.commission}
                              error={errors.commission}
                              touched={touched.commission}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              type={'text'}
                              placeholder={'Commission per booking'}
                              onChange={evt => {
                                handleChange(evt);
                                setFieldValue('commission', evt.target.value);
                              }}
                          />
                        </div>
                      </div>
                      <div className="lower">
                        <div className="text-center">
                          <GhostButton className="btn-ghost cancel" title={'cancel'} onClick={this.props.closeModal}
                                       type="button"/>
                          <PurpleRoundButton className="btn-purple-round save" title={'save'} type="submit"
                                             disabled={this._disableButton(values, errors)}/>
                        </div>
                      </div>
                    </div>
                  </form>
              )}
            </Formik>
          </ModalBody>
        </Modal>
    );
  }
}

