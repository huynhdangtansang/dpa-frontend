/**
 *
 * Asynchronously loads the component for TeamListPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
