import React from "react";
import moment from "moment";
import _ from "lodash";
import NeonButton from 'components/NeonButton';
import EstimateForm from './EstimateForm';
import EstimateResponse from './EstimateResponse';
import Rating from './Rating';
import RefundNote from './RefundNote';
import { urlLink } from 'helper/route';

export default class Activity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditEstimate: false
    };
  }

  componentWillMount() {
  }

  formatDeposit = (deposit) => {
    return deposit.slice(0, 1) + parseFloat(deposit.slice(1)).toFixed(2);
  };
  actContent = (act) => {
    let companyLink;
    let clientLink;
    let jobLink;
    let providerLink;

    let { jobId: jobId = {}, content: content = '' } = act;

    let {
      jobName: serviceName = '',
      clientName: clientName = '',
      deposit: deposit = '$0',
      companyInfo: {
        name: companyName = ''
      } = { name: '' },
      assignedTo: {
        fullName: providerNameAccept = ''
      } = { fullName: '' },
      providerInfo: {
        fullName: providerName = ''
      } = { fullName: '' },
      balance: balance = ''
    } = act.data;

    switch (act.type || act.typeActivity || '') {
      case 'updateJob':
        jobLink = urlLink.viewJob + '?id=' + (!_.isUndefined(jobId._id) ? jobId._id : '') + '&tab=2';//tab = 2 is tab overview
        content = content.replace('{jobName}', `<span class="highlight" onclick="hyperlink('${jobLink}');">${serviceName}</span>`);
        content = content.replace('#{jobId}', `<span class="highlight"> #${jobId.jobId}</span>`);
        content = content.replace('{userName}', `<span class="highlight" onclick="hyperlink('${clientLink}');">${clientName}</span>`);
        break;
      case 'newJob':
        jobLink = urlLink.viewJob + '?id=' + (!_.isUndefined(jobId._id) ? jobId._id : '') + '&tab=2';//tab = 2 is tab overview
        clientLink = urlLink.viewClient + '?id=' + (!_.isUndefined(act.data && act.data.clientId) ? act.data.clientId : '');
        content = content.replace('{jobName}', `<span class="highlight" onclick="hyperlink('${jobLink}');">${serviceName}</span>`);
        content = content.replace('#{jobId}', `<span class="highlight"> #${jobId.jobId}</span>`);
        content = content.replace('{clientName}', `<span class="highlight" onclick="hyperlink('${clientLink}');">${clientName}</span>`);
        break;
      case 'topCompany':
        jobLink = urlLink.viewJob + '?id=' + (!_.isUndefined(jobId._id) ? jobId._id : '') + '&tab=2';//tab = 2 is tab overview
        content = content.replace('{jobName}', `<span class="highlight" onclick="hyperlink('${jobLink}');">${serviceName}</span>`);
        content = content.replace('#{jobId}', `<span class="highlight"> #${jobId.jobId}</span>`);
        break;
      case 'cancelJob':
        jobLink = urlLink.viewJob + '?id=' + (!_.isUndefined(jobId._id) ? jobId._id : '') + '&tab=2';//tab = 2 is tab overview
        content = content.replace('{jobName}', `<span class="highlight" onclick="hyperlink('${jobLink}');">${serviceName}</span>`);
        content = content.replace('#{jobId}', `<span class="highlight"> #${jobId.jobId}</span>`);
        break;
      case 'acceptBy':
        providerLink = urlLink.employee + '?id=' + act.data.assignedTo._id;
        companyLink = urlLink.viewCompany + '?id=' + act.data.companyInfo._id;
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerNameAccept}</span>`);
        content = content.replace('{companyName}', `<span class="highlight" onclick="hyperlink('${companyLink}');">${companyName}</span>`);
        break;
      case 'declinedJob':
        companyLink = urlLink.viewCompany + '?id=' + act.data.companyInfo._id;
        providerLink = urlLink.employee + '?id=' + (act.data && !_.isEmpty(act.data.providerInfo) ? act.data.providerInfo._id : '');
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        content = content.replace('{serviceName}', `<span class="highlight" onclick="hyperlink('${companyLink}');">${companyName}</span>`);
        break;
      case 'notifyRunningLate':
        providerName = (act.data && act.data.createBy && act.data.createBy.fullName) ? act.data.createBy.fullName : '';
        providerLink = urlLink.employee + '?id=' + act.data.createBy._id;
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'notifyEstimateMater':
        providerLink = urlLink.employee + '?id=' + (act.data && !_.isEmpty(act.data.providerInfo) ? act.data.providerInfo._id : '');
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'notifyDepositCost':
        providerLink = urlLink.employee + '?id=' + (act.data && !_.isEmpty(act.data.providerInfo) ? act.data.providerInfo._id : '');
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'providerConfirmOriginalForCompany':
        providerName = (act.data && act.data.providerName) ? act.data.providerName : '';
        providerLink = urlLink.employee + '?id=' + (act.data && !_.isEmpty(act.data.providerId) ? act.data.providerId : '');
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'confirmJobWhenDepositMaster':
        clientLink = urlLink.viewClient + '?id=' + (act.data && !_.isEmpty(act.data._id) ? act.data._id : '');
        content = content.replace('{clientName}', `<span class="highlight" onclick="hyperlink('${clientLink}');">${clientName}</span>`);
        content = content.replace('{deposit}', `<span class="highlight">${this.formatDeposit(deposit)}</span>`);
        break;
      case 'providerCompleteMaster':
        providerName = (act.data && act.data.providerName) ? act.data.providerName : '';
        providerLink = urlLink.employee + '?id=' + act.data._id;
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'providerCompleteAndRateMaster':
        clientName = (act.data && act.data.clientInfo) ? act.data.clientInfo.fullName : '';
        clientLink = urlLink.viewClient + '?id=' + act.data.clientInfo._id;
        providerLink = urlLink.employee + '?id=' + act.data.providerInfo._id;
        content = content.replace('{clientName}', `<span class="highlight" onclick="hyperlink('${clientLink}');">${clientName}</span>`);
        content = content.replace('{providerName}', `<span class="highlight" onclick="hyperlink('${providerLink}');">${providerName}</span>`);
        break;
      case 'paymentMaster':
        companyLink = urlLink.viewCompany + '?id=' + act.data.companyInfo._id;
        content = content.replace('{companyName}', `<span class="highlight" onclick="hyperlink('${companyLink}');">${companyName}</span>`);
        content = content.replace('{balance}', `<span class="highlight">$${balance}</span>`);
        break;
      case 'paymentRefundMaster':
        clientName = (act.data && act.data.customerInfo) ? act.data.customerInfo.fullName : '';
        clientLink = urlLink.viewClient + '?id=' + act.data.customerInfo._id;
        content = content.replace('{clientName}', `<span class="highlight" onclick="hyperlink('${clientLink}');">${clientName}</span>`);
        content = content.replace('{balance}', balance);
        break;
    }
    return content;
  };
  iconClassName = (notifyBy) => {
    if (notifyBy === 'job' || notifyBy === 'fixle') {
      return 'icon-job';
    } else if (notifyBy === 'provider') {
      return 'icon-user';
    } else if (notifyBy === 'payment') {
      return 'icon-cheap_2';
    }
  };
  showEstimateForm = (type) => {
    if (type === 'notifyEstimateMater') {
      return true
    }
  };
  showEstimateEdit = (value) => {
    this.setState({ showEditEstimate: value });
  };
  showEstimateResponse = (type) => {
    if (type === 'notifyDepositCost' || type === 'providerConfirmOriginalForCompany' || type === 'providerEditEstimate') {
      return true;
    }
    return false;
  };
  getRate = (act) => {
    if (_.isObject(act.data) && _.isObject(act.data.dataRate) && act.data.dataRate.rate) {
      return act.data.dataRate.rate
    }
    return 0;
  };
  getClient = (act) => {
    if (_.isObject(act.data) && _.isObject(act.data.clientInfo)) {
      return act.data.clientInfo.fullName;
    } else return '';
  };
  getComment = (act) => {
    if (_.isObject(act.data) && _.isObject(act.data.dataRate) && act.data.dataRate.content) {
      return act.data.dataRate.content;
    } else return '';
  };
  getAvatar = (act) => {
    if (_.isObject(act.data) && _.isObject(act.data.clientInfo) && _.isObject(act.data.clientInfo.avatar)) {
      return act.data.clientInfo.avatar.fileName;
    } else return './avatar.jpg';
  };

  render() {
    const act = this.props.activity;
    const isTop = this.props.isTop;
    return (
      <div className="activity-item">
        <div className="time-cell">
          <div className="time">
            <span>{moment(act.createdAt).format('hh:mm A')}</span>
          </div>
        </div>
        <div className="icon-cell">
          <span className={this.iconClassName(act.notifyBy)}></span>
        </div>
        <div className="activity-details">
          <div className="text">
            <div className="activity-type">
              <span>{act.notifyBy}</span>
            </div>
            <div className="activity-content">
              <div className="text-content" dangerouslySetInnerHTML={{ __html: this.actContent(act) }}>
              </div>
              {this.showEstimateResponse(act.type) &&
                <EstimateResponse
                  data={(act.data && act.data.estimation) ? act.data.estimation : {}}
                  showEditEstimate={this.state.showEditEstimate}
                  showModal={() => {
                    this.showEstimateEdit(true);
                  }}
                  closeModal={() => {
                    this.showEstimateEdit(false);
                  }}
                  onSubmit={data => {
                    this.props.updateEstimate(data);
                  }}
                />
              }
              {(act.type === 'providerCompleteAndRateMaster') &&
                <Rating
                  rate={this.getRate(act)}
                  avatar={this.getAvatar(act)}
                  clientName={this.getClient(act)}
                  comment={this.getComment(act)}
                  time={moment(act.createdAt).format('hh:mm A')}
                />
              }
              {(act.type === 'paymentRefundMaster') &&
                <RefundNote note={(act.data && act.data.feedback) ? act.data.feedback : ''} />
              }
              {(act.type === 'notifyEstimateMater') &&
                <EstimateForm
                  data={act.data.estimation}
                  onSubmit={(data) => {
                    this.props.submitEstimate(data);
                  }}
                  disabled={!isTop} />
              }
              {act.data &&
                <div className="action-content modify-job">
                  {act.data.isReassign &&
                    <NeonButton hidden={!act.data.isReassign} className={isTop ? '' : 'disabled'} title={'Re-assign'}
                      onClick={this.props.showReassign} />
                  }
                  {act.data.isCompleteAndPay &&
                    <NeonButton hidden={!act.data.isPayment} className={isTop ? '' : 'disabled'} title={'Complete & Pay'}
                      onClick={this.props.completeAndPay} />
                  }
                  {act.data.isReschedule &&
                    <NeonButton hidden={!act.data.isReschedule} className={isTop ? '' : 'disabled'} title={'Reschedule'}
                      onClick={this.props.showReschedule} />
                  }
                  {act.data.isEditEstimate &&
                    <NeonButton className={isTop ? '' : 'disabled'} title={'Edit Estimate'}
                      onClick={() => {
                        //preventDefault in component NeonButton so not need call in this
                        this.showEstimateEdit(true);
                      }} />
                  }
                  {act.data.isRefund &&
                    <NeonButton className={isTop ? '' : 'disabled'} title={'Refund'} onClick={this.props.showRefund} />
                  }
                  {act.data.isCancel &&
                    <NeonButton hidden={!act.data.isCancel} className={isTop ? '' : 'disabled'} title={'Cancel job'}
                      onClick={this.props.showCancelJob} />
                  }
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}