// import { take, call, put, select } from 'redux-saga/effects';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
//Lib
import _ from "lodash";
import moment from "moment";

export function* apiGetChartByType(data) {
  if (_.isObject(data)) {
    let params = !_.isUndefined(data.searchData) ? data.searchData : {};
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    //yield put(loadRepos());
    const requestUrl = config.serverUrl + data.typeChart + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(data.key + 'Data', response.data.data));
      //yield put(reposLoaded());
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      //yield put(reposLoaded());
    }
  }
}

export function* apiGetDropdown(data) {
  if (_.isObject(data)) {
    let params = !_.isUndefined(data.searchData) ? data.searchData : {};
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    //yield put(loadRepos());
    const requestUrl = config.serverUrl + data.typeDropdown + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      if (data.key === 'currentPeriod')
        yield put(Actions.getDropdownSuccess(data.key, response.data.data));
      else yield put(Actions.getDropdownSuccess(data.key, response.data.data.data));
      //yield put(reposLoaded());
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      //yield put(reposLoaded());
    }
  }
}

export function* apiGetAllData() {
  //yield put(loadRepos());
  yield all(Object.keys(config.api.chart).map((key) => call(apiGetChartByType, {
    typeChart: config.api.chart[key],
    key: key,
    searchData: {
      showWithType: 'daily',
      period: parseInt(moment().format('YYYYY')),
      //month: moment().format('MM').toString(),
      year: parseInt(moment().format('YYYYY'))
    },
  })));
  //load list for each dropdown
  yield all(Object.keys(config.api.statistic).map((key) => call(apiGetDropdown, {
    typeDropdown: config.api.statistic[key],
    key: key,
    searchData: {
      showWithType: 'daily',
      period: parseInt(moment().format('YYYYY')),
      //month: moment().format('MM').toString(),
      year: parseInt(moment().format('YYYYY'))
    },
  })));
}

export function* apiGetSuburb(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/suburb';
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getSuccess('suburbListDropdown', response.data.data));
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      yield put(Actions.getSuccess('suburbListDropdown', []));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_ALL_DATA, apiGetAllData);
  yield takeLatest(Constants.GET_CHART_BY_TYPE, apiGetChartByType);
}
