import { put, takeLatest } from 'redux-saga/effects';
import { GET_JOB, UPDATE_JOB_DESCRIPTION } from './constants';
import { getSuccess, updateDesError } from './actions';
import config from 'config';
import axios from 'axios';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import { goBack } from 'react-router-redux';
import _ from "lodash";

export function* apiGetJob(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.job + '/' + data.jobId;
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateJobDescription(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.job + '/' + data.jobId;
    try {
      yield axios.put(requestUrl, data.updateData);
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.message : error.response.data.errors[0].errorMessage
      }));
      yield put(updateDesError(error.response.data));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_JOB, apiGetJob);
  yield takeLatest(UPDATE_JOB_DESCRIPTION, apiUpdateJobDescription);
}
