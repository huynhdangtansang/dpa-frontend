// import { take, call, put, select } from 'redux-saga/effects';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import * as Actions from './actions';
import * as Constants from './constants';
import config from 'config';
import axios from 'axios';
import { goBack } from 'react-router-redux';

export function* apiGetAllDropdown() {
  //load data is serviceList, clientList, companyList
  yield all([
    call(apiGetService, { searchData: {} }),
    call(apiGetClient, { searchData: { active: true } }),
  ])
}

export function* apiGetService(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.service + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess('serviceListDropdown', response.data.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(Actions.getSuccess('serviceListDropdown', []));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetClient(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.client + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess('clientListDropdown', response.data.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiGetCompany(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
        //this function to process sort by service ID
        function (key) {
          if (key === 'serviceIds') {
            if (params[key].length > 0) {
              return (key + '=' + encodeURIComponent('["' + params[key] + '"]'));
            } else {
              return (key + '=' + esc(params[key]));
            }
          } else {
            return (key + '=' + esc(params[key]));
          }
        }
    ).join('&');
    const requestUrl = config.serverUrl + config.api.company.companies_list + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess('companyListDropdown', response.data.data.data));
      yield put(reposLoaded());
      return (response.data);
    } catch (error) {
      //error will get object empty
      yield put(Actions.getSuccess('companyListDropdown', []));
      yield put(reposLoaded());
      return (error.response);
    }
  }
}

export function* apiGetCompanies(data) {
  if (data) {
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.job + config.api.job_new.get_company + '?serviceId=' + data.serviceId;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getCompaniesComplete(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(Actions.getCompaniesComplete(error.response.data));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetEmployees(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => k + '=' + esc(params[k]))
        .join('&');
    const requestUrl = config.serverUrl + config.api.job + config.api.job_new.get_employee + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getEmployeesComplete(response.data));
    } catch (error) {
      yield put(Actions.getEmployeesComplete(error.response.data));
    }
  }
}

export function* apiAddJob(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.job + '/master/createJob';
    yield put(loadRepos());
    try {
      const response = yield axios.post(requestUrl, data.newJob);
      yield put(Actions.addComplete(response.data));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(Actions.addComplete(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_ALL_DATA_DROPDOWN, apiGetAllDropdown);
  yield takeLatest(Constants.ADD_JOB, apiAddJob);
  yield takeLatest(Constants.GET_COMPANIES, apiGetCompanies);
  yield takeLatest(Constants.GET_EMPLOYEES, apiGetEmployees);
}
