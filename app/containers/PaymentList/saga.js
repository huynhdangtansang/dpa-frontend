// import { take, call, put, select } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from 'lodash';
import * as Actions from './actions';
import * as Constants from './constants';

export function* apiGetPaymentList(data) {
  if (data) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
        (key) => {
          if (key === 'status') {
            let temp = key + '=';
            if (params[key].length > 0) {
              params[key].map((item, index) => {
                if (item !== '') {//other option, not all
                  temp += item.value;
                  if (index !== params[key].length - 1) {
                    temp += ',';
                  }
                }
              });
            }
            return temp;
          } else {
            return (key + '=' + esc(params[key]));
          }
        }
    ).join('&');
    yield put(loadRepos());
    const requestUrl = config.serverUrl + config.api.payment + '?' + query;
    try {
      const response = yield axios.get(requestUrl);
      
      yield put(Actions.getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* getListAgain(data) {
  if (data) {
    yield put(Actions.getList(data.newSearchData));
  }
}

export function* apiRemovePayment(data) {
  if (data.deleteData) {
    const requestUrl = config.serverUrl + config.api.payment + '/removePayouts';
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(Actions.getList(data.searchData));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetPdf(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.payment + '/generatePdfPaymentMany';
    yield put(loadRepos());
    try {
      const response = yield axios.post(requestUrl, { ids: data.serviceId });
      yield put(reposLoaded());
      let array = response.data.data;
      array.forEach(function (file) {
        window.open(file.path);
      });
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_LIST, apiGetPaymentList);
  yield takeLatest(Constants.UPDATE_SEARCH_DATA, getListAgain);
  yield takeLatest(Constants.REMOVE_PAYMENT, apiRemovePayment);
  yield takeLatest(Constants.GET_PDF, apiGetPdf);
}
