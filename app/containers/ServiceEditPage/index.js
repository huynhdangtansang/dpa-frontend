/**
 *
 * ServiceEditPage
 *
 */
//Lib
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import { WithContext as ReactTags } from 'react-tag-input';
//Saga, action, reducer, selector
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectServiceEditPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  addArea,
  addCert,
  addCoupon,
  addTag,
  changeImage,
  deleteTag,
  getServiceDetails,
  resetState,
  showModal,
  updateArea,
  updateCert,
  updateCoupon,
  updateService
} from './actions';
import { loadRepos, updateError } from 'containers/App/actions';
//css
import './style.scss';
import 'components/TagInput/style.scss';
//lib
//Components
import SubmitButton from 'components/SubmitButton';
import InputForm from "components/InputForm";
import InputFile from "components/InputFile";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import CouponItem from "components/CouponItem";
import CertItem from "components/CertItem";
import CropImage from 'components/CropImage';
import InputFormNumberFormat from "components/InputFormNumberFormat";

const validateForm = Yup.object().shape({
  'serviceName': Yup.string()
    .required('')
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid service name (no special characters)')
    .min(8, 'Invalid service name (At least 8 characters)')
    .max(30, 'Invalid service name (Maximum at 30 characters)'),
  'hourlyRate': Yup.string()
    // .matches(/^[0-9]*$/, 'Commission must be a number'),
    .matches(/^-?\d*\.?\d*$/, 'Hourly rate must be a number'),
  'minimumHours': Yup.number()
    // .matches(/^-?\d*\.?\d*$/, 'Minimum hours must be a number')
    .moreThan(0, 'Minimum hours is greater than 0')
    .max(24, 'Maximum of minimum hours is 24 hours')
    .typeError('Minimum hours must be a number'),
  'commission': Yup.string()
    .matches(/^-?\d*\.?\d*$/, 'Commission must be a number')
    .test('val', 'Commission must be from 0 to 100', val => 0 <= parseInt(val) && parseInt(val) <= 100)
    .typeError('Commission must be a number'),
});
const defaultCrop = {
  x: 25,
  y: 25,
  width: 50,
  heigh: 50,
  aspect: 1
};
const rectangleCrop = {
  x: 15,
  y: 13,
  width: 75,
  aspect: 145 / 264
};

/* eslint-disable react/prefer-stateless-function */
export class ServiceEditPage extends React.Component {
  _handleScrollSection = (section) => {
    const { scrollBarRef } = this.props;
    switch (section) {
      case 'info':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.serviceInfoRef) ? this.serviceInfoRef.current.offsetTop : 0);
        break;
      case 'charge':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.fixleChargeRef) ? this.fixleChargeRef.current.offsetTop : 0);
        break;
      case 'coupon':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.couponRef) ? this.couponRef.current.offsetTop : 0);
        break;
      case 'keyword':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.keywordRef) ? this.keywordRef.current.offsetTop : 0);
        break;
      case 'cert':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.certRef) ? this.certRef.current.offsetTop : 0);
        break;
      case 'area':
        scrollBarRef.current.scrollTop(!_.isEmpty(this.areasRef) ? this.areasRef.current.offsetTop : 0);
        break;
      default:
        scrollBarRef.current.scrollTop(0);//scroll to to if reload page
    }
  };
  _handleAddSection = (section) => {
    switch (section) {
      case 'coupon':
        this.props.addCoupon();
        break;
      case 'cert':
        this.props.addCert();
        break;
      case 'area':
        this.props.addArea();
        break;
      default:
        return false;
    }
  };
  _handleImageChange = (e, type) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e, type);
    }
  };
  openFileBrowser = (type) => {
    if (type == 'icon') {
      this.refs.iconUploader.click();
    } else {
      this.refs.backgroundUploader.click();
    }
  };
  uploadImage = (e, type) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        if (type == 'icon') {
          this.setState({
            //iconFile: file,
            cropSrc: fileBase64
          }, () => {
            // this.props.showModal('showIconModal', true);
            //not need drop image
            this.changeIconImage(fileBase64, file);
          });
        } else {
          this.setState({
            //backgroundFile: file,
            cropSrc: fileBase64
          }, () => {
            this.props.showModal('showBackgroundModal', true);
          });
        }
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeImageInStore('backgroundSrc', src);
    this.setState({
      backgroundFile: file,
      cropSrc: '',
    });
  };
  changeIconImage = (src, file) => {
    this.props.changeImageInStore('iconSrc', src);
    this.setState({
      iconFile: file,
      cropSrc: '',
    });
  };
  closeModal = () => {
    this.props.showModal('showBackgroundModal', false);
    this.props.showModal('showIconModal', false);
    this.setState({
      cropSrc: ''
    });
  };
  handleTagsChange = (tagsList) => {
    this.setState({ keywords: tagsList });
  };
  submitForm = (e) => {
    const { couponCodes, certifications, areas, keywords, deletedKeywords } = this.props.serviceeditpage;
    let formData = new FormData();
    if (this.state.iconFile) {
      formData.append('icon', this.state.iconFile);
    }
    if (this.state.backgroundFile) {
      formData.append('background', this.state.backgroundFile);
    }
    formData.append('name', e.serviceName);
    let rateHours = {
      unit: "$",
      value: e.hourlyRate
    };
    formData.append('rateHours', JSON.stringify(rateHours));
    let minCharge = {
      unit: "$",
      value: e.hourlyRate * e.minimumHours
    };
    formData.append('minCharge', JSON.stringify(minCharge));
    let minHours = {
      unit: "",
      value: e.minimumHours,
    };
    formData.append('minHours', JSON.stringify(minHours));
    let commission = {
      unit: "%",
      value: e.commission
    };
    formData.append('commission', JSON.stringify(commission));
    formData.append('couponCodes', JSON.stringify(couponCodes));
    let newKeywords = keywords.concat(deletedKeywords);
    let uploadKeywords = newKeywords.map((keyword) => {
      return {
        id: keyword.id,
        keyName: keyword.text,
        action: keyword.action
      }
    });
    formData.append('keywords', JSON.stringify(uploadKeywords));
    formData.append('certifications', JSON.stringify(certifications));
    let newAreas = areas.map((area) => {
      let suburbs = area.suburbs.split(',');
      return {
        ...area,
        suburbs: suburbs,
      }
    });
    formData.append('areas', JSON.stringify(newAreas));
    this.setState({ updateData: formData }, () => {
      this.props.showModal('showConfirmModal', true);
    })
  };
  invalidCoupon = () => {
    const { couponCodes } = this.props.serviceeditpage;
    let list = couponCodes.filter(coupon => coupon.action !== 'delete');
    if (list.length > 0) {
      let errorList = list.filter(coupon => coupon.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };
  invalidCert = () => {
    const { certifications } = this.props.serviceeditpage;
    let list = certifications.filter(cert => cert.action !== 'delete');
    if (list.length > 0) {
      let errorList = list.filter(cert => cert.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };
  invalidArea = () => {
    const { areas } = this.props.serviceeditpage;
    let list = areas.filter(area => area.action !== 'delete');
    if (list.length > 0) {
      let errorList = list.filter(area => area.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      cropSrc: '',
      radioCrop: {
        x: 0,
        y: 0,
        width: 75,
        aspect: 145 / 264
      },
      showBackgroundModal: false
    };
    this.serviceInfoRef = React.createRef();
    this.fixleChargeRef = React.createRef();
    this.couponRef = React.createRef();
    this.keywordRef = React.createRef();
    this.certRef = React.createRef();
    this.areasRef = React.createRef();
  }

  componentWillMount() {
  }

  componentDidMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let serviceID = temp[1];
    this.props.getService(serviceID)
      .then(() => {
        if (!_.isUndefined(this.props.location.state) && !_.isEmpty(this.props.location.state.sectionRef)) {
          //scroll to section by offsetTop
          this._handleScrollSection(this.props.location.state.sectionRef);
          if (this.props.location.state.action && this.props.location.state.action === 'add') {
            //add item in section if in detail service click to button add
            this._handleAddSection(this.props.location.state.sectionRef);
          }
        }
      }, () => {
      });
  }

  componentWillUnmount() {
    this.props.resetState();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'serviceName',
      'hourlyRate',
      'minimumHours',
      'commission',
      'background',
      'icon'
    ];
    for (let key of keys) {
      if (_.isEmpty(value[key].toString()) || error[key]) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const {
      serviceDetails, couponCodes, errors, keywords, certifications, areas,
      showConfirmModal, showBackgroundModal, showIconModal,
      backgroundSrc, iconSrc
    } = this.props.serviceeditpage;
    let apiErrors = errors;
    return (
      <div className="edit-service-profile">
        <div className="header-edit-add-page edit-service-header">
          <div className="action">
            <div className="return" id='return' onClick={() => {
              this.props.history.goBack();
            }}>
              <span className="icon-arrow-left"></span>
            </div>

            <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return">
              Back</UncontrolledTooltip>
          </div>
          <div className="title">
            <span>Edit Service</span>
          </div>
        </div>
        <Formik ref={ref => {
          this.formik = ref
        }}
          initialValues={{
            serviceName: serviceDetails ? serviceDetails.name : '',
            hourlyRate: serviceDetails ? serviceDetails.rateHours.value : '',
            minimumHours: serviceDetails ? serviceDetails.minHours.value : '',
            minimumCharge: serviceDetails ? serviceDetails.minCharge.value : '',
            commission: serviceDetails ? serviceDetails.commission.value : '',
            icon: iconSrc ? iconSrc : '',
            background: backgroundSrc ? backgroundSrc : ''
          }}
          enableReinitialize={true}
          validationSchema={validateForm}
          onSubmit={(e) => {
            this.submitForm(e);
          }}>
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue
          }) => (
              <form onSubmit={handleSubmit}>
                <div className="content-add-edit edit-service-content">
                  {/* INFO */}
                  <div className="information service-info" ref={this.serviceInfoRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Service information</span>
                        </div>
                      </div>
                      <div className="col-md-5 right service-details">
                        <div className="details-wrapper">
                          <div className="service-icon">
                            <div className="icon-wrapper">
                              <img className="icon-uploaded" src={iconSrc ? iconSrc : './default-user.png'} alt="icon"
                                onError={(e) => {
                                  e.target.onerror = null;
                                  e.target.src = './default-user.png'
                                }} />
                            </div>
                            <div className="change-avatar add-icon">
                              <InputFile
                                name={'icon'}
                                src={iconSrc}
                                onBlur={handleBlur}
                                btnText={_.isEmpty(iconSrc) ? 'Add Service Icon' : 'Change Service Icon'}
                                relatedValues={values}
                                onChange={evt => {
                                  setFieldValue('icon', evt);
                                  this._handleImageChange(evt, 'icon');
                                }} />
                            </div>
                          </div>
                          <div className="details">
                            {/* SERVICE NAME */}
                            <InputForm
                              label="service name"
                              name={'serviceName'}
                              value={values.serviceName}
                              error={errors.serviceName}
                              touched={touched.serviceName}
                              onChange={evt => {
                                handleChange(evt);
                              }}
                              onBlur={handleBlur}
                              placeholder={'Service name'} />

                            {/* HOURLY RATE */}
                            <InputFormNumberFormat
                              label="hourly rate"
                              name={'hourlyRate'}
                              placeholder={'Hourly rate'}
                              type={'input'}
                              prefix={'$'}
                              decimalScale={2}
                              value={values.hourlyRate}
                              error={errors.hourlyRate}
                              touched={touched.hourlyRate}
                              onChange={evt => {
                                setFieldValue('hourlyRate', evt.floatValue);
                              }}
                              onBlur={handleBlur}
                            />

                            {/* MINIMUM HOURS */}
                            <InputForm
                              label="minimum hours"
                              name={'minimumHours'}
                              value={values.minimumHours}
                              error={errors.minimumHours}
                              touched={touched.minimumHours}
                              onChange={evt => {
                                setFieldValue('minimumHours', evt.target.value);
                              }}
                              onBlur={handleBlur}
                              placeholder={'Minimum hours'}
                              type={'text'} />
                            {/* MINIMUM CHARGE */}
                            <InputFormNumberFormat
                              label="minimum charge"
                              name={'minimumCharge'}
                              placeholder={'Minimum hours x hourly rate'}
                              prefix={'$'}
                              decimalScale={2}
                              value={(values.minimumHours === '' || values.hourlyRate === '') ? '' : parseFloat(values.minimumHours * values.hourlyRate).toFixed(2)}
                              error={errors.minimumCharge}
                              touched={touched.minimumCharge}
                              onChange={evt => {
                                setFieldValue('minimumCharge', evt.floatValue);
                              }}
                              onBlur={handleBlur}
                            />
                            {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                              return (
                                <div key={error.errorCode} className="errors">
                                  <span className="icon-error"></span>
                                  <div className="error-item">
                                    <span>{error.errorMessage}</span>
                                  </div>
                                </div>
                              );
                            }) : []}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-3 service-background">
                        <div className="background-wrapper">
                          <div className="title">
                            <span>Background Image</span>
                          </div>
                          <div className="background-container">
                            {backgroundSrc !== '' ?
                              <img className="background-uploaded" src={backgroundSrc} alt="icon" onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = './default-user.png'
                              }} /> :
                              <span className="icon-camera"></span>}
                          </div>
                          <div className="upload-background">
                            <InputFile
                              name={'background'}
                              src={backgroundSrc}
                              onBlur={handleBlur}
                              btnText={_.isEmpty(backgroundSrc) ? 'Upload Bg Image' : 'Change Bg Image'}
                              relatedValues={values}
                              //validateAfterOtherInputFields={true}
                              onChange={evt => {
                                setFieldValue('background', evt);
                                this._handleImageChange(evt, 'background');
                              }} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* CHARGE */}
                  <div className="information fixle-charge" ref={this.fixleChargeRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Fixle charge</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">
                          {/* COMMISSION */}
                          <InputFormNumberFormat
                            label="commission per booking"
                            name={'commission'}
                            value={values.commission}
                            error={errors.commission}
                            touched={touched.commission}
                            suffix={'%'}
                            decimalScale={0}
                            onChange={evt => {
                              setFieldValue('commission', evt.floatValue);
                            }}
                            onBlur={handleBlur}
                            placeholder={'Commission per booking'}
                            type={'input'} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* COUPON */}
                  <div className="information coupon-code" ref={this.couponRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Coupon code</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {couponCodes.map((coupon, index) => {
                          return (
                            <CouponItem
                              key={index}
                              index={index}
                              name={coupon.name}
                              code={coupon.code}
                              discount={coupon.disCount}
                              discountType={coupon.disCountType}
                              expiryDate={coupon.expiredAt}
                              handleChange={(id, type, value, error) => {
                                let newCoupon = coupon;
                                newCoupon[type] = value;
                                if (coupon.id === '') {
                                  newCoupon['action'] = 'add';
                                } else {
                                  newCoupon['action'] = 'edit';
                                }
                                if (newCoupon.disCount !== '' && newCoupon.expiredAt !== '' && error === false) {
                                  newCoupon.error = false;
                                } else {
                                  newCoupon.error = true;
                                }
                                this.props.updateCoupon(id, newCoupon);
                              }}
                              delete={id => {
                                let newCoupon = coupon;
                                coupon['action'] = 'delete';
                                this.props.updateCoupon(id, newCoupon);
                              }}
                              hidden={coupon.action === 'delete'}
                            />
                          )
                        })}
                        <div className="addition-action">
                          <PurpleRoundButton
                            title={'Add Coupon'}
                            className="btn-purple-round add-coupon"
                            type="button"
                            onClick={() => {
                              this.props.addCoupon();
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* KEYWORDS */}
                  <div className="information keywords" ref={this.keywordRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Keywords</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">
                          <div className="info-item">
                            <div className="title">
                              <span>Keywords</span>
                            </div>
                            <div className="data list-tags">
                              <ReactTags
                                autofocus={false}
                                tags={keywords}
                                handleDelete={index => {
                                  this.props.deleteTag(index);
                                }}
                                handleAddition={tag => {
                                  tag.id = '';
                                  tag['action'] = 'add';
                                  this.props.addTag(tag);
                                }}
                                placeholder={'Keywords'}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* CERT */}
                  <div className="information certification" ref={this.certRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Certification required</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {certifications.map((cert, index) => (
                          <CertItem
                            key={cert.id}
                            hidden={cert.action === 'delete'}
                            cert={cert}
                            onChange={(value, error) => {
                              let newItem = cert;
                              newItem.content = value;
                              if (cert.id === '') {
                                newItem.action = 'add';
                              } else {
                                newItem.action = 'edit';
                              }
                              if (error === false && 8 <= newItem.content.length && newItem.content.length <= 30) {
                                newItem.error = false;
                              } else {
                                newItem.error = true;
                              }
                              this.props.updateCert(index, newItem);
                            }}
                            delete={() => {
                              let newItem = cert;
                              newItem.action = 'delete';
                              this.props.updateCert(index, newItem);
                            }}
                          />
                        ))}
                        <div className="addition-action">
                          <PurpleRoundButton title={'Add Certification'}
                            className="btn-purple-round add-certification"
                            onClick={() => {
                              this.props.addCert();
                            }} type="button" />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* AREAS */}
                  <div className="information areas-supplied" ref={this.areasRef}>
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Areas supplied</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {areas.map((area, index) => {
                          return (
                            <div key={area.id !== '' ? area.id : index} className="area-item"
                              hidden={area.action === 'delete'}>
                              <div className="details">
                                <InputForm
                                  label="city and country"
                                  value={area.country}
                                  onChange={evt => {
                                    let newItem = area;
                                    newItem.city = evt.target.value;
                                    newItem.country = evt.target.value;
                                    if (area.id === '') {
                                      newItem.action = 'add';
                                    } else {
                                      newItem.action = 'edit';
                                    }
                                    if (newItem.city === '' || newItem.city === '' || newItem.suburbs === '') {
                                      newItem.error = true;
                                    } else {
                                      newItem.error = false;
                                    }
                                    this.props.updateArea(index, newItem);
                                  }}
                                  placeholder={'City and country'} />
                                <InputForm
                                  label="suburbs"
                                  value={area.suburbs}
                                  onChange={evt => {
                                    let newItem = area;
                                    newItem.suburbs = evt.target.value;
                                    if (area.id === '') {
                                      newItem.action = 'add';
                                    } else {
                                      newItem.action = 'edit';
                                    }
                                    if (newItem.city === '' || newItem.city === '' || newItem.suburbs === '') {
                                      newItem.error = true;
                                    } else {
                                      newItem.error = false;
                                    }
                                    this.props.updateArea(index, newItem);
                                  }}
                                  placeholder={'Suburbs'} />
                              </div>
                              <div className="delete">
                                <div className="icon-contain">
                                  <span className="icon-bin" onClick={() => {
                                    let newItem = area;
                                    newItem.action = 'delete';
                                    this.props.updateArea(index, newItem);
                                  }}></span>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                        <div className="addition-action">
                          <PurpleRoundButton title={'Add Area'} className="btn-purple-round add-area" onClick={() => {
                            this.props.addArea();
                          }} type="button" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer edit-service-footer">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.history.goBack();
                  }} type="button" />
                  <SubmitButton
                    type={'submit'}
                    disabled={this._disableButton(values, errors) || this.invalidCert() || this.invalidCoupon() || this.invalidArea() || keywords.length === 0}
                    content={'Save'} />
                </div>
              </form>
            )}
        </Formik>

        {(showBackgroundModal && this.state.cropSrc !== '') &&
          <CropImage
            show={showBackgroundModal}
            cropSrc={this.state.cropSrc}
            crop={rectangleCrop}
            closeModal={this.closeModal}
            changeImage={this.changeImage}
          />
        }
        {(showIconModal && this.state.cropSrc !== '') &&
          <CropImage
            show={showIconModal}
            cropSrc={this.state.cropSrc}
            crop={defaultCrop}
            closeModal={this.closeModal}
            changeImage={this.changeIconImage}
          />
        }

        <Modal isOpen={showConfirmModal} className="logout-modal">
          <ModalBody>
            <div className="add-success">
              <div className="upper">
                <div className="title">
                  <span>Save This Change</span>
                </div>
                <div className="description">
                  <span>Are you sure want to save this change. This action could influence on all data involved.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.props.showModal('showConfirmModal', false);
                }} />
                <PurpleRoundButton className="btn-purple-round save" title={'Save'} onClick={() => {
                  if (this.state.updateData) {
                    this.props.showModal('showConfirmModal', false);
                    this.props.update(serviceDetails._id, this.state.updateData);
                    this.props.startLoad();
                  }
                }} />
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

ServiceEditPage.propTypes = {
  resetState: PropTypes.func,
  dispatch: PropTypes.func,
  getService: PropTypes.func,
  update: PropTypes.func,
  addCoupon: PropTypes.func,
  updateCoupon: PropTypes.func,
  addCert: PropTypes.func,
  updateCert: PropTypes.func,
  addArea: PropTypes.func,
  updateArea: PropTypes.func,
  changeImage: PropTypes.func,
  startLoad: PropTypes.func,
  addTag: PropTypes.func,
  deleteTag: PropTypes.func,
  showModal: PropTypes.func,
  changeImageInStore: PropTypes.func,
  updateError: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  serviceeditpage: makeSelectServiceEditPage()
});

function mapDispatchToProps(dispatch) {
  return {
    resetState: () => {
      dispatch(resetState());
    },
    getService: (id) => {
      return new Promise((resolve, reject) => {
        dispatch(getServiceDetails({ id, resolve, reject }));
      });
    },
    update: (id, updateData) => {
      dispatch(updateService(id, updateData));
    },
    addCoupon: () => {
      dispatch(addCoupon());
    },
    updateCoupon: (id, newItem) => {
      dispatch(updateCoupon(id, newItem));
    },
    addCert: () => {
      dispatch(addCert());
    },
    updateCert: (id, newItem) => {
      dispatch(updateCert(id, newItem));
    },
    addArea: () => {
      dispatch(addArea());
    },
    updateArea: (id, newItem) => {
      dispatch(updateArea(id, newItem));
    },
    changeImage: (objectType, src) => {
      dispatch(changeImage(objectType, src));
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    addTag: (newTag) => {
      dispatch(addTag(newTag));
    },
    deleteTag: (id) => {
      dispatch(deleteTag(id));
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    changeImageInStore: (key, url) => {
      dispatch(changeImage(key, url));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "serviceEditPage", reducer });
const withSaga = injectSaga({ key: "serviceEditPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(ServiceEditPage);
