/*
 *
 * ServiceEditPage actions
 *
 */
import {
  ADD_AREA,
  ADD_CERT,
  ADD_COUPON,
  ADD_TAG,
  CHANGE_IMAGE,
  DELETE_TAG,
  GET_SERVICE_DETAILS,
  GET_SUCCESS,
  RESET_STATE,
  SHOW_MODAL,
  UPDATE_AREA,
  UPDATE_CERT,
  UPDATE_COUPON,
  UPDATE_SERVICE
} from "./constants";
import moment from 'moment';

export function resetState() {
  return {
    type: RESET_STATE,
  };
}

export function getServiceDetails(param) {
  return {
    type: GET_SERVICE_DETAILS,
    param
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function updateService(id, updateData) {
  return {
    type: UPDATE_SERVICE,
    id,
    updateData
  }
}

export function addCoupon() {
  const newCoupon = {
    id: '',
    name: '',
    code: '',
    disCount: '',
    disCountType: '%',
    expiredAt: moment().add(1, 'd').format('L'),
    action: 'add',
    error: true
  };
  return {
    type: ADD_COUPON,
    newCoupon
  }
}

export function updateCoupon(id, newItem) {
  return {
    type: UPDATE_COUPON,
    id,
    newItem
  }
}

export function addCert() {
  const newCert = {
    id: '',
    content: '',
    action: 'add',
    error: true
  };
  return {
    type: ADD_CERT,
    newCert
  }
}

export function updateCert(id, newItem) {
  return {
    type: UPDATE_CERT,
    id,
    newItem
  }
}

export function addArea() {
  const newArea = {
    id: '',
    country: '',
    city: '',
    suburbs: '',
    action: 'add',
    error: true
  };
  return {
    type: ADD_AREA,
    newArea
  }
}

export function updateArea(id, newItem) {
  return {
    type: UPDATE_AREA,
    id,
    newItem
  }
}

export function changeImage(key, value) {
  return {
    type: CHANGE_IMAGE,
    key,
    value
  }
}

export function addTag(newTag) {
  return {
    type: ADD_TAG,
    newTag
  }
}

export function deleteTag(id) {
  return {
    type: DELETE_TAG,
    id
  }
}

export function showModal(modal, value) {
  return {
    type: SHOW_MODAL,
    modal,
    value
  }
}
