/**
 *
 * RadioButton
 *
 */
import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './style.scss'

function RadioButton(props) {
  return (
      <div className={'form-input radio-btn'}>
        <label className={'radio-item'}>
          <input
              type="checkbox"
              name={props.name}
              checked={props.checked}
              onChange={() => {
                props.onChange(props.value);
              }}
          />
          <span className={'item-label'}>{props.label}</span>
        </label>
      </div>
  );
}

RadioButton.propTypes = {};
export default RadioButton;
