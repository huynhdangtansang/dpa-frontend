/**
 *
 * Selection
 *
 */
// eslint-disable-next-line no-unused-vars
import React from "react";
import Select from 'react-select';
import ClassName from 'classnames';
import './style.scss';
import AsyncSelect from "react-select/lib/Async";
import makeAnimated from "react-select/lib/animated";

function Selection(props) {
  return (
      <div className={ClassName('selection-wrapper', 'single-select', props.period && 'select-period')}>
        {props.title && (
            <label className={'form-label'} htmlFor={props.name}>{props.title}</label>
        )}

        {props.isAsyncList !== true &&
        <Select
            className={`f-select-container input ${props.className}`}
            options={props.options}
            type={props.type}
            value={props.value}
            classNamePrefix={`f-select`}
            name={props.name}
            isSearchable={false}
            closeMenuOnSelect={true}
            closeMenuOnScroll={false}
            onFocus={() => {
              window.isInputFocus = true;
            }}
            {...props}
        />}

        {props.isAsyncList === true && <AsyncSelect
            getOptionLabel
            className={`f-select-container input ${props.className}`}
            options={props.options}
            loadOptions={props.loadOptions}
            isClearable={false}
            type={props.type}
            value={props.value}
            classNamePrefix={`f-select`}
            name={props.name}
            isSearchable={true}
            closeMenuOnSelect={true}
            closeMenuOnScroll={false}
            onFocus={() => {
              window.isInputFocus = true;
            }}
            components={makeAnimated()}
            {...props}
        />}
      </div>
  );
}

Selection.propTypes = {};
export default Selection;
