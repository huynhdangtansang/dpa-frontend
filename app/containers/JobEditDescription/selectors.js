import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the jobEditDescription state domain
 */

const selectJobEditDescriptionDomain = state =>
    state.get("jobEditDescription", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by JobEditDescription
 */

const makeSelectJobEditDescription = () =>
    createSelector(selectJobEditDescriptionDomain, substate => substate.toJS());
export default makeSelectJobEditDescription;
export { selectJobEditDescriptionDomain };
