import { put, takeLatest } from 'redux-saga/effects';
import { ADD_SERVICE } from './constants';
import { addError, addSuccess } from './actions';
import config from 'config';
import axios from 'axios';
import { loadRepos, reposLoaded } from 'containers/App/actions';
import { goBack } from 'react-router-redux';

export function* apiAddNewService(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.service;
    try {
      yield put(loadRepos());
      const response = yield axios.post(requestUrl,
          data.data,
      );
      yield put(addSuccess(response.data));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(addError(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ADD_SERVICE, apiAddNewService)
}
