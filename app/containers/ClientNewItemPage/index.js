/**
 *
 * ClientNewItemPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
//lib
import _, { debounce, findIndex, indexOf } from "lodash";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectClientNewItemPage, makeSelectErrors } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { addClient, changeAvatar, resetError } from "./actions";
import { loadRepos, updateError } from 'containers/App/actions';
//data
import { country, listError } from 'helper/data';
import { loadCountries, removePlus } from 'helper/exportFunction';
import './style.scss';
import InputForm from "components/InputForm";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import SubmitButton from 'components/SubmitButton';
import CropImage from 'components/CropImage';
import AddressItem from 'components/AddressItem';
import Selection from 'components/Selection';
import InputFile from "components/InputFile";

const validateForm = Yup.object().shape({
  'name': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (no special characters)')
      .min(6, 'Invalid name (At least 6 characters)')
      .max(30, 'Invalid name (Maximum at 30 characters)'),
  'email': Yup.string()
      .email('Invalid email'),
  'phoneNumber': Yup.string()
      .matches(/^[0-9]*$/, 'Invalid phone number')
      .min(5, 'Invalid phone number'),
});

/* eslint-disable react/prefer-stateless-function */
export class ClientNewItemPage extends React.PureComponent {
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeClientAvatar(src);
    this.setState({
      avatar: src,
      cropSrc: '',
      avatarFile: file
    });
  };
  getContactCountryValue = (value) => {
    this.setState({ contactCountry: value });
  };
  closeModal = () => {
    this.setState({
      showAvatarModal: false,
      cropSrc: ''
    });
  };
  invalidAddress = () => {
    const { addressList } = this.state;
    let list = addressList;
    if (list.length > 0) {
      let errorList = list.filter(address => address.error === true);
      if (errorList.length === 0) {
        return false;
      }
    }
    return true;
  };
  addAddress = () => {
    const { addressList } = this.state;
    let list = addressList;
    let newAddress = {
      isPrimary: false,
      error: true
    };
    if (list.length === 0) {
      newAddress.isPrimary = true;
      this.setState({ addressList: [...list, newAddress] });
    } else {
      this.setState({ addressList: [...list, newAddress] });
    }
  };
  deleteAddress = (id) => {
    const { addressList } = this.state;
    let list = addressList;
    this.setState({ addressList: list.filter((address, index) => index !== id) }, () => {
      if (list[id].isPrimary == true) {
        if (list.length > 1) {
          this.setAddressPrimary(0);
        }
      }
    })
  };
  setAddressPrimary = (id) => {
    const { addressList } = this.state;
    let list = addressList;
    let newAddress = list[id];
    newAddress.isPrimary = true;
    this.setState({
      addressList: list.map((address, index) => {
        if (index == id) {
          return {
            ...address,
            newAddress
          }
        } else {
          return {
            ...address,
            isPrimary: false
          }
        }
      })
    });
  };
  updateAddressItem = (id, value) => {
    const { addressList } = this.state;
    let list = addressList;
    this.setState({
      addressList: list.map((address, index) => {
        if (index === id) {
          for (let key in value) {
            address[key] = value[key];
          }
          address.error = false;
          return address;
        } else {
          return address;
        }
      })
    });
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
    } else {
      this.uploadImage(e);
    }
  };
  //Clear api error
  clearApiError = (type) => {
    const { apiErrors } = this.props.clientnewitempage;
    if (apiErrors && apiErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, apiErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      avatar: '',
      showAvatarModal: false,
      addressList: [],
      showConfirmModal: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    this.props.resetError();
  }

  componentDidMount() {
    this.addAddress();
    this.props.changeClientAvatar('');
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'avatar',
      'name',
      'email',
      'avatar',
      'phoneNumber'
    ];
    for (let key of keys) {
      if ((value[key] === null || error[key] || !value[key].toString())) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { avatar, apiErrors } = this.props.clientnewitempage;
    return (
        <div className="new-client">
          <div className="header-edit-add-page new-client-header">
            <div className="action">
              <div className="return" id="return" onClick={() => {
                this.props.history.goBack();
              }}>
                <span className="icon-arrow-left"></span>
              </div>
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
            </div>
            <div className="title">
              <span>New Client</span>
            </div>
          </div>
          <input type="file" id="file" ref="fileUploader" style={{ display: "none" }}
                 onChange={(e) => {
                   this.uploadImage(e);
                   e.target.value = null;
                 }}
          />

          <Formik ref={ref => {
            this.formik = ref
          }}
                  initialValues={{
                    name: '',
                    email: '',
                    phoneNumber: '',
                    avatar: '',
                    status: { label: 'Active', value: true },
                    contactArea: {
                      value: 61,
                      label: 'Australia'
                    },
                  }}
                  enableReinitialize={true}
                  validationSchema={validateForm}
                  onSubmit={(e) => {
                    let formData = new FormData();
                    if (this.state.avatarFile) {
                      formData.append('file', this.state.avatarFile);
                    }
                    formData.append('fullName', e.name);
                    formData.append('email', e.email);
                    formData.append('countryName', e.contactArea.label);
                    formData.append('countryCode', '+' + e.contactArea.value);
                    if (e.phoneNumber.substring(0, 1) === '0') {
                      e.phoneNumber = e.phoneNumber.substring(1);
                    } else {
                      e.phoneNumber = e.phoneNumber;
                    }
                    formData.append('phoneNumber', e.phoneNumber);
                    formData.append('active', e.status);
                    formData.append('addresses', JSON.stringify(this.state.addressList));
                    this.setState({ addData: formData }, () => {
                      this.setState({ showConfirmModal: true });
                    });
                  }}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="content-add-edit new-client-content">
                    <div className="information personal-information">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Personal Information</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          <div className="avatar-image client-avatar form-input">
                            <div className="avatar-wrapper">
                              {avatar === '' ? <span className="icon-camera"></span> :
                                  <img src={avatar}
                                       onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = './default-user.png'
                                       }}
                                       alt="avatar"
                                  />}
                            </div>

                            <InputFile name={'avatar'}
                                       src={avatar ? avatar : ''}
                                       onBlur={handleBlur}
                                       btnText={!_.isEmpty(avatar) ? 'change avatar' : 'upload avatar'}
                                       relatedValues={values}
                                       validateAfterOtherInputFields={true}
                                       onChange={evt => {
                                         this.props.changeClientAvatar('');
                                         setFieldValue('avatar', evt);
                                         this._handleImageChange(evt);
                                       }}/>
                            {}
                          </div>
                          <div className="details">
                            {/* FULL NAME */}
                            <InputForm
                                label="name"
                                name={'name'}
                                value={values.name}
                                error={errors.name}
                                apiError={apiErrors}
                                touched={touched.name}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('name');
                                }}
                                onBlur={handleBlur}
                                placeholder={'Full name'}
                            />

                            {/* EMAIL */}
                            <InputForm
                                label="email"
                                name={'email'}
                                value={values.email}
                                error={errors.email}
                                apiError={apiErrors}
                                touched={touched.email}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('email');
                                }}
                                onBlur={handleBlur}
                                placeholder={'you@example.com'}
                            />

                            {/* CONTACT NUMBER */}
                            <div className='input-group form-input'>
                              {}
                              <div className="info-item contact-number">
                                <Selection options={country}
                                           title={'Contact number'}
                                           name={'contactArea'}
                                           value={values.contactArea}
                                           isAsyncList={true}
                                           loadOptions={debounce(loadCountries, 700)}
                                           getOptionLabel={option => option.label}
                                           getOptionValue={option => option.value}
                                           type={'contactArea'}
                                           onBlur={handleBlur}
                                           onChange={option => {
                                             setFieldValue('contactArea', {
                                               value: option.country_code,
                                               label: option.label
                                             });
                                           }}/>
                                <InputForm
                                    name='phoneNumber'
                                    prependLabel={'+' + removePlus(values.contactArea.value)}
                                    value={values.phoneNumber}
                                    error={errors.phoneNumber}
                                    apiError={apiErrors}
                                    touched={touched.phoneNumber}
                                    onChange={evt => {
                                      handleChange(evt);
                                      this.clearApiError('phoneNumber');
                                    }}
                                    onBlur={handleBlur}
                                    placeholder={'Phone number'}
                                />
                              </div>
                            </div>

                            {/* STATUS */}
                            <Selection
                                options={[
                                  { label: 'Active', value: true },
                                  { label: 'Inactive', value: false },
                                ]}
                                name={'status'}
                                title={'status'}
                                selectTabIndex={5}
                                value={values.status}
                                getOptionLabel={option => option.label}
                                getOptionValue={option => option.value}
                                type={'status'}
                                onBlur={handleBlur}
                                onChange={option => {
                                  setFieldValue('status', option);
                                  this.getStatusValue(option);
                                }}/>
                            {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                              return (
                                  <div key={error.errorCode} className="errors">
                                    <span className="icon-error"></span>
                                    <div className="error-item">
                                      <span>{error.errorMessage}</span>
                                    </div>
                                  </div>
                              );
                            }) : []}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="information address-details">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Address Details</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          {this.state.addressList.map((address, index) => {
                            return (
                                <AddressItem
                                    name='address'
                                    key={index}
                                    index={index}
                                    id={index}
                                    data={address}
                                    error={errors.address}
                                    delete={this.deleteAddress}
                                    setPrimary={this.setAddressPrimary}
                                    handleChange={(index, value) => {
                                      let newAddress = { ...address, ...value };
                                      if (address.id === '') {
                                        newAddress.action = 'add';
                                      } else {
                                        newAddress.action = 'edit';
                                      }
                                      newAddress.error = false;
                                      const { addressList } = this.state;
                                      let list = addressList; //creates the clone of the state
                                      list[index] = newAddress;
                                      this.setState({ addressList: list });
                                      this.forceUpdate();
                                    }}
                                    deactiveAddress={(index) => {
                                      const { addressList } = this.state;
                                      let list = addressList;
                                      list[index].error = true;
                                      this.setState({ addressList: list });
                                      this.forceUpdate();
                                    }}
                                    className={'address-item'}/>
                            );
                          })}
                          <div className="addition-action">
                            <PurpleRoundButton
                                title={'New Address'}
                                className="btn-purple-round add-coupon"
                                type="button"
                                onClick={() => {
                                  this.addAddress();
                                }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="footer new-client-footer">
                    <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                      this.props.history.goBack();
                    }} type='button'/>
                    <SubmitButton type={'submit'}
                                  disabled={this._disableButton(values, errors, touched) || this.invalidAddress()}
                                  content={'Save'}/>
                  </div>
                </form>
            )}
          </Formik>

          {(this.state.cropSrc && this.state.cropSrc !== '') ?
              <CropImage
                  show={this.state.showAvatarModal}
                  cropSrc={this.state.cropSrc}
                  closeModal={this.closeModal}
                  changeImage={this.changeImage}
              />
              : []
          }
          <Modal isOpen={this.state.showConfirmModal} className="logout-modal update-confirm">
            <ModalBody>
              <div className="upper">
                <div className="title">
                  <span>Save This Client</span>
                </div>
                <div className="description">
                  <span>Are you sure want to save this client. This action could influence on all data influence.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.setState({ showConfirmModal: false });
                }}/>
                <PurpleRoundButton className="btn-purple-round sign-out"
                                   title={'Save'}
                                   onClick={() => {
                                     if (this.state.addData) {
                                       this.props.Add(this.state.addData);
                                       this.setState({ showConfirmModal: false });
                                       this.props.startLoad();
                                     }
                                   }}/>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

ClientNewItemPage.propTypes = {
  dispatch: PropTypes.func,
  Add: PropTypes.func,
  changeClientAvatar: PropTypes.func,
  startLoad: PropTypes.func,
  updateError: PropTypes.func,
  resetError: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  clientnewitempage: makeSelectClientNewItemPage(),
  errors: makeSelectErrors()
});

function mapDispatchToProps(dispatch) {
  return {
    Add: (profile) => {
      dispatch(addClient(profile));
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    resetError: () => {
      dispatch(resetError());
    },
    changeClientAvatar: (url) => {
      dispatch(changeAvatar(url));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "clientNewItemPage", reducer });
const withSaga = injectSaga({ key: "clientNewItemPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ClientNewItemPage);
