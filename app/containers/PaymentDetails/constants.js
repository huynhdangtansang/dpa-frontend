/*
 *
 * PaymentDetails constants
 *
 */
export const DEFAULT_ACTION = "app/PaymentDetails/DEFAULT_ACTION";
export const GET_PAYMENT_DETAILS = "/PaymentDetails/GET_PAYMENT_DETAILS";
export const GET_SUCCESS = "/PaymentDetails/GET_SUCCESS";
export const GET_ERROR = "/PaymentDetails/GET_ERROR";
export const CHANGE_PAYMENT_STATUS = "/PaymentDetails/CHANGE_PAYMENT_STATUS";
export const REMOVE_PAYMENT = "/PaymentDetails/REMOVE_PAYMENT";
export const DELETE_SUCCESS = "/PaymentDetails/DELETE_SUCCESS";
export const DELETE_ERROR = "/PaymentDetails/DELETE_PAYMENT";
export const ADD_IMAGE = "/PaymentDetails/ADD_IMAGE";
export const REMOVE_IMAGE = "JobEdtiDescription/REMOVE_IMAGE";
export const ADD_NOTE = "JobEditDescription/ADD_NOTE";
export const DELETE_NOTE = "JobEditDescription/DELETE_NOTE";
export const GET_PDF = "JobEditDescription/GET_PDF";