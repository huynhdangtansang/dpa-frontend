/*
 *
 * TeamNewItemPage constants
 *
 */
export const DEFAULT_ACTION = "app/TeamNewItemPage/DEFAULT_ACTION";
export const ADD_MEMBER = "TeamNewItemPage/ADD_MEMBER";
export const ADD_SUCCESS = "TeamNewItemPage/ADD_SUCCESS";
export const ADD_ERROR = "TeamNewItemPage/ADD_ERROR";
export const RESET_STATE = "TeamNewItemPage/RESET_STATE";
export const RESET_ERROR = "TeamNewItemPage/SET_ERROR";
export const CHANGE_AVATAR = "TeamNewItemPage/CHANGE_AVATAR";
