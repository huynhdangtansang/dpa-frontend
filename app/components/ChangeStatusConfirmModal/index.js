import React from "react";
import TextareaCounter from 'components/TextareaCounter';
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';

export default class ChangeStatusConfirmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  title = (object, action) => {
    let objectString = '';
    if (object === 'company') {
      (action === 'single') ? objectString = object : objectString = 'companies';
    } else {
      (action === 'single') ? objectString = object : objectString = object + 's';
    }
    return objectString.charAt(0).toUpperCase() + objectString.slice(1);
  };
  description = (object, currentStatus, action) => {
    let firstString = '';
    let objectString = '';
    if (object === 'company') {
      (action === 'single') ? objectString = object : objectString = 'companies';
    } else {
      (action === 'single') ? objectString = object : objectString = object + 's';
    }
    firstString = 'Are you sure to want to ' + (currentStatus === true ? 'deactivate ' : 'activate ') + (action === 'single' ? 'this ' : 'these ') + objectString + '? ';
    let secondString = '';
    if (object === 'client') {
      secondString = (action === 'single' ? 'This ' : 'These ') + objectString +
          (currentStatus === true ? ` will not able to make booking and all ${(action === 'single' ? 'his' : 'their')} recent transaction${(action === 'single' ? '' : 's')} will be cancelled.` :
              ' will able to get jobs again.');
    } else if (object === 'member') {
      secondString = 'This action could influence on all datas involved such as jobs, companies, support ticket.';
    } else if (object === 'employee') {
      secondString = (action === 'single' ? 'This ' : 'These ') + objectString +
          (currentStatus === true ? ` will not able to get any jobs and all ${(action === 'single' ? 'his' : 'their')} recent jobs will be cancelled.` :
              ' will able to get jobs again.');
    } else {
      secondString = (action === 'single' ? 'This ' : 'These ') + objectString +
          (currentStatus === true ? ` will not able to get any jobs and all its recent jobs will be cancelled.` :
              ' will able to get jobs again.');
    }
    return firstString + secondString;
  };
  placeholder = (object, action) => {
    let objectString = '';
    if (object === 'company') {
      (action === 'single') ? objectString = object : objectString = 'companies';
    } else {
      (action === 'single') ? objectString = object : objectString = object + 's';
    }
    return (action === 'single' ? 'Why is this ' : 'Why are these ') + objectString + ' deactivated?';
  };

  render() {
    return (
        <Modal isOpen={this.props.show} className="logout-modal change-status-modal">
          <ModalBody>
            <div className="change-status">
              <div className="upper">
                <div className="title">
                  <span>{this.props.currentStatus ? 'Deactivate' : 'Activate'} {this.props.action === 'single' ? 'This' : 'These'} {this.title(this.props.object, this.props.action)}</span>
                </div>
                <div className="description">
                  <p>
                    {this.description(this.props.object, this.props.currentStatus, this.props.action)}
                  </p>
                  {this.props.currentStatus ?
                      <div>
                        <TextareaCounter placeholder={this.placeholder(this.props.object, this.props.action)}
                                         title={'Reason'}
                                         rows={8}
                                         maxLength={50}
                                         value={this.props.reason}
                                         onChange={(e) => this.props.handleChange(e.target.value)}/>
                      </div>
                      : []
                  }
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'}
                             onClick={() => {
                               this.props.closeModal();
                             }}/>

                <PurpleRoundButton className="btn-purple-round sign-out"
                                   type="button"
                                   title={this.props.currentStatus ? 'Deactivate' : 'Activate'}
                                   disabled={(this.props.reason.length > 0 && this.props.currentStatus === true) || (this.props.currentStatus === false) ? false : true}
                                   onClick={() => {
                                     this.props.closeModal();
                                     this.props.changeStatus();
                                   }}/>
              </div>
            </div>
          </ModalBody>
        </Modal>
    )
  }
}