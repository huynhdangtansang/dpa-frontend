/**
 *
 * CompanyTabOverview
 *
 */
import React from 'react';
//Component
import Switch from 'react-switch';
import ClassNames from 'classnames';
import TextareaCounter from 'components/TextareaCounter';
import PurpleRoundButton from 'components/PurpleRoundButton';
import ChangeTopPriorityConfirmModal from 'components/ChangeTopPriorityConfirmModal';
import ActivitiesCompany from 'components/ActivitiesCompany';
//Library
import _ from "lodash";
import moment from 'moment';

/* eslint-disable react/prefer-stateless-function */
class CompanyTabOverview extends React.PureComponent {
  editInfo = () => {
    const { companyData } = this.props.companydetailspage;
    this.props.history.push('/dashboard/companies/company-edit-profile?id=' + companyData._id);
  };

  constructor(props) {
    super(props);
    this.state = {
      notes: ''
    };
  }

  render() {
    const { companyData, showConfirmPriority, selectedServicePriority, FORMATCURRENCY, activityList } = this.props.companydetailspage;
    const { address: { name: addressName = '' } } = companyData;

    return (
      <div className="overview content-details-with-activities">
        {companyData && (
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                {/* Company info */}
                <div className="information company-info">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Company Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="company-logo">
                        <img src={companyData.logo ? companyData.logo.fileName : './default-user.png'} alt="logo"
                          onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }} />
                      </div>
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Company Name</span>
                          </div>
                          <div className="data">
                            <span>{companyData.name}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>abn number</span>
                          </div>
                          <div className="data">
                            <span>{companyData.abnNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>company address</span>
                          </div>
                          <div className="data">
                            <span>{addressName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Company Contact Person</span>
                          </div>
                          <div className="data">
                            <span>{companyData.contactPerson ? companyData.contactPerson : (companyData.createdBy ? companyData.createdBy.fullName : '')}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Contact number</span>
                          </div>
                          <div className="data">
                            <span>{companyData.countryCode ? companyData.countryCode : ''}{companyData.phoneNumber ? companyData.phoneNumber : ''}</span>
                          </div>
                        </div>
                        {companyData.isApproved && (
                          <div className="info-item">
                            <div className="title">
                              <span>About us</span>
                            </div>
                            <div className="data">
                              <span>{companyData.description}</span>
                            </div>
                          </div>
                        )}
                        {companyData.isApproved && (
                          <div className="info-item">
                            <div className="title">
                              <span>Status</span>
                            </div>
                            <div className="data">
                              <span>{companyData.active ? 'Active' : 'Deactive'}</span>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>

                {/* Service For Company has been approved*/}
                <div className="information" hidden={!companyData.isApproved}>
                  <div className="title">
                    <span>Services</span>
                  </div>
                  <div className="list table-assigned">
                    <table>
                      <thead className="title">
                        <tr>
                          <td className="service"><span>Services</span></td>
                          <td className="top-priority"><span>Top Priority</span></td>
                        </tr>
                      </thead>

                      <tbody className="content">
                        {companyData.topPriority && companyData.topPriority.map((item, index) => (
                          <tr key={item._id}>
                            <td className="service">
                              <span>{item.serviceId ? item.serviceId.name : ''}</span>
                            </td>
                            <td className="top-priority">
                              <Switch
                                onChange={() => {
                                  this.props.toggleTopPriorityConfirmModal(true, index);
                                }}
                                checked={item.isPriority}
                                onColor="#917bd5"
                                offColor="#212121"
                                height={15}
                                width={27}
                                handleDiameter={10}
                                uncheckedIcon={false}
                                checkedIcon={false}
                                className="react-switch"
                              />
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>

                {/* Service For Company in approval area*/}
                <div className="information" hidden={companyData.isApproved}>
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Services</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="list list-services-approval">
                        {companyData.services && companyData.services.map((item) => (
                          <div className="item-service" key={item._id}>
                            <img className="logo-service"
                              src={item.icon ? item.icon.fileName : './default-user.png'}
                              onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = './default-user.png'
                              }}
                            ></img>
                            <span className="name-service">{item.name}</span>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="icon-contain" onClick={() => {
                    this.editInfo();
                  }}>
                    <span className="icon-edit"></span>
                  </div>
                </div>

                {/* Accept Rate Card */}
                <div className="information">
                  <div className="title">
                    <span>Accept Rate Card</span>
                  </div>
                  <div className="list table-assigned">
                    <table>
                      <thead className="title">
                        <tr>
                          <td className="service"><span>Services</span></td>
                          <td className=""><span>hourly rate</span></td>
                          <td className=""><span>minimum hours</span></td>
                          <td className=""><span>minimum charge</span></td>
                          <td className=""><span>fixle charge</span></td>
                          <td className="top-priority"><span>Acceptance</span></td>
                        </tr>
                      </thead>

                      <tbody className="content">
                        {companyData.acceptRateCards && companyData.acceptRateCards.map((item) => (
                          item.serviceId &&
                          <tr key={item.serviceId._id}>
                            <td className="service">
                              <span>{item.serviceId.name}</span>
                            </td>
                            <td className="">
                              <span>{item.serviceId.rateHours.unit + FORMATCURRENCY.format(item.serviceId.rateHours.value)}</span>
                            </td>
                            <td className="">
                              <span>{item.serviceId.minHours.value}</span>
                            </td>
                            <td className="">
                              <span>{item.serviceId.minCharge.unit + FORMATCURRENCY.format(item.serviceId.minCharge.value)}</span>
                            </td>
                            <td className="">
                              <span>{item.serviceId.commission.value}{item.serviceId.commission.unit}</span>
                            </td>
                            <td className="top-priority">
                              <Switch
                                onChange={() => {
                                }}
                                checked={item.isAccept ? item.isAccept : false}
                                disabled
                                onColor="#917bd5"
                                offColor="#212121"
                                height={15}
                                width={27}
                                handleDiameter={10}
                                uncheckedIcon={false}
                                checkedIcon={false}
                                className="react-switch"
                              />
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                  <div className="icon-contain" onClick={() => {
                    this.editInfo();
                  }}>
                    <span className="icon-edit"></span>
                  </div>
                </div>

                {/* Lisence */}
                <div className="information">
                  <div className="title">
                    <span>Licenses and Certifications</span>
                  </div>
                  <div className="list table-lisence">
                    <table>
                      <thead className="title">
                        <tr>
                          <td className=""><span>items</span></td>
                          <td className="certifications"><span>certifications required</span></td>
                          <td className=""><span>expiry date</span></td>
                          <td className=""><span>proof</span></td>
                        </tr>
                      </thead>

                      <tbody className="content">
                        {companyData.licenses && companyData.licenses.map((item) => (
                          <tr key={item._id}>
                            <td className="">
                              <span>{item.serviceId ? item.serviceId.name : '-'}</span>
                            </td>
                            <td className="">
                              <span>{item.certificationId ? item.certificationId.content : '-'}</span>
                            </td>
                            <td className="">
                              <span>{item.expiredAt ? moment(item.expiredAt).format('ddd D MMM YYYY') : '-'}</span>
                            </td>
                            <td className="column-img">
                              <div className="fixle-list-image-overflow">
                                {!item.proofs.length ? '-' : item.proofs.map((image) => (
                                  <img key={image._id} src={image.fileName} onError={(e) => {
                                    e.target.onerror = null;
                                    e.target.src = './default-user.png'
                                  }} />
                                ))}
                              </div>
                            </td>
                          </tr>
                        ))}

                      </tbody>
                    </table>
                  </div>
                </div>

                {/* Bank Details */}
                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Bank Details</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      {_.isArray(companyData.banks) && companyData.banks.length > 0 && (
                        <div className="details bank-item" key={companyData.banks[0]._id}>
                          <div className="info-item">
                            <div className="title">
                              <span>account name</span>
                            </div>
                            <div className="data">
                              <span>{companyData.banks[0].accountName}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>bank name</span>
                            </div>
                            <div className="data">
                              <span>{companyData.banks[0].bankName}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>BSB number</span>
                            </div>
                            <div className="data">
                              <span>{companyData.banks[0].bsbNumber}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Account Number</span>
                            </div>
                            <div className="data">
                              <span>{companyData.banks[0].accountNumber}</span>
                            </div>
                          </div>
                        </div>
                      )}

                    </div>
                  </div>
                </div>

                {/* Region and Hours */}
                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Region and Hours</span>
                      </div>
                    </div>
                    {_.isArray(companyData.areas) && companyData.areas.length > 0 && (
                      <div className="col-md-8 right">
                        <div className="details">
                          <div className="info-item">
                            <div className="title">
                              <span>Time zone</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas[0].timeZone ? companyData.areas[0].timeZone : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Country</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas[0].country ? companyData.areas[0].country : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>City</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas[0].city ? companyData.areas[0].city : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Suburbs</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas ? (_.isArray(companyData.areas[0].suburbs) ? companyData.areas[0].suburbs.join(', ') : companyData.areas[0].suburbs) : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Time format</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas[0].formatTime ? companyData.areas[0].formatTime : ''}</span>
                            </div>
                          </div>
                          <div className="info-item">
                            <div className="title">
                              <span>Data format</span>
                            </div>
                            <div className="data">
                              <span>{companyData.areas[0].formatDay ? companyData.areas[0].formatDay : ''}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="icon-contain" onClick={() => {
                    this.editInfo();
                  }}>
                    <span className="icon-edit"></span>
                  </div>
                </div>
              </div>
              <div className={ClassNames("col-md-4 right-part", companyData.isApproved ? 'activities' : 'notes')}>
                {companyData.isApproved ?
                  <div className="information">
                    <div className="title">
                      <span>Activities</span>
                    </div>
                    {!_.isEmpty(activityList) && _.isArray(activityList) && activityList.map((perday) => (
                      <div className="time-count" key={perday.day}>
                        <div className="section">
                          <div
                            className="date">{moment(perday.day).isSame(moment().format('MM/DD/YYYY')) ? 'Today' : perday.day}</div>
                          <div className="list-activities">
                            {perday.activities.map((activity) => (
                              <ActivitiesCompany
                                key={activity._id}
                                activity={activity}
                              />
                            ))}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                  :
                  <div className="information">
                    <div className="title">
                      <span>Quick Notes</span>
                    </div>
                    <div className="details new-note">

                      <TextareaCounter placeholder={'Type something'} title={'Your note'}
                        rows={7}
                        maxLength={50}
                        value={this.props.companydetailspage.note}
                        onChange={(e) => {
                          this.props.handleChangeNote(e.target.value);
                        }} />

                      <PurpleRoundButton
                        className="btn-purple-round"
                        title={'Submit'}
                        disabled={this.props.companydetailspage.note.length <= 0}
                        onClick={(e) => {
                          e.preventDefault();
                          this.props.createNote(companyData._id, this.props.companydetailspage.note);
                        }} />
                    </div>

                    <div className="list-old-note">
                      {!_.isEmpty(companyData.notes) && companyData.notes.map(item => (
                        <div key={item._id} className="item-note row align-items-center">
                          <div className="col-11 p-0">
                            <div className="content">
                              {item.content}
                            </div>
                            <div className="create">
                              {moment(item.createdAt).format("h:mm A, ddd D MMM YYYY")} by <span
                                className="purple-color">{item.createdBy.fullName}</span>
                            </div>
                          </div>
                          <div className="col-1 p-0 text-center remove">
                            <i className="icon-bin pull-right"
                              onClick={(e) => {
                                e.preventDefault();
                                this.props.deleteNote(companyData._id, item._id);
                              }}></i>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                }

              </div>
            </div>

            <ChangeTopPriorityConfirmModal
              show={showConfirmPriority}
              currentStatus={selectedServicePriority ? selectedServicePriority.isPriority : false}
              closeModal={() => {
                //e.preventDefault();
                this.props.toggleTopPriorityConfirmModal(false, -1);
              }}
              changeStatus={() => {
                this.props.toggleTopPriorityConfirmModal(false, -1);
                this.props.setTopPriority(companyData._id, selectedServicePriority.serviceId._id, !selectedServicePriority.isPriority);
              }}
            />
          </div>
        )}
      </div>
    );
  }
}

CompanyTabOverview.propTypes = {};
export default CompanyTabOverview;
