import { fromJS } from 'immutable';
import paymentDetailsReducer from '../reducer';

describe('paymentDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(paymentDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
