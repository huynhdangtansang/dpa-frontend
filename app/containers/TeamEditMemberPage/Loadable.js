/**
 *
 * Asynchronously loads the component for TeamEditMemberPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
