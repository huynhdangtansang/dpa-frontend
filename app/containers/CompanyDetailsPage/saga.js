// import { take, call, put, select } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import { goBack } from 'react-router-redux';
import * as Constants from './constants';
import * as Actions from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";

export function* apiGetCompany(data) {
  const { resolve, reject, id: id = '' } = data;
  if (id) {
    //this need load spinner when reload this page or first time load info
    if (_.isFunction(resolve) && _.isFunction(reject))
      yield put(loadRepos());
    try {
      const requestUrl = config.serverUrl + config.api.company.companies_details + id;
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(response.data));
      if (_.isFunction(resolve))
        resolve(response.data.data);
      yield put(reposLoaded());
    } catch (error) {
      yield put(reposLoaded());
      yield put(Actions.getError(error));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      if (_.isFunction(reject))
        reject(error.response.data);
    }
  }
}

export function* apiGetEmployee(data) {
  if (data) {
    let params = !_.isEmpty(data.searchData) ? data.searchData : {};
    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => k + '=' + esc(params[k]))
      .join('&');
    const requestUrl = config.serverUrl + config.api.company.member_search.prefix + data.id + config.api.company.member_search.suffix + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getEmployeeSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiSetActiveCompany(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.deactivesCompanies;
    const { id: id = '',
      reason: reason = '',
      isActive: isActive = false,
      resolve,
      reject
    } = data;

    yield put(loadRepos());
    try {
      const response = yield axios.put(requestUrl,
        {
          "ids": [id],
          "reason": reason,
          "active": isActive
        }
      );
      yield put(Actions.selectAllEmployee(false));//unselect all selectedEmployee
      if (_.isFunction(resolve)) {
        resolve(response);
      }
    } catch (error) {
      yield put(reposLoaded());
      if (_.isFunction(reject)) {
        reject(error.response);
      }
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiSetRemoveCompany(data) {
  const { id: id = '',
    reason: reason = '',
  } = data;
  if (id) {
    const requestUrl = config.serverUrl + config.api.company.deleteCompanies;
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, {
        data: {
          "ids": [id],
          "reason": reason,
        }
      }
      );
      yield put(goBack());
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiSetActiveEmployee(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.member_status.prefix + data.companyID + config.api.company.member_status.suffix;
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, {
        "reason": data.statusData.reason,
        "active": data.statusData.active,
        "ids": data.statusData.ids
      }
      );
      yield put(Actions.selectAllEmployee(false));//unselect all selectedEmployee
      yield put(Actions.getEmployeeCompany(data.companyID, data.searchData));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiSetRemoveEmployee(data) {
  if (data && data.companyID) {
    const requestUrl = config.serverUrl + config.api.company.member_delete.prefix + data.companyID + config.api.company.member_delete.suffix;
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(Actions.selectAllEmployee(false));//unselect all selectedEmployee
      yield put(Actions.getEmployeeCompany(data.companyID, data.searchData));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiApproveCompany(data) {
  if (data.id) {
    const requestUrl = config.serverUrl + config.api.company.approvalCompanies;
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, { ids: [data.id] });
      // Update detail company
      yield put(Actions.getCompany(data.id));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiSetTopPriority(data) {
  const { companyID: companyID = '', serviceID: serviceID = '', value: value = false } = data;
  if (!_.isEmpty(companyID)) {
    const requestUrl = config.serverUrl + config.api.company.companies_setTopPriority.prefix + companyID + config.api.company.companies_setTopPriority.suffix;
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl,
        {
          "serviceId": serviceID,
          "isPriority": value,
        }
      );
      yield put(reposLoaded());
      yield put(Actions.getCompany(companyID));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiCreateNote(data) {
  if (data.companyID) {
    const requestUrl = config.serverUrl + config.api.company.companies_createNote.prefix + data.companyID + config.api.company.companies_createNote.suffix;
    try {
      yield axios.post(requestUrl,
        {
          "content": data.noteText,
        }
      );
      yield put(Actions.changeNote(''));
      yield put(Actions.getCompany(data.companyID));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiDeleteNote(data) {
  if (data.companyID && data.noteID) {
    const requestUrl = config.serverUrl + config.api.company.companies_deletNote.prefix + data.companyID + config.api.company.companies_deletNote.suffix + data.noteID;
    try {
      yield axios.delete(requestUrl);
      yield put(Actions.getCompany(data.companyID));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetPeriodEmployee(data) {
  if (!_.isUndefined(data.companyID)) {
    const requestUrl = config.serverUrl + config.api.admin + data.companyID + '/getPeriodByCompanyId';
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getPeriodEmployeesSuccess(response.data.data.period));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetJob(data) {
  if (data && data.id) {
    let params = data.searchData;
    const esc = encodeURIComponent;
    const query = Object.keys(params).map(
      function (key) {
        if (key === 'status' || key === 'employee') {
          let temp = key + '=';
          if (params[key].length > 0) {
            params[key].map((item, index) => {
              if (item !== '') {//other option, not all
                temp += item.value;
                if (index !== params[key].length - 1) {
                  temp += ',';
                }
              }
            });
          }
          return temp;
        } else {
          return (key + '=' + esc(params[key]));
        }
      }
    ).join('&');
    const requestUrl = config.serverUrl + config.api.company.job_search.prefix + data.id + config.api.company.job_search.suffix + '?' + query;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getJobSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetEarning(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.company.payment.prefix + data.companyID + config.api.company.payment.suffix;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getEarningCompanySuccess(response.data.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

export function* apiGetActivitiesCompany(data) {
  if (data && data.companyID) {
    const requestUrl = config.serverUrl + config.api.company.activity.prefix + data.companyID + config.api.company.activity.suffix;
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getActivitiesCompanySuccess(response.data.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_COMPANY_DATA, apiGetCompany);
  yield takeLatest(Constants.SET_ACTIVE_COMPANY, apiSetActiveCompany);
  yield takeLatest(Constants.SET_REMOVE_COMPANY, apiSetRemoveCompany);
  yield takeLatest(Constants.APPROVE_COMPANY, apiApproveCompany);
  yield takeLatest(Constants.SET_TOP_PRIORITY, apiSetTopPriority);
  //employee
  yield takeLatest(Constants.GET_EMPLOYEE_DATA, apiGetEmployee);
  yield takeLatest(Constants.SET_ACTIVE_EMPLOYEE, apiSetActiveEmployee);
  yield takeLatest(Constants.DELETE_EMPLOYEE, apiSetRemoveEmployee);
  yield takeLatest(Constants.GET_PERIOD_EMPLOYEE, apiGetPeriodEmployee);
  //approval company
  yield takeLatest(Constants.CREATE_NOTE, apiCreateNote);
  yield takeLatest(Constants.DELETE_NOTE, apiDeleteNote);
  //job
  yield takeLatest(Constants.GET_JOB_DATA, apiGetJob);
  //earning
  yield takeLatest(Constants.GET_EARNING_COMPANY, apiGetEarning);
  //activities
  yield takeLatest(Constants.GET_ACTIVITIES_COMPANY, apiGetActivitiesCompany);
}
