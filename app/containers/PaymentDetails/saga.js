import { put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import config from 'config';
import { goBack } from 'react-router-redux';
import axios from 'axios';
import _ from 'lodash'
import { loadRepos, reposLoaded, updateError } from "containers/App/actions";

export function* apiGetPaymentDetails(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.payment + '/' + data.id;
    if (_.isBoolean(data.loadSpinner) && data.loadSpinner === true) {
      yield put(loadRepos());
    }
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(Actions.getError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiRemovePayment(data) {
  if (data.deleteData) {
    const requestUrl = config.serverUrl + config.api.payment + '/removePayouts';
    try {
      yield axios.delete(requestUrl, { data: data.deleteData });
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiAddNote(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.payment + '/' + data.id + '/createNote';
    yield put(loadRepos());
    try {
      yield axios.post(requestUrl, data.noteData);
      yield put(Actions.getPaymentDetails(data.id));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteNote(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.payment + '/' + data.paymentId + '/deleteNote/' + data.noteId;
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl);
      yield put(Actions.getPaymentDetails(data.paymentId));
    } catch (error) {
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiGetPdf(data) {
  const requestUrl = config.serverUrl + config.api.payment + '/generatePdfPaymentMany';
  yield put(loadRepos());
  try {
    const response = yield axios.post(requestUrl, { ids: data.paymentId });
    yield put(reposLoaded());
    let array = response.data.data;
    array.forEach(function (file) {
      window.open(file.path);
    });
  } catch (error) {
    yield put(reposLoaded());
    yield put(updateError({
      error: true,
      title: 'Error!!!',
      message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
    }));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_PAYMENT_DETAILS, apiGetPaymentDetails);
  yield takeLatest(Constants.REMOVE_PAYMENT, apiRemovePayment);
  yield takeLatest(Constants.ADD_NOTE, apiAddNote);
  yield takeLatest(Constants.DELETE_NOTE, apiDeleteNote);
  yield takeLatest(Constants.GET_PDF, apiGetPdf);
}
