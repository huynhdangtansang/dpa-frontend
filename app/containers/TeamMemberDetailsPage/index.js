/**
 *
 * TeamMemberDetailsPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import moment from 'moment';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectTeamMemberDetailsPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import _ from "lodash";
import * as Actions from "./actions";
//CSS
import './style.scss';
//Components
import GhostButton from 'components/GhostButton';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import { urlLink } from 'helper/route';
import ActivitiesCompany from 'components/ActivitiesCompany';

//helper
import { capitalizeTheFirstLetter } from 'helper/exportFunction';

/* eslint-disable react/prefer-stateless-function */
export class TeamMemberDetailsPage extends React.PureComponent {
  changeStatus = () => {
    const { memberDetails } = this.props.teammemberdetailspage;
    let listIds = [];
    listIds.push(memberDetails._id);
    let data = {
      ids: listIds
    };
    if (memberDetails.active == true) {
      data['reason'] = this.state.reason;
      data['active'] = !memberDetails.active;
    } else {
      data['active'] = !memberDetails.active;
    }
    this.props.changeStatus(memberDetails._id, data);
  };
  deleteMember = () => {
    const { memberDetails } = this.props.teammemberdetailspage;
    let listIds = [];
    listIds.push(memberDetails._id);
    let data = {
      ids: listIds,
      reason: this.state.reason
    };
    this.props.delete(data);
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeStatusConfirm = () => {
    this.setState({ showChangeStatusConfirm: false });
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      reason: '',
      showChangeStatusConfirm: false
    }
  }

  componentWillMount() {
    let url = window.location.href;
    let temp = url.split('?id=');
    let memberID = temp[1];
    this.props.getDetails(memberID);
    this.props.getActivitiesMember(memberID);
  }

  componentDidMount() {
    this.props.history.listen(() => {
      let url = window.location.href;
      let temp = url.split('?id=');
      let memberID = temp[1];
      this.props.getDetails(memberID);
      this.props.getActivitiesMember(memberID);
    })
  }

  render() {
    const { memberDetails, activityList } = this.props.teammemberdetailspage;
    return (
      <div className="member-profile">
        <div className="header-edit-add-page member-profile-header">
          <div className="action">
            <div className="return" id="return" onClick={() => {
              this.props.history.goBack();
            }}>
              <span className={'icon-arrow-left'}></span>
            </div>
            {memberDetails && (<div className="btn-group float-right">
              <GhostButton className={'btn-ghost remove'} title={'Remove'} onClick={() => {
                this.setState({ showRemoveConfirm: true });
              }} />
              <GhostButton className={'btn-ghost edit'} title={'Edit'} onClick={() => {
                this.props.history.push(urlLink.editMember + "?id=" + memberDetails._id);
              }} />
              <GhostButton className={'btn-ghost deactive'}
                title={memberDetails.active == true ? 'Deactivate' : 'Activate'} onClick={() => {
                  this.setState({ showChangeStatusConfirm: true });
                }} />
            </div>)}

          </div>
          <div className="title">
            <span>{memberDetails && memberDetails.fullName}</span>
          </div>
        </div>
        <div className="content content-details-with-activities member-profile-content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8 left-part">
                {/* Company info */}
                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Perosnal Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="company-logo">
                        <img
                          src={memberDetails && memberDetails.avatar ? memberDetails.avatar.fileName : './default-user.png'}
                          alt="logo" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }} />
                      </div>
                      <div className="details">
                        <div className="info-item">
                          <div className="title">
                            <span>Full Name</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && memberDetails.fullName}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Email</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && memberDetails.email}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Phone Nubmer</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && memberDetails.phoneNumber}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Location</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && memberDetails.location}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Position</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && capitalizeTheFirstLetter(memberDetails.subRole)}</span>
                          </div>
                        </div>
                        <div className="info-item">
                          <div className="title">
                            <span>Status</span>
                          </div>
                          <div className="data">
                            <span>{memberDetails && memberDetails.active === true ? 'Active' : 'Inactive'}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="icon-contain">
                    <span className="icon-edit" onClick={() => {
                      this.props.history.push(urlLink.editMember + "?id=" + memberDetails._id);
                    }}></span>
                  </div>
                </div>
              </div>
              <div className="col-md-4 right-part">
                <div className="information">
                  <div className="title">
                    <span>Activities</span>
                  </div>

                  {!_.isEmpty(activityList) && _.isArray(activityList) && activityList.map((perday) => (
                    <div className="time-count" key={perday.day}>
                      <div className="section">
                        <div
                          className="date">{moment(perday.day).isSame(moment().format('MM/DD/YYYY')) ? 'Today' : perday.day}</div>
                        <div className="list-activities">
                          {perday.activities.map((activity) => (
                            <ActivitiesCompany
                              key={activity._id}
                              activity={activity}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                  ))}

                </div>
              </div>
            </div>
          </div>
        </div>
        <ChangeStatusConfirmModal
          show={this.state.showChangeStatusConfirm}
          reason={this.state.reason}
          currentStatus={memberDetails && memberDetails.active}
          object={'member'}
          action={'single'}
          handleChange={this.handleReasonChange}
          closeModal={() => {
            this.setState({
              showChangeStatusConfirm: false,
              reason: ''
            });
          }}
          changeStatus={this.changeStatus} />
        <RemoveConfirmModal
          show={this.state.showRemoveConfirm}
          reason={this.state.reason}
          action={'single'}
          object={'member'}
          handleChange={this.handleReasonChange}
          closeModal={() => {
            this.setState({
              showRemoveConfirm: false,
              reason: ''
            });
          }}
          removeObject={() => {
            this.deleteMember();
          }} />
      </div>
    );
  }
}

TeamMemberDetailsPage.propTypes = {
  dispatch: PropTypes.func,
  getDetails: PropTypes.func,
  changeStatus: PropTypes.func,
  delete: PropTypes.func,
  getActivitiesMember: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  teammemberdetailspage: makeSelectTeamMemberDetailsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    getDetails: (id) => {
      dispatch(Actions.getMemberDetails(id));
    },
    changeStatus: (id, data) => {
      dispatch(Actions.changeMemberStatus(id, data));
    },
    delete: (data) => {
      dispatch(Actions.deleteMember(data));
    },
    //Activities member
    getActivitiesMember: (id) => {
      dispatch(Actions.getActivitiesMember(id));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "teamMemberDetailsPage", reducer });
const withSaga = injectSaga({ key: "teamMemberDetailsPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(TeamMemberDetailsPage);
