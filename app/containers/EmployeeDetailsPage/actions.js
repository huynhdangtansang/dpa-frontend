/*
 *
 * TeamMemberDetailsPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function getMemberDetails(id) {
  return {
    type: Constants.GET_MEMBER_DETAILS,
    id
  }
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_ERROR,
    response
  }
}

export function changeMemberStatus(employeeID, dataUpdate) {
  return {
    type: Constants.CHANGE_MEMBER_STATUS,
    employeeID,
    dataUpdate
  }
}

export function deleteMember(data) {
  return {
    type: Constants.DELETE_MEMBER,
    data
  }
}

//Store
export function changeEmployeeStore(event) {
  return {
    type: Constants.CHANGE_EMPLOYEE_DETAIL,
    event
  };
}

//Earning
export function getEmployeeEarning(value) {
  return {
    type: Constants.GET_EMPLOYEE_EARNING,
    value
  };
}

export function getEmployeeEarningSuccess(list) {
  return {
    type: Constants.GET_EMPLOYEE_EARNING_SUCCESS,
    list
  };
}

export function getEmployeeEarningError(error) {
  return {
    type: Constants.GET_EMPLOYEE_EARNING_ERROR,
    error
  };
}

//Get employee review
export function getEmployeeReview(value) {
  return {
    type: Constants.GET_EMPLOYEE_REVIEW,
    value
  };
}

export function getEmployeeReviewSuccess(list) {
  return {
    type: Constants.GET_EMPLOYEE_REVIEW_SUCCESS,
    list
  };
}

export function getEmployeeReviewError(error) {
  return {
    type: Constants.GET_EMPLOYEE_REVIEW_ERROR,
    error
  };
}

//Job
export function getJobAssigned(id, searchData) {
  return {
    type: Constants.GET_JOB_ASSIGNED,
    id,
    searchData
  }
}

export function getJobAssignedSuccess(response) {
  return {
    type: Constants.GET_JOB_ASSIGNED_SUCCESS,
    response
  }
}

//Activities
export function getActivitiesEmployee(id) {
  return {
    type: Constants.GET_ACTIVITIES,
    id
  }
}

export function getActivitiesEmployeeSuccess(response) {
  return {
    type: Constants.GET_ACTIVITIES_SUCCESS,
    response
  }
}