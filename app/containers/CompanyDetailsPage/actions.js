/*
 *
 * CompanyDetailsPage actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: Constants.RESET_STATE
  };
}

export function getCompany(companyID, resolve, reject) {
  return {
    type: Constants.GET_COMPANY_DATA,
    id: companyID,
    resolve,
    reject
  };
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response: response
  }
}

export function getEmployeeCompany(companyID, searchData) {
  return {
    type: Constants.GET_EMPLOYEE_DATA,
    id: companyID,
    searchData
  };
}

export function getEmployeeSuccess(response) {
  return {
    type: Constants.GET_EMPLOYEE_SUCCESS,
    response: response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_ERROR,
    response: response
  }
}

export function setActiveCompany(companyID, reason, isActive, resolve, reject) {
  return {
    type: Constants.SET_ACTIVE_COMPANY,
    id: companyID,
    reason: reason,
    isActive: isActive,
    resolve,
    reject
  }
}

export function setRemoveCompany(companyID, reason) {
  return {
    type: Constants.SET_REMOVE_COMPANY,
    id: companyID,
    reason: reason
  }
}

export function setActiveEmployee(companyID, statusData, searchData) {
  return {
    type: Constants.SET_ACTIVE_EMPLOYEE,
    companyID,
    statusData,
    searchData
  }
}

export function removeEmployee(companyID, deleteData, searchData) {
  return {
    type: Constants.DELETE_EMPLOYEE,
    companyID,
    deleteData,
    searchData
  }
}

export function approveCompany(companyID) {
  return {
    type: Constants.APPROVE_COMPANY,
    id: companyID
  };
}

export function updateSearchEmployee(key, value) {
  return {
    type: Constants.UPDATE_SEARCH_EMPLOYEE,
    key,
    value
  };
}

export function selectEmployee(isSelected, selectItem) {
  return {
    type: Constants.SELECT_EMPLOYEE,
    isSelected,
    selectItem
  }
}

export function selectAllEmployee(isSelected) {
  return {
    type: Constants.SELECT_ALL_EMPLOYEE,
    isSelected
  }
}

export function toggleTopPriorityConfirmModal(value, index) {
  return {
    type: Constants.TOGGLE_TOP_PRIORITY,
    value,
    index
  }
}

export function setTopPriority(companyID, serviceID, value) {
  return {
    type: Constants.SET_TOP_PRIORITY,
    companyID,
    serviceID,
    value
  }
}

export function changeNote(noteText) {
  return {
    type: Constants.CHANGE_NOTE,
    noteText
  }
}

export function createNote(companyID, noteText) {
  return {
    type: Constants.CREATE_NOTE,
    companyID,
    noteText
  }
}

export function deleteNote(companyID, noteID) {
  return {
    type: Constants.DELETE_NOTE,
    companyID,
    noteID
  }
}

//---------------------Common---------------------
export function updateTab(newTab) {
  return {
    type: Constants.UPDATE_TAB,
    newTab
  }
}

export function updatePage(key, value) {
  return {
    type: Constants.UPDATE_PAGE,
    key,
    value
  };
}

export function updateSearchData(key, newSearchData) {
  return {
    type: Constants.UPDATE_SEARCH_DATA,
    key,
    newSearchData
  };
}

export function changeStoreValue(event) {
  return {
    type: Constants.CHANGE_STORE_VALUE,
    event
  };
}

export function chooseItem(key, item) {
  return {
    type: Constants.CHOOSE_ITEM,
    key,
    item
  }
}

//-----------------Employee-----------------------
export function getPeriodEmployee(companyID) {
  return {
    type: Constants.GET_PERIOD_EMPLOYEE,
    companyID
  }
}

export function getPeriodEmployeesSuccess(period) {
  return {
    type: Constants.GET_PERIOD_EMPLOYEE_SUCCESS,
    period
  }
}

export function updateDropdownPeriod(newPeriod) {
  return {
    type: Constants.UPDATE_PERIOD_EMPLOYEE,
    newPeriod
  }
}

//--------------------------Job------------------------------
export function getJobCompany(companyID, searchData) {
  return {
    type: Constants.GET_JOB_DATA,
    id: companyID,
    searchData
  };
}

export function getJobSuccess(response) {
  return {
    type: Constants.GET_JOB_SUCCESS,
    response
  };
}

export function updateSearchJob(key, value) {
  return {
    type: Constants.UPDATE_SEARCH_JOB,
    key,
    value
  };
}

//---------------------Earning--------------------
export function getEarningCompany(companyID) {
  return {
    type: Constants.GET_EARNING_COMPANY,
    companyID
  };
}

export function getEarningCompanySuccess(response) {
  return {
    type: Constants.GET_EARNING_COMPANY_SUCCESS,
    response
  };
}

export function getEarningCompanyError(response) {
  return {
    type: Constants.GET_EARNING_COMPANY_ERROR,
    response
  };
}

//--------------------Activities--------------------
export function getActivitiesCompany(companyID) {
  return {
    type: Constants.GET_ACTIVITIES_COMPANY,
    companyID
  };
}

export function getActivitiesCompanySuccess(response) {
  return {
    type: Constants.GET_ACTIVITIES_COMPANY_SUCCESS,
    response
  };
}
