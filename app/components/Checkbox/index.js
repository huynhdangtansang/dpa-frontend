/**
 *
 * Checkbox
 *
 */
import React from "react";
import PropTypes from 'prop-types';
import _ from 'lodash';
import './style.scss';

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className={'fixle-checkbox-circle'}>
          <label>
            <input
                className="fixle-input"
                type={_.isUndefined(this.props.type) ? "checkbox" : this.props.type}
                onChange={this.props.onChange}
                checked={this.props.checked}
                {...this.props}
            />
            <span className={'fixle-span'}></span>
            <span className={'fixle-label'}>{this.props.label}</span>
          </label>
        </div>
    );
  }
}
Checkbox.propTypes = {
  value: PropTypes.bool,
  disabled: PropTypes.bool
};
