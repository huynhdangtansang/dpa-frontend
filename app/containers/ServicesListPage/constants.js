/*
 *
 * ServicesListPage constants
 *
 */
export const GET_SERVICES_LIST = "/ServicesListPage/GET_SERVICES_LIST";
export const GET_SUCCESS = "/ServicesListPage/GET_SUCCESS";
export const DELETE_SERVICE = "/ServicesListPage/DELETE_SERVICE";
export const DELETE_SUCCESS = "/ServicesListPage/DELETE_SUCCESS";
export const DELETE_ERROR = "/ServicesListPage/DELETE_ERROR";
export const SELECT_SERVICE = "/ServicesListPage/SELECT_SERVICE";
export const SELECT_ALL_SERVICE = "/ServicesListPage/SELECT_ALL_SERVICE";
export const UPDATE_SEARCH_DATA = "/ServicesListPage/CHANGE_SEARCH_DATA";
export const UPDATE_PAGE = "/ServicesListPage/UPDATE_PAGE";
export const UPDATE_MODIFY_TYPE = "/ServicesListPage/UPDATE_MODIFY_TYPE";
export const SHOW_REMOVE_CONFIRM = "/ServicesListPage/SHOW_REMOVE_CONFIRM";
export const SET_CURRENT_SERVICE = "/ServiceListPage/SET_CURRENT_SERVICE";
export const UPDATE_COMMISSION = "/ServiceListPage/UPDATE_COMMISSION";
export const SHOW_COMMISSION_CONFIRM = "/ServiceListPage/SHOW_COMMISSION_CONFIRM";
export const GET_PDF = "/ServiceListPage/GET_PDF";