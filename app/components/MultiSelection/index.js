/**
 *
 * MultiSelection
 *
 */
import React from "react";
import './style.scss'

// import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
function MultiSelection(props) {
  return (
      <div className={`multi-selection ${props.className}`}>
        <label className={'multi-select-label'}>{props.title}</label>
        <ReactMultiSelectCheckboxes options={props.options}/>
      </div>
  );
}

MultiSelection.propTypes = {};
export default MultiSelection;
