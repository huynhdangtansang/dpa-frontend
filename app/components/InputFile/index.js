/**
 *
 * InputFile
 *
 */
import React from "react";
import classnames from 'classnames';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './style.scss';

const imageValidateMessage = {
  required: 'Missing avatar',
  wrongFormat: 'Invalid file (jpeg, png or gif format required)',
  exceedSize: 'Invalid file (maximum 2MB size)'
};
export default class InputFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: ''
    };
  }

  // Check validate in case user already input other fields
  checkValidate = (values, name) => {
    let checkNullValue = Object.keys(values).filter((fieldName) => {
      return values[fieldName] === '' || values[fieldName] === null
    });
    if (checkNullValue.length === 1) {
      if (name === checkNullValue[0]) {
        this.setState({
          error: (checkNullValue[0] === 'avatar' || checkNullValue[0] === 'background' || checkNullValue[0] === 'icon'), // Only have avatar field left
          message: imageValidateMessage.required
        })
      }
    }
  };
  handleImageChange = (e) => {
    e.preventDefault();
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      let file = e.target.files[0];
      let type = file.name.toLowerCase().split('.');
      // Check type
      if (type[1] === 'jpeg' || type[1] === 'jpg' || type[1] === 'png') {
        // Check size
        if (file.size > 2 * 1024 * 1024) {//2MB
          // This allow clear file input value, allow adding the same file in the next time
          e.target.value = null;
          resolve({ error: true, errorType: 'exceedSize' })
        } else {
          reader.onloadend = () => {
            // This allow clear file input value, allow adding the same file in the next time
            resolve({ error: false, data: { fileUrl: reader.result, file: file, rawData: e } });
            // This allow clear file input value, allow adding the same file in the next time
            e.target.value = null;
          };
        }
        reader.readAsDataURL(file);
      } else {
        // This allow clear file input value, allow adding the same file in the next time
        e.target.value = null;
        resolve({ error: true, errorType: 'wrongFormat' })
      }
    });
  };
  changeImageValue = (data) => {
    if (data.error) {
      data.message = imageValidateMessage[data.errorType];
      if (this.props.inlineErrorMessge !== false) {
        this.setState({ error: true, message: data.message })
      }
    } else {
      //this.setState({error: false, message: ''});
    }
    this.props.onChange(data)
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    // If current error is shown
    if (this.state.error) {
      // User choose the correct image
      if ((nextProps.src !== '' && nextProps.src !== null)) {
        // Hide message
        this.setState({ error: false, message: '' });
      }
    } else { // No error related to photo, check missing image
      // If this mode is enable
      if (this.props.validateAfterOtherInputFields === true) {
        // Validate image if user have on all fields
        this.checkValidate(nextProps.relatedValues, this.props.name)
      }
    }
  }

  render() {
    const { error, message } = this.state;
    return (
      <div className={classnames('file-wrapper form-input', this.props.className)}>
        <input className={`file`}
          type="file"
          name={this.props.name}
          onChange={(e) => {
            e.persist(); // Make this event can be reused
            this.handleImageChange(e).then((data) => {
              this.changeImageValue(data);
            })
          }}
          onBlur={this.props.onBlur} />
        <div className={'btn-content'}>{this.props.btnText}</div>

        {error && <div className={'error-text'}>
          <i className={'icon-error'} />
          <span className={'error-content'}>{message}</span>
        </div>}
      </div>
    );
  }
}
