/*
 *
 * BotMessagesPages actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function getMessageList() {
  return {
    type: Constants.GET_MESSAGE_LIST
  };
}

export function getMessageListSuccess(data) {
  return {
    type: Constants.GET_MESSAGE_LIST_SUCCESS,
    data
  };
}


