/*
 *
 * JobEditDescription reducer
 *
 */
import { fromJS } from "immutable";
import { ADD_IMAGE, CHANGE_LOCATION, GET_SUCCESS, REMOVE_IMAGE, UPDATE_ERROR } from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  description: '',
  location: {},
  images: [],
  errors: []
});

function convertImagesArray(array) {
  return array.map((image) => {
    return {
      id: image._id,
      fileName: image.fileName
    }
  })
}

function jobEditDescriptionReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case GET_SUCCESS: {
      const { job } = action.response.data;
      return state
          .set('description', fromJS(job.description))
          .set('location', fromJS(job.location))
          .set('images', fromJS(convertImagesArray(job.images)))
          .set('errors', fromJS([]));
    }
    case UPDATE_ERROR:
      return state
          .set('errors', action.response.errors);
    case ADD_IMAGE:
      return state
          .updateIn(['images'], arr => arr.push(action.newImage));
    case REMOVE_IMAGE:
      return state
          .set('images', state.get('images').filter((image, index) => index !== action.id));
    case CHANGE_LOCATION:
      return state
          .set('location', action.newLocation);
    default:
      return state;
  }
}

export default jobEditDescriptionReducer;
