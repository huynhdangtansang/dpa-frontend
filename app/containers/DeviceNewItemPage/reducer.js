/*
 *
 * DeviceNewItemPage reducer
 *
 */
import { fromJS } from "immutable";
import { ADD_ERROR, ADD_SUCCESS, CHANGE_AVATAR, RESET_ERROR } from "./constants";
import { LOCATION_CHANGE } from "react-router-redux";

export const initialState = fromJS({
  apiErrors: [],
  addressList: [],
  avatar: '',
});

function deviceNewItemPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return initialState;
    case CHANGE_AVATAR:
      return state
          .set('avatar', action.url);
    case ADD_SUCCESS:
    case ADD_ERROR:
      return state
          .set('apiErrors', action.response.errors);
    case RESET_ERROR:
      return state
          .set('apiErrors', []);
    default:
      return state;
  }
}

export default deviceNewItemPageReducer;
