/**
 *
 * EmployeeTabControl
 *
 */
import React from "react";
import { Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

/* eslint-disable react/prefer-stateless-function */
class EmployeeTabControl extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <Nav tabs className="tab">
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '1' })}
                onClick={() => {
                  this.props.toggle('1');
                }}>
              <span>Overview</span>
            </NavLink>
          </NavItem>
          <NavItem className="tab-item">
            <NavLink
                className={classnames({ selected: this.props.activeTab === '2' })}
                onClick={() => {
                  this.props.toggle('2');
                }}>
              <span>Reviews</span>
            </NavLink>
          </NavItem>

        </Nav>
    );
  }
}

EmployeeTabControl.propTypes = {};
export default EmployeeTabControl;
