import { put, takeLatest } from 'redux-saga/effects';
import * as Constants from './constants';
import * as Actions from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import axios from 'axios';
import _ from "lodash";
import { goBack } from 'react-router-redux';

export function* apiGetMemberDetails(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(Actions.getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(Actions.getError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiChangeMemberStatus(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/deactives';
    yield put(loadRepos());
    try {
      yield axios.put(requestUrl, data.dataUpdate);
      yield put(reposLoaded());
      yield put(Actions.getMemberDetails(data.id));
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiDeleteMember(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.teamMember + '/deletes';
    yield put(loadRepos());
    try {
      yield axios.delete(requestUrl, { data: data.data });
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(reposLoaded());
    }
  }
}

export function* apiGetActivitiesMember(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.members + data.id + '/getActivity';
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(reposLoaded());
      yield put(Actions.getActivitiesMemberSuccess(response.data));
    } catch (error) {
      yield put(reposLoaded());
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(Actions.getError(error.response.data));
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(Constants.GET_MEMBER_DETAILS, apiGetMemberDetails);
  yield takeLatest(Constants.CHANGE_MEMBER_STATUS, apiChangeMemberStatus);
  yield takeLatest(Constants.DELETE_MEMBER, apiDeleteMember);
  //Activities
  yield takeLatest(Constants.GET_ACTIVITIES, apiGetActivitiesMember);
}
