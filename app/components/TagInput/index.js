import React from "react";
import { WithContext as ReactTags } from 'react-tag-input';
import './style.scss';

export default class TagInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: this.props.tags
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAddition = this.handleAddition.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
    this.handleTagClick = this.handleTagClick.bind(this);
  }

  handleDelete = (i) => {
    const { tags } = this.props;
    let newTags = tags.filter((tag, index) => index !== i);
    this.props.handleChange(newTags);
  };
  handleAddition = (tag) => {
    const { tags } = this.props;
    tags.push(tag);
    this.props.handleChange(tags);
  };

  handleDrag(tag, currPos, newPos) {
    const tags = [...this.state.tags];
    const newTags = tags.slice();
    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);
    // re-render
    //this.setState({ tags: newTags });
    this.props.handleChange(newTags);
  }

  handleTagClick(index) {
    return index;
  }

  render() {
    return (
        <ReactTags tags={this.props.tags}
                   autofocus={false}
                   handleDelete={this.handleDelete}
                   handleAddition={this.handleAddition}
                   handleDrag={this.handleDrag}
                   placeholder={this.props.placeholder}
                   handleTagClick={this.handleTagClick}/>
    )
  }
}