/*
 *
 * TeamEditMemberPage actions
 *
 */
import {
  CHANGE_AVATAR,
  DEFAULT_ACTION,
  GET_ERROR,
  GET_MEMBER_DETAILS,
  GET_SUCCESS,
  RESET_ERROR,
  UPDATE_ERROR,
  UPDATE_PROFILE,
  UPDATE_SUCCESS
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getMemberDetails(id) {
  return {
    type: GET_MEMBER_DETAILS,
    id
  }
}

export function getSuccess(response) {
  return {
    type: GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: GET_ERROR,
    response
  }
}

export function updateProfile(id, profile) {
  return {
    type: UPDATE_PROFILE,
    id,
    profile
  }
}

export function updateSuccess(response) {
  return {
    type: UPDATE_SUCCESS,
    response
  }
}

export function updateError(response) {
  return {
    type: UPDATE_ERROR,
    response
  }
}

export function changeAvatar(src) {
  return {
    type: CHANGE_AVATAR,
    src
  }
}

export function resetError() {
  return {
    type: RESET_ERROR
  }
}