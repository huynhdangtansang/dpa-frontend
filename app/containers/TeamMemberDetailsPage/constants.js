/*
 *
 * TeamMemberDetailsPage constants
 *
 */
export const DEFAULT_ACTION = "app/TeamMemberDetailsPage/DEFAULT_ACTION";
export const GET_MEMBER_DETAILS = "TeamMemberDetailsPage/GET_MEMBER_DETAILS";
export const GET_SUCCESS = "TeamMemberDetailsPage/GET_SUCCESS";
export const GET_ERROR = "TeamMemberDetailsPage/GET_ERROR";
export const CHANGE_MEMBER_STATUS = "TeamListPage/CHANGE_MEMBER_STATUS";
export const DELETE_MEMBER = "TeamListPage/DELETE_MEMBER";
//ACTIVITIES
export const GET_ACTIVITIES = "TeamListPage/GET_ACTIVITIES";
export const GET_ACTIVITIES_SUCCESS = "TeamListPage/GET_ACTIVITIES_SUCCESS";