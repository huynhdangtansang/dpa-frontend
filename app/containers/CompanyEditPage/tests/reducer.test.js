import { fromJS } from 'immutable';
import editCompanyPageReducer from '../reducer';

describe('editCompanyPageReducer', () => {
  it('returns the initial state', () => {
    expect(editCompanyPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
