import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the approvalAreaListPage state domain
 */

const selectApprovalAreaListPageDomain = state =>
    state.get("approvalAreaListPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by ApprovalAreaListPage
 */

const makeSelectApprovalAreaListPage = () =>
    createSelector(selectApprovalAreaListPageDomain, substate => substate.toJS());
export { makeSelectApprovalAreaListPage };
export { selectApprovalAreaListPageDomain };
