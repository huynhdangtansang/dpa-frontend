import { put, takeLatest } from 'redux-saga/effects';
import { GET_CLIENT_DETAILS, UPDATE_CLIENT_PROFILE } from './constants';
import { getError, getSuccess, updateErr, updateSuccess } from './actions';
import { loadRepos, reposLoaded, updateError } from 'containers/App/actions';
import config from 'config';
import _ from "lodash";
import axios from 'axios';
import { goBack } from 'react-router-redux';

export function* apiGetClientDetails(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.get(requestUrl);
      yield put(getSuccess(response.data));
      yield put(reposLoaded());
    } catch (error) {
      yield put(getError(error.response.data));
      yield put(updateError({
        error: true,
        title: 'Error!!!',
        message: !_.isArray(error.response.data.errors) ? error.response.data.error : error.response.data.errors[0].errorMessage
      }));
      yield put(reposLoaded());
    }
  }
}

export function* apiUpdateClient(data) {
  if (data) {
    const requestUrl = config.serverUrl + config.api.client + '/' + data.id;
    yield put(loadRepos());
    try {
      const response = yield axios.put(requestUrl, data.profile);
      yield put(updateSuccess(response.data));
      yield put(reposLoaded());
      yield put(goBack());
    } catch (error) {
      yield put(updateErr(error.response.data));
      yield put(reposLoaded());
    }
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_CLIENT_DETAILS, apiGetClientDetails);
  yield takeLatest(UPDATE_CLIENT_PROFILE, apiUpdateClient);
}
