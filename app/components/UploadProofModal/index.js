import React from "react";
import { Modal, ModalBody } from 'reactstrap';
import './style.scss';
//Library
import _ from "lodash";
import moment from 'moment';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import ProofUpload from "components/ProofUpload";
import Datepicker from 'components/Datepicker';

const MAX_PROOFS = 4;
const MAX_FILE_SIZE = 2 * 1024 * 1024;//2MB
export default class UploadProofModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorProofs: false
    }
  }

  handleDateChange = (date) => {
    this.props.updateDateExpiredTemp(moment(date));
  };

  //Handle image
  _handleImageChange(e) {
    e.preventDefault();
    this.setState({ errorProofs: false });
    //upload over MAX_PROOFS images will not do
    if (0 <= e.target.files.length + this.props.dataProof.length &&
      e.target.files.length + this.props.dataProof.length <= MAX_PROOFS) {
      Array.from(e.target.files).forEach(item => {
        let image = { file: '', fileName: '' };
        let reader = new FileReader();
        let file = item;
        let type = file.name.split('.');
        let lengType = type.length;
        if (type[lengType - 1] === 'jpeg' || type[lengType - 1] === 'jpg' || type[lengType - 1] === 'png' || type[lengType - 1] === 'gif') {
          reader.onloadend = () => {
            //file size need lower than 2MB
            if (file.size <= MAX_FILE_SIZE) {
              image.file = file;
              image.fileName = reader.result;
              this.props.addImage(image);
            }
          };
          reader.readAsDataURL(file);
        } else {
          this.setState({ errorProofs: true });
        }
      });
      e.target.value = null;
    } else {
      this.setState({ errorProofs: true });
    }
  }

  _removeItemListProof(index) {
    this.props.removeImage(index);
  }

  render() {
    return (
      <Modal isOpen={this.props.show} className="logout-modal modal-edit-proof">
        <ModalBody>
          <div className="change-status">
            <div className="upper">
              <div className="title">
                <span>{this.props.itemLicense && _.isEmpty(this.props.itemLicense.proofs) ? 'Upload' : 'Change'} Proof</span>
              </div>
              <div className="description">
                <div className="input-group form-input">
                  <label className="form-label" htmlFor="expirydate">Expiry Date</label>

                  <Datepicker
                    name={'appoinmentTime'}
                    hoursFromNow={0}
                    selected={this.props.expiredAt}
                    placeholder={"Select Date"}
                    onChange={date => {
                      this.handleDateChange(date);
                    }}
                  />
                </div>

                <ProofUpload
                  images={this.props.dataProof ? this.props.dataProof : []}
                  removeItem={index => {
                    this._removeItemListProof(index)
                  }}
                  onChange={evt => {
                    if (this.props.dataProof.length < MAX_PROOFS) {
                      this._handleImageChange(evt);
                    }
                  }} />

                {this.state.errorProofs ? (
                  <div className="form-input">
                    <div className="error-text">
                      <i className="icon-error"></i>
                      <div className="error-item">
                        <span>Invalid proof (maximum {MAX_PROOFS} proofs)</span>
                      </div>
                    </div>
                  </div>
                ) : []}

              </div>
            </div>
            <div className="lower">
              <GhostButton className="btn-ghost cancel" title={'Cancel'}
                onClick={() => {
                  this.props.cancelModal();
                }} />
              <PurpleRoundButton className="btn-purple-round sign-out"
                title={'Next'}
                disabled={moment(this.props.expiredAt).isBefore(moment(), 'day') || this.props.dataProof.length === 0}
                onClick={() => {
                  this.props.updateLicenseItem('expiredAt', this.props.expiredAt);
                  this.props.updateLicenseItem('proofs', this.props.dataProof);
                }} />
            </div>
          </div>
        </ModalBody>
      </Modal>
    )
  }
}