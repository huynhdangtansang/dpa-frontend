/*
 *
 * TeamNewItemPage actions
 *
 */
import {
  ADD_ERROR,
  ADD_MEMBER,
  ADD_SUCCESS,
  CHANGE_AVATAR,
  DEFAULT_ACTION,
  RESET_ERROR,
  RESET_STATE
} from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function addMember(profile) {
  return {
    type: ADD_MEMBER,
    profile
  }
}

export function addSuccess(response) {
  return {
    type: ADD_SUCCESS,
    response
  }
}

export function addError(response) {
  return {
    type: ADD_ERROR,
    response
  }
}

export function resetState() {
  return {
    type: RESET_STATE
  }
}

export function resetError() {
  return {
    type: RESET_ERROR
  }
}

export function changeAvatar(url) {
  return {
    type: CHANGE_AVATAR,
    url: url
  }
}