import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the jobNewPage state domain
 */

const selectJobNewPageDomain = state => state.get("jobNewPage", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by JobNewPage
 */

const makeSelectJobNewPage = () =>
    createSelector(selectJobNewPageDomain, substate => substate.toJS());
export default makeSelectJobNewPage;
export { selectJobNewPageDomain };
