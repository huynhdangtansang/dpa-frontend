/*
 *
 * EmployeeDetailsPage constants
 *
 */
export const DEFAULT_ACTION = "app/EmployeeDetailsPage/DEFAULT_ACTION";
export const CHANGE_EMPLOYEE_DETAIL = "employeeDetailsPage/CHANGE_EMPLOYEE_DETAIL";
export const GET_MEMBER_DETAILS = "employeeDetailsPage/GET_MEMBER_DETAILS";
export const GET_SUCCESS = "employeeDetailsPage/GET_SUCCESS";
export const GET_ERROR = "employeeDetailsPage/GET_ERROR";
export const CHANGE_MEMBER_STATUS = "employeeDetailsPage/CHANGE_MEMBER_STATUS";
export const DELETE_MEMBER = "employeeDetailsPage/DELETE_MEMBER";
//Get earning
export const GET_EMPLOYEE_EARNING = "employeeDetailsPage/GET_EMPLOYEE_EARNING";
export const GET_EMPLOYEE_EARNING_SUCCESS = "employeeDetailsPage/GET_EMPLOYEE_EARNING_SUCCESS";
export const GET_EMPLOYEE_EARNING_ERROR = "employeeDetailsPage/GET_EMPLOYEE_EARNING_ERROR";
//Get review
export const GET_EMPLOYEE_REVIEW = "employeeDetailsPage/GET_EMPLOYEE_REVIEW";
export const GET_EMPLOYEE_REVIEW_SUCCESS = "employeeDetailsPage/GET_EMPLOYEE_REVIEW_SUCCESS";
export const GET_EMPLOYEE_REVIEW_ERROR = "employeeDetailsPage/GET_EMPLOYEE_REVIEW_ERROR";
//JOB
export const GET_JOB_ASSIGNED = "employeeDetailsPage/GET_JOB_ASSIGNED";
export const GET_JOB_ASSIGNED_SUCCESS = "employeeDetailsPage/GET_JOB_ASSIGNED_SUCCESS";
//ACTIVITIES
export const GET_ACTIVITIES = "employeeDetailsPage/GET_ACTIVITIES";
export const GET_ACTIVITIES_SUCCESS = "employeeDetailsPage/GET_ACTIVITIES_SUCCESS";