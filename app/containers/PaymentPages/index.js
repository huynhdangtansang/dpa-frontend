import React from "react";
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { urlLink } from 'helper/route';
import PaymentList from '../PaymentList/Loadable';
import PaymentDetails from '../PaymentDetails/Loadable';

export default class PaymentPages extends React.Component {
  render() {
    return (
        <div className="payment-pages">
          <Router>
            <Switch>
              <Route exact path={urlLink.payments} component={PaymentList}/>
              <Route exact path={urlLink.viewPayment} component={PaymentDetails}/>
            </Switch>
          </Router>
        </div>
    )
  }
}

