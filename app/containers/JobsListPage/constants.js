/*
 *
 * JobsListPage constants
 *
 */
export const RESET_STATE = "JobsListPage/RESET_STATE";
//API
export const GET_LIST = "JobsListPage/GET_LIST";
export const GET_SUCCESS = "JobsListPage/GET_SUCCESS";
export const GET_ERROR = "JobsListPage/GET_ERROR";
export const GET_COMPANIES = "JobsListPage/GET_COMPANIES";
export const GET_COMPANIES_SUCCESS = "JobsListPage/GET_COMPANIES_SUCCESS";
export const REMOVE_JOB = "JobListPage/REMOVE_JOB";
export const REMOVE_SUCCESS = "JobListPage/REMOVE_SUCCESS";
//Serverside
export const UPDATE_SEARCH_DATA = "JobsListPage/UPDATE_SEARCH_DATA";
export const UPDATE_SEARCH_COMPANY_DATA = "JobsListPage/UPDATE_SEARCH_COMPANY_DATA";
export const CHANGE_PAGE = "JobsListPage/CHANGE_PAGE";
export const SELECT_JOB = "JobsListPage/SELECT_JOB";
export const SELECT_ALL_JOB = "JobsListPage/SELECT_ALL_JOB";
export const CHANGE_STATUS_FILTER = "JobListPage/CHANGE_STATUS_FILTER";
export const CHANGE_COMPANY = "JobListPage/CHANGE_COMPANY";
export const SHOW_MODAL = "JobListPage/SHOW_MODAL";
export const CHANGE_STORE_VALUE = "JobListPage/CHANGE_STORE_VALUE";
