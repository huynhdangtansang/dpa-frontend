/**
 *
 * UserProfilePage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Modal, ModalBody, UncontrolledTooltip } from 'reactstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectAvatar, makeSelectEditMode, makeSelectUserData, makeSelectUserProfilePage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import {
  changeAvatar,
  changeIsSent,
  changeUpdateState,
  getProfile,
  resendVerifyCode,
  resetError,
  resetStore,
  sendEmailConfirm,
  sendVerifyCode,
  setEditMode,
  setValueErrors,
  updateProfile
} from "./actions";
import { updateError } from "containers/App/actions";
//lib
import _, { debounce, findIndex, indexOf } from "lodash";
//data
import { country, listError } from 'helper/data';
import { loadCountries, removePlus, capitalizeTheFirstLetter } from 'helper/exportFunction';
import './style.scss';
//components
import CropImage from 'components/CropImage';
import InputFile from "components/InputFile";
import SubmitButton from 'components/SubmitButton';
import VerifyCodeModal from "components/VerifyCodeModal";
import InputForm from "components/InputForm";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import { urlLink } from 'helper/route';
import Selection from 'components/Selection';

const validateForm = Yup.object().shape({
  'email': Yup.string()
    .email(" Invalid email"),
  'name': Yup.string()
    .min(6, 'Invalid name (At least 6 characters)')
    .max(30, 'Invalid name (Maximum at 30 characters)'),
  'phoneNumber': Yup.string()
    .matches(/^[0-9]*$/, 'Invalid phone number')
    .min(5, 'Invalid phone number'),
});

/* eslint-disable react/prefer-stateless-function */
export class UserProfilePage extends React.PureComponent {
  checkVerifyCode = (isUpdated, phoneNotVerify) => {
    if (isUpdated == true) {
      if (phoneNotVerify == false) {
        this.setState({ showUpdateSuccess: true });
      } else {
        this.setState({ showVerifyModal: true });
      }
    }
  };
  checkVerifyCodeIsSent = (isSent) => {
    if (isSent == true) {
      this.setState({
        showVerifyModal: false,
      }, () => {
        this.setState({ showUpdateSuccess: true });
      });
    }
  };
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: true,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e);
    }
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  closeModal = () => {
    this.setState({
      showAvatarModal: false,
      cropSrc: ''
    });
  };
  changeImage = (src, file) => {
    this.props.changeUserAvatar(src);
    this.setState({
      cropSrc: '',
      avatarFile: file,
      tempAvatarSrc: src
    });
  };
  accessEditMode = () => {
    this.setState({ tempAvatarSrc: this.props.avatar }, () => {
      this.props.changeEditMode(true);
    });
  };
  getStatusValue = (value) => {
    this.setState({ status: value });
  };
  getCountryValue = (value) => {
    this.setState({ countryName: value });
  };
  getPositionValue = (value) => {
    this.setState({ position: value });
  };
  submitVerifyCode = (code) => {
    const { userData } = this.props.userprofilepage;
    this.props.sendCode(userData.email, code);
  };
  resendVerifyCode = () => {
    const { userData } = this.props.userprofilepage;
    this.props.resendCode(userData.email);
  };
  //Clear api error
  clearApiError = (type) => {
    const { updateErrors } = this.props.userprofilepage;
    if (updateErrors && updateErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, updateErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };
  // Convert value from api to select option value
  processOptionValue = (type, value) => {
    switch (type) {
      case 'position': {
        return {
          label: this.capitalizeTheFirstLetter(value),
          value: value
        }
      }
      case 'status': {
        return {
          label: value === true ? 'Active' : 'Inactive',
          value: value
        }
      }
    }
  };
  capitalizeTheFirstLetter = (str) => {
    if (str === '' || str === null || str === undefined) {
      return ''
    }
    return str.charAt(0).toUpperCase() + str.slice(1)
  };

  constructor(props) {
    super(props);
    this.state = {
      position: '',
      countryName: '',
      countryCode: '',
      status: '',
      name: '',
      email: '',
      phone: '',
      cropSrc: '',
      showAvatarModal: false,
      showUpdateConfirm: false,
      showVerifyModal: false,
      showUpdateSuccess: false,
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    this.props.getUserProfile();
  }

  componentWillUnmount() {
    this.props.resetStore();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'name',
      'email',
      'phoneNumber'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { userData, updateErrors, verifyErrors, isUpdated, phoneNotVerify, isSent, avatar } = this.props.userprofilepage;
    this.checkVerifyCode(isUpdated, phoneNotVerify);
    this.checkVerifyCodeIsSent(isSent);
    let profile = (
      <div className="content-add-edit user-profile-content">
        <div className="information">
          <div className="row">
            <div className="col-md-4 left">
              <div className="title">
                <span>Personal Information</span>
              </div>
            </div>
            <div className="col-md-8 right">
              <div className="avatar-image user-profile-avatar">
                <div className="avatar-wrapper">
                  <img src={this.props.avatar}
                    alt="avatar"
                    onError={(e) => {
                      e.target.onerror = null;
                      e.target.src = './default-user.png'
                    }}
                  />
                </div>
              </div>
              <div className="details">
                <div className="info-item">
                  <div className="title">
                    <span>Name</span>
                  </div>
                  <div className="data">
                    <p>{!_.isEmpty(userData) ? userData.fullName : ''}</p>
                  </div>
                </div>
                <div className="info-item user-email">
                  <div className="title">
                    <span>Email</span>
                  </div>
                  <div className="data">
                    <p>{!_.isEmpty(userData) ? userData.email : ''}</p>
                  </div>
                </div>
                {!_.isEmpty(userData) &&
                  userData.isEmailChange ?
                  <PurpleRoundButton
                    title="Send Confirmation Link"
                    className="btn-purple-round resend-email"
                    onClick={() => {
                      this.props.sendEmail();
                    }}
                  />
                  : []
                }
                <div className="info-item">
                  <div className="title">
                    <span>Contact Number</span>
                  </div>
                  <div className="data">
                    <p>{!_.isEmpty(userData) ? userData.phoneNumber : ''}</p>
                  </div>
                </div>
                <div className="info-item">
                  <div className="title">
                    <span>Position</span>
                  </div>
                  <div className="data">
                    <p>{(userData && userData.subRole) ? capitalizeTheFirstLetter(userData.subRole) : 'Admin'}</p>
                  </div>
                </div>
                <div className="info-item">
                  <div className="title">
                    <span>Status</span>
                  </div>
                  <div className="data">
                    <p>{!_.isEmpty(userData) ? (userData.active == true ? 'Active' : 'Unactive') : ''}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="icon-contain">
              <span className="icon-edit" onClick={() => {
                this.accessEditMode();
              }}></span>
            </div>
          </div>
        </div>
      </div>
    );
    let profileEdit = (
      <Formik
        ref={ref => (this.formik = ref)}
        initialValues={{
          email: !_.isEmpty(userData) ? userData.email : '',
          name: !_.isEmpty(userData) ? userData.fullName : '',
          phoneNumber: !_.isEmpty(userData) ? userData.phoneNumber : '',
          contactArea: {
            value: !_.isEmpty(userData) ? userData.countryCode : '',
            label: !_.isEmpty(userData) ? userData.countryName : ''
          },
          position: this.processOptionValue('position', userData.subRole),
          status: this.processOptionValue('status', userData.active),
        }}
        enableReinitialize={true}
        validationSchema={validateForm}
        onSubmit={evt => {
          let phoneNumber = '';
          if (evt.phoneNumber.toString().substring(0, 1) === '0') {
            phoneNumber = evt.phoneNumber.toString().substring(1);
          } else {
            phoneNumber = evt.phoneNumber;
          }
          this.setState({
            name: evt.name,
            phone: phoneNumber,
            email: evt.email,
            countryName: evt.contactArea.label,
            countryCode: '+' + removePlus(evt.contactArea.value),
            position: evt.position.value,
            status: evt.status.value,
          }, () => {
            this.setState({ showUpdateConfirm: true });
          });
        }}>
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue
        }) => (
            <form id="profileForm" onSubmit={handleSubmit}>
              <div className="content-add-edit user-profile-content">
                <div className="information">
                  <div className="row">
                    <div className="col-md-4 left">
                      <div className="title">
                        <span>Personal Information</span>
                      </div>
                    </div>
                    <div className="col-md-8 right">
                      <div className="avatar-image user-profile-avatar">
                        <div className="avatar-wrapper">
                          <img src={avatar} alt="avatar" onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = './default-user.png'
                          }} />
                        </div>


                        <InputFile name={'avatar'}
                          src={avatar}
                          onBlur={handleBlur}
                          btnText={avatar ? 'change avatar' : 'upload avatar'}
                          onChange={evt => {
                            setFieldValue('avatar', avatar);
                            this._handleImageChange(evt);
                          }} />

                      </div>

                      <div className="details">
                        {/* NAME */}
                        <InputForm label={'name'}
                          name={'name'}
                          value={values.name}
                          error={errors.name}
                          apiError={updateErrors}
                          touched={touched.name}
                          placeholder={'Full name'}
                          onChange={evt => {
                            handleChange(evt);
                            this.clearApiError('name');
                          }}
                          onBlur={handleBlur} />

                        {/* EMAIL */}
                        <InputForm label={'email'}
                          name={'email'}
                          value={values.email}
                          error={errors.email}
                          apiError={updateErrors}
                          touched={touched.email}
                          placeholder={'you@example.com'}
                          onChange={evt => {
                            handleChange(evt);
                            this.clearApiError('email');
                          }}
                          onBlur={handleBlur} />

                        {/* CONTACT NUMBER */}
                        <div className="info-item contact-number">


                          <Selection
                            options={country}
                            title={'Contact number'}
                            name={'contactArea'}
                            value={values.contactArea}
                            isAsyncList={true}
                            loadOptions={debounce(loadCountries, 700)}
                            getOptionLabel={option => option.label}
                            getOptionValue={option => option.value}
                            type={'contactArea'}
                            onBlur={handleBlur}
                            onChange={option => {
                              setFieldValue('contactArea', { value: option.country_code, label: option.label });
                              this.clearApiError('phoneNumber');
                            }} />
                          <InputForm
                            name={'phoneNumber'}
                            type={'number'}
                            prependLabel={'+' + removePlus(values.contactArea.value)}
                            value={values.phoneNumber}
                            error={errors.phoneNumber}
                            apiError={updateErrors}
                            touched={touched.phoneNumber}
                            placeholder={'Phone number'}
                            onChange={evt => {
                              handleChange(evt);
                              this.clearApiError('phoneNumber');
                            }}
                            onBlur={handleBlur} />

                        </div>

                        {/* POSITION */}
                        <Selection
                          options={[
                            { label: 'Admin', value: 'admin' },
                            { label: 'Member', value: 'member' },
                          ]}
                          name={'position'}
                          title={'position'}
                          value={values.position}
                          getOptionLabel={option => option.label}
                          getOptionValue={option => option.value}
                          type={'position'}
                          onBlur={handleBlur}
                          onChange={option => {
                            setFieldValue('position', option);
                            this.getPositionValue(option);
                          }} />

                        {/* STATUS */}
                        <Selection
                          options={[
                            { label: 'Active', value: true },
                            { label: 'Inactive', value: false },
                          ]}
                          name={'status'}
                          title={'status'}
                          value={values.status}
                          getOptionLabel={option => option.label}
                          getOptionValue={option => option.value}
                          type={'status'}
                          onBlur={handleBlur}
                          onChange={option => {
                            setFieldValue('status', option);
                            this.getStatusValue(option);
                          }} />
                      </div>
                      {(updateErrors && updateErrors.length > 0) ? updateErrors.map((error) => {
                        return (
                          <div key={error.errorCode} className="errors">
                            <span className="icon-error"></span>
                            <div className="error-item">
                              <span>{error.errorMessage}</span>
                            </div>
                          </div>
                        );
                      }) : []}
                    </div>
                  </div>
                </div>
              </div>
              <div className="footer user-profile-footer">
                <GhostButton type="button" className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.props.changeEditMode(false);
                }} />
                <SubmitButton type={'submit'}
                  disabled={this._disableButton(values, errors)}
                  content={'Save'} />
              </div>
            </form>
          )}
      </Formik>
    );
    return (
      <div className="user-profile">
        <div className="header-edit-add-page user-profile-header">
          <div className="action">
            <div className="return" id="return"
              onClick={() => {
                if (this.props.edit == true) {
                  this.props.changeEditMode(false);
                } else {
                  this.props.history.goBack();
                }
              }}>
              <span className="icon-arrow-left" />
              <UncontrolledTooltip className="fixle-tooltip" placement="bottom" target="return" container='return'>
                Back</UncontrolledTooltip>
            </div>
            <div className="float-right btn-group" hidden={this.props.edit}>
              <GhostButton className={'btn-ghost change-password'} title={'Change Password'} onClick={() => {
                this.props.history.push(urlLink.changePassword);
              }} />
              <GhostButton className={'btn-ghost edit'} title={'Edit'} onClick={() => {
                this.accessEditMode();
              }} />
            </div>
          </div>
          <div className="title">
            <span>{this.props.edit == true ? 'Edit Profile' : 'Profile'}</span>
          </div>
        </div>
        <input type="file" id="file" ref="fileUploader" style={{ display: "none" }}
          onChange={(e) => {
            this.uploadImage(e);
            e.target.value = null;
          }} />
        {this.props.edit === true ? profileEdit : profile}
        {(this.state.cropSrc && this.state.cropSrc !== '') ?
          <CropImage
            show={this.state.showAvatarModal}
            cropSrc={this.state.cropSrc}
            closeModal={this.closeModal}
            changeImage={this.changeImage}
          />
          : []
        }
        <Modal isOpen={this.state.showUpdateConfirm} className="remove-confirm-modal update-confirm">
          <ModalBody>
            <div className="remove-confirm">
              <div className="upper">
                <div className="title">
                  <span>Save This Change</span>
                </div>
                <div className="description">
                  <span>Are you sure to want to save this change? This action could influence on all datas involved.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton type="button" className="btn-ghost cancel" title={'cancel'}
                  onClick={() => {
                    this.setState({ showUpdateConfirm: false });
                  }} />
                <PurpleRoundButton className="btn-purple-round save" title={'save'} onClick={() => {
                  this.setState({ showUpdateConfirm: false });
                  let formData = new FormData();
                  formData.append('fullName', this.state.name);
                  formData.append('countryName', this.state.countryName);
                  formData.append('countryCode', this.state.countryCode);
                  formData.append('phoneNumber', this.state.phone);
                  formData.append('email', this.state.email);
                  formData.append('active', this.state.status);
                  formData.append('subRole', this.state.position);
                  if (this.state.avatarFile) {
                    formData.append('file', this.state.avatarFile);
                  }
                  this.props.updateUserProfile(this.state.status, formData);
                }}
                />
              </div>
            </div>
          </ModalBody>
        </Modal>

        <VerifyCodeModal
          show={this.state.showVerifyModal}
          toggle={() => {
            this.props.setStateAgain();
          }}
          errors={verifyErrors}
          onSubmit={this.submitVerifyCode}
          onResend={this.resendVerifyCode}
          //clear error when change code
          clearErrors={() => {
            this.props.setValueErrors('verifyErrors', []);
          }}
        />

        <Modal isOpen={this.state.showUpdateSuccess} className="update-success-modal">
          <ModalBody>
            <div className="remove-confirm">
              <div className="upper">
                <div className="title">
                  <span>Edit Profile</span>
                </div>
                <div className="description">
                  <span>Your profile information has been updated successfully.</span>
                </div>
              </div>
              <div className="lower">
                <div className="text-right">
                  <PurpleRoundButton className="btn-purple-round save" title={'OK'}
                    onClick={() => {
                      this.props.setStateAgain();
                      this.setState({ showUpdateSuccess: false });
                    }}
                  />
                </div>
              </div>
            </div>
          </ModalBody>
        </Modal>

      </div>
    );
  }
}

UserProfilePage.propTypes = {
  dispatch: PropTypes.func,
  resetError: PropTypes.func,
  resetStore: PropTypes.func,
  getUserProfile: PropTypes.func,
  updateUserProfile: PropTypes.func,
  changeEditMode: PropTypes.func,
  changeUserAvatar: PropTypes.func,
  sendEmail: PropTypes.func,
  sendCode: PropTypes.func,
  setStateAgain: PropTypes.func,
  resendCode: PropTypes.func,
  updateError: PropTypes.func,
  setValueErrors: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  userprofilepage: makeSelectUserProfilePage(),
  userData: makeSelectUserData(),
  edit: makeSelectEditMode(),
  avatar: makeSelectAvatar()
});

function mapDispatchToProps(dispatch) {
  return {
    resetError: () => {
      dispatch(resetError());
    },
    resetStore: () => {
      dispatch(resetStore());
    },
    getUserProfile: () => {
      dispatch(getProfile());
    },
    updateUserProfile: (status, data) => {
      dispatch(updateProfile(status, data));
    },
    changeEditMode: (value) => {
      dispatch(setEditMode(value));
    },
    changeUserAvatar: (url) => {
      dispatch(changeAvatar(url));
    },
    sendEmail: () => {
      dispatch(sendEmailConfirm());
    },
    sendCode: (email, code) => {
      dispatch(sendVerifyCode(email, code));
    },
    setStateAgain: () => {
      dispatch(changeUpdateState(false));
      dispatch(changeIsSent(false));
      dispatch(setEditMode(false));
    },
    resendCode: (email) => {
      dispatch(resendVerifyCode(email));
    },
    updateError(data) {
      dispatch(updateError(data))
    },
    setValueErrors(key, value) {
      dispatch(setValueErrors(key, value));
    }
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "userProfilePage", reducer });
const withSaga = injectSaga({ key: "userProfilePage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(UserProfilePage);
