/**
 *
 * ServicesListPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectServicesList, makeSelectServicesListPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
//lib
import _ from 'lodash';
import classnames from 'classnames';
import {
  changeShowCommissionConfirm,
  changeShowRemoveConfirm,
  deleteService,
  getPdf,
  getServicesList,
  selectAllService,
  selectService,
  udpateCurrentService,
  updateCommission,
  updateModifyType,
  updatePage,
  updateSearchData
} from "./actions";
import { loadRepos } from 'containers/App/actions';
import './style.scss';
import GhostButton from 'components/GhostButton';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { DropdownItem, DropdownMenu, Form, Input } from 'reactstrap';
import FixleChargeModal from 'components/FixleChargeModal';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
import debounce from 'components/Debounce';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ServicesListPage extends React.PureComponent {
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
    };
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  showRemoveConfirm = (data) => {
    this.setState({
      showRemoveConfirm: true,
      chosenService: data
    });
  };
  sortData = (field, order) => {
    const { searchData } = this.props.serviceslistpage;
    let temp = searchData;
    temp.sort = field;
    temp.sortType = order;
    this.props.updateSearch(temp);
  };
  handleSearchChange = debounce(value => {
    const { searchData } = this.props.serviceslistpage;
    let temp = searchData;
    temp.search = value;
    this.props.updateSearch(temp);
  }, 500);
  changePagination = (type) => {
    const { currentPage, searchData, pagination } = this.props.serviceslistpage;
    const limit = pagination.limit;
    let temp = searchData;
    if (type == 'prev') {
      let newPage = currentPage - 1;
      let newOffset = limit * (newPage - 1);
      this.props.changePage(newPage);
      temp.offset = newOffset;
      this.props.updateSearch(temp);
    } else {
      let newPage = currentPage + 1;
      let newOffset = limit * (newPage - 1);
      this.props.changePage(newPage);
      temp.offset = newOffset;
      this.props.updateSearch(temp);
    }
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { searchData } = this.props.serviceslistpage;
    this.props.getDataList(searchData);
  }, 700);
  singleAction = (type) => {
    const { chosenService } = this.props.serviceslistpage;
    let listIds = [];
    listIds.push(chosenService.id);
    let data = {
      ids: listIds
    };
    if (type === 'remove') {
      this.props.delete(data);
    }
  };
  multiAction = (type) => {
    const { selectedServices } = this.props.serviceslistpage;
    let list = selectedServices;
    let i = 0;
    let listIds = [];
    list.forEach((client) => {
      listIds.push(client.id);
      if (i == list.length - 1) {
        let data = {
          ids: listIds
        };
        if (type === 'remove') {
          this.props.delete(data);
        }
      } else {
        i++;
      }
    });
  };
  updateCharge = (newCommission) => {
    const { modifyType, chosenService, selectedServices } = this.props.serviceslistpage;
    let data = {
      commission: newCommission.toString()
    };
    if (modifyType === 'single') {
      let listIds = [];
      listIds.push(chosenService.id);
      data['ids'] = listIds;
      this.props.changeCommission(data);
    } else {
      let listIds = [];
      let list = selectedServices;
      let i = 0;
      list.forEach((client) => {
        listIds.push(client.id);
        if (i == list.length - 1) {
          data['ids'] = listIds;
          this.props.changeCommission(data);
        } else {
          i++;
        }
      });
    }
  };

  constructor(props) {
    super(props);
    this.state = {}
  }

  componentWillMount() {
    const { searchData } = this.props.serviceslistpage;
    this.props.getDataList(searchData);
  }

  componentWillUnmount() {
    this.props.selectAll(false);
  }

  render() {
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    const options = {
      withoutNoDataText: true,
      //defaultSortName: 'name',
      defaultSortOrder: 'asc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    const actionFormat = (cell, row) => {
      let editMenu = (
          <DropdownMenu>
            <DropdownItem onClick={() => {
              this.props.history.push(urlLink.viewService + '?id=' + row._id);
            }}>View</DropdownItem>
            <DropdownItem onClick={() => {
              this.props.setService({ id: row._id });
              this.props.showCommissionConfirm(true);
              this.props.changeModifyType('single');
            }}>Exit Fixle Charge</DropdownItem>
            <DropdownItem onClick={() => {
              this.props.history.push(urlLink.editService + '?id=' + row._id);
            }}>Edit</DropdownItem>
            <DropdownItem onClick={() => {
              this.props.getPdf([row._id]);
            }}>Print</DropdownItem>
            <DropdownItem onClick={() => {
              this.props.setService({ id: row._id });
              this.props.showRemoveConfirm(true);
              this.props.changeModifyType('single');
            }}>
              Remove
            </DropdownItem>
          </DropdownMenu>
      );
      return (
          <ActionFormatter menu={editMenu}/>
      );
    };
    const valueFormat = (cell) => {
      if (typeof (cell.unit) !== 'undefined' && cell.value) {
        switch (cell.unit) {
          case '$':
            return (<span>{cell.unit}{parseFloat(cell.value).toFixed(2)}</span>);
          case '%':
            return (<span>{cell.value}{cell.unit}</span>);
          default:
            return (<span>{cell.value}</span>)
        }
      }
    };
    const keywordsFormat = (cell) => {
      return cell.length;
    };
    const iconFormat = (cell, row) => {
      return (
          <img className='avatar-company'
               src={row.icon ? row.icon.fileName : './default-user.png'}
               onError={(e) => {
                 e.target.onerror = null;
                 e.target.src = './default-user.png'
               }}
               onClick={() => {
                 this.props.history.push(urlLink.viewService + '?id=' + row._id);
               }}>
          </img>
      )
    };
    const nameFormat = (cell, row) => {
      return (
          <span onClick={() => {
            this.props.history.push(urlLink.viewService + '?id=' + row._id);
          }}>{cell}</span>
      );
    };
    const {
      servicesList, pagination, selectedServices, currentPage,
      showRemoveConfirm, modifyType, showChangeCommissionConfirm,
      searchData
    } = this.props.serviceslistpage;
    return (
        <div className="services-list">
          <div className="header-list-page">
            <Form inline className="align-items-center" onSubmit={() => {
              this.props.updateSearch(searchData);
            }}>
              <PurpleAddRoundButton className={'btn-add-purple-round'} onClick={() => {
                this.props.history.push(urlLink.addService)
              }} title={'New Service'} iconClassName={'icon-plus'} type="button"/>
              <div className="search-bar services-search">
                <Input placeholder="Search service" onChange={(e) => {
                  this.handleSearchChange(e.target.value);
                }}/>
                <span className="icon-search"></span>
              </div>
              {servicesList && servicesList.length > 0 && pagination && (
                  <TotalFormatter
                      offset={pagination.offset}
                      limit={pagination.limit}
                      total={pagination.total}
                      page={currentPage}
                      changePagination={this.changePagination}
                  />
              )}
            </Form>
          </div>
          <div className={classnames("content content-list-page", selectedServices.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_service"
                data={_.isEmpty(servicesList) ? [] : servicesList}
                options={options}
                bordered={false}
                containerClass='table-fixle services-table'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'
                bodyContainerClass='table-fixle-body'
                selectRow={selectRowProp}>
              <TableHeaderColumn dataField='_id' isKey hidden>Service ID</TableHeaderColumn>
              <TableHeaderColumn dataField='icon' width={46 / 14 + 'rem'} dataFormat={iconFormat}
                                 dataAlign={'center'}></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='name' width={178 / 14 + 'rem'} dataFormat={nameFormat}
                                 columnClassName='name'>Service Name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='minCharge' width={138 / 14 + 'rem'} dataFormat={valueFormat}
                                 dataAlign={'center'}>Minimum Charge</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='rateHours' dataFormat={valueFormat} dataAlign={'center'}>Hourly
                Rate</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='minHours' dataFormat={valueFormat} dataAlign={'center'}>Minimum
                Hours</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='commission' dataFormat={valueFormat} dataAlign={'center'}>Commission
                (%)</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='keywords' dataFormat={keywordsFormat} dataAlign={'center'}>No. Of
                Keyword</TableHeaderColumn>
              <TableHeaderColumn dataField='data' dataFormat={actionFormat} columnClassName={'edit-dropdown'}
                                 dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>
          {selectedServices.length > 0 &&
          <div className="footer services-list-footer">
            <GhostButton title={'Edit Fixle Charge'} className="btn-ghost btn-edit-charge" onClick={() => {
              this.props.changeModifyType('multi');
              this.props.showCommissionConfirm(true);
            }}/>
            <GhostButton title={'Print'} className="btn-ghost print" onClick={() => {
              let array = selectedServices.map((service) => {
                return service.id;
              });
              this.props.getPdf(array);
            }}
            />
            <GhostButton title={'Remove'} className="btn-ghost btn-remove" onClick={() => {
              this.props.changeModifyType(selectedServices.length > 1 ? 'multi' : 'single');
              this.props.showRemoveConfirm(true);
            }}/>
          </div>
          }

          <RemoveConfirmModal
              show={showRemoveConfirm}
              reason={this.state.reason}
              object={'service'}
              action={modifyType}
              handleChange={(e) => {
                this.setState({ reason: e });
              }}
              closeModal={() => {
                this.props.showRemoveConfirm(false);
              }}
              removeObject={() => {
                if (modifyType === 'single') {
                  this.singleAction('remove');
                } else {
                  this.multiAction('remove');
                }
                this.refs.table_service.cleanSelected();  // this.refs.table is a ref for BootstrapTable
              }}/>

          <FixleChargeModal
              show={showChangeCommissionConfirm}
              onSubmit={commission => {
                this.props.showCommissionConfirm(false);
                this.updateCharge(commission);
                this.refs.table_service.cleanSelected();  // this.refs.table is a ref for BootstrapTable
              }}
              closeModal={() => {
                this.props.showCommissionConfirm(false);
              }}
          />
        </div>
    );
  }
}

ServicesListPage.propTypes = {
  startLoad: PropTypes.func,
  dispatch: PropTypes.func,
  getDataList: PropTypes.func,
  delete: PropTypes.func,
  updateSearch: PropTypes.func,
  changePage: PropTypes.func,
  select: PropTypes.func,
  selectAll: PropTypes.func,
  showRemoveConfirm: PropTypes.func,
  changeModifyType: PropTypes.func,
  setService: PropTypes.func,
  showCommissionConfirm: PropTypes.func,
  changeCommission: PropTypes.func,
  getPdf: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  serviceslistpage: makeSelectServicesListPage(),
  servicesList: makeSelectServicesList()
});

function mapDispatchToProps(dispatch) {
  return {
    getDataList: (searchData) => {
      dispatch(getServicesList(searchData));
    },
    delete: (deleteData) => {
      dispatch(deleteService(deleteData));
    },
    updateSearch: (newSearchData) => {
      dispatch(updateSearchData(newSearchData));
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    changePage: (newPage) => {
      dispatch(updatePage(newPage));
    },
    select: (isSelected, selectItem) => {
      dispatch(selectService(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(selectAllService(isSelected));
    },
    showRemoveConfirm: (value) => {
      dispatch(changeShowRemoveConfirm(value));
    },
    changeModifyType: (value) => {
      dispatch(updateModifyType(value));
    },
    setService: (service) => {
      dispatch(udpateCurrentService(service));
    },
    showCommissionConfirm: (value) => {
      dispatch(changeShowCommissionConfirm(value));
    },
    changeCommission: (updateData) => {
      dispatch(updateCommission(updateData));
    },
    getPdf: (serviceId) => {
      dispatch(getPdf(serviceId));
    }
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "servicesListPage", reducer });
const withSaga = injectSaga({ key: "servicesListPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(ServicesListPage);
