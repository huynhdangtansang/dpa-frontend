/*
 *
 * DeviceNewItemPage constants
 *
 */
export const ADD_DEVICE = "DeviceNewItemPage/ADD_DEVICE";
export const ADD_SUCCESS = "DeviceNewItemPage/ADD_SUCCESS";
export const ADD_ERROR = "DeviceNewItemPage/ADD_ERROR";
export const RESET_ERROR = "DeviceNewItemPage/SET_ERROR";
export const CHANGE_AVATAR = "DeviceNewItemPage/CHANGE_AVATAR";
