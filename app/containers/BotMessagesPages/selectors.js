import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the botMessagesPages state domain
 */

const selectBotMessagesPagesDomain = state =>
    state.get("botMessagesPages", initialState);
/**
 * Other specific selectors
 */
/**
 * Default selector used by BotMessagesPages
 */

const makeSelectBotMessagesPages = () =>
    createSelector(selectBotMessagesPagesDomain, substate => substate.toJS());
export default makeSelectBotMessagesPages;
export { selectBotMessagesPagesDomain };
