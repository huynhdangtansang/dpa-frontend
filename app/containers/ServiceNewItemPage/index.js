/**
 *
 * ServiceNewItemPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
import _ from "lodash";
import { Modal, ModalBody } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectCouponList, makeSelectErrors, makeSelectIsSuccess, makeSelectServiceNewItemPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as Actions from "./actions";
import './style.scss';
import SubmitButton from 'components/SubmitButton';
import InputForm from "components/InputForm";
import InputFile from "components/InputFile";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import TagInput from "components/TagInput";
import CouponItem from "components/CouponItem";
import CertItem from "components/CertItem";
import CropImage from 'components/CropImage';
import { updateError } from 'containers/App/actions';
import InputFormNumberFormat from "components/InputFormNumberFormat";

const validateForm = Yup.object().shape({
  'serviceName': Yup.string()
    .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid service name (no special characters)')
    .min(8, 'Invalid service name (At least 8 characters)')
    .max(30, 'Invalid service name (Maximum at 30 characters)'),
  'hourlyRate': Yup.number()
    // .matches(/^[0-9]*$/, 'Hourly rate must be a number')
    .moreThan(0, 'Minimum hours is greater than 0'),
  'minimumHours': Yup.number()
    // .matches(/^-?\d*\.?\d*$/, 'Minimum hours must be a number')
    .moreThan(0, 'Minimum hours is greater than 0')
    .max(24, 'Maximum of minimum hours is 24 hours')
    .typeError('Minimum hours must be a number'),
  'commission': Yup.string()
    .matches(/^[0-9]*$/, 'Commission must be a number')
    .test('val', 'Commission must be from 0 to 100', val => 0 <= parseInt(val) && parseInt(val) <= 100)
    .typeError('Commission must be a number'),
});
const defaultCrop = {
  x: 25,
  y: 25,
  width: 50,
  heigh: 50,
  aspect: 1
};
const rectangleCrop = {
  x: 15,
  y: 13,
  width: 75,
  aspect: 145 / 264
};

/* eslint-disable react/prefer-stateless-function */
export class ServiceNewItemPage extends React.PureComponent {
  //----------------CROP AND UPLOAD IMAGE-------------------------
  _handleImageChange = (e, type) => {
    if (e.error) { // Error, remove old image
      // Show error message
      this.props.updateError({
        error: false,
        title: 'Error!!!',
        message: e.message
      });
    } else {
      this.uploadImage(e, type);
    }
  };
  openFileBrowser = (type) => {
    if (type == 'icon') {
      this.refs.iconUploader.click();
    } else {
      this.refs.backgroundUploader.click();
    }
  };
  uploadImage = (e, type) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        if (type == 'icon') {
          this.setState({
            iconFile: file,
            cropSrc: fileBase64
          }, () => {
            //this.props.showModal('showIconModal', true);
            //not need show modal crop
            this.changeIconImage(fileBase64, file);
          });
        } else {
          this.setState({
            backgroundFile: file,
            cropSrc: fileBase64
          }, () => {
            this.props.showModal('showBackgroundModal', true);
          });
        }
      };
      reader.readAsDataURL(file);
    }
  };
  changeImage = (src, file) => {
    this.props.changeImageInStore('backgroundSrc', src);
    this.setState({
      backgroundFile: file,
      cropSrc: '',
    });
  };
  changeIconImage = (src, file) => {
    this.props.changeImageInStore('iconSrc', src);
    this.setState({
      iconFile: file,
      cropSrc: '',
    });
  };
  closeModal = () => {
    this.props.showModal('showBackgroundModal', false);
    this.props.showModal('showIconModal', false);
    this.setState({
      cropSrc: ''
    });
  };
  handleTagsChange = (tagsList) => {
    this.setState({
      keywords: tagsList,
      keywordsLength: tagsList.length
    });
  };
  addCoupon = () => {
    const newCoupon = {
      name: '',
      code: '',
      disCountType: "%",
      disCount: '',
      expiredAt: '',
      error: true
    };
    this.props.addNewCoupon(newCoupon);
  };
  deleteCoupon = (id) => {
    this.props.deleteCoupon(id);
  };
  updateCouponsValue = (id, type, value, error) => {
    const { couponsList } = this.props.servicenewitempage;
    let list = couponsList;
    let newCoupon = list[id];
    newCoupon[type] = value;
    if (newCoupon.disCount !== '' && newCoupon.expiredAt !== '' && error === false) {
      newCoupon.error = false;
    } else {
      newCoupon.error = true;
    }
    this.props.updateCoupon(id, newCoupon);
  };
  //---------------------------------------------------------------
  invalidCoupon = () => {
    const { couponsList } = this.props.servicenewitempage;
    if (couponsList.length > 0) {
      let errorList = couponsList.filter(coupon => coupon.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };
  addCert = () => {
    const newCert = {
      content: '',
      error: true
    };
    this.props.addCert(newCert);
  };
  deleteCert = (id) => {
    this.props.deleteCert(id);
  };
  updateCert = (id, value, error) => {
    const { certifications } = this.props.servicenewitempage;
    let list = certifications;
    let newItem = list[id];
    newItem.content = value;
    if (error === false && 8 <= newItem.content.length && newItem.content.length <= 30) {
      newItem.error = false;
    } else {
      newItem.error = true;
    }
    this.props.updateCert(id, newItem);
  };
  invalidCert = () => {
    const { certifications } = this.props.servicenewitempage;
    if (certifications.length > 0) {
      let errorList = certifications.filter(cert => cert.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };
  addArea = () => {
    const newArea = {
      country: '',
      city: '',
      suburbs: '',
      error: true
    };
    this.props.addArea(newArea);
  };
  deleteArea = (id) => {
    this.props.deleteArea(id);
  };
  updateArea = (id, type, value) => {
    const { areas } = this.props.servicenewitempage;
    let list = areas;
    let newArea = list[id];
    if (type === 'cityAndCountry') {
      newArea.country = value;
      newArea.city = value;
    } else {
      newArea.suburbs = value;
    }
    if (newArea.country !== '' && newArea.city !== '' && newArea.suburbs !== '') {
      newArea.error = false;
    } else {
      newArea.error = true;
    }
    this.props.updateArea(id, newArea);
  };
  invalidArea = () => {
    const { areas } = this.props.servicenewitempage;
    if (areas.length > 0) {
      let errorList = areas.filter(area => area.error === true);
      if (errorList.length === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      keywords: [],
      cropSrc: '',
      iconFile: '',
      backgroundFile: '',
      keywordsLength: 0
    };
    this._disableButton = this._disableButton.bind(this);
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'serviceName',
      'hourlyRate',
      'minimumHours',
      'commission',
      'background',
      'icon'
    ];
    for (let key of keys) {
      if (_.isEmpty(value[key].toString()) || error[key]) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  componentDidMount() {
    this.addCoupon();
    this.addCert();
    this.addArea();
  }

  componentWillUnmount() {
    this.props.setInit();
  }

  render() {
    const {
      errorsApi,
      couponsList, certifications, areas,
      showConfirmModal, showBackgroundModal, showIconModal,
      backgroundSrc, iconSrc
    } = this.props.servicenewitempage;
    return (
      <div className="new-service">
        <div className="header-edit-add-page new-service-header">
          <div className="action">
            <div className="return" id="return" onClick={() => {
              this.props.history.goBack();
            }}>
              <span className="icon-arrow-left"></span>
            </div>
          </div>
          <div className="title">
            <span>New Service</span>
          </div>
        </div>
        <input type="file" id="file" ref="iconUploader" style={{ display: "none" }}
          onChange={evt => {
            this.uploadIconImage(evt, 'icon');
            evt.target.value = null;
          }} />

        <Formik ref={ref => {
          this.formik = ref
        }}
          initialValues={{
            serviceName: '',
            hourlyRate: '',
            minimumHours: '',
            commission: '',
            background: '',
            icon: ''
          }}
          enableReinitialize={true}
          validationSchema={validateForm}
          onSubmit={(e) => {
            let formData = new FormData();
            if (this.state.iconFile) {
              formData.append('icon', this.state.iconFile);
            }
            if (this.state.backgroundFile) {
              formData.append('background', this.state.backgroundFile);
            }
            formData.append('name', e.serviceName);
            let rateHours = {
              unit: "$",
              value: e.hourlyRate
            };
            formData.append('rateHours', JSON.stringify(rateHours));
            let minCharge = {
              unit: "$",
              value: e.hourlyRate * e.minimumHours
            };
            formData.append('minCharge', JSON.stringify(minCharge));
            let minHours = {
              unit: "",
              value: e.minimumHours,
            };
            formData.append('minHours', JSON.stringify(minHours));
            let commission = {
              unit: "%",
              value: e.commission
            };
            formData.append('commission', JSON.stringify(commission));
            formData.append('couponCodes', JSON.stringify(couponsList));
            let keywords = [];
            this.state.keywords.forEach(key => {
              keywords.push(key.text);
            });
            formData.append('keywords', JSON.stringify(keywords));
            formData.append('certifications', JSON.stringify(certifications));
            let array = areas.map((item) => {
              let string = item.suburbs.split(',');
              return {
                ...item,
                suburbs: string
              }
            });
            formData.append('areas', JSON.stringify(array));
            this.setState({ addData: formData }, () => {
              this.props.showModal('showConfirmModal', true);
            })
          }}>
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue
          }) => (
              <form onSubmit={handleSubmit}>
                <div className="content-add-edit new-service-content">
                  <div className="information service-info">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Service information</span>
                        </div>
                      </div>
                      <div className="col-md-5 right service-details">
                        <div className="details-wrapper">
                          <div className="service-icon">
                            <div className="icon-wrapper">
                              {!_.isEmpty(iconSrc) ?
                                <img className="icon-uploaded" src={iconSrc} alt="icon" onError={(e) => {
                                  e.target.onerror = null;
                                  e.target.src = './default-user.png'
                                }} /> : <span className="icon-camera"></span>}
                            </div>
                            <div className="change-avatar add-icon">
                              <InputFile
                                name={'icon'}
                                src={iconSrc}
                                onBlur={handleBlur}
                                btnText={_.isEmpty(iconSrc) ? 'Add Service Icon' : 'Change Service Icon'}
                                relatedValues={values}
                                validateAfterOtherInputFields={true}
                                onChange={evt => {
                                  this.props.changeImageInStore('iconSrc', '');
                                  setFieldValue('icon', evt);
                                  this._handleImageChange(evt, 'icon');
                                }} />
                            </div>
                          </div>
                          <div className="details">
                            {/* SERVICE NAME */}
                            <InputForm
                              label="service name"
                              name={'serviceName'}
                              value={values.serviceName}
                              error={errors.serviceName}
                              touched={touched.serviceName}
                              onChange={evt => {
                                handleChange(evt);
                              }}
                              onBlur={handleBlur}
                              placeholder={'Service name'} />
                            {/* HOURLY RATE */}
                            <InputFormNumberFormat
                              label="hourly rate"
                              name={'hourlyRate'}
                              placeholder={'Hourly rate'}
                              type={'input'}
                              prefix={'$'}
                              decimalScale={2}
                              value={values.hourlyRate}
                              error={errors.hourlyRate}
                              touched={touched.hourlyRate}
                              onChange={evt => {
                                setFieldValue('hourlyRate', evt.floatValue);
                              }}
                              onBlur={handleBlur}
                            />
                            {/* MINIMUM HOURS */}
                            <InputForm
                              label="minimum hours"
                              name={'minimumHours'}
                              value={values.minimumHours}
                              error={errors.minimumHours}
                              touched={touched.minimumHours}
                              onChange={evt => {
                                setFieldValue('minimumHours', evt.target.value);
                              }}
                              onBlur={handleBlur}
                              placeholder={'Minimum hours'}
                              type={'string'} />
                            {/* MINIMUM CHARGE */}
                            <InputFormNumberFormat
                              label="minimum charge"
                              name={'minimumCharge'}
                              placeholder={'Minimum hours x hourly rate'}
                              prefix={'$'}
                              decimalScale={2}
                              value={(values.minimumHours === '' || values.hourlyRate === '') ? '' : parseFloat(values.minimumHours * values.hourlyRate).toFixed(2)}
                              error={errors.minimumCharge}
                              touched={touched.minimumCharge}
                              onChange={evt => {
                                setFieldValue('minimumCharge', evt.floatValue);
                              }}
                              onBlur={handleBlur}
                            />
                            {(errorsApi && errorsApi.length > 0) ? errorsApi.map((error) => {
                              return (
                                <div key={error.errorCode} className="errors">
                                  <span className="icon-error"></span>
                                  <div className="error-item">
                                    <span>{error.errorMessage}</span>
                                  </div>
                                </div>
                              );
                            }) : []}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-3 service-background">
                        <div className="background-wrapper">
                          <div className="title">
                            <span>Background Image</span>
                          </div>
                          <div className="background-container">
                            {backgroundSrc !== '' ?
                              <img className="background-uploaded" src={backgroundSrc} alt="icon" onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = './default-user.png'
                              }} /> :
                              <span className="icon-camera"></span>}
                          </div>
                          <div className="upload-background">
                            <InputFile
                              name={'background'}
                              src={backgroundSrc}
                              onBlur={handleBlur}
                              btnText={_.isEmpty(backgroundSrc) ? 'Upload Bg Image' : 'Change Bg Image'}
                              relatedValues={values}
                              validateAfterOtherInputFields={true}
                              onChange={evt => {
                                this.props.changeImageInStore('backgroundSrc', '');
                                setFieldValue('background', evt);
                                this._handleImageChange(evt, 'background');
                              }} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="information fixle-charge">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Fixle charge</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">
                          {/* COMMISSION */}
                          <InputFormNumberFormat
                            label="commission per booking"
                            name={'commission'}
                            value={values.commission}
                            error={errors.commission}
                            touched={touched.commission}
                            suffix={'%'}
                            decimalScale={0}
                            onChange={evt => {
                              setFieldValue('commission', evt.floatValue);
                            }}
                            onBlur={handleBlur}
                            placeholder={'Commission per booking'}
                            type={'input'} />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="information coupon-code">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Coupon code</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {couponsList.map((coupon, index) => {
                          return (
                            <CouponItem
                              key={index}
                              index={index}
                              name={coupon.name}
                              code={coupon.code}
                              discount={coupon.disCount}
                              discountType={coupon.disCountType}
                              expiryDate={coupon.expiredAt}
                              handleChange={this.updateCouponsValue}
                              delete={this.deleteCoupon}
                            />
                          );
                        })}
                        <div className="addition-action">
                          <PurpleRoundButton
                            title={'Add Coupon'}
                            className="btn-purple-round add-coupon"
                            type="button"
                            onClick={() => {
                              this.addCoupon();
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* KEYWORDS */}
                  <div className="information keywords">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Keywords</span>
                        </div>
                      </div>
                      <div className="col-md-8 right">
                        <div className="details">
                          <div className="info-item">
                            <div className="title">
                              <span>Keywords</span>
                            </div>
                            <div className="data list-tags">
                              <TagInput
                                tags={this.state.keywords}
                                handleChange={this.handleTagsChange}
                                placeholder={'Keywords'} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="information certification">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Certification required</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {certifications.map((item, index) => {
                          return (
                            <CertItem
                              key={index}
                              onChange={(value, errors) => {
                                this.updateCert(index, value, errors);
                              }}
                              delete={() => {
                                this.deleteCert(index);
                              }}
                            />
                          );
                        })}
                        <div className="addition-action">
                          <PurpleRoundButton title={'Add Certification'}
                            className="btn-purple-round add-certification"
                            onClick={() => {
                              this.addCert();
                            }} type="button" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="information areas-supplied">
                    <div className="row">
                      <div className="col-md-4 left">
                        <div className="title">
                          <span>Areas supplied</span>
                        </div>
                      </div>
                      <div className="col-md-8 right special">
                        {areas.map((item, index) => {
                          return (
                            <div key={index} className="area-item">
                              <div className="details">
                                <InputForm
                                  label="city and country"
                                  name={'cityAndCountry'}
                                  value={item.country}
                                  onChange={evt => {
                                    this.updateArea(index, 'cityAndCountry', evt.target.value);
                                  }}
                                  placeholder={'City and country'} />
                                <InputForm
                                  label="suburbs"
                                  name={'suburbs'}
                                  value={item.suburbs}
                                  onChange={evt => {
                                    this.updateArea(index, 'suburbs', evt.target.value);
                                  }}
                                  placeholder={'Suburbs'} />
                              </div>
                              <div className="delete">
                                <div className="icon-contain">
                                  <span className="icon-bin" onClick={() => {
                                    this.deleteArea(index);
                                  }}></span>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                        <div className="addition-action">
                          <PurpleRoundButton title={'Add Area'} className="btn-purple-round add-area" type="button"
                            onClick={() => {
                              this.addArea();
                            }} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer new-service-footer">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.history.goBack();
                  }} type="button" />
                  <SubmitButton
                    type={'submit'}
                    disabled={this._disableButton(values, errors) || this.invalidCoupon() || this.invalidCert() || this.invalidArea() || this.state.keywordsLength === 0}
                    content={'Save'} />
                </div>
              </form>
            )}
        </Formik>

        <Modal isOpen={showConfirmModal} className="logout-modal">
          <ModalBody>
            <div className="add-success">
              <div className="upper">
                <div className="title">
                  <span>Save This Service</span>
                </div>
                <div className="description">
                  <span>Are you sure want to save this service. This action could influence on all data involved.</span>
                </div>
              </div>
              <div className="lower">
                <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                  this.props.showModal('confirm', false);
                }} />
                <PurpleRoundButton className="btn-purple-round save" title={'Save'} onClick={() => {
                  if (this.state.addData) {
                    this.props.showModal('showConfirmModal', false);
                    this.props.addNewService(this.state.addData);
                  }
                }} />
              </div>
            </div>
          </ModalBody>
        </Modal>

        {(showBackgroundModal && this.state.cropSrc !== '') &&
          <CropImage
            show={showBackgroundModal}
            cropSrc={this.state.cropSrc}
            crop={rectangleCrop}
            closeModal={this.closeModal}
            changeImage={this.changeImage}
          />
        }
        {(showIconModal && this.state.cropSrc !== '') &&
          <CropImage
            show={showIconModal}
            cropSrc={this.state.cropSrc}
            crop={defaultCrop}
            closeModal={this.closeModal}
            changeImage={this.changeIconImage}
          />
        }
      </div>
    )
  }
}

ServiceNewItemPage.propTypes = {
  dispatch: PropTypes.func,
  addNewService: PropTypes.func,
  changeIsSuccess: PropTypes.func,
  addNewCoupon: PropTypes.func,
  deleteCoupon: PropTypes.func,
  updateCoupon: PropTypes.func,
  addCert: PropTypes.func,
  deleteCert: PropTypes.func,
  updateCert: PropTypes.func,
  addArea: PropTypes.func,
  deleteArea: PropTypes.func,
  updateArea: PropTypes.func,
  showModal: PropTypes.func,
  setInit: PropTypes.func,
  updateError: PropTypes.func,
  changeImageInStore: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  servicenewitempage: makeSelectServiceNewItemPage(),
  isSuccess: makeSelectIsSuccess(),
  errors: makeSelectErrors(),
  couponsList: makeSelectCouponList()
});

function mapDispatchToProps(dispatch) {
  return {
    addNewService: (data) => {
      dispatch(Actions.addService(data));
    },
    changeIsSuccess: (value) => {
      dispatch(Actions.setSuccess(value));
    },
    addNewCoupon: (couponItem) => {
      dispatch(Actions.addCoupon(couponItem));
    },
    deleteCoupon: (id) => {
      dispatch(Actions.deleteCoupon(id));
    },
    updateCoupon: (id, updatedItem) => {
      dispatch(Actions.updateCoupon(id, updatedItem));
    },
    addCert: (newItem) => {
      dispatch(Actions.addCert(newItem));
    },
    deleteCert: (id) => {
      dispatch(Actions.deleteCert(id));
    },
    updateCert: (id, updatedItem) => {
      dispatch(Actions.updateCert(id, updatedItem));
    },
    addArea: (newItem) => {
      dispatch(Actions.addArea(newItem));
    },
    deleteArea: (id) => {
      dispatch(Actions.deleteArea(id));
    },
    updateArea: (id, updatedItem) => {
      dispatch(Actions.updateArea(id, updatedItem));
    },
    showModal: (modal, value) => {
      dispatch(Actions.showModal(modal, value));
    },
    setInit: () => {
      dispatch(Actions.setInit());
    },
    updateError(data) {
      dispatch(updateError(data))
    },
    changeImageInStore: (key, url) => {
      dispatch(Actions.changeImage(key, url));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: "serviceNewItemPage", reducer });
const withSaga = injectSaga({ key: "serviceNewItemPage", saga });
export default compose(
  withReducer,
  withSaga,
  withConnect
)(ServiceNewItemPage);
