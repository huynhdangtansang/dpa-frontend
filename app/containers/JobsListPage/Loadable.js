/**
 *
 * Asynchronously loads the component for JobsListPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
