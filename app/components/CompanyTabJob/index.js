/**
 *
 * CompanyTabJob
 *
 */
import React from 'react';
//Library
import _, { findIndex } from "lodash";
import moment from 'moment';
import debounce from 'components/Debounce';
import classnames from 'classnames';
// Combo import table
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
// -------------------
import { Form, Input, InputGroup } from 'reactstrap';
import MultiSelectDropdown from 'components/MultiSelectDropdown';
import SingleSelectDropdown from 'components/SingleSelectDropdown';
import { urlLink } from 'helper/route';

class CompanyTabJob extends React.PureComponent {
  serviceTypeFormat = (cell, row) => {
    return (
        <div className="service-type" onClick={() => {
          this.props.history.push(urlLink.viewJob + '?id=' + row._id);
        }}>
          <span>{cell.name}</span>
        </div>
    )
  };
  appointmentFormat = (cell, row) => {
    return (
        <div className="appointment">
          <div
              className="time">{(moment(cell).format('h:mma') + '-' + moment(cell).add(row.serviceInfo.minHours.value, 'h').format('h:mma')).toUpperCase()}</div>
          <div className="date">{moment(cell).format('ddd D MMM YYYY')}</div>
        </div>
    )
  };
  statusFormat = (cell) => {
    return (
        <div className="content active">{cell}</div>
    )
  };
  clientFormat = (cell) => {
    if (cell) {
      return (
          <img className="image"
               src={cell.avatar ? cell.avatar.fileName : ''}
               onError={(e) => {
                 e.target.onerror = null;
                 e.target.src = './default-user.png'
               }}
               alt="image"/>
      )
    } else {
      return [];
    }
  };
  employeeFormat = (cell, row) => {
    return (
        <div className="assigned">
          <div className="provider">
            {row.assignBy ?
                <img className="logo-provider"
                     src={row.assignBy.avatar.fileName}
                     onError={(e) => {
                       e.target.onerror = null;
                       e.target.src = './default-user.png'
                     }}
                     alt="provider"/> :
                <div className="undefined">
                  <span className="icon-minus"></span>
                </div>
            }
          </div>
        </div>
    )
  };
  sortData = (field, order) => {
    const { searchDataForJob, companyData } = this.props.companydetailspage;
    let newSearchData = searchDataForJob;
    newSearchData.sort = field;
    newSearchData.sortType = order;
    this.props.updatePage('currentPageJob', 1);
    this.props.updateSearchData('searchDataForJob', newSearchData);
    this.props.getJobCompany(companyData._id, newSearchData);
    //this.getListAfterChangeData('job');
  };
  statusContent = (statusList) => {
    let checkedList = statusList.filter(status => status.checked === true);
    if (checkedList.length === 0 || checkedList.length === statusList.length) {
      return 'All';
    } else {
      let string = '';
      let i = 0;
      checkedList.map((status) => {
        if (i === checkedList.length - 1) {
          string += status.label;
        } else {
          string += status.label + ', ';
        }
        i++;
      });
      return string;
    }
  };
  //-----------------------PAGINATION-----------------------
  changePagination = (type) => {
    const { jobData, searchDataForJob, currentPageJob } = this.props.companydetailspage;
    const limit = jobData.pagination.limit;
    let newSearchData = searchDataForJob;
    let newPage, newOffset;
    if (type == 'prev') {
      newPage = currentPageJob - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    } else {//next
      newPage = currentPageJob + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    }
    this.props.updatePage('currentPageJob', newPage);
    this.props.updateSearchJob('offset', newOffset);
    //this.setState(state);
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { companyData, searchDataForJob } = this.props.companydetailspage;
    this.props.getJobCompany(companyData._id, searchDataForJob);
  }, 1000);
  onStatusChange = (option) => {
    const { statusList, searchDataForJob } = this.props.companydetailspage;
    if (option.value === '') {//option all
      this.props.onChangeStoreData({ key: ['searchDataForJob', 'status'], value: [statusList[0]] });
    } else {
      if (findIndex(searchDataForJob.status, item => item.value === option.value) > -1) {
        this.props.onChangeStoreData({
          key: ['searchDataForJob', 'status'],
          value: searchDataForJob.status.filter(item => item.value !== option.value)
        });
      } else {
        searchDataForJob.status.push(option);
        this.props.onChangeStoreData({
          key: ['searchDataForJob', 'status'],
          value: searchDataForJob.status.filter(item => item.value !== '')
        });
      }
    }
    this.getListAfterChangeData('job');
  };
  //Get list after change search data
  getListAfterChangeData = debounce((key) => {
    this.props.updatePage('currentPageJob', 1);
    if (key === 'job') {//get job DON'T NEED OPTION
      const { searchDataForJob, companyData } = this.props.companydetailspage;
      this.props.getJobCompany(companyData._id, searchDataForJob);
    }
    if (key === 'employee') {
      const { searchDataForEmployee, companyData } = this.props.companydetailspage;
      this.props.getEmployeeCompany(companyData._id, searchDataForEmployee);
    }
  }, 600);
  //-------------------------------------------------------
  handleSearchChange = (value => {
    // const { searchDataForJob } = this.props.companydetailspage;
    // let temp = searchDataForJob;
    // temp.offset = 0;
    // temp.search = value;
    this.props.onChangeStoreData({ key: ['searchDataForJob', 'offset'], value: 0 });
    this.props.onChangeStoreData({ key: ['searchDataForJob', 'search'], value: value });
    this.getListAfterChangeData('job');
  });
  handleSelectEmployee = (item) => {
    const { chosenEmployee } = this.props.companydetailspage;
    if (chosenEmployee._id !== item._id) {
      this.props.chooseItem('chosenEmployee', item);
      this.props.onChangeStoreData({ key: ['searchDataForJob', 'employeeId'], value: [item._id] });
    } else {
      this.props.chooseItem('chosenEmployee', { _id: '' });
      this.props.onChangeStoreData({ key: ['searchDataForJob', 'employeeId'], value: [] });
    }
    this.getListAfterChangeData('job');
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // const { activeTab } = this.props.companydetailspage;
    // if (activeTab === '3') {
    //   const { searchDataForJob } = this.props.companydetailspage;
    //   let url = window.location.href;
    //   let temp = url.split('?id=');
    //   this.props.getJobCompany(temp[1], searchDataForJob);
    // }
  }

  render() {
    const { jobData, searchDataForJob, statusList, currentPageJob, FORMATCURRENCY, chosenEmployee, employeeData } = this.props.companydetailspage;
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      //onSelect: this.onRowSelect,
      //onSelectAll: this.onSelectAll,
    };
    const options = {
      withoutNoDataText: true,
      defaultSortName: 'jobId',
      defaultSortOrder: 'desc',
      sortIndicator: false,  // disable sort indicator
      onSortChange: this.sortData
    };
    return (
        <div className="jobs">
          <div className="header-list-page">
            <Form inline className="align-items-center"
                  onSubmit={(e) => {
                    e.preventDefault();
                    //this.handleSearchChange(e);
                  }}>

              <InputGroup className="search-bar">
                <Input placeholder="Search job name or ID"
                       defaultValue={searchDataForJob.search}
                       onChange={(e) => {
                         e.preventDefault();
                         this.handleSearchChange(e.target.value);
                       }}/>
                <span className="icon-search"></span>
              </InputGroup>

              <MultiSelectDropdown
                  title={'Status'}
                  value={searchDataForJob.status}
                  options={_.isEmpty(statusList) ? [] : statusList}
                  onSelect={(option) => {
                    this.onStatusChange(option);
                  }}
              />

              <SingleSelectDropdown
                  title={'Employee: '}
                  imageField={'avatar'}
                  nameField={'fullName'}
                  chosenItemLabel={chosenEmployee.fullName}
                  chosenItemID={chosenEmployee._id}
                  options={_.isEmpty(employeeData) ? [] : employeeData.data}
                  onSelect={(item) => {
                    this.handleSelectEmployee(item);
                  }}
              />

              {!_.isEmpty(jobData) && (
                  <TotalFormatter
                      offset={jobData.pagination.offset}
                      limit={jobData.pagination.limit}
                      total={jobData.pagination.total}
                      page={currentPageJob}
                      changePagination={this.changePagination}
                  />
              )}

            </Form>
          </div>

          <div className={classnames("content content-list-page")}>
            <BootstrapTable
                data={jobData ? jobData.data : []}
                options={options}
                selectRow={selectRowProp}
                bordered={false}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'>
              <TableHeaderColumn dataField="_id" isKey hidden>Job ID</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="serviceId" width={200 / 14 + 'rem'} columnClassName="name"
                                 dataFormat={this.serviceTypeFormat}>Job name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="jobId" width={168 / 14 + 'rem'} dataAlign={'center'}>Job
                ID</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="suburb" width={134 / 14 + 'rem'} dataFormat={(cell, row) => {
                return !_.isUndefined(row.location && row.location) ? row.location.suburb : '-'
              }}>Suburb</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="appointmentTime"
                                 dataFormat={this.appointmentFormat}>Appointment</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="status" dataFormat={this.statusFormat} width={121 / 14 + 'rem'}
                                 columnClassName="status status-job">Status</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="employee" dataAlign={'center'} width={134 / 14 + 'rem'}
                                 dataFormat={this.employeeFormat}>Employee</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField="value" dataAlign={'center'} width={110 / 14 + 'rem'}
                                 headerAlign={'left'} dataFormat={(cell, row) => {
                return '$' + FORMATCURRENCY.format(!_.isUndefined(row.estimation && row.estimation) ? row.estimation.total : 0)
              }}>Value</TableHeaderColumn>
            </BootstrapTable>
          </div>

        </div>
    );
  }
}

CompanyTabJob.propTypes = {};
export default CompanyTabJob;
