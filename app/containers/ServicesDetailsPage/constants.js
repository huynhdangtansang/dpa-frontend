/*
 *
 * ServicesDetailsPage constants
 *
 */
export const RESET_STATE = "/ServicesDetailsPage/RESET_STATE";
export const GET_SERVICE_DATA = "/ServicesDetailsPage/GET_SERVICE_DATA";
export const GET_SUCCESS = "/ServicesDetailsPage/GET_SUCCESS";
export const SET_EDIT_MODE = "ServicesDetailsPage/SET_EDIT_MODE";
export const DELETE_SERVICE = "ServicesDetailsPage/DELETE_SERVICE";
export const UPDATE_COMMISSION = "ServicesDetailsPage/UDPATE_COMMISSION";
export const SHOW_REMOVE_CONFIRM = "ServicesDetailsPage/SHOW_REMOVE_CONFIRM";
export const SHOW_COMMISSION_CONFIRM = "ServicesDetailsPage/SHOW_COMMISSION_CONFIRM";
export const GET_PDF = "ServicesDetailsPage/GET_PDF";