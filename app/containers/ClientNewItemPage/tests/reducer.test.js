import { fromJS } from 'immutable';
import clientNewItemPageReducer from '../reducer';

describe('clientNewItemPageReducer', () => {
  it('returns the initial state', () => {
    expect(clientNewItemPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
