/**
 *
 * ClientsListPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { makeSelectClientsList, makeSelectClientsListPage } from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { loadRepos } from 'containers/App/actions';
import {
  changeClientStatus,
  changeIsSuccess,
  deleteClient,
  getClientsList,
  getSuburbsList,
  resetPassword,
  resetState,
  selectAllClient,
  selectClient,
  showModal,
  updateSearchData
} from './actions';
//Lib
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  UncontrolledDropdown
} from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Scrollbars } from 'react-custom-scrollbars';
import classnames from 'classnames';
//CSS
import './style.scss';
//Components
import PurpleAddRoundButton from 'components/PurpleAddRoundButton';
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import { ActionFormatter, CustomMultiSelect, TotalFormatter } from 'components/TableFormatter';
import ChangeStatusConfirmModal from 'components/ChangeStatusConfirmModal';
import RemoveConfirmModal from 'components/RemoveConfirmModal';
import debounce from 'components/Debounce';
import { getEvent } from 'helper/socketConnection';
import { urlLink } from 'helper/route';

/* eslint-disable react/prefer-stateless-function */
export class ClientsListPage extends React.PureComponent {
  handleSearchChange = debounce(value => {
    const { searchData } = this.props.clientslistpage;
    let newSearchData = searchData;
    newSearchData.search = value;
    newSearchData.offset = 0;
    this.props.updateSearchData(newSearchData, 1);
    this.props.getClientsList(searchData);
  }, 500);
  handleSuburbsChange = (value) => {
    const { searchData } = this.props.clientslistpage;
    let newSearchData = searchData;
    newSearchData.suburb = value;
    newSearchData.offset = 0;
    this.props.updateSearchData(newSearchData, 1);
    this.props.getClientsList(searchData);
  };
  sortData = (field, order) => {
    const { searchData } = this.props.clientslistpage;
    let newSearchData = searchData;
    if (field === 'address') {
      newSearchData.sort = 'suburb';
    } else newSearchData.sort = field;
    newSearchData.sortType = order;
    newSearchData.offset = 0;
    this.props.updateSearchData(newSearchData, 1);
    this.props.getClientsList(newSearchData);
  };
  changePagination = (type) => {
    const { clientsList, searchData, currentPage } = this.props.clientslistpage;
    const { limit } = clientsList.pagination;
    let newSearchData = searchData;
    let newPage, newOffset;
    if (type == 'prev') {
      newPage = currentPage - 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    } else {
      newPage = currentPage + 1;
      newOffset = limit * (newPage - 1);
      newSearchData.offset = newOffset;
    }
    this.props.updateSearchData(newSearchData, newPage);
    this.actionAfterChangePage();
  };
  actionAfterChangePage = debounce(() => {
    const { searchData } = this.props.clientslistpage;
    this.props.getClientsList(searchData);
  }, 700);
  onRowSelect = (row, isSelected) => {
    let selectItem = {
      id: row._id,
      status: row.active
    };
    this.props.select(isSelected, selectItem);
  };
  onSelectAll = (isSelected) => {
    this.props.selectAll(isSelected);
  };
  checkChangeStatusButton = () => {
    const { selectedClients } = this.props.clientslistpage;
    let list = selectedClients;
    if (list.length > 0) {
      const compareStatus = list[0].status;
      let differentItems = list.filter((item) => item.status !== compareStatus);
      if (differentItems.length === 0) {
        return {
          isHide: false,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      } else {
        return {
          isHide: true,
          title: compareStatus === true ? 'Deactive' : 'Active',
        };
      }
    }
    return {
      isHide: false,
      title: '',
    }
  };
  statusFormat = (cell) => {
    return (
        <div className="status">
          <div className={classnames("content", cell ? 'active' : 'inactive')}>{cell ? 'Active' : 'Inactive'}</div>
        </div>
    )
  };
  showTotal = (start, to, total) => {
    return (
        <p className="showed-items">
          <span>{start}-{to} of {total} items showed</span>
        </p>
    );
  };
  singleAction = (type, clientData) => {
    if (type == 'changeStatus') {
      this.setState({ chosenClient: clientData }, () => {
        this.openStatusConfirm('single');
      });
    } else if (type == 'remove') {
      this.setState({ chosenClient: clientData }, () => {
        this.openRemoveConfirm('single');
      })
    }
  };
  multiAction = (type) => {
    if (type === 'changeStatus') {
      this.openStatusConfirm('multi');
    } else if (type === 'remove') {
      this.openRemoveConfirm('multi');
    }
  };
  openStatusConfirm = (type) => {
    this.setState({
      modifyType: type,
      showChangeStatusConfirm: true
    });
  };
  openRemoveConfirm = (type) => {
    this.setState({
      modifyType: type,
      showRemoveConfirm: true
    });
  };
  handleReasonChange = (value) => {
    this.setState({ reason: value });
  };
  closeStatusConfirm = () => {
    this.setState({
      showChangeStatusConfirm: false,
      reason: ''
    });
  };
  closeRemoveConfirm = () => {
    this.setState({
      showRemoveConfirm: false,
      reason: ''
    });
  };
  closeSuccessSendMail = () => {
    this.props.changeSuccess(false);
  };
  resetPassword = () => {
    if (this.state.modifyType === 'single') {
      let client = this.state.chosenClient;
      let listIds = [];
      listIds.push(client.id);
      let data = {
        ids: listIds
      };
      this.props.resetPass(data);
    } else {
      let listIds = [];
      let { selectedClients } = this.props.clientslistpage;
      let list = selectedClients;
      let i = 0;
      list.forEach((client) => {
        listIds.push(client.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds
          };
          this.props.resetPass(data);
        } else {
          i++;
        }
      });
    }
    this.refs.table_client.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  changeStatus = () => {
    const { searchData } = this.props.clientslistpage;
    if (this.state.modifyType == 'single') {
      let id = this.state.chosenClient.id;
      let status = this.state.chosenClient.status;
      let listIds = [];
      listIds.push(id);
      let data = {};
      if (status == true) {
        data['ids'] = listIds;
        data['reason'] = this.state.reason;
        data['active'] = !status;
      } else {
        data['ids'] = listIds;
        data['active'] = !status;
      }
      this.props.changeStatus(data, searchData);
    } else {
      let listIds = [];
      let { selectedClients } = this.props.clientslistpage;
      let list = selectedClients;
      let i = 0;
      const changeStatusButton = this.checkChangeStatusButton();
      list.forEach((client) => {
        listIds.push(client.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds,
            active: changeStatusButton.title === 'Deactive' ? false : true
          };
          if (changeStatusButton.title === 'Deactive') {
            data['reason'] = this.state.reason;
          }
          this.props.changeStatus(data, searchData);
        } else {
          i++;
        }
      });
    }
    this.refs.table_client.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  removeClient = () => {
    const { searchData } = this.props.clientslistpage;
    if (this.state.modifyType === 'single') {
      let id = this.state.chosenClient.id;
      let listIds = [];
      listIds.push(id);
      let data = {
        ids: listIds,
        reason: this.state.reason
      };
      this.props.deleteClientFromList(data, searchData);
    } else {//multi
      let listIds = [];
      const { selectedClients } = this.props.clientslistpage;
      let list = selectedClients;
      let i = 0;
      list.forEach((client) => {
        listIds.push(client.id);
        if (i == list.length - 1) {
          let data = {
            ids: listIds,
            reason: this.state.reason
          };
          this.props.deleteClientFromList(data, searchData);
        } else {
          i++;
        }
      });
    }
    this.refs.table_client.cleanSelected();  // this.refs.table is a ref for BootstrapTable
  };
  changeStatusModal = () => {
    if (this.state.modifyType == 'single') {
      let currentClient = this.state.chosenClient;
      return (
          <ChangeStatusConfirmModal
              show={this.state.showChangeStatusConfirm}
              reason={this.state.reason}
              currentStatus={currentClient.status}
              object={'client'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeStatusConfirm}
              changeStatus={this.changeStatus}/>
      )
    } else {
      const { selectedClients } = this.props.clientslistpage;
      const changeStatusButton = this.checkChangeStatusButton();
      if (selectedClients.length > 0) {
        return (
            <ChangeStatusConfirmModal
                show={this.state.showChangeStatusConfirm}
                reason={this.state.reason}
                currentStatus={changeStatusButton.title === 'Deactive' ? true : false}
                object={'client'}
                action={this.state.modifyType}
                handleChange={this.handleReasonChange}
                closeModal={this.closeStatusConfirm}
                changeStatus={this.changeStatus}/>
        )
      }
    }
  };
  suburbFormat = (cell) => {
    if (cell) {
      let addressList = cell;
      if (addressList.length > 0) {
        let primaryAddress = addressList.filter((address) => address.isPrimary === true);
        if (primaryAddress.length > 0) {
          return (
              primaryAddress[0].suburb && primaryAddress[0].suburb
          );
        }
      }
    }
  };
  currencyFormat = (cell) => {
    return <span>${parseFloat(cell).toFixed(2)}</span>
  };

  constructor(props) {
    super(props);
    this.state = {
      showRemoveConfirm: false,
      showChangeStatusConfirm: false,
      selectedClients: [],
      reason: '',
      chosenClient: {},
      modifyType: 'single',
      //hideChangeStatusButton: false,
      changeStatusType: 'Deactive',
    };
    getEvent('notification:newUserActivity', () => {
      let url = window.location.hash;
      url = url.replace('#', '');
      if (url === urlLink.clients) {
        const { searchData } = this.props.clientslistpage;
        this.props.getClientsList(searchData);
      }
    });
  }

  componentWillMount() {
    const { searchData } = this.props.clientslistpage;
    this.props.getClientsList(searchData);
    this.props.getSuburbs();
  }

  componentWillUnmount() {
    //this.props.selectAll(false);
    this.props.setDefault();
  }

  enumFormatter(cell, row, enumObject) {
    return enumObject[cell];
  }

  render() {
    const {
      clientsList, dataList, selectedClients, suburbsList,
      showResetPasswordConfirm, searchData, currentPage
    } = this.props.clientslistpage;
    const changeStatusButton = this.checkChangeStatusButton();
    const selectRowProp = {
      mode: 'checkbox',
      bgColor: '#f5f5f5',
      columnWidth: 68 / 14 + 'rem',
      customComponent: CustomMultiSelect,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    const options = {
      defaultSortName: 'fullName',
      defaultSortOrder: 'asc',  // default sort order
      sortIndicator: false,  // disable sort indicator
      withoutNoDataText: true,
      onSortChange: this.sortData
    };
    const actionFormat = (cell, row) => {
      const editMenu = (
          <DropdownMenu>
            <DropdownItem onClick={() => {
              this.props.history.push(urlLink.viewClient + '?id=' + row._id);
            }}>View</DropdownItem>
            <DropdownItem onClick={() => {
              this.singleAction('changeStatus', { id: row._id, status: row.active });
            }}>{row.active == true ? 'Deactivate' : 'Activate'}</DropdownItem>
            <DropdownItem onClick={() => {
              this.props.history.push(urlLink.editClient + '?id=' + row._id)
            }}>Edit</DropdownItem>
            <DropdownItem onClick={() => {
              this.setState({
                modifyType: 'single',
                chosenClient: {
                  id: row._id,
                  status: row.active
                }
              }, () => {
                this.props.showModal('resetPassword', true);
              })
            }}>Reset Password</DropdownItem>
            <DropdownItem onClick={() => {
              this.singleAction('remove', { id: row._id, status: row.active });
            }}>Remove</DropdownItem>
          </DropdownMenu>
      );
      return (
          <ActionFormatter menu={editMenu}/>
      );
    };
    const iconFormat = (cell, row) => {
      return (
          <img
              className='avatar-client'
              src={row.avatar ? row.avatar.fileName : './default-user.png'}
              onError={(e) => {
                e.target.onerror = null;
                e.target.src = './default-user.png'
              }}
              onClick={() => {
                this.props.history.push(urlLink.viewClient + '?id=' + row._id);
              }}/>
      );
    };
    const nameFormat = (cell, row) => {
      return (
          <div onClick={() => {
            this.props.history.push(urlLink.viewClient + '?id=' + row._id);
          }}>{cell}</div>
      );
    };
    return (
        <div className="clients-list">
          <div className="header-list-page">
            <Form inline onSubmit={() => {
              this.handleFilter();
            }} className=" align-items-center">

              <PurpleAddRoundButton className={'btn-add-purple-round'} onClick={() => {
                this.props.history.push(urlLink.addClient)
              }} title={'New Client'} iconClassName={'icon-plus'} type="button"/>

              <InputGroup className="search-bar">
                <Input placeholder="Search client name, email or phone number"
                       onChange={(e) => {
                         e.preventDefault();
                         this.handleSearchChange(e.target.value);
                       }}
                />
                <span className="icon-search"></span>
              </InputGroup>

              <UncontrolledDropdown className="period-dropdown">
                <DropdownToggle>
                  <span className="title">Suburb:&nbsp;</span>
                  <span className="content">{searchData.suburb !== '' ? searchData.suburb : 'All'}</span>
                  <span className="fixle-caret icon-triangle-down"></span>
                </DropdownToggle>
                <DropdownMenu>
                  <Scrollbars
                      // This will activate auto hide
                      autoHide
                      // Hide delay in ms
                      autoHideTimeout={1000}
                      autoHeight
                      autoHeightMin={0}
                      autoHeightMax={200}>
                    <DropdownItem onClick={() => {
                      this.handleSuburbsChange('');
                    }}>All</DropdownItem>
                    {suburbsList.map((suburb, index) => {
                      return (
                          <DropdownItem key={index} onClick={() => {
                            this.handleSuburbsChange(suburb);
                          }}>{suburb}</DropdownItem>
                      )
                    })}
                  </Scrollbars>
                </DropdownMenu>
              </UncontrolledDropdown>
              {dataList && dataList.length > 0 && clientsList.pagination && (
                  <TotalFormatter
                      offset={clientsList.pagination.offset}
                      limit={clientsList.pagination.limit}
                      total={clientsList.pagination.total}
                      page={currentPage}
                      changePagination={this.changePagination}
                  />
              )}
            </Form>

          </div>
          <div className={classnames("content content-list-page", selectedClients.length > 0 ? 'selecting' : '')}>
            <BootstrapTable
                ref="table_client"
                data={dataList}
                options={options}
                bordered={false}
                selectRow={selectRowProp}
                containerClass='table-fixle'
                tableHeaderClass='table-fixle-header'
                tableBodyClass='table-fixle-content'>
              <TableHeaderColumn dataField='_id' isKey hidden>Client ID</TableHeaderColumn>
              <TableHeaderColumn dataField='icon' width={46 / 14 + 'rem'} dataFormat={iconFormat}></TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='fullName' width={150 / 14 + 'rem'} columnClassName={'name'}
                                 dataFormat={nameFormat}>Client Name</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='email' width={186 / 14 + 'rem'}>Email</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='phoneNumber' width={162 / 14 + 'rem'}>Contact
                Number</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='address' dataFormat={this.suburbFormat}>Suburb</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='beforeCompleted' dataAlign='center'>Active</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='completed' dataAlign='center'
                                 width={95 / 14 + 'rem'}>Completed</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='unpaid' dataFormat={this.currencyFormat}
                                 dataAlign='center'>Unpaid</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='paid' dataFormat={this.currencyFormat}
                                 dataAlign='center'>Paid</TableHeaderColumn>
              <TableHeaderColumn dataSort dataField='active' dataFormat={this.statusFormat}
                                 columnClassName={'status'}>Status</TableHeaderColumn>
              <TableHeaderColumn dataField='data' dataFormat={actionFormat} columnClassName={'edit-dropdown'}
                                 dataAlign='right'>Action</TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div className="footer clients-list-footer" hidden={selectedClients.length === 0}>
            <GhostButton hidden={changeStatusButton.isHide}
                         title={changeStatusButton.title === 'Deactive' ? 'Deactivate' : 'Activate'}
                         className="btn-ghost btn-edit-charge"
                         onClick={() => {
                           if (selectedClients.length === 1) {
                             this.singleAction('changeStatus', selectedClients[0]);
                           } else
                             this.multiAction('changeStatus');
                         }}/>
            <GhostButton title={'Reset Password'} className="btn-ghost print" onClick={() => {
              this.setState({ modifyType: 'multi' }, () => {
                this.props.showModal('resetPassword', true);
              });
            }}/>
            <GhostButton title={'Remove'} className="btn-ghost btn-remove" onClick={() => {
              if (selectedClients.length === 1) {
                this.singleAction('remove', selectedClients[0]);
              } else
                this.multiAction('remove');
            }}/>
          </div>

          {this.changeStatusModal()}

          <RemoveConfirmModal
              show={this.state.showRemoveConfirm}
              reason={this.state.reason}
              object={'client'}
              action={this.state.modifyType}
              handleChange={this.handleReasonChange}
              closeModal={this.closeRemoveConfirm}
              removeObject={this.removeClient}/>

          <Modal isOpen={showResetPasswordConfirm} className="logout-modal">
            <ModalBody>
              <div className="reset-password">
                <div className="upper">
                  <div className="title">
                    <span>Reset {this.state.modifyType === 'single' ? 'This Client' : 'These Clients'} Password</span>
                  </div>
                  <div className="description">
                  <span>
                    Are you sure want to reset {this.state.modifyType === 'single' ? 'this client' : 'these clients'} password.
                    This action will send reset password email to {this.state.modifyType === 'single' ? 'this client' : 'these clients'}.
                  </span>
                  </div>
                </div>
                <div className="lower">
                  <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                    this.props.showModal('resetPassword', false);
                  }}/>
                  <PurpleRoundButton className="btn-purple-round reset-password"
                                     title={'Reset Password'}
                                     onClick={() => {
                                       this.props.showModal('resetPassword', false);
                                       if (selectedClients.length > 1) {
                                         this.setState({ modifyType: 'multi' }, () => {
                                           this.resetPassword();
                                         });
                                       } else {
                                         this.setState({ modifyType: 'single' }, () => {
                                           this.resetPassword();
                                         });
                                       }
                                     }}/>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

ClientsListPage.propTypes = {
  dispatch: PropTypes.func,
  getClientsList: PropTypes.func,
  deleteClientFromList: PropTypes.func,
  changeStatus: PropTypes.func,
  resetPass: PropTypes.func,
  changeSuccess: PropTypes.func,
  startLoad: PropTypes.func,
  select: PropTypes.func,
  selectAll: PropTypes.func,
  getSuburbs: PropTypes.func,
  showModal: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  clientslistpage: makeSelectClientsListPage(),
  clientsList: makeSelectClientsList()
});
const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "clientsListPage", reducer });
const withSaga = injectSaga({ key: "clientsListPage", saga });

function mapDispatchToProps(dispatch) {
  return {
    setDefault: () => {
      dispatch(resetState());
    },
    getClientsList: (dataSearch) => {
      dispatch(getClientsList(dataSearch));
    },
    deleteClientFromList: (data, searchData) => {
      dispatch(deleteClient(data, searchData));
    },
    changeStatus: (data, searchData) => {
      dispatch(changeClientStatus(data, searchData));
    },
    resetPass: (data) => {
      dispatch(resetPassword(data));
    },
    changeSuccess: (value) => {
      dispatch(changeIsSuccess(value));
    },
    startLoad: () => {
      dispatch(loadRepos());
    },
    select: (isSelected, selectItem) => {
      dispatch(selectClient(isSelected, selectItem));
    },
    selectAll: (isSelected) => {
      dispatch(selectAllClient(isSelected));
    },
    getSuburbs: () => {
      dispatch(getSuburbsList());
    },
    showModal: (modal, value) => {
      dispatch(showModal(modal, value));
    },
    updateSearchData: (newSearchData, newPage) => {
      dispatch(updateSearchData(newSearchData, newPage));
    },
  };
}

export default compose(
    withReducer,
    withSaga,
    withConnect
)(ClientsListPage);
