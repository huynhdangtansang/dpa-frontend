import React from 'react';
import './styles.scss';
import PropTypes from "prop-types";

export default class PurpleAddRoundButton extends React.Component {
  render() {
    return (
        <button type={this.props.type} className={this.props.className} onClick={this.props.onClick}>
          <i className={this.props.iconClassName}></i><span className="title">{this.props.title}</span>
        </button>
    );
  }
}
PurpleAddRoundButton.propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string,
  className: PropTypes.string,
  iconClassName: PropTypes.string
};