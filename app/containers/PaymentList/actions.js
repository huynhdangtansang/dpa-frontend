/*
 *
 * PaymentList actions
 *
 */
import * as Constants from "./constants";

export function defaultAction() {
  return {
    type: Constants.DEFAULT_ACTION
  };
}

export function resetState() {
  return {
    type: Constants.RESET_STATE
  };
}

//API
export function getList(searchData) {
  return {
    type: Constants.GET_LIST,
    searchData
  };
}

export function getSuccess(response) {
  return {
    type: Constants.GET_SUCCESS,
    response
  }
}

export function getError(response) {
  return {
    type: Constants.GET_ERROR,
    response
  }
}

export function removePayment(deleteData, searchData) {
  return {
    type: Constants.REMOVE_PAYMENT,
    deleteData,
    searchData
  }
}

//Search data
export function updateSearchData(newSearchData) {
  return {
    type: Constants.UPDATE_SEARCH_DATA,
    newSearchData
  }
}

//Pagination
export function changePage(newPage) {
  return {
    type: Constants.CHANGE_PAGE,
    newPage
  }
}

//Select
export function selectPayment(isSelected, selectItem) {
  return {
    type: Constants.SELECT_PAYMENT,
    isSelected,
    selectItem
  }
}

export function selectAllPayment(isSelected) {
  return {
    type: Constants.SELECT_ALL_PAYMENT,
    isSelected
  }
}

//Print
export function getPdf(serviceId) {
  return {
    type: Constants.GET_PDF,
    serviceId
  }
}