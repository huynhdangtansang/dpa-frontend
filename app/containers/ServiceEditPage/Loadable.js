/**
 *
 * Asynchronously loads the component for ServiceEditPage
 *
 */
import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
