import { fromJS } from 'immutable';
import clientsListPageReducer from '../reducer';

describe('clientsListPageReducer', () => {
  it('returns the initial state', () => {
    expect(clientsListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
