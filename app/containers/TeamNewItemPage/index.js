/**
 *
 * TeamNewItemPage
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Formik } from 'formik';
import * as Yup from 'yup';
//lib
import _, { debounce, findIndex, indexOf } from "lodash";
import { Modal, ModalBody } from 'reactstrap';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectTeamNewItemPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { addMember, changeAvatar, resetError, resetState } from "./actions";
//CSS
import './style.scss';
//data
import { country, listError } from 'helper/data';
import { placeAutoComplete, removePlus } from 'helper/exportFunction';
//Components
import InputForm from "components/InputForm";
import InputFile from "components/InputFile";
import GhostButton from 'components/GhostButton';
import PurpleRoundButton from 'components/PurpleRoundButton';
import SubmitButton from 'components/SubmitButton';
import CropImage from 'components/CropImage';
import Selection from 'components/Selection';

const validateForm = Yup.object().shape({
  'email': Yup.string()
      .email(" Invalid email"),
  'name': Yup.string()
      .matches(/^[-a-zA-Z0-9]+(\s+[-a-zA-Z0-9]+)*$/, 'Invalid name (no special characters)')
      .min(6, 'Invalid name (At least 6 characters)')
      .max(30, 'Invalid name (Maximum at 30 characters)'),
  'phone': Yup.string()
      .matches(/^[0-9]*$/, 'Invalid phone number')
      .min(5, 'Invalid phone number'),
  'location': Yup.string()
});

/* eslint-disable react/prefer-stateless-function */
export class TeamNewItemPage extends React.PureComponent {
  openImageBrowser = () => {
    this.refs.fileUploader.click();
  };
  changeImage = (src, file) => {
    this.props.changeMemberAvatar(src);
    this.setState({
      avatarSrc: src,
      cropSrc: '',
      avatarFile: file
    });
  };
  closeModal = () => {
    this.setState({
      showAvatarModal: false,
      cropSrc: ''
    });
  };
  uploadImage = (e) => {
    let file = e.data && e.data.file ? e.data.file : null;
    let fileBase64 = e.data && e.data.fileUrl ? e.data.fileUrl : null;
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          showAvatarModal: true,
          cropSrc: fileBase64
        });
      };
      reader.readAsDataURL(file);
    }
  };
  getStatusValue = (value) => {
    this.setState({ status: value });
  };
  getContactCountryValue = (value) => {
    this.setState({ countryName: value });
  };
  getPositionValue = (value) => {
    this.setState({ position: value });
  };
  addMember = () => {
    if (this.state.memberData) {
      this.props.addNewMember(this.state.memberData);
    }
  };
  _handleImageChange = (e) => {
    if (e.error) { // Error, remove old image
    } else {
      this.uploadImage(e);
    }
  };
  //Handle country
  loadCountries = (inputValue, callback) => {
    placeAutoComplete('country', inputValue).then((rs) => {
      // Update country list
      //this.props.updateAddEmployeePageData('countryList', rs);
      callback(rs);
    })
  };
  //Clear api error
  clearApiError = (type) => {
    const { apiErrors } = this.props.teamnewitempage;
    if (apiErrors && apiErrors.length > 0) {
      let index = findIndex(listError, val => val.name === type);
      if (index > -1 && indexOf(listError[index].error, apiErrors[0].errorCode) > -1) {
        this.props.resetError();
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      cropSrc: '',
      showAvatarModal: false,
      showSaveConfirm: false
    };
    this._disableButton = this._disableButton.bind(this);
  }

  componentWillMount() {
    this.props.resetState();
  }

  _disableButton(value, error) {
    //Loop through validation fields
    const keys = [
      'avatar',
      'name',
      'email',
      'phone',
      'location'
    ];
    for (let key of keys) {
      if (value[key] === null || error[key] || !value[key].toString()) {
        //If this field has error or
        return true;
      }
    }
    return false;
  }

  render() {
    const { avatar, apiErrors } = this.props.teamnewitempage;
    return (
        <div className="new-member">
          <div className="header-edit-add-page new-member-header">
            <div className="action">
              <div className="return" id="return" onClick={() => {
                this.props.history.goBack();
              }}>
                <span className="icon-arrow-left"></span>
              </div>
            </div>
            <div className="title">
              <span>New Member</span>
            </div>
          </div>

          <Formik ref={ref => {
            this.formik = ref
          }}
                  initialValues={{
                    avatar: '',
                    name: '',
                    email: '',
                    phone: '',
                    location: '',
                    contactArea: {
                      value: 61,
                      label: 'Australia'
                    },
                    position: { label: 'Admin', value: 'admin' },
                    status: { label: 'Active', value: true }
                  }}
                  enableReinitialize={true}
                  validationSchema={validateForm}
                  onSubmit={(e) => {
                    let formData = new FormData();
                    if (this.state.avatarFile) {
                      formData.append('file', this.state.avatarFile);
                    }
                    formData.append('fullName', e.name);
                    formData.append('email', e.email);
                    formData.append('countryName', e.contactArea.label);
                    formData.append('countryCode', '+' + e.contactArea.value);
                    formData.append('phoneNumber', e.phone);
                    formData.append('location', e.location);
                    formData.append('subRole', e.position.value);
                    formData.append('active', e.status.value);
                    this.setState({
                      memberData: formData
                    }, () => {
                      this.setState({ showSaveConfirm: true })
                    });
                  }}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="content-add-edit new-member-content">
                    <div className="information">
                      <div className="row">
                        <div className="col-md-4 left">
                          <div className="title">
                            <span>Personal Information</span>
                          </div>
                        </div>
                        <div className="col-md-8 right">
                          <div className="avatar-image member-avatar form-input">
                            <div className="avatar-wrapper">
                              {avatar === '' ? <span className="icon-camera"></span> :
                                  <img src={avatar}
                                       onError={(e) => {
                                         e.target.onerror = null;
                                         e.target.src = './default-user.png'
                                       }}
                                       alt="avatar"
                                  />}
                            </div>

                            <InputFile name={'avatar'}
                                       src={avatar ? avatar : ''}
                                       onBlur={handleBlur}
                                       btnText={!_.isEmpty(avatar) ? 'change avatar' : 'upload avatar'}
                                       relatedValues={values}
                                       validateAfterOtherInputFields={true}
                                       onChange={evt => {
                                         this.props.changeMemberAvatar('');
                                         setFieldValue('avatar', evt);
                                         this._handleImageChange(evt);
                                       }}/>

                          </div>
                          <div className="details">
                            <InputForm
                                label="name"
                                name={'name'}
                                placeholder='Full name'
                                value={values.name}
                                error={errors.name}
                                touched={touched.name}
                                apiError={apiErrors}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('name');
                                }}
                                onBlur={handleBlur}/>
                            <InputForm
                                label="email"
                                name='email'
                                placeholder='you@example.com'
                                value={values.email}
                                error={errors.email}
                                touched={touched.email}
                                apiError={apiErrors}
                                onChange={evt => {
                                  handleChange(evt);
                                  this.clearApiError('email');
                                }}
                                onBlur={handleBlur}/>
                            <div className='input-group form-input'>
                              <div className="info-item contact-number">
                                <Selection
                                    options={country}
                                    title={'Contact number'}
                                    name={'contactArea'}
                                    value={values.contactArea}
                                    isAsyncList={true}
                                    loadOptions={debounce(this.loadCountries, 700)}
                                    getOptionLabel={option => option.label}
                                    getOptionValue={option => option.value}
                                    type={'contactArea'}
                                    onBlur={handleBlur}
                                    onChange={option => {
                                      setFieldValue('contactArea', { value: option.country_code, label: option.label });
                                    }}/>
                                <InputForm
                                    name='phone'
                                    placeholder='Phone number'
                                    prependLabel={'+' + removePlus(values.contactArea.value)}
                                    value={values.phone}
                                    error={errors.phone}
                                    touched={touched.phone}
                                    apiError={apiErrors}
                                    onChange={evt => {
                                      handleChange(evt);
                                      this.clearApiError('phoneNumber');
                                    }}
                                    onBlur={handleBlur}
                                    type="number"/>
                              </div>
                            </div>
                            <div className="info-item location">
                              <InputForm
                                  label="location"
                                  name='location'
                                  placeholder="Location"
                                  value={values.location}
                                  error={errors.location}
                                  touched={touched.location}
                                  apiError={apiErrors}
                                  onChange={evt => {
                                    handleChange(evt);
                                    this.clearApiError('');
                                  }}
                                  onBlur={handleBlur}/>
                            </div>
                            {/* POSITION */}


                            <Selection
                                options={[
                                  { label: 'Admin', value: 'admin' },
                                  { label: 'Member', value: 'member' },
                                ]}
                                name={'position'}
                                title={'position'}
                                value={values.position}
                                getOptionLabel={option => option.label}
                                getOptionValue={option => option.value}
                                type={'position'}
                                onBlur={handleBlur}
                                onChange={option => {
                                  setFieldValue('position', option);
                                }}/>
                            {/* STATUS */}

                            <Selection
                                options={[
                                  { label: 'Active', value: true },
                                  { label: 'Inactive', value: false },
                                ]}
                                name={'status'}
                                title={'status'}
                                value={values.status}
                                getOptionLabel={option => option.label}
                                getOptionValue={option => option.value}
                                type={'status'}
                                onBlur={handleBlur}
                                onChange={option => {
                                  setFieldValue('status', option);
                                }}/>
                          </div>
                          {(apiErrors && apiErrors.length > 0) ? apiErrors.map((error) => {
                            return (
                                <div key={error.errorCode} className="errors">
                                  <span className="icon-error"></span>
                                  <div className="error-item">
                                    <span>{error.errorMessage}</span>
                                  </div>
                                </div>
                            );
                          }) : []}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="footer new-member-footer">
                    <GhostButton className="btn-ghost cancel" title={'Cancel'} onClick={() => {
                      this.props.history.goBack();
                    }}/>
                    <SubmitButton type={'submit'}
                                  disabled={this._disableButton(values, errors) || (apiErrors && apiErrors.length > 0)}
                                  content={'Save'}/>
                  </div>
                </form>
            )}
          </Formik>
          {(this.state.cropSrc && this.state.cropSrc !== '') ?
              <CropImage
                  show={this.state.showAvatarModal}
                  cropSrc={this.state.cropSrc}
                  closeModal={this.closeModal}
                  changeImage={this.changeImage}
              />
              : []
          }
          <Modal isOpen={this.state.showSaveConfirm} className="logout-modal">
            <ModalBody>
              <div className="save-confirm">
                <div className="upper">
                  <div className="title">
                    <span>Save This Member</span>
                  </div>
                  <div className="description">
									<span>
										Are you sure to save this member? This action could influence on all data involved.
                  </span>
                  </div>
                </div>
                <div className="lower">
                  <GhostButton className="btn-ghost cancel" title={'cancel'} onClick={() => {
                    this.setState({ showSaveConfirm: false });
                  }}/>
                  <PurpleRoundButton className="btn-purple-round remove" title={'save'} onClick={() => {
                    this.setState({ showSaveConfirm: false });
                    this.addMember();
                  }}
                  />
                </div>
              </div>
            </ModalBody>
          </Modal>
        </div>
    );
  }
}

TeamNewItemPage.propTypes = {
  dispatch: PropTypes.func,
  addNewMember: PropTypes.func,
  resetState: PropTypes.func,
  resetError: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  teamnewitempage: makeSelectTeamNewItemPage()
});

function mapDispatchToProps(dispatch) {
  return {
    addNewMember: (profile) => {
      dispatch(addMember(profile));
    },
    changeMemberAvatar: (url) => {
      dispatch(changeAvatar(url));
    },
    resetState: () => {
      dispatch(resetState());
    },
    resetError: () => {
      dispatch(resetError());
    },
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);
const withReducer = injectReducer({ key: "teamNewItemPage", reducer });
const withSaga = injectSaga({ key: "teamNewItemPage", saga });
export default compose(
    withReducer,
    withSaga,
    withConnect
)(TeamNewItemPage);
